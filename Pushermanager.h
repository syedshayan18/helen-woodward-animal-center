//
//  Pushermanager.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/28/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PusherSwift/PusherSwift-Swift.h"
#import "Vet247Doctor-Swift.h"


@interface Pushermanager : NSObject
typedef void(^Completion)(NSDictionary*);
@property Pusher *pusher;
-(void)subscribeManager:(NSString *)event completion:(Completion) compblock;

+(Pushermanager *)instance;
-(void) disconnect;
-(void)connect;


@end
