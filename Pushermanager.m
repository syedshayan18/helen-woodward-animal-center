//
//  Pushermanager.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/28/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "Pushermanager.h"
#import "PusherSwift/PusherSwift-Swift.h"

@implementation Pushermanager
@synthesize pusher;


-(void)connect {
    
    OCAuthMethod *authMethod = [[OCAuthMethod alloc] initWithAuthEndpoint:@""];
    OCPusherHost *host = [[OCPusherHost alloc] initWithCluster:@"eu"];
    PusherClientOptions *options = [[PusherClientOptions alloc]
                                    initWithOcAuthMethod:authMethod
                                    attemptToReturnJSONObject:YES
                                    autoReconnect:YES
                                    ocHost:host
                                    port:nil
                                    encrypted:YES];
    pusher=[[Pusher alloc] initWithKey:@"1a63b78afe4123a9b4db"];
    [pusher connect];
   
    
  }
-(void)subscribeManager:(NSString *)event completion:(Completion)compblock{
    NSString *channel = [NSString stringWithFormat:@"chat-user-%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
   PusherChannel *myChannel = [pusher subscribeWithChannelName:channel];
    
    [myChannel bindWithEventName:event callback:^void (NSDictionary *data) {
        NSString *message = data[@"message"];
        NSLog(@"%@",message);
        compblock(data);
    }];
    
   

}

-(void) disconnect{
    [pusher disconnect];


}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

+ (Pushermanager *)instance
{
    static Pushermanager *instance = nil;
    
    @synchronized(self)
    {
        if (instance == nil)
        {
            instance = [[Pushermanager alloc] init];
        }
    }
    
    return instance;
}








@end
