//
//  SignUpController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/4/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "SignUpController.h"

#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "LoginController.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

@interface SignUpController ()

@property  CGPoint centerPoint;

@property (assign) BOOL termsAndConditionsButtonClicked;

@property (strong,nonatomic) UIImageView *backGroundImage;

@property (strong,nonatomic) UIImageView *logoImageView;

@property (strong,nonatomic) UIView *registerationFieldsView;

@property (strong,nonatomic) UIImageView *registerationFieldBGImageView;

@property (strong,nonatomic) UIScrollView *dataScroll;

@property (strong,nonatomic) UIView *textFieldsView;

@property (strong,nonatomic) UITextField *fNameTextField, *lNameTextField;

@property (strong,nonatomic) UIImageView *fNameTextFiedBG, *lNameTextFieldBG;

@property (strong,nonatomic) UIImageView *fNameIcon, *lNameIcon;

@property (strong,nonatomic) UITextField *emailTextField;

@property (strong,nonatomic) UIImageView *emailTextFieldBG;

@property (strong,nonatomic) UIImageView *emailIcon;

@property (strong,nonatomic) UITextField *phoneTextField;

@property (strong,nonatomic) UIImageView *phoneImageBg;

@property (strong,nonatomic) UIImageView *phoneIcon;

@property (strong,nonatomic) UITextField *passwordTextField;

@property (strong,nonatomic) UIImageView *passwordTextFieldBG;

@property (strong,nonatomic) UIImageView *passwordIcon;

@property (strong,nonatomic) UITextField *confirmPasswordTextField;

@property (strong,nonatomic) UIImageView *confirmPasswordTextFieldBG;

@property (strong,nonatomic) UIImageView *confirmPasswordIcon;

@property (strong,nonatomic) UILabel *registerLabel;

@property (strong,nonatomic) UIButton *termsAndConditionsButton;

@property (strong, nonatomic) UIImageView * tickImageView;

@property (strong,nonatomic) UIButton *registerButton, *backButton;

@property (strong,nonatomic) UIImageView *horrizentalLines;

@end

@implementation SignUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.centerPoint=self.view.center;
    
    [self addComponents];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addComponents{
    
    [self setUpBGImage];
    
    [self setUpBackButton];
    
    [self setUpImageView];
    
    [self setUpRegisterationFieldBGImageView];
    
    [self setUpRegisterationFieldsView];
    
    [self setUpRegisterLabel];
    
    [self setupDataScroll];
    
//    [self setUpTermsAndConditionsButton];
    
//    [self setUpTickImageView];
    
    [self setUpRegisterButton];
   
    [self setUpTextFieldsView];

}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}

-(void) setUpBackButton {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(35, 20, 5, 0);
    
    self.backButton=[ControlsFactory getButton];
    
    self.backButton.backgroundColor=[UIColor clearColor];
    
    [self.backButton setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    
    self.backButton.imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    [self.backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.top.mas_equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(30);
        
    }];
    
}

- (void) setUpImageView{
    
    self.logoImageView =[ControlsFactory getImageView];
    
    UIEdgeInsets paddingForpetsImageView = UIEdgeInsetsMake(25, 50, 0, 0);
    
    self.logoImageView.image=[UIImage imageNamed:@"logo.png"];
    
    [self.view addSubview:self.logoImageView];
    
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForpetsImageView.top);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        if (isiPhone6 || isiPhone6Plus) {
            
            make.height.mas_equalTo(197);
            
            make.width.mas_equalTo(176);
        }
        else
        {
            make.height.mas_equalTo(107);
            
            make.width.mas_equalTo(98);
        }


        
    }];
}

- (void) setUpRegisterationFieldsView{
    
    self.registerationFieldsView =[ControlsFactory getView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 10, 10, 10);
    
    self.registerationFieldsView.backgroundColor=[UIColor clearColor];
    
    self.registerationFieldsView.userInteractionEnabled=YES;
    
    [self.view addSubview:self.registerationFieldsView];
    
    [self.registerationFieldsView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.logoImageView.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        if (isiPhone6) {
            
            UIEdgeInsets padding= UIEdgeInsetsMake(0, 10, 50, 60);
            
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
            
        }
        else if (isiPhone6Plus)
        {
            UIEdgeInsets padding= UIEdgeInsetsMake(0, 10, 120, 60);
            
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);

        }
        else{
            
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        }
        
        
    }];

}

- (void) setUpRegisterationFieldBGImageView
{
    self.registerationFieldBGImageView=[ControlsFactory getImageView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 10, 10, 10);
    
    self.registerationFieldBGImageView.backgroundColor=[UIColor clearColor];
    
    self.registerationFieldBGImageView.image=[UIImage imageNamed:@"border.png"];
    
    [self.registerationFieldBGImageView setUserInteractionEnabled:YES];
    
    [self.view addSubview:self.registerationFieldBGImageView];
    
    [self.registerationFieldBGImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.logoImageView.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        if (isiPhone6) {
            
           UIEdgeInsets padding= UIEdgeInsetsMake(0, 10, 50, 60);
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);

        }
        else if (isiPhone6Plus)
        {
            UIEdgeInsets padding= UIEdgeInsetsMake(0, 10, 120, 60);
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
            
        }
        else{
            
            make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        }
        
    }];

    
}

- (void) setUpRegisterLabel{
    
    self.registerLabel=[ControlsFactory getLabel];
    
    self.registerLabel.text=@"Register";
    
    self.registerLabel.textAlignment=NSTextAlignmentCenter;
    
    self.registerLabel.font= [UIFont systemFontOfSize:20.0];
    
    self.registerLabel.textColor =[UIColor whiteColor];
    
    self.registerLabel.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(12, 10, 80, 10);
    
    [self.registerationFieldsView addSubview:self.registerLabel];
    
    [self.registerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.registerationFieldsView.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        make.width.mas_equalTo(150);
        
        make.height.mas_equalTo(40);
        
    }];
    
}

- (void) setupDataScroll{
    
    self.dataScroll=[ControlsFactory getScrollView];
    
    self.dataScroll.backgroundColor=[UIColor clearColor];
    
    self.dataScroll.scrollEnabled=YES;
    
    self.dataScroll.bounces=NO;
    
    self.dataScroll.userInteractionEnabled=YES;
    
    self.dataScroll.delaysContentTouches=NO;
 
    UIEdgeInsets padding = UIEdgeInsetsMake(8, 10, 112, 10);
    
    [self.registerationFieldsView addSubview:self.dataScroll];
    
        [self.dataScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.registerLabel.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.registerationFieldsView.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.registerationFieldsView.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.registerationFieldsView.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

- (void) setUpTermsAndConditionsButton
{
    self.termsAndConditionsButton= [ControlsFactory getButton];
    
    self.termsAndConditionsButton.backgroundColor = [UIColor clearColor];
    
    self.termsAndConditionsButton.adjustsImageWhenHighlighted = NO;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(20, 15, 20, 15);

    [self.termsAndConditionsButton addTarget:self action:@selector(termsAndConditionsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.termsAndConditionsButton setImage:[UIImage imageNamed:@"termsAndConditionsBeforePressed.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:self.termsAndConditionsButton];
    
    [self.termsAndConditionsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.dataScroll.mas_bottom).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.registerationFieldsView.mas_centerX);
        
        make.width.mas_equalTo(249);
        
        make.height.mas_equalTo(22);
    }];
    
}

-(void) setUpTickImageView
{
    self.tickImageView = [ControlsFactory getImageView];
    
    self.tickImageView.backgroundColor = [UIColor clearColor];
    
    self.tickImageView.image = [UIImage imageNamed:@"tickDoneImage.png"];

    [self.view addSubview:self.tickImageView];

    UIEdgeInsets padding = UIEdgeInsetsMake(3, 3, 20, 15);
    
    [self.tickImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.termsAndConditionsButton.mas_top).with.offset(-padding.top);
        
        make.left.equalTo(self.termsAndConditionsButton.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(23);
        
        make.height.mas_equalTo(20);
    }];
    
    self.tickImageView.hidden = YES;
    
    self.termsAndConditionsButtonClicked = NO;

}

-(void)termsAndConditionsButtonClicked:(UIButton *)btn
{
    if (!self.termsAndConditionsButtonClicked)
    {
        self.tickImageView.hidden = NO;
        
        self.termsAndConditionsButtonClicked=YES;
    }
    else if (self.termsAndConditionsButtonClicked)
    {
        self.tickImageView.hidden = YES;
        
        self.termsAndConditionsButtonClicked=NO;
    }
    
}

- (void) setUpRegisterButton
{
    
    self.registerButton=[ControlsFactory getButton];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 15, 20, 15);
    
    [self.registerButton setImage:[UIImage imageNamed:@"registerButton.png"] forState:UIControlStateNormal];

    [self.registerButton addTarget:self action:@selector(registerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.registerButton];
    
    [self.registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.registerationFieldsView.mas_centerX);

        make.left.equalTo(self.registerationFieldsView.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.registerationFieldsView.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.registerationFieldsView.mas_bottom).with.offset(padding.top);
        
        make.bottom.equalTo(self.registerationFieldsView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(50);
    }];
    
}

-(void)registerButtonClicked:(UIButton *)btn{
    
     [self.view endEditing:YES];
     
      NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
     
     if ([self checkTextFields]) {
    
         NetworkingClass *network = [[NetworkingClass alloc] init];
         
         network.delegate = self;
         
         [dataDict setObject:self.fNameTextField.text forKey:@"fName"];
         
         [dataDict setObject:self.lNameTextField.text forKey:@"lName"];
         
         [dataDict setObject:self.emailTextField.text forKey:@"email"];
         
         [dataDict setObject:self.phoneTextField.text forKey:@"phonenumber"];
         
         [dataDict setObject:self.passwordTextField.text forKey:@"password"];
         
         [dataDict setObject:self.confirmPasswordTextField.text forKey:@"confirmpassword"];
        
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         
         [network signUp:dataDict];
    }
  
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"success"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"firstLoggin"];
            
            //  [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_NOT_Cleared forKey:AnnualSubscriptionPaid_KEY];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        
    }
    else {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Email Already Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [errorAlert show];
    }

}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    
}

- (BOOL) checkTextFields {
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ( self.fNameTextField.text && self.fNameTextField.text.length<1) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"First Name Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    else if ( self.lNameTextField.text && self.lNameTextField.text.length<1) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    else if (self.emailTextField.text && self.emailTextField.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Email Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        
    }
    else if ([emailTest evaluateWithObject:self.emailTextField.text] == NO) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Valid Email Address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    
    else if (self.phoneTextField.text && self.phoneTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Phonenumber Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    else if (self.passwordTextField.text && self.passwordTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    else if (self.confirmPasswordTextField.text && self.confirmPasswordTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    else if ([self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text] )
    {
//        if (self.termsAndConditionsButtonClicked) {
//            
            return YES;
//            
//        }
//        else
//        {
//            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Agree to terms and conditions" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            
//            [errorAlert show];
//            
//            return NO;
//        }
        
    }
    
    else {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password missmatch" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    return NO;
}

- (void) setUpTextFieldsView{
    
    [self initTextFields];
    
    [self setUpNameTextField];
    
    [self setUpEmailTextField];
    
    [self setUpPhoneNumberTextField];
    
    [self setUpPasswordTextField];
//    
   [self setUpConfirmPasswordField];
//    
}

- (void) setUpNameTextField
 {
     self.fNameTextFiedBG.image=[UIImage imageNamed:@"textFieldBG.png"];
     
     self.fNameTextFiedBG.backgroundColor=[UIColor clearColor];
     
     [self.dataScroll addSubview:self.fNameTextFiedBG];
     
     UIEdgeInsets padding= UIEdgeInsetsMake(0, 0, 0, 0);
     
     [self.fNameTextFiedBG mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
         
         make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
         
         make.top.equalTo(self.dataScroll.mas_top).with.offset(padding.top);
         
         make.height.mas_equalTo(50);
         
         make.width.mas_equalTo(self.dataScroll.mas_width);
     }];
     
     padding= UIEdgeInsetsMake(0, 15, 0, 0);
     
     self.fNameIcon.image=[UIImage imageNamed:@"nameIcon.png"];
     
     self.fNameIcon.backgroundColor=[UIColor clearColor];
     
     [self.dataScroll addSubview:self.fNameIcon];
     
     [self.fNameIcon mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
         
         make.centerY.mas_equalTo(self.fNameTextFiedBG.mas_centerY);
         
         make.height.mas_equalTo(22);
         
         make.width.mas_equalTo(23);
     }];

     padding= UIEdgeInsetsMake(0, 10, 0, 60);
     
     self.fNameTextField.backgroundColor=[UIColor clearColor];
     
     [self.fNameTextField setReturnKeyType:UIReturnKeyNext];
     
     self.fNameTextField.placeholder=@"First Name";
     
     [self.fNameTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
     
     self.fNameTextField.delegate=self;
     
     [self.dataScroll addSubview:self.fNameTextField];
     
     [self.fNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.fNameIcon.mas_right).with.offset(padding.left);
         
         make.right.equalTo(self.fNameTextFiedBG.mas_right).with.offset(-padding.right);
         
         make.centerY.mas_equalTo(self.fNameTextFiedBG.mas_centerY);
         
         make.height.mas_equalTo(30);
     }];
     
     self.lNameTextFieldBG.image=[UIImage imageNamed:@"textFieldBG.png"];
     
     self.lNameTextFieldBG.backgroundColor=[UIColor clearColor];
     
     [self.dataScroll addSubview:self.lNameTextFieldBG];
     
    padding= UIEdgeInsetsMake(5, 0, 0, 0);
     
     [self.lNameTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
         
         make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
         
         make.top.equalTo(self.fNameTextFiedBG.mas_bottom).with.offset(padding.top);
         
         make.height.mas_equalTo(50);
         
         make.width.mas_equalTo(self.dataScroll.mas_width);
     }];
     
     padding= UIEdgeInsetsMake(0, 15, 0, 0);
     
     self.lNameIcon.image=[UIImage imageNamed:@"nameIcon.png"];
     
     self.lNameIcon.backgroundColor=[UIColor clearColor];
     
     [self.dataScroll addSubview:self.lNameIcon];
     
     [self.lNameIcon mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
         
         make.centerY.mas_equalTo(self.lNameTextFieldBG.mas_centerY);
         
         make.height.mas_equalTo(22);
         
         make.width.mas_equalTo(23);
     }];
    
     padding= UIEdgeInsetsMake(0, 10, 0, 60);
     
     self.lNameTextField.backgroundColor=[UIColor clearColor];
     
     [self.lNameTextField setReturnKeyType:UIReturnKeyNext];
     
     self.lNameTextField.placeholder=@"Last Name";
     
     [self.lNameTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
     
     self.lNameTextField.delegate=self;
     
     [self.dataScroll addSubview:self.lNameTextField];
     
     [self.lNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
         
         make.left.equalTo(self.lNameIcon.mas_right).with.offset(padding.left);
         
         make.right.equalTo(self.lNameTextFieldBG.mas_right).with.offset(-padding.right);
         
         make.centerY.mas_equalTo(self.lNameTextFieldBG.mas_centerY);
         
         make.height.mas_equalTo(30);
     }];



 }

- (void) setUpEmailTextField{
    
    self.emailTextFieldBG.image=[UIImage imageNamed:@"textFieldBG.png"];
    
    self.emailTextFieldBG.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.emailTextFieldBG];
    
    UIEdgeInsets padding= UIEdgeInsetsMake(5, 0, 0, 0);
    
    [self.emailTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.lNameTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(50);
    }];
    
    padding= UIEdgeInsetsMake(0, 15, 0, 0);
    
    self.emailIcon.image=[UIImage imageNamed:@"email.png"];
    
    self.emailIcon.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.emailIcon];
    
    [self.emailIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.centerY.mas_equalTo(self.emailTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(17);
        
        make.width.mas_equalTo(22);
    }];

    padding= UIEdgeInsetsMake(0, 10, 0, 60);
    
    self.emailTextField.backgroundColor=[UIColor clearColor];
    
    self.emailTextField.placeholder=@"Email Address";
    
    [self.emailTextField setReturnKeyType:UIReturnKeyNext];
    
    [self.emailTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.emailTextField.delegate=self;
    
    self.emailTextField.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.emailTextField];
    
    [self.emailTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.emailIcon.mas_right).with.offset(padding.left);
        
        make.right.equalTo(self.emailTextFieldBG.mas_right).with.offset(-padding.right);
        
        make.centerY.mas_equalTo(self.emailTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(30);
    }];
    
}

- (void) setUpPhoneNumberTextField {
    
    self.phoneImageBg.image=[UIImage imageNamed:@"textFieldBG.png"];
    
    self.phoneImageBg.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.phoneImageBg];
    
    UIEdgeInsets padding= UIEdgeInsetsMake(5, 0, 0, 0);
    
    [self.phoneImageBg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.emailTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(50);
    }];
    
    padding= UIEdgeInsetsMake(0, 15, 0, 0);
    
    self.phoneIcon.image=[UIImage imageNamed:@"phone.png"];
    
    self.phoneIcon.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.phoneIcon];
    
    [self.phoneIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.centerY.mas_equalTo(self.phoneImageBg.mas_centerY);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(23);
    }];
    
    padding= UIEdgeInsetsMake(0, 10, 0, 60);
    
    self.phoneTextField.backgroundColor=[UIColor clearColor];
    
    [self.phoneTextField setReturnKeyType:UIReturnKeyNext];
    
    self.phoneTextField.placeholder=@"Phone Number";
    
    [self.phoneTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.phoneTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    self.phoneTextField.delegate=self;
    
    [self.dataScroll addSubview:self.phoneTextField];
    
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.phoneIcon.mas_right).with.offset(padding.left);
        
        make.right.equalTo(self.phoneImageBg.mas_right).with.offset(-padding.right);
        
        make.centerY.mas_equalTo(self.phoneImageBg.mas_centerY);
        
        make.height.mas_equalTo(30);
    }];

}

- (void) setUpPasswordTextField{
    
    self.passwordTextFieldBG.image=[UIImage imageNamed:@"textFieldBG.png"];
    
    self.passwordTextFieldBG.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.passwordTextFieldBG];
    
    UIEdgeInsets padding= UIEdgeInsetsMake(5, 0, 0, 0);
    
    [self.passwordTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.phoneImageBg.mas_bottom).with.offset(padding.top);
        
       // make.bottom.equalTo(self.dataScroll.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(50);
    }];
    
    padding= UIEdgeInsetsMake(0, 15, 0, 0);
    
    self.passwordIcon.image=[UIImage imageNamed:@"lock.png"];
    
    self.passwordIcon.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.passwordIcon];
    
    [self.passwordIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.centerY.mas_equalTo(self.passwordTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(23);
    }];
    
    padding= UIEdgeInsetsMake(0, 10, 0, 60);
    
    self.passwordTextField.backgroundColor=[UIColor clearColor];
    
    [self.passwordTextField setReturnKeyType:UIReturnKeyNext];
    
    self.passwordTextField.placeholder=@"Password";
    
    self.passwordTextField.secureTextEntry=YES;
    
    [self.passwordTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.passwordTextField.delegate=self;
    
    [self.dataScroll addSubview:self.passwordTextField];
    
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.passwordIcon.mas_right).with.offset(padding.left);
        
        make.right.equalTo(self.passwordTextFieldBG.mas_right).with.offset(-padding.right);
        
        make.centerY.mas_equalTo(self.passwordTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(30);
    }];
}

- (void)setUpConfirmPasswordField{
    
    self.confirmPasswordTextFieldBG.image=[UIImage imageNamed:@"textFieldBG.png"];
    
    self.confirmPasswordTextFieldBG.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.confirmPasswordTextFieldBG];
    
    UIEdgeInsets padding= UIEdgeInsetsMake(5, 0, 0, 0);
    
    [self.confirmPasswordTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.dataScroll.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.passwordTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.bottom.equalTo(self.dataScroll.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(50);
    }];
    
    padding= UIEdgeInsetsMake(0, 15, 0, 0);
    
    self.confirmPasswordIcon.image=[UIImage imageNamed:@"lock.png"];
    
    self.confirmPasswordIcon.backgroundColor=[UIColor clearColor];
    
    [self.dataScroll addSubview:self.confirmPasswordIcon];
    
    [self.confirmPasswordIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.dataScroll.mas_left).with.offset(padding.left);
        
        make.centerY.mas_equalTo(self.confirmPasswordTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(23);
    }];

    padding= UIEdgeInsetsMake(0, 10, 0, 60);
    
    self.confirmPasswordTextField.backgroundColor=[UIColor clearColor];
    
    self.confirmPasswordTextField.placeholder=@"Confirm Password";
    
    [self.confirmPasswordTextField setValue:[UIColor colorWithRed:110.0/255 green:163.0/255 blue:117.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.confirmPasswordTextField.delegate=self;
    
    [self.dataScroll addSubview:self.confirmPasswordTextField];
    
    self.confirmPasswordTextField.secureTextEntry=YES;
    
    [self.confirmPasswordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.confirmPasswordIcon.mas_right).with.offset(padding.left);
        
        make.right.equalTo(self.confirmPasswordTextFieldBG.mas_right).with.offset(-padding.right);
        
        make.centerY.mas_equalTo(self.confirmPasswordTextFieldBG.mas_centerY);
        
        make.height.mas_equalTo(30);
    }];
    
}

- (void) initTextFields{
    
    self.fNameTextField=[ControlsFactory getTextField];
    
    self.fNameTextFiedBG =[ControlsFactory getImageView];
    
    self.fNameIcon =[ControlsFactory getImageView];
    
    self.lNameTextField=[ControlsFactory getTextField];
    
    self.lNameTextFieldBG=[ControlsFactory getImageView];
    
    self.lNameIcon=[ControlsFactory getImageView];
    
    self.phoneTextField=[ControlsFactory getTextField];
    
    self.phoneImageBg =[ControlsFactory getImageView];
    
    self.phoneIcon=[ControlsFactory getImageView];
    
    self.emailTextField=[ControlsFactory getTextField];
    
    self.emailTextFieldBG=[ControlsFactory getImageView];
    
    self.emailIcon=[ControlsFactory getImageView];
    
    self.passwordTextField =[ControlsFactory getTextField];
    
    self.passwordTextFieldBG=[ControlsFactory getImageView];
    
    self.passwordIcon=[ControlsFactory getImageView];
    
    self.confirmPasswordTextField =[ControlsFactory getTextField];
    
    self.confirmPasswordTextFieldBG=[ControlsFactory getImageView];

    self.confirmPasswordIcon=[ControlsFactory getImageView];
    
}

- (void) backButtonClicked: (UIButton *) button
{
    
    [self.navigationController popViewControllerAnimated:YES];

}
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:self.fNameTextField]) {
        
        [self.lNameTextField becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.lNameTextField])
    {
        [self.emailTextField becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.emailTextField])
    {
        
        [self.phoneTextField becomeFirstResponder];
        
    }
    else if ([textField isEqual:self.phoneTextField])
    {
        
        [self.passwordTextField becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.passwordTextField])
    {
        
        [self.confirmPasswordTextField becomeFirstResponder];
        
    }
    else if (textField== self.confirmPasswordTextField) {
        
        [textField resignFirstResponder];
        
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
     self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (isiPhone6 || isiPhone6Plus) {
        
        self.view.center=CGPointMake(self.centerPoint.x, 220);

    }
    else{

        self.view.center=CGPointMake(self.centerPoint.x, 80);

    }
    if (isiPhone4)
    {
        
        if (textField ==self.lNameTextField) {
            
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 50.0) animated:YES];
        }
        
        else if (textField == self.emailTextField)
        {
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 100.0) animated:YES];
            
        }
        
        else if (textField == self.phoneTextField)
        {
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 150.0) animated:YES];
            
        }
        
        else if (textField == self.passwordTextField)
        {
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 200.0) animated:YES];
            
        }

    }
    
    else if (isiPhone5 )
    {
         if (textField == self.phoneTextField)
        {
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 110.0) animated:YES];
            
        }
    
    }
    else if (isiPhone6 || isiPhone6Plus)
    {
        if (textField == self.phoneTextField)
        {
            [self.dataScroll setContentOffset:CGPointMake(self.lNameTextFieldBG.frame.origin.x, 55.0) animated:YES];
            
        }

        
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneTextField)
    {
        NSString *newString = [self.phoneTextField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
