//
//  MessageCell.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/3/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell
@property (strong, nonatomic) UILabel *nameLbl;
@property (strong,nonatomic) UILabel *messageTxt;
@property (strong, nonatomic) UIImageView *imageIcon;
@property (strong, nonatomic) UIView *grayView;
@property (strong,nonatomic) UILabel *numLbl;
@end
