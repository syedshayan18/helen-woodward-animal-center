//
//  ProfileController.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/8/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIActionSheetDelegate>

@end
