//
//  MessageController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "MessageController.h"

#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "MessageCell.h"

#import "DashboardController.h"

#import "ChatController.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "ChatMessage.h"

#import "SelectPetController.h"

#import "PetdetailsViewController.h"

#import "PusherSwift/PusherSwift-Swift.h"

#define MessageCellIdentifier @"MessageCell"

@interface MessageController ()

@property (assign) BOOL  dataFromServer;

@property (assign) BOOL searching;

@property int numberOfRows;



@property (strong,nonatomic) UILabel *messagesLbl;

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UIButton *backBtn;

@property (strong,nonatomic) UIButton *addBtn;

@property (strong,nonatomic) UIButton *addButtonBG;

@property (strong,nonatomic) UIView *searchView;

@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) UIImageView *txtFldImgView;

@property (strong,nonatomic) UITextField *txtFld;

@property (strong,nonatomic) UIImageView *searchIcon;

@property (strong,nonatomic) UIView *blurView;

@property (strong,nonatomic) SelectPetController *petSelectPopupController;

//@property (strong,nonatomic) NSMutableArray *dataArray;

@property (strong,nonatomic) NSArray *dataArray;

@property (strong,nonatomic) NSMutableArray *tableData, *dataToBeProcessed,*doctorNames,*filteredDoctorNames,*filteredtableData;

@end

@implementation MessageController


-(void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageFromDoctorOnMessagesScreen:) name:@"messageFromDoctorOnMessagesScreen" object:nil];
    
    //[self.view endEditing:YES];
    
    self.searching=FALSE;
    
    self.txtFld.text=@"";
    
    [self.blurView removeFromSuperview];
    
    [self.dataToBeProcessed removeAllObjects];
    
    self.doctorNames=[[NSMutableArray alloc] init];   // The names of Doctor with whome conversation has been made. We have seperated doctor Names from
    
    // data we fetched from servers
    
    [self.filteredDoctorNames removeAllObjects];
    
    [self.filteredtableData removeAllObjects];

}


- (void)viewDidLoad {
    
   

    [super viewDidLoad];
    
     [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    
    self.filteredDoctorNames=[[NSMutableArray alloc] init];
    
    self.filteredtableData=[[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(crossButtonClicked:)
                                                 name:@"crossButton"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveToChat:)
                                                 name:@"moveToChat"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ChatToMessages:)
                                                 name:@"fromChatToMessages"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    
    self.blurView=[[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    
    self.blurView.alpha=.6;
    
    self.blurView.backgroundColor=[UIColor blackColor];
    
    self.tableData=[[NSMutableArray alloc] init];
    
    self.dataToBeProcessed=[[NSMutableArray alloc] init];
    
    self.dataFromServer = NO;
    
    self.numberOfRows=1;
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    // Do any additional setup after loading the view.
    
    [self addingComponents];
    
    [self getMessages];
    
    
    
}



- (void) addingComponents {
    
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0];
  
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
   
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
  
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
    
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
       // make.top.equalTo(self.view.mas_top);
       
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
       
        make.height.mas_equalTo(75);
        
    }];
    
    self.backBtn=[ControlsFactory getButton];
    
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    
    [self.backBtn addTarget:self action:@selector(backbutton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(54);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
    }];
    
    UIEdgeInsets paddingForMessagesLabel = UIEdgeInsetsMake(0, 0, 15, 0);

    self.messagesLbl =[ControlsFactory getLabel];
    
    self.messagesLbl.text=@"Messages";
    
    [self.messagesLbl setTextAlignment:NSTextAlignmentCenter];
    
    self.messagesLbl.font= [UIFont systemFontOfSize:20.0];
    
    self.messagesLbl.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.messagesLbl];
   
    [self.messagesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
               
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForMessagesLabel.bottom);
        
        
    }];
    
    self.addBtn =[ControlsFactory getButton];
    
   [self.addBtn setBackgroundImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
    
    [self.addBtn addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 10);
    
    [self.headerView addSubview:self.addBtn];
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(23);
        
        make.width.mas_equalTo(22);
        
        
    }];
    self.addButtonBG =[ControlsFactory getButton];
    
    self.addButtonBG.backgroundColor=[UIColor clearColor];
    
    [self.addButtonBG addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 5);
    
    [self.headerView addSubview:self.addButtonBG];
    
    [self.addButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(50);
        
        make.width.mas_equalTo(70);
        
        
    }];

    self.searchView=[ControlsFactory getView];
    
    self.searchView.backgroundColor=[UIColor whiteColor];
    
   
    
    UIEdgeInsets paddingForSearchVIew= UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.searchView];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.height.mas_equalTo(70);
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForSearchVIew.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForSearchVIew.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForSearchVIew.top);
        
        //make.bottom.equalTo(self.view.mas_bottom).with.offset(-paddingForSearchVIew.bottom);
    }];
    
    self.tableView =[ControlsFactory getTableView];
    
    self.tableView.delegate=self;
    
    self.tableView.dataSource=self;
    
    self.tableView.backgroundColor=[UIColor whiteColor];
    
    [self.tableView registerClass:[MessageCell class]   forCellReuseIdentifier:MessageCellIdentifier];
    
    [self.view addSubview:self.tableView];
    
    UIEdgeInsets paddingforTableView =UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make){
       
        make.left.equalTo(self.view.mas_left).with.offset(paddingforTableView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingforTableView.right);
        
       // make.top.equalTo(self.searchView.mas_bottom);
    
        make.top.equalTo(self.searchView.mas_bottom).with.offset(paddingforTableView.top);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-paddingforTableView.bottom);
        
    }];
    
    self.txtFldImgView=[ControlsFactory getImageView];
    
    self.txtFldImgView.image=[UIImage imageNamed:@"searchRectangle.png"];
    
    UIEdgeInsets paddingForTxtFldImg = UIEdgeInsetsMake(15, 15, 15, 15);
    
    [self.searchView addSubview:self.txtFldImgView];
    
    [self.txtFldImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.searchView.mas_left).with.offset(paddingForTxtFldImg.left);
        
        make.right.equalTo(self.searchView.mas_right).with.offset(-paddingForTxtFldImg.right);
        
        // make.top.equalTo(self.searchView.mas_bottom);
        
        make.top.equalTo(self.searchView.mas_top).with.offset(paddingForTxtFldImg.top);
        
        make.bottom.equalTo(self.searchView.mas_bottom).with.offset(-paddingForTxtFldImg.bottom);
        
    }];
    
    self.searchIcon=[ControlsFactory getImageView];
    
    self.searchIcon.image=[UIImage imageNamed:@"searchIcon.png"];
    
    UIEdgeInsets paddingForSearchIcon= UIEdgeInsetsMake(10, 10, 10, 15);
    
    [self.txtFldImgView addSubview:self.searchIcon];
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForSearchIcon.right);
        
        make.centerY.mas_equalTo(self.txtFldImgView.mas_centerY);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(18);
        
    }];

    
    self.txtFld = [ControlsFactory getTextField];
    
    self.txtFld.backgroundColor=[UIColor clearColor];

    self.txtFld.placeholder=@"Search Messages";
    
    [self.txtFld setValue:[UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.txtFld.delegate=self;
    
    UIEdgeInsets paddingForTxtFld= UIEdgeInsetsMake(0, 25, 0, 0);
    
    [self.txtFldImgView addSubview:self.txtFld];
    
    self.txtFldImgView.userInteractionEnabled = YES;
    
    [self.txtFld mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.txtFldImgView.mas_left).with.offset(paddingForTxtFld.left);
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForTxtFld.right);
        
        make.top.equalTo(self.txtFldImgView.mas_top).with.offset(paddingForTxtFld.top);
        
        make.bottom.equalTo(self.txtFldImgView.mas_bottom).with.offset(-paddingForTxtFld.bottom);
        
    }];
}


#pragma mark Table view delegates and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataFromServer) {
        
        if (!self.searching) {
        
            return self.tableData.count;
        }
        else
        {
            return  self.filteredtableData.count;
        }
        
    }
    
    else
    {
    
        return 1;
    
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:MessageCellIdentifier forIndexPath:indexPath];
    
        if (self.dataFromServer) {
            
            if (!self.searching) {
                
                cell.nameLbl.text=@"Not Available";
                
                cell.messageTxt.text=nil;
                
                cell.imageIcon.image=nil;
                
                cell.nameLbl.text=[[self.tableData objectAtIndex:indexPath.row] objectForKey:@"doctorName"];
                
                cell.messageTxt.text=[[self.tableData objectAtIndex:indexPath.row] objectForKey:@"message"];
                
                cell.imageIcon.image=[UIImage imageNamed:@"doctor.png"];
            
            }
            
            else
            {
                cell.nameLbl.text=@"Not Available";
                
                cell.messageTxt.text=nil;
                
                cell.imageIcon.image=nil;
                
                cell.nameLbl.text=[[self.filteredtableData objectAtIndex:indexPath.row] objectForKey:@"doctorName"];
                
                cell.messageTxt.text=[[self.filteredtableData objectAtIndex:indexPath.row] objectForKey:@"message"];
                
                cell.imageIcon.image=[UIImage imageNamed:@"doctor.png"];
            }
            
        }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //ChatViewController *chatViewController=[[ChatViewController alloc]init];
    
    NSString *threadId;
    
    if (self.searching) {
        
        if (_filteredtableData.count !=0){
            
            threadId=[[self.filteredtableData objectAtIndex:indexPath.row] objectForKey:@"threadId"];
        }
        else {
            return;
        }

        
        
       
//        threadId=[[self.filteredtableData objectAtIndex:indexPath.row] objectForKey:@"threadId"];
        
        [self.view endEditing:YES];
        
    }
    else{
        if (_tableData.count !=0){
        
        threadId=[[self.tableData objectAtIndex:indexPath.row] objectForKey:@"threadId"];
        }
        else {
            return;
        }
        
    }
    
    [self dataToBeProcessedFunction:threadId];

    
    ChatController *chatController=[[ChatController alloc] init];
    
    [self addChildViewController:chatController];
    
    chatController.chatData=self.dataToBeProcessed;
    
    chatController.firstChat=@"NO";  // this implies the fact that a chat has been done previously
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"messageFromDoctorOnMessagesScreen" object:nil];
    
    [self.navigationController pushViewController:chatController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71.5;
}

#pragma mark TextField Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField          // became first responder
{
    
    

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    string = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (string.length==0) {
        
        self.searching=FALSE;
        
        [self.filteredDoctorNames removeAllObjects];
        
        [self.filteredtableData removeAllObjects];
        
        
    }
    else
    {
        self.searching=TRUE;
        
        // use contains instead of beginswith if want to search anywhere in the string
        
        if (self.txtFld.text.length==0) {
            
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",string];
            
            self.filteredDoctorNames  =[NSMutableArray arrayWithArray:[self.doctorNames filteredArrayUsingPredicate:predicate]];

        }
        else
        {
        
            NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",string];
            
            self.filteredDoctorNames  =[NSMutableArray arrayWithArray:[self.doctorNames filteredArrayUsingPredicate:predicate]];

        }
        
    }
    
    if (self.searching) {
        
//        if (self.filteredDoctorNames.count) {
        
            [[self filteredtableData] removeAllObjects];  // The Following function filterTableData is called when ever a new character is added
            
            // into search field now whats happen, if was that new filterData for TableView was repeating doctorNames so first we flush all previous fields
            
            //and then we added new ones
            
            [self filtertableData];
//        }
    }
    
    [self.tableView reloadData];
    
    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [self.view endEditing:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void) backbutton:(UIButton *) button
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) filtertableData {
    
    if(self.filteredDoctorNames.count == 0)
    {
        return;
    }
    
    int j=0;
    
    for (int i=0; i<self.tableData.count; i++) {
        
        NSString *doctorName=[[self.tableData objectAtIndex:i] objectForKey:@"doctorName"];
        
        NSString *doctorNamedToBeComparedWith=[self.filteredDoctorNames objectAtIndex:j];
        
        if ([doctorName isEqualToString:doctorNamedToBeComparedWith]) {
            
            [self.filteredtableData addObject:[self.tableData objectAtIndex:i]];
            
            j++;

            if (j==self.filteredDoctorNames.count)
            {   
                break;
            }
            
        }
        
    }
   // NSLog(@"OK");
}

- (void) findRows{
    
    NSString* threadId=[[self.dataArray objectAtIndex:0] objectForKey:@"threadId"];
    
    for (int i=0; i< self.dataArray.count; i++) {
        
        if ([threadId isEqualToString:[[self.dataArray objectAtIndex:i] objectForKey:@"threadId"]]) {
            
        }
        else
        {
    
            [self.tableData addObject:[self.dataArray objectAtIndex:i-1]];
            
            NSString *doctorName= [[self.dataArray objectAtIndex:i-1] objectForKey:@"doctorName"];
            
            [self.doctorNames addObject:doctorName];
            
           // NSLog(@"%@",[[self.dataArray objectAtIndex:i] objectForKey:@"threadId"]);
            
            threadId=[[self.dataArray objectAtIndex:i] objectForKey:@"threadId"];
            
            self.numberOfRows++;
            
        }
        
    }
    
    [self.tableData addObject:[self.dataArray objectAtIndex:self.dataArray.count-1]];
    
    [self.doctorNames addObject:[[self.dataArray objectAtIndex:self.dataArray.count-1] objectForKey:@"doctorName"]];
    
}

- (void) dataToBeProcessedFunction:(NSString *) threadId {

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
   
    [df setDateFormat:@"yyyy-MM-dd hh:mm:ss Z"];
    
    
    for (int i=0; i<self.dataArray.count; i++) {
        
        if ([[[self.dataArray objectAtIndex:i] objectForKey:@"threadId"] isEqualToString:threadId]) {
            
            ChatMessage *chat=[[ChatMessage alloc] init];
            
            NSDate *myDate=[[NSDate alloc] init];
            
             myDate = [[self.dataArray objectAtIndex:i] objectForKey:@"messageCreated"];
            
            chat.doctorName=[[self.dataArray objectAtIndex:i] objectForKey:@"doctorName"];
            
            chat.message=[[self.dataArray objectAtIndex:i] objectForKey:@"message"];
            
            chat.messageBy=[[self.dataArray objectAtIndex:i] objectForKey:@"messageBy"];
            
            //chat.messageCreated=[[self.dataArray objectAtIndex:i] objectForKey:@"messageCreated"];

            chat.messageDate=[[self.dataArray objectAtIndex:i] objectForKey:@"messageCreated"];

            chat.doctorImage=[[self.dataArray objectAtIndex:i] objectForKey:@"doctorImage"];
            
            chat.messageId=[[self.dataArray objectAtIndex:i] objectForKey:@"messageId"];
            
            chat.messageImageLink=[[self.dataArray objectAtIndex:i] objectForKey:@"messageImage"];
            
            chat.petId=[[self.dataArray objectAtIndex:i] objectForKey:@"petId"];
            
            chat.threadId=[[self.dataArray objectAtIndex:i] objectForKey:@"threadId"];

            chat.threadStatus=[[self.dataArray objectAtIndex:i] objectForKey:@"threadStatus"];
            
            [self.dataToBeProcessed addObject:chat];

        }
    }
}
- (void) addButtonClicked:(UIButton*)button
{
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Reload_PETDATA"];
    PetdetailsViewController *petdetails = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"petdetails"];
    [self.navigationController pushViewController:petdetails animated:YES];
    petdetails.hideButtonformessage=YES;
    
   // [self.navigationController pushViewController:petdetails animated:YES];
    
    
//    
//    self.petSelectPopupController = [[SelectPetController alloc] init];
//    
//    self.petSelectPopupController.view.frame=CGRectMake(self.view.frame.origin.x+20.0, self.view.frame.origin.y+20.0, self.view.frame.size.width-40,self.view.frame.size.height-40 );
//    
//    
//    [self.view addSubview:self.blurView];
//    
//    [self addChildViewController:self.petSelectPopupController];
//    
//    [self.view addSubview:self.petSelectPopupController.view];
//    
//    [self.petSelectPopupController didMoveToParentViewController:self];
//    
    
//    petController=nil;
    
}

#pragma mark WebApi Calls

- (void) getMessages {
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [network getMessages:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
}


#pragma mark Server Response 

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
}
-(void)response:(id)data {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
           
           [MBProgressHUD hideHUDForView:self.view animated:YES];


    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"status"] isEqualToString:@"failure"]) {
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        
        else if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
            
            
                self.dataFromServer=YES;
                
                [self.tableData removeAllObjects];
                
                self.dataArray=[data objectForKey:@"data"];
                
                [self findRows];
                
                [self.tableView reloadData];

                [self.tableView reloadData];
           
            
        }
                
    }
        
    });
    
    
}



- (void) crossButtonClicked:(NSNotification *) notification
{
    [self.blurView removeFromSuperview];
}

- (void) moveToChat:(NSNotification *) notification
{
    [self.blurView removeFromSuperview];
    
    NSDictionary *petData=[notification userInfo];
    
    ChatController *chatController=[[ChatController alloc] init];
    
    [self addChildViewController:chatController];
    
    chatController.chatData=nil;
    
    chatController.firstChat=@"YES";
    
    chatController.petData=petData;
    
    [self.navigationController pushViewController:chatController animated:YES];

}

- (void) ChatToMessages:(NSNotification *) notification
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [network getMessages:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];

}

-(void) messageFromDoctorOnMessagesScreen:(NSNotification *)note {
    
    //NSDictionary *data=[note userInfo];
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [network getMessages:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
    self.searching=FALSE;
    
    self.txtFld.text=@"";

    [self.dataToBeProcessed removeAllObjects];
    
    self.doctorNames=[[NSMutableArray alloc] init];   // The names of Doctor with whome conversation has been made. We have seperated doctor Names from
    
    // data we fetched from servers
    
    [self.filteredDoctorNames removeAllObjects];
    
    [self.filteredtableData removeAllObjects];
}


- (void) keyboardWillShow:(NSNotification *)note {
    
    NSDictionary *userInfo = [note userInfo];
    
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    
    NSLog(@"tableView center: %@",NSStringFromCGPoint(self.tableView.center));
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
