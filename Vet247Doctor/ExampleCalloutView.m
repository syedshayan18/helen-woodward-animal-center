#import "ExampleCalloutView.h"

#import "ExampleCalloutAnnotation.h"

@implementation ExampleCalloutView

- (void)dealloc
{
//    [_adress release];
//
//    [_gebied release];
//    
//    [_schouwBtn release];
//    
//    [_bgImage release];
//    
//    [super dealloc];
}

-(id) instantiateObject
{
    self = [super init];
    
    [[NSBundle mainBundle] loadNibNamed:@"ExampleCalloutView" owner:self options:nil];
    
//    self.adress.font = HelveticaNeueLTCom(13);
//    
//    self.gebied.font = HelveticaNeueLTCom(12);
//    
//    [self.schouwBtnLbl setFont:DroidSans(13)];
    
    return self;
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation Map:(MKMapView *)map{
    
    if(!(self = [super initWithAnnotation:annotation reuseIdentifier:@"ExampleCalloutView" Map:map]))
    return nil;
    
    self.referralAddressLabel.numberOfLines = 3;
    
    [self.referralAddressLabel sizeToFit];
    
    self.referralAddressLabel.minimumScaleFactor = 8.;
    
    [self.referralAddressLabel setFont:[UIFont fontWithName:@"ReferralsAddress" size:14]];
    
    self.referralAddressLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.showReferralDetailButton setTitle:@"" forState:UIControlStateNormal];
    
    self.showReferralDetailButton.backgroundColor = [UIColor clearColor];
    
    [[NSBundle mainBundle] loadNibNamed:@"ExampleCalloutView" owner:self options:nil];
    
    self.adress.font = HelveticaNeueLTCom(13);
    
    self.gebied.font = HelveticaNeueLTCom(12);
    
    [self.schouwBtnLbl setFont:DroidSans(13)];
 
    //k.....
#if  defined (Zwijndrecht)  || defined (Rijksvastgoedbedrijf)
    
    if(!isiPhoneDevice)
    {
        
        [self settingFramesOfPopUpButtonComponents];
    }
    else
    {
        if (isiPhone6)
        {
            [self settingFramesOfPopUpButtonComponents];
        
        }
        else
        {
            
             [self settingFramesOfPopUpButtonComponents];
        
            self.bgImage.frame=CGRectMake(self.bgImage.frame.origin.x, self.bgImage.frame.origin.y, 205, 106);
           
            // self.bgImage.image=[UIImage imageNamed:@"POIBg-redIphone.png"];
            
            self.schouwBtnLbl.frame=CGRectMake(self.schouwBtnLbl.frame.origin.x-10, self.schouwBtnLbl.frame.origin.y-15, self.schouwBtnLbl.frame.size.width, self.schouwBtnLbl.frame.size.height);

            self.schouwBtn.frame=CGRectMake(self.bgImage.frame.origin.x,self.schouwBtnLbl.frame.origin.y, 205, self.schouwBtnLbl.frame.size.height);
           
            //self.schouwBtnLbl.frame.size.width+30;
            
            //self.schouwBtn.backgroundColor=[UIColor redColor];
            
        }
    }
//k.....
 #endif
    
    return self;
}
//k...
-(void)settingFramesOfPopUpButtonComponents
{

    self.adress.font = [UIFont fontWithName:@"ArialMT" size:13];
    
    self.gebied.font = [UIFont fontWithName:@"ArialMT" size:11];
    
    self.adress.textColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:118.0/255.0 alpha:1.0];
    
    self.gebied.textColor=[UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:118.0/255.0 alpha:1.0];
 
    [self.schouwBtnLbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
    
    self.schouwBtn.frame=CGRectMake(self.bgImage.frame.origin.x,self.schouwBtnLbl.frame.origin.y-5, 237,37);
   
    // self.schouwBtn.backgroundColor=[UIColor redColor];

}

-(void)populateCalloutView:(ShapeMarkerData *)data
{
    
    self.adress.text = data.infowindow_title;

    self.gebied.text = data.infowindow_message;
    
    if ([data.status isEqualToString:@"red"])
    {
        self.bgImage.image = [UIImage imageNamed:@"POIBg-red.png"];
    
        self.schouwBtn.hidden = NO;
        
        self.schouwBtnLbl.hidden = NO;
        
    }
    else if ([data.status isEqualToString:@"green"])
    {
        self.bgImage.image = [UIImage imageNamed:@"POIBg-green.png"];
    
        self.schouwBtn.hidden = YES;
        
        self.schouwBtnLbl.hidden = YES;
        
    }
    else
    {
        self.bgImage.image = [UIImage imageNamed:@"POIBg-orange.png"];
    
        self.schouwBtn.hidden = NO;
        
        self.schouwBtnLbl.hidden = NO;
    }
}

- (IBAction)startSchouwAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"%@" ,self.calloutDataInfo.gebied);
   
    NSLog(@"%@" ,[btn.superview class]);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(startSchouwPressed:)])
    {
        
        }
}

@end