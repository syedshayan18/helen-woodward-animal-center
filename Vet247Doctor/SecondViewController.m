//
//  SecondViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "SecondViewController.h"
#import "PetProfileDataManager.h"
#import "PetProfileKeys.h"
#import "MyPetsViewController.h"
#import "AHContainerViewController.h"
@interface SecondViewController ()
@property BOOL radiobutton;
@property BOOL radiobutton1;
@end

@implementation SecondViewController
@synthesize secondunable;
- (void)viewDidLoad {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fieldsunable:)
                                                 name:@"fieldsunable"
                                               object:nil];
    
     [super viewDidLoad];
    
    UITapGestureRecognizer *tapGestureYes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vacineYes:)];
    
    [self.lbl_vaccineYes addGestureRecognizer:tapGestureYes];
    
    
    UITapGestureRecognizer *tapGestureNo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vacineNo:)];
    
    [self.lbl_VacineNo addGestureRecognizer:tapGestureNo];

    
    NSAttributedString *history = [[NSAttributedString alloc] initWithString:@"HISTORY" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_history.attributedPlaceholder = history;
    NSAttributedString *medicalA = [[NSAttributedString alloc] initWithString:@"MEDICAL ALLERGIES" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_medicalallergies.attributedPlaceholder = medicalA;
    NSAttributedString *medical = [[NSAttributedString alloc] initWithString:@"MEDICAL" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_Medical.attributedPlaceholder = medical;
    NSAttributedString *diet = [[NSAttributedString alloc] initWithString:@"DIET" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_diet.attributedPlaceholder = diet;
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
//    {
    
        [self populateDataToView];
        
  //  }

   
    // Do any additional setup after loading the view.
}
- (void)fieldsunable:(NSNotification *)note {
    
    _txt_history.enabled=YES;
    _txt_Medical.enabled=YES;
    _txt_medicalallergies.enabled=YES;
    _txt_diet.enabled=YES;
    _btn_vacineYes.userInteractionEnabled=YES;
    _btn_vacineNo.userInteractionEnabled=YES;
    
    _lbl_vaccineYes.userInteractionEnabled=YES;
    _lbl_VacineNo.userInteractionEnabled=YES;

   
    
}


-(void) viewDidAppear:(BOOL)animated{
    
    AHContainerViewController *container = (AHContainerViewController*) self.parentViewController;
    MyPetsViewController *mypet = (MyPetsViewController*) container.parentViewController;
   
    if (mypet.btn_edit.isSelected==YES){
        
        _txt_history.enabled=YES;
        _txt_Medical.enabled=YES;
        _txt_medicalallergies.enabled=YES;
        _txt_diet.enabled=YES;
        _btn_vacineYes.userInteractionEnabled=YES;
        _btn_vacineNo.userInteractionEnabled=YES;
        
        _lbl_vaccineYes.userInteractionEnabled=YES;
        _lbl_VacineNo.userInteractionEnabled=YES;
        
        
    }
    

     

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) checkHistoryInfoTextField
{
    //NSString *phoneRegEx = @"[0-9]{1,11}";
    
    //NSPredicate *ageTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    // Making view controller to handle error messages
    //    UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    //
    //
    //    UIAlertController * checkHistoryInfoFieldsErrorAlert =   [UIAlertController
    //                                                              alertControllerWithTitle:@"Error"
    //                                                              message:@""
    //                                                              preferredStyle:UIAlertControllerStyleAlert];
    //    UIAlertAction* ok = [UIAlertAction
    //                         actionWithTitle:@"OK"
    //                         style:UIAlertActionStyleDefault
    //                         handler:^(UIAlertAction * action)
    //                         {
    //                             [checkHistoryInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                         }];
    //    UIAlertAction* cancel = [UIAlertAction
    //                             actionWithTitle:@"Cancel"
    //                             style:UIAlertActionStyleDefault
    //                             handler:^(UIAlertAction * action)
    //                             {
    //                                 [checkHistoryInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                             }];
    //
    //    [checkHistoryInfoFieldsErrorAlert addAction:ok];
    //    [checkHistoryInfoFieldsErrorAlert addAction:cancel];
    //
    //    if ( self.historyTextView.tag == 1 || (self.historyTextView.text && self.historyTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Illness History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.allergiTextView.tag == 1 || (self.allergiTextView.text && self.allergiTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Medical Allergic History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.allergiTextView.tag == 1 || (self.medicationTextView.text && self.medicationTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Kindly enter Medication regarding History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.dietTextView.tag == 1 || (self.dietTextView.text && self.dietTextView.text.length < 1))
    //    {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Pet's Diet"];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //
    //    else if ([self.vaccinatedYesButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedNoButton.currentImage isEqual:self.unSelectedImage]) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Kindly select Vaccinated YES or NO"];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    
    PetProfileDataManager * addHistoryInfo = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    if(self.txt_history.tag !=1)
    {
        [addHistoryInfo insertPetProfileInfo: self.txt_history.text :AnimalIllnessHistoryKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalIllnessHistoryKey];
    }
    
    if(self.txt_medicalallergies.tag != 1)
    {
        [addHistoryInfo insertPetProfileInfo: self.txt_medicalallergies.text :MedicalAlergiesKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :MedicalAlergiesKey];
    }
    
    if(self.txt_Medical.tag !=1)
    {
        [addHistoryInfo insertPetProfileInfo: self.txt_Medical.text :AnimalMedicationKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalMedicationKey];
    }
    
    if(self.txt_diet.tag != 1)
    {
        [addHistoryInfo insertPetProfileInfo: self.txt_diet.text :AnimalDietKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalDietKey];
    }
    
    if(self.btn_vacineYes.isSelected==YES)
    {
        [addHistoryInfo insertPetProfileInfo: @"Yes" :AnimalVaccinatedKey];
    }
    else if(self.btn_vacineNo.isSelected==NO)
    {
        [addHistoryInfo insertPetProfileInfo: @"No" :AnimalVaccinatedKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalVaccinatedKey];
    }
    
    return  YES;
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if([[NSPetData objectForKey:AnimalIllnessHistoryKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalIllnessHistoryKey] == nil)
    {
        //        self.historyTextView.text = [NSPetData objectForKey:AnimalIllnessHistoryKey];
        //        self.historyTextView.textColor = [UIColor blackColor];
        //        self.historyTextView.tag = 0;
    }
    else
    {
        self.txt_history.text = [NSPetData objectForKey:AnimalIllnessHistoryKey];
        self.txt_history.textColor = [UIColor blackColor];
        self.txt_history.tag = 0;
    }
    
    if([[NSPetData objectForKey:MedicalAlergiesKey] isEqualToString:@""] || [NSPetData objectForKey:MedicalAlergiesKey] == nil)
    {}
    
    else
    {
        self.txt_medicalallergies.text = [NSPetData objectForKey:MedicalAlergiesKey];
        self.txt_medicalallergies.textColor = [UIColor blackColor];
        self.txt_medicalallergies.tag = 0;
    }
    
    if([[NSPetData objectForKey:AnimalMedicationKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalMedicationKey] == nil)
    {}
    else
    {
        self.txt_Medical.text = [NSPetData objectForKey:AnimalMedicationKey];
        self.txt_Medical.textColor = [UIColor blackColor];
        self.txt_Medical.tag = 0;
    }
    
    if([[NSPetData objectForKey:AnimalDietKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalMedicationKey] == nil)
    {}
    else
    {
        self.txt_diet.text = [NSPetData objectForKey:AnimalDietKey];
        self.txt_diet.textColor = [UIColor blackColor];
        self.txt_diet.tag = 0;
    }
    
    if ([[NSPetData objectForKey:AnimalVaccinatedKey] isEqualToString:@"Yes"]) {
        
        [self.btn_vacineYes setSelected:YES];
    }
    
    else if([[NSPetData objectForKey:AnimalVaccinatedKey] isEqualToString:@"No"])
    {
        [self.btn_vacineNo setSelected:YES];
    }
    
}


-(void) vacineYes:(UITapGestureRecognizer *)sender{
    if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==NO)
    {
        [self.btn_vacineYes setSelected:YES];
    }
    
    else if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==YES)
    {
        
        
        
        [self.btn_vacineYes setSelected:YES];
        
        [self.btn_vacineNo setSelected:NO];
    }
    
    

    
}


-(void) vacineNo:(UITapGestureRecognizer *)sender{
    
    if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==NO)
    {
        
        
        
        [self.btn_vacineNo setSelected:YES];
    }
    
    else if (self.btn_vacineNo.isSelected==NO && self.btn_vacineYes.isSelected==YES)
    {
        
        
        [self.btn_vacineNo setSelected:YES];
        
        [self.btn_vacineYes setSelected:NO];
        
        
      
    }

    
}

- (IBAction)btn_vacineyes:( UIButton *)sender {
    
    if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==NO)
    {
        [self.btn_vacineYes setSelected:YES];
    }
    
    else if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==YES)
    {
        
        
       
        [self.btn_vacineYes setSelected:YES];
        
        [self.btn_vacineNo setSelected:NO];
    }
    
    

    
    
}

- (IBAction)btn_vacineno:( UIButton *)sender {
    if (self.btn_vacineYes.isSelected==NO && self.btn_vacineNo.isSelected==NO)
    {
        
       
    
        [self.btn_vacineNo setSelected:YES];
    }
    
    else if (self.btn_vacineNo.isSelected==NO && self.btn_vacineYes.isSelected==YES)
    {
       
        
        [self.btn_vacineNo setSelected:YES];
        
        [self.btn_vacineYes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }

    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
