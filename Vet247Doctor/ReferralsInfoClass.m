//
//  ReferralsInfoClass.m
//  Vet247Doctor
//
//  Created by APPLE on 8/22/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ReferralsInfoClass.h"

@implementation ReferralsInfoClass

static ReferralsInfoClass * sharedInstance = nil;

+(ReferralsInfoClass*) getPetReferralsDataManagerSharedInstance
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[ReferralsInfoClass alloc] init];
        sharedInstance.dataContainer = [[NSMutableDictionary alloc] init];
    });
    
    return sharedInstance;
}

//-(void)insertReferralsInfo:(NSString*)value :(NSString*)Nskey
//{
//    
//    [self.dataContainer setObject:value forKey:Nskey];
//    
//    NSLog(@"\n %@ = %@ \n", Nskey, [self.dataContainer objectForKey:Nskey]);
//    
//}

-(NSMutableDictionary *) getPetReferralsInfo
{
    if(sharedInstance != nil)
    {
        return sharedInstance.dataContainer;
    }
    else
    {
        return nil;
    }
}

-(void) setPetData:(NSMutableDictionary*) referralsData
{
    referralsData = [referralsData mutableCopy];
    
    if(sharedInstance != nil)
    {
        sharedInstance.dataContainer = referralsData;
    }
}

+(void) removePetReferralsInfo
{
    
    [sharedInstance.dataContainer removeAllObjects];
    
    NSLog(@"\n Referals data is removed. ahaa...");
}

@end
