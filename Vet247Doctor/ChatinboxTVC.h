//
//  ChatinboxTVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/5/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatinboxTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_chatuserimage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_username;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastmessage;

@end
