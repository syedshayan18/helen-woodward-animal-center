//
//  MyLostnFoundPetVeiwController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "MyLostnFoundPetVeiwController.h"
#import "MyLostnFoundPetCVC.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "Lostnffoundmodel.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "PetProfilelostnfoundViewController.h"
@interface MyLostnFoundPetVeiwController ()

@end

@implementation MyLostnFoundPetVeiwController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self networkrequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _petData.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float width = (collectionView.frame.size.width/2-2);
    
    float height =  [UIScreen mainScreen].bounds.size.height * 0.4929577465;
    
    return CGSizeMake(width, height);
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    MyLostnFoundPetCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.layer.cornerRadius=5;
    cell.clipsToBounds=YES;
    if ([[[_petData objectAtIndex:indexPath.row]animaltag] isEqualToString:@"LOST"]){
        
        cell.lbl_petstatus.textColor = [UIColor redColor];
        cell.lbl_petname.text =[[_petData objectAtIndex:indexPath.row]petname];
        cell.lbl_petsex.text = [[_petData objectAtIndex:indexPath.row]petlostSex];
        cell.lbl_petbreed.text =[[_petData objectAtIndex:indexPath.row]petlostbreed];
        NSString *animalage=[NSString stringWithFormat:@"%@ Year Old",[[_petData objectAtIndex:indexPath.row]petage]];
        cell.lbl_age.text = animalage;
        cell.lbl_loston.text = [NSString stringWithFormat:@"Lost on:%@",[[_petData objectAtIndex:indexPath.row]expirydate]];;
        cell.lbl_petstatus.text=[[_petData objectAtIndex:indexPath.row]animaltag];
        
        NSURL *url = [[NSURL alloc]initWithString:[_petData[indexPath.row] petimagelost]];
        [cell.img_lostnfound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"post_bg_green"]];
        
        
        
    }
    else {
        
        cell.lbl_petstatus.text = @"FOUND";
        cell.lbl_petname.text=nil;
        cell.lbl_age.text=nil;
        cell.lbl_loston.text=nil;
        cell.lbl_petsex.text=nil;
        cell.lbl_petbreed.text=nil;
        NSURL *url = [[NSURL alloc]initWithString:[_petData[indexPath.row]petimagefound]];
        [cell.img_lostnfound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"post_bg_green"]];
        cell.lbl_petstatus.textColor = [UIColor colorWithRed:60.0/255.0 green:123.0/255.0 blue:58.0/255.0 alpha:1.0];
      
        
    }

    [cell.btn_deletepet addTarget:self action:@selector(btn_deletepet:) forControlEvents:UIControlEventTouchUpInside];
    cell.btn_deletepet.tag =indexPath.row;
    
    
    return cell;
}

-(IBAction)btn_deletepet:(UIButton *)sender{
    NSLog(@"%ld",sender.tag);
    _buttontag=sender.tag;
    [self deletepetrequest];
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    
    _index = indexPath.row;
    [self performSegueWithIdentifier:@"lostfounddetails" sender:nil];
}


-(void)deletepetrequest{
    
    NSString *reportID = [[_petData objectAtIndex:_buttontag] reportID];
    NSString *geturl = [NSString stringWithFormat:@"%@delete_report.php?report_id=%@",BaseURL,reportID];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetworkingClass deletemylostnfound:geturl completion:^(id finished, NSError *error) {
        
        if (error==nil){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            NSLog(@"%@",finished);
            NSString *item = [_petData objectAtIndex:_buttontag];
            [self.petData removeObject:item];
            [_CollectionView reloadData];
            
        }
        else {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                
                NSLog(@"ok");
                
                
                
                
                
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        
        
    }];
    
    
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
}

-(void)networkrequest {
    
    
    
    
    NSString *posturl =[NSString stringWithFormat:@"%@get_lost_found.php",BaseURL];
    NSString * userid= [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    body[@"user_id"] = userid;
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  
    [NetworkingClass getuserlostnfound:posturl param:body completion:^(id finished, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        NSLog(@"%@",error);
        if (error==nil){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
            NSLog(@"%@",finished);

            _petData = [[NSMutableArray alloc]init];
            
            // NSString *message = ;
            
            
            if([finished isKindOfClass:[NSDictionary class]]){
                if ([finished valueForKey:@"message"]){
                    
                    [_CollectionView reloadData];
                    
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Pet" message:@"No lost and found not found!" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSLog(@"ok");
                        
                        
                        
                        
                        
                    }];
                    
                    
                    [alert addAction:okAction];
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    return ;
                }
            }
            else {
                
                for (int i=0;i<[finished count]; i++){
                    
                    
                    Lostnffoundmodel *model = [[Lostnffoundmodel alloc]initWithDictionary:finished[i]];
                    [_petData addObject:model];
                    
                }
                
                NSLog(@"%@",_petData);
                [_CollectionView reloadData];
                
                
                
            }
            
            
        }
        
        
        else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                
                NSLog(@"ok");
                
                
                
                
                
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            
            
        }

    }];
    
    
  
  
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PetProfilelostnfoundViewController *petprofile = (PetProfilelostnfoundViewController *)[segue destinationViewController];
    petprofile.petmodel=[_petData objectAtIndex:_index];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
