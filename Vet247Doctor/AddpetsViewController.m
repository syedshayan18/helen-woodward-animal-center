//
//  AddpetsViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/20/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "AddpetsViewController.h"
#import "AHContainerViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "MyPetsViewController.h"
@interface AddpetsViewController ()

@end

@implementation AddpetsViewController
AHContainerViewController *containerview;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _btn_addpet.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_addpet.clipsToBounds = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}
-(IBAction)addpets:(UIButton *)sender{
    
    
    
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        
        
        if ([containerview.currentViewController isKindOfClass:SecondViewController.self])
        {
            
            SecondViewController *secondVC = (SecondViewController *) containerview.currentViewController;
            secondVC.txt_history.enabled=YES;
            secondVC.txt_Medical.enabled=YES;
            secondVC.txt_medicalallergies.enabled=YES;
            secondVC.txt_diet.enabled=YES;
            secondVC.btn_vacineYes.userInteractionEnabled=YES;
            secondVC.btn_vacineNo.userInteractionEnabled=YES;
            
            
        }
        if ([containerview.currentViewController isKindOfClass:FirstViewController.self]){
            FirstViewController *firstVC = (FirstViewController *) containerview.currentViewController;
            firstVC.txt_age.enabled =YES;
            firstVC.txt_species.enabled=YES;
            firstVC.txt_breed.enabled=YES;
            firstVC.txt_color.enabled=YES;
            firstVC.txt_weight.enabled=YES;
            firstVC.txt_animal.enabled = YES;
            firstVC.btn_male.userInteractionEnabled=YES;
            firstVC.btn_female.userInteractionEnabled=YES;
            firstVC.btn_spayrd.userInteractionEnabled=YES;
            firstVC.btn_neutered.userInteractionEnabled=YES;
        }
        
        if ([containerview.currentViewController isKindOfClass:ThirdViewController.self]){
            ThirdViewController *thirdVC = (ThirdViewController *) containerview.currentViewController;
            thirdVC.btn_insurranceyes.userInteractionEnabled=YES;
            thirdVC.btn_insuuranceno.userInteractionEnabled=YES;
            thirdVC.btn_trackerYes.userInteractionEnabled=YES;
            thirdVC.btn_trackerNo.userInteractionEnabled=YES;
            thirdVC.btn_vertYes.userInteractionEnabled=YES;
            thirdVC.vertNo.userInteractionEnabled=YES;
            thirdVC.txt_insurance.enabled=YES;
            thirdVC.txt_policyno.enabled=YES;
            thirdVC.txt_tracker.enabled=YES;
            thirdVC.txt_pin.enabled=YES;
            thirdVC.txt_name.enabled=YES;
            thirdVC.txt_phoneno.enabled=YES;
        }
        
        
        
        
    });
    
    
    MyPetsViewController *mypets = [[MyPetsViewController alloc]init];
   
    mypets.check=YES;
    

    [self performSegueWithIdentifier:@"addpets" sender:nil];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
