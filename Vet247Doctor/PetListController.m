//
//  setPetController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/31/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetListController.h"

#import "Masonry.h"

#import "ControlsFactory.h"

#import "ChatController.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "PetProfileController.h"

#import "PetProfileDataManager.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import "SDWebImage/UIImageView+WebCache.h"

static float cellHeight=71.5f;

@interface PetListController ()

@property int numberOfRows;

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *messagesLbl;

@property (strong,nonatomic) UIButton *backBtn;

@property (strong, nonatomic) UIButton * addBtn,*addButtonBG;

@property (strong, nonatomic) UIView * searchView;

@property (strong, nonatomic) UITextField * txtFld;

@property (strong, nonatomic) UIImageView * txtFldImgView;

@property (strong, nonatomic) UIImageView * searchIcon;

@property (strong,nonatomic) UITableView *tableView;

@property (assign) BOOL searching;

@property (strong,nonatomic) NSMutableArray *petData;

@property (strong,nonatomic) NSMutableArray *tableData, *dataToBeProcessed,*petNames,*filteredPetNames,*filteredtableData;

@end

@implementation PetListController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.filteredPetNames=[[NSMutableArray alloc] init];
    
    self.filteredtableData=[[NSMutableArray alloc] init];
    
    self.tableData = [[NSMutableArray alloc] init];
    
    [self addComponents];
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.numberOfRows = 1;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self.view endEditing:YES];
    
    self.txtFld.text=@"";
    
    self.searching=FALSE;
    
    [self.dataToBeProcessed removeAllObjects];
    
    self.petNames=[[NSMutableArray alloc] init];   // The names of Doctor with whome conversation has been made. We have seperated doctor Names from
    
    // data we fetched from servers
    
    [self.filteredPetNames removeAllObjects];
    
    [self.filteredtableData removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self fetchPets];
    
    NSLog(@"%@", self.petData);
    
    [self.tableView reloadData]; // to reload selected cell
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchPets
{
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [network getPets:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];

}

-(void) addComponents {
    
    [self setUpHeaderView];
    
}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        // make.top.equalTo(self.view.mas_top);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    self.backBtn=[ControlsFactory getButton];
    
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    
    [self.backBtn addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(54);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
    }];
    
    UIEdgeInsets paddingForMessagesLabel = UIEdgeInsetsMake(0, 0, 15, 0);
    
    self.messagesLbl =[ControlsFactory getLabel];
    
    self.messagesLbl.text=@"Pets";
    
    [self.messagesLbl setTextAlignment:NSTextAlignmentCenter];
    
    self.messagesLbl.font= [UIFont systemFontOfSize:20.0];
    
    self.messagesLbl.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.messagesLbl];
    
    [self.messagesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForMessagesLabel.bottom);
        
        
    }];
    
    self.addBtn =[ControlsFactory getButton];
    
    [self.addBtn setBackgroundImage:[UIImage imageNamed:@"plusBtn.png"] forState:UIControlStateNormal];
    
    [self.addBtn addTarget:self action:@selector(addButtoonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 10);
    
    [self.headerView addSubview:self.addBtn];
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(23);
        
        make.width.mas_equalTo(22);
        
    }];
    
    self.addButtonBG =[ControlsFactory getButton];
    
    [self.addButtonBG setBackgroundColor:[UIColor clearColor]];
    
    [self.addButtonBG addTarget:self action:@selector(addButtoonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 3);
    
    [self.headerView addSubview:self.addButtonBG];
    
    [self.addButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(50);
        
        make.width.mas_equalTo(80);
        
    }];
    
    
    self.searchView=[ControlsFactory getView];
    
    self.searchView.backgroundColor=[UIColor whiteColor];
    
    UIEdgeInsets paddingForSearchVIew= UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.searchView];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.height.mas_equalTo(70);
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForSearchVIew.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForSearchVIew.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForSearchVIew.top);
        
        //make.bottom.equalTo(self.view.mas_bottom).with.offset(-paddingForSearchVIew.bottom);
    }];
    
    self.txtFldImgView=[ControlsFactory getImageView];
    
    self.txtFldImgView.image=[UIImage imageNamed:@"searchRectangle.png"];
    
    UIEdgeInsets paddingForTxtFldImg = UIEdgeInsetsMake(15, 15, 15, 15);
    
    [self.searchView addSubview:self.txtFldImgView];
    
    [self.txtFldImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.searchView.mas_left).with.offset(paddingForTxtFldImg.left);
        
        make.right.equalTo(self.searchView.mas_right).with.offset(-paddingForTxtFldImg.right);
        
        // make.top.equalTo(self.searchView.mas_bottom);
        
        make.top.equalTo(self.searchView.mas_top).with.offset(paddingForTxtFldImg.top);
        
        make.bottom.equalTo(self.searchView.mas_bottom).with.offset(-paddingForTxtFldImg.bottom);
        
    }];
    
    self.searchIcon=[ControlsFactory getImageView];
    
    self.searchIcon.image=[UIImage imageNamed:@"searchIcon.png"];
    
    UIEdgeInsets paddingForSearchIcon= UIEdgeInsetsMake(10, 10, 10, 15);
    
    [self.txtFldImgView addSubview:self.searchIcon];
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForSearchIcon.right);
        
        make.centerY.mas_equalTo(self.txtFldImgView.mas_centerY);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(18);
        
    }];
    
    
    self.txtFld = [ControlsFactory getTextField];
    
    self.txtFld.backgroundColor=[UIColor clearColor];
    
    self.txtFld.placeholder=@"Search Pets";
    
    [self.txtFld setValue:[UIColor colorWithRed:255.0/255 green:157.0/255 blue:59.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.txtFld.delegate=self;
    
    UIEdgeInsets paddingForTxtFld= UIEdgeInsetsMake(0, 25, 0, 0);
    
    [self.txtFldImgView addSubview:self.txtFld];
    
    self.txtFldImgView.userInteractionEnabled = YES;
    
    [self.txtFld mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.txtFldImgView.mas_left).with.offset(paddingForTxtFld.left);
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForTxtFld.right);
        
        make.top.equalTo(self.txtFldImgView.mas_top).with.offset(paddingForTxtFld.top);
        
        make.bottom.equalTo(self.txtFldImgView.mas_bottom).with.offset(-paddingForTxtFld.bottom);
        
    }];
}

-(void) setUpTableView {
    
    self.tableView =[ControlsFactory getTableView];
    
    self.tableView.delegate=self;
    
    self.tableView.dataSource=self;
    
    self.tableView.backgroundColor=[UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    
    UIEdgeInsets paddingforTableView =UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingforTableView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingforTableView.right);
        
        make.top.mas_equalTo(self.view.mas_top).with.offset(145);
        
        make.height.mas_equalTo(self.petData.count*cellHeight);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-paddingforTableView.bottom);
        
    }];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void) backButtonClicked :(UIButton *) button
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addButtoonClicked :(UIButton *) btn
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"UpdatePET"];
    
    PetProfileController * populatePetProfile = [[PetProfileController alloc] init];
    
    [self.navigationController pushViewController:populatePetProfile animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71.1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //if (self.dataFromServer) {
    
    if (!self.searching) {
        
        return self.tableData.count;
    }
    else
    {
        return  self.filteredtableData.count;
    }
    
    //}
    
    //    else
    //    {
    //        return 1;
    //    }
    //    if (!self.petData.count) {
    //
    //        return 1;
    //
    //    }
    //    else
    //    {
    //
    //        return self.petData.count;
    //
    //    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    //  Cleanning Old Data!
    UILabel *temp_name_lbl = (UILabel*) [cell viewWithTag:1001];
    UIImageView *temp_imageView = (UIImageView*) [cell viewWithTag:2001];
    
    [temp_imageView removeFromSuperview];
    [temp_name_lbl removeFromSuperview];
    
    if (!self.petData.count) {
    }
    else
    {
        NSDictionary *cellData;
        
        if(!self.searching)
        {
            cellData = [self.tableData objectAtIndex: indexPath.row];
        }
        else
        {
            cellData = [self.filteredtableData objectAtIndex: indexPath.row];
        }
        
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 55, 55)];
        
        imgView.tag = 2001;
        
        imgView.backgroundColor=[UIColor clearColor];
        
        [imgView.layer setCornerRadius:27.5f];
        
        [imgView.layer setMasksToBounds:YES];
        
        if([[cellData objectForKey:@"image"] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/pet/"])
        {
            
            [imgView setImage:[UIImage imageNamed:@"dog1.jpg"]];
            
        }
        else
        {
            
            [imgView sd_setImageWithURL:[NSURL URLWithString:[cellData objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            
        }
        
        [cell.contentView addSubview:imgView];
        
        UILabel *nameLable = [[UILabel alloc] initWithFrame:CGRectMake(85, 15, self.view.frame.size.width-100 , 40)];
        
        nameLable.tag = 1001;
        
        nameLable.text=[cellData objectForKey:@"animalName"];
        
        nameLable.textColor = [UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0];
        
        [cell.contentView addSubview:nameLable];
        
    }
    
    return cell;
    
} // -- end cellForRowAtIndex()


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *selectedPetData;
    
    if(!self.searching)
    {
        selectedPetData = [self.tableData objectAtIndex: indexPath.row];
        //cellData = [self.tableData objectAtIndex: indexPath.row];
    }
    else
    {
        selectedPetData = [self.filteredtableData objectAtIndex: indexPath.row];
        //cellData = [self.filteredtableData objectAtIndex: indexPath.row];
    }
    
    
    
    PetProfileDataManager * savePetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    [savePetsData setPetData:selectedPetData];
    
    if ([savePetsData getPetProfileInfo] == nil) {
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"UpdatePET"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[selectedPetData objectForKey:@"id"] forKey:@"UpdatePETId"];
    
    //NSLog(@"This Pet id is going to be inserted in to Pet Profile data manager: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePETId"]);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing:YES];
    PetProfileController * populatePetProfile = [[PetProfileController alloc] init];
    
    [self.navigationController pushViewController:populatePetProfile animated:YES];
    
    
    
}  // -- end didSelect()

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    string = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (string.length==0) {
        
        self.searching = FALSE;
        
        [self.filteredPetNames removeAllObjects];
        
        [self.filteredtableData removeAllObjects];
        
    }
    else
    {
        self.searching = TRUE;
        
        //use contains instead of beginswith if want to search anywhere in the string
        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",string];
        
        self.filteredPetNames  = [NSMutableArray arrayWithArray:[self.petNames filteredArrayUsingPredicate:predicate]];
        //        NSLog(@"%@",self.petData);
        //
        //        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"[SELF objectForKey:%@] contains[cd] %@",@"animalName",self.txtFld.text];
        //
        //        self.filteredtableData  = [NSMutableArray arrayWithArray:[self.tableData filteredArrayUsingPredicate:predicate]];
        //
        //        NSLog(@"%@", self.filteredtableData);
        //
        
    }
    
    if (self.searching) {
        
//        if (self.filteredPetNames.count) {
        
            [[self filteredtableData] removeAllObjects];  // The Following function filterTableData is called when ever a new character is added
            
            // into search field now whats happeninf was that new filterData for TableView was repeating doctorNames so first we flush all previous fields
            
            //and then we added new ones
            
            [self filterTableData];
//        }
    }
    
    [self.tableView reloadData];
    
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void) filterTableData
{
    if(self.filteredPetNames.count == 0)
    {
        return;
    }
    
    int j=0;
    
    for (int i=0; i<self.tableData.count; i++)
    {
        NSString *doctorName=[[self.tableData objectAtIndex:i] objectForKey:@"animalName"];
        
        NSString *doctorNamedToBeComparedWith=[self.filteredPetNames objectAtIndex:j];
        
        if ([doctorName isEqualToString:doctorNamedToBeComparedWith])
        {
            
            [self.filteredtableData addObject:[self.tableData objectAtIndex:i]];
            
            j++;
            
            if (j==self.filteredPetNames.count)
            {
                break;
            }
        }
    }
    
}

-(void)response:(id)data {
    
    //dispatch_async(dispatch_get_main_queue(), ^{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    
    if([data isKindOfClass:[NSArray class]])
    {
        self.petData = data;
        
        [self setUpTableView];
        
        [self.tableData removeAllObjects];
        
        [self findRows];
        
        [self.tableView reloadData];
        
    }
    else if ([data objectForKey:@"msg"])
    {
        [self setUpTableView];
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [errorAlert show];
        
    }
    //});
    
}

- (void) findRows{
    
    for (int i=0; i< self.petData.count; i++) {
        
        [self.tableData addObject:[self.petData objectAtIndex:i]];
        
        NSString *pet_Name= [[self.petData objectAtIndex:i] objectForKey:@"animalName"];
        
        [self.petNames addObject:pet_Name];
        
        self.numberOfRows++;
        
    }
    
    //    [self.tableData addObject:[self.petData objectAtIndex:self.petData.count-1]];
    //
    //    [self.petNames addObject:[[self.petData objectAtIndex:self.petData.count-1] objectForKey:@"animalName"]];
    //
}


//- (void)layoutSubviews {
//    [super layoutSubviews];
//    self.imageView.frame = CGRectMake(0,0,32,32);
//}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self setUpTableView];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end



