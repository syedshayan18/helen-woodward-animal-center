//
//  ChatController.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/28/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

//#import <UIKit/UIKit.h>

#import "Masonry.h"

#import "MessageController.h"

#import "ChatMessageCell.h"

#import "PropertyTrackerModel.h"

@class ChatController;

//@class DemoMessagesViewController;


@interface ChatController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (strong,nonatomic) NSMutableArray *chatData;

@property (strong,nonatomic) NSString *firstChat;

@property (strong,nonatomic) NSDictionary *petData;

@property PropertyTrackerModel *pet;

@end

