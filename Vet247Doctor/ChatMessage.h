//
//  ChatMessage.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/30/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatMessage : NSObject

@property (strong,nonatomic) NSString *doctorName;

@property (strong,nonatomic) NSString *doctorImage;

@property (strong,nonatomic) NSString *message;

@property (strong,nonatomic) NSString *messageBy;

@property (strong,nonatomic) NSString *messageDate;

@property (strong,nonatomic) NSString *messageId;

@property (strong,nonatomic) NSString *messageImageLink;

@property (strong,nonatomic) NSString *petId;

@property (strong,nonatomic) NSString *threadId;

@property (strong,nonatomic) NSString *threadStatus;

@end
