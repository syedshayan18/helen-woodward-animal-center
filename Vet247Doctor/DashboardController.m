//
//  DashboardController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/7/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>

#import "MBProgressHUD.h"

#import "DashboardController.h"

//#import "AppDelegate.h"

#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "MessageController.h"

#import "ZZMainViewController.h"

#import "ProfileController.h"

#import "LoginController.h"

#import "PaymentController.h"

#import "NetworkingClass.h"

#import "PetListController.h"

#import "PetReferralsController.h"

#import "OpenTokController.h"

#import "PackagesVC.h"

@interface DashboardController ()

{
    AVAudioPlayer *_audioPlayer;
    
}

@property (strong,nonatomic) UIImageView *callViewBG;

@property (strong,nonatomic) UIImageView *avatarImage1;

@property (strong,nonatomic) UIButton *declineButton;

@property (strong,nonatomic) NSString *isRinging,*showSocketError;

@property (strong,nonatomic) UIImageView *backGroundImage;

@property (strong,nonatomic) UIScrollView *scroll;

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *headerViewlabel,*connecting;

@property (strong,nonatomic) UIImageView *logoImageView;

@property (strong,nonatomic) UIImageView *horrizentalLines;

@property (strong,nonatomic) UIImageView *verticleLine;

@property (strong,nonatomic) UIButton *petsButton;

@property (strong,nonatomic) UIButton *historyButton;

@property (strong,nonatomic) UIButton *messageButton, *callButton, *insuranceButton, *referralsButton;

@property (strong,nonatomic) UIImageView *bottomLine;

@property (strong,nonatomic) UIButton *profileButton;

@property (strong,nonatomic) UIButton *logOutButton;

@property (strong,nonatomic) UIView *connectionView;

@property (strong,nonatomic) NSString *showBusyPopUp;

@property (strong, nonatomic) NSString * calledByKeyValue;

@end


@implementation DashboardController

SocketManager *_manager;

static bool checkDoctorEveryThirtySeconds=FALSE;

-(void)viewWillAppear:(BOOL)animated {

    
    self.showBusyPopUp=@"NO";
}

- (void)viewDidLoad {
 
    [super viewDidLoad];
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leaveCallScreen) name:LeaveCallScreen object:nil];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    [self addComponents];
}


- (void) addComponents{
    
    [self setUpBGImage];
    
    [self setUpHeaderView];
    
    [self setUpProfileButton];
    
    [self setUpLogOutButton];
    
    [self setUpLogoImageView];
    
    [self setUpScroll];
    
    [self setUpHorrizentalLines];
    
    [self setUpVerticleLine];
    
    [self setUpPetsButton];
    
    [self setUpMessageButton];
    
    [self setUpCallButton];
    
    [self setUpReferralsButton];
}

-(void) paymentAuthentication {
    
    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    if(![self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }

    [sendPaymentRecivedNetworkObject paymentAuthentication:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
}

-(void)paymentResponse:(id) data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"your payment authentication not valid"] || [[data objectForKey:@"status"] isEqualToString:@"failure"]) {
        
        if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
            return;
        }
        
        
        PackagesVC *packageController = [[PackagesVC alloc] init];
        
        [self.navigationController pushViewController:packageController animated:YES];
        
//        UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
//                                                             alertControllerWithTitle:@"Message"
//                                                             message:@"Sorry, your Package is not authenticated, Press 'OK' to activate, otherwise Press 'Cancel'"
//                                                             preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 PackagesVC *packageController = [[PackagesVC alloc] init];
//                                 
//                                 [self.navigationController pushViewController:packageController animated:YES];
//                             }];
//        
//        UIAlertAction * cancel = [UIAlertAction
//                                  actionWithTitle:@"Cancel"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//                                      
//                                  }];
//        
//        [paymentSuccessfullyRecieved addAction:ok];
//        
//        [paymentSuccessfullyRecieved addAction:cancel];
//        
//        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }

    else if ([[data objectForKey:@"status"] isEqualToString:@"success"])
    {
        if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
            return;
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
        
        [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_Cleared forKey:AnnualSubscriptionPaid_KEY];
        
        if ([self.calledByKeyValue isEqualToString:@"Message"]) {

            MessageController *messageController=[[MessageController alloc] init];
            
            [self.navigationController pushViewController:messageController animated:YES];
        }
    
        if ([self.calledByKeyValue isEqualToString:@"Call"]) {
            
            [self callPortionCodeBlockWhenCallButtonClicked];
        }
    }
}

-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text = @"Home";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
    
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(100);
        
    }];
}

- (void) setUpProfileButton {
    
    self.profileButton=[ControlsFactory getButton];
       
    UIEdgeInsets padding=UIEdgeInsetsMake(0, 10, 5, 0);
    
    [self.profileButton setTitle:@"Profile" forState:UIControlStateNormal];
    
    [self.profileButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.profileButton addTarget:self action:@selector(profileButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.profileButton.titleLabel.font=[UIFont systemFontOfSize:20.0];
   
    [self.headerView addSubview:self.profileButton];
    
    [self.profileButton mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(40);

        make.width.mas_equalTo(60);
        
    }];
}

- (void) setUpLogOutButton {
    
    self.logOutButton=[ControlsFactory getButton];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(0, 10, 5, 10);
    
    [self.logOutButton setTitle:@"Logout" forState:UIControlStateNormal];
    
    [self.logOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.logOutButton addTarget:self action:@selector(logoutButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.logOutButton.titleLabel.font=[UIFont systemFontOfSize:20.0];
    
    [self.headerView addSubview:self.logOutButton];
    
    [self.logOutButton mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.right.mas_equalTo(self.headerView.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(70);
        
    }];

    
    
    
}
- (void) setUpLogoImageView{
    
    self.logoImageView =[ControlsFactory getImageView];
    
    UIEdgeInsets paddingForpetsImageView = UIEdgeInsetsMake(25, 50, 0, 0);
    
    self.logoImageView.image=[UIImage imageNamed:@"logo.png"];
    
    [self.view addSubview:self.logoImageView];
    
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForpetsImageView.top);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        
        if(isiPhone4)
        {
            make.height.mas_equalTo(107);
            
            make.width.mas_equalTo(98);
        }
        else if(isiPhone5)
        {
            make.height.mas_equalTo(147);
            
            make.width.mas_equalTo(128);
        }
        else if (isiPhone6 || isiPhone6Plus) {
            
            make.height.mas_equalTo(197);
            
            make.width.mas_equalTo(176);
        }
        else
        {
            make.height.mas_equalTo(197);
            
            make.width.mas_equalTo(176);
        }
        
    }];
}


- (void) setUpScroll{
    
    UIEdgeInsets scrollViewPad=UIEdgeInsetsMake(35, 0, 0, 0);
    
//    if (isiPhone6 || isiPhone6Plus) {
//        
//        scrollViewPad=UIEdgeInsetsMake(35, 0, 0, 0);
//        
//    }
    
    self.scroll=[ControlsFactory getScrollView];
    
    self.scroll.bounces=NO;
    
    self.scroll.backgroundColor=[UIColor clearColor];
    
    [self.view addSubview:self.scroll];
    
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.logoImageView.mas_bottom).with.offset(scrollViewPad.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(scrollViewPad.left);
  
        make.height.mas_equalTo(222);
        
//        make.bottom.equalTo(self.scroll.mas_top).with.offset(scrollViewPad.bottom + 224);

//        make.bottom.equalTo(self.view.mas_bottom).with.offset(-scrollViewPad.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-scrollViewPad.right);
        
    }];
    
    self.scroll.scrollEnabled = NO;
}

- (void)setUpHorrizentalLines{
    
    self.horrizentalLines =[ControlsFactory getImageView];
    
    self.horrizentalLines.image=[UIImage imageNamed:@"horrizentalLines.png"];
    
    UIEdgeInsets paddingForLines = UIEdgeInsetsMake(2, 0, 112, 0);
    
    [self.scroll addSubview:self.horrizentalLines];
    
    [self.horrizentalLines mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.scroll.mas_top).with.offset(paddingForLines.top);
        
        make.left.equalTo(self.scroll.mas_left).with.offset(paddingForLines.left);
        
        make.bottom.equalTo(self.scroll.mas_bottom).with.offset(-paddingForLines.bottom);

        make.width.mas_equalTo(self.view.frame.size.width);
        
    }];

}

-(void) setUpVerticleLine{
    
    self.verticleLine=[ControlsFactory getImageView];
    
    self.verticleLine.image=[UIImage imageNamed:@"verticleLine.png"];
    
    UIEdgeInsets paddingForLine = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.scroll addSubview:self.verticleLine];
    
    [self.verticleLine mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.horrizentalLines.mas_top).with.offset(paddingForLine.top);
        
        make.centerX.mas_equalTo(self.scroll.mas_centerX);
        
        make.bottom.equalTo(self.scroll.mas_bottom).with.offset(-paddingForLine.bottom);
       
        make.width.mas_equalTo(2);
        
    }];

}

- (void) setUpPetsButton{
    
    self.petsButton=[ControlsFactory getButton];
    
    [self.petsButton setImage:[UIImage imageNamed:@"pets.png"] forState:UIControlStateNormal];
    
    [self.petsButton addTarget:self action:@selector(petsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    
    UIEdgeInsets padding = UIEdgeInsetsMake(12, 0, 12, 0);
    
   [self.scroll addSubview:self.petsButton];
    
    [self.petsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.horrizentalLines.mas_top).with.offset(padding.top);
        
        //NSLog(@"scroll view frame: %@", NSStringFromCGRect(self.scroll.frame));
        
        make.centerX.mas_equalTo(@(-self.view.frame.size.width/4));
        
        make.height.mas_equalTo(88);
        
        make.width.mas_equalTo(71);
        
    }];
    
}

//- (void) setUpHistoryButton{
//    
//    self.historyButton=[ControlsFactory getButton];
//    
//    [self.historyButton setImage:[UIImage imageNamed:@"history.png"] forState:UIControlStateNormal];
//    
//    UIEdgeInsets padding = UIEdgeInsetsMake(12, 0, 12, 0);
//    
//    [self.scroll addSubview:self.historyButton];
//    
//    [self.historyButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.equalTo(self.horrizentalLines.mas_top).with.offset(padding.top);
//        
//        //NSLog(@"scroll view frame: %@", NSStringFromCGRect(self.scroll.frame));
//        
//        make.centerX.mas_equalTo(@(self.view.frame.size.width/4));
//        
//        make.height.mas_equalTo(88);
//        
//        make.width.mas_equalTo(66);
//        
//    }];
//    
//}

- (void) setUpMessageButton {
    
    self.messageButton=[ControlsFactory getButton];
    
    [self.messageButton setImage:[UIImage imageNamed:@"message.png"] forState:UIControlStateNormal];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(12, 0, 12, 0);
    
    [self.messageButton addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.scroll addSubview:self.messageButton];
    
    [self.messageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.horrizentalLines.mas_top).with.offset(padding.top);
        
        //NSLog(@"scroll view frame: %@", NSStringFromCGRect(self.scroll.frame));
        
        make.centerX.mas_equalTo(@(self.view.frame.size.width/4));
        
        make.height.mas_equalTo(88);
        
        make.width.mas_equalTo(115);
        
    }];

}

- (void) setUpCallButton{
    
    self.callButton=[ControlsFactory getButton];
    
    [self.callButton setImage:[UIImage imageNamed:@"call.png"] forState:UIControlStateNormal];
    
    [self.callButton addTarget:self action:@selector(callButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(24, 0, 12, 0);
    
    [self.scroll addSubview:self.callButton];
    
    [self.callButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.messageButton.mas_bottom).with.offset(padding.top);
        
        make.centerX.mas_equalTo(@(self.view.frame.size.width/4));
        
        make.height.mas_equalTo(88);
        
        make.width.mas_equalTo(71);
        
    }];
    
}

//- (void) setUpInsuranceButton {
//    
//    self.insuranceButton=[ControlsFactory getButton];
//    
//    [self.insuranceButton setImage:[UIImage imageNamed:@"insurance.png"] forState:UIControlStateNormal];
//    
//    UIEdgeInsets padding = UIEdgeInsetsMake(24, 0, 12, 0);
//    
//    [self.scroll addSubview:self.insuranceButton];
//    
//    [self.insuranceButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.equalTo(self.messageButton.mas_bottom).with.offset(padding.top);
//        
//        make.centerX.mas_equalTo(@(-self.view.frame.size.width/4));
//        
//        make.bottom.mas_equalTo(self.scroll.mas_bottom).with.offset(-padding.bottom);
//        
//        make.height.mas_equalTo(88);
//        
//        make.width.mas_equalTo(87);
//        
//    }];
//    
//}
- (void) setUpReferralsButton{
    
    self.referralsButton=[ControlsFactory getButton];
    
    [self.referralsButton setImage:[UIImage imageNamed:@"referrals.png"] forState:UIControlStateNormal];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(24, 0, 12, 0);
    
    [self.referralsButton addTarget:self action:@selector(referralsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scroll addSubview:self.referralsButton];
    
    [self.referralsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petsButton.mas_bottom).with.offset(padding.top);
        
        //NSLog(@"scroll view frame: %@", NSStringFromCGRect(self.scroll.frame));
        
        make.centerX.mas_equalTo(@(-self.view.frame.size.width/4));
        
        make.height.mas_equalTo(88);
        
        make.width.mas_equalTo(115);
        
        //        make.top.equalTo(self.callButton.mas_bottom).with.offset(padding.top);
        //
        //        make.centerX.mas_equalTo(@(self.view.frame.size.width/4));
        //
        //        make.height.mas_equalTo(88);
        //
        //        make.width.mas_equalTo(87);
        //
        //        make.bottom.mas_equalTo(self.scroll.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

- (void) setUpBottomLine{
    
    self.bottomLine =[ControlsFactory getImageView];
    
    self.bottomLine.image=[UIImage imageNamed:@"bottomLine.png"];
    
    UIEdgeInsets paddingForLines = UIEdgeInsetsMake(0, 0, 112, 0);
    
    [self.scroll addSubview:self.bottomLine];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.scroll.mas_bottom).with.offset(paddingForLines.top);
        
        make.left.equalTo(self.scroll.mas_left).with.offset(paddingForLines.left);
        
        make.height.mas_equalTo(2);
        
        make.width.mas_equalTo(self.view.frame.size.width);
        
    }];
    
}

- (void) messageButtonClicked: (UIButton*) button {
//    
    NSString *annualPaymentPackageChecking;
    
    annualPaymentPackageChecking = [[NSUserDefaults standardUserDefaults] objectForKey:AnnualSubscriptionPaid_KEY];
    
    NSLog(@"\n\nChecking payment : %@\n\n",annualPaymentPackageChecking);
    
    if([annualPaymentPackageChecking  isEqualToString:AnnualSubscriptionPaid_NOT_Cleared] ||  annualPaymentPackageChecking == nil) {
        
        self.calledByKeyValue = @"Message";
        [self paymentAuthentication];
    }
    else if ([annualPaymentPackageChecking isEqualToString:AnnualSubscriptionPaid_Cleared])
    {
        self.calledByKeyValue = @"SetExtraCheck";
        [self paymentAuthentication];
        MessageController *messageController=[[MessageController alloc] init];
        [self.navigationController pushViewController:messageController animated:YES];
    }
}

#pragma Code section payment checking
//-(void) showMessageForPayment {
//
//    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
//                                                         alertControllerWithTitle:@"Request"
//                                                         message:@"Sorry, your Pcakage is not authenticated, Press 'OK' to activate otherwise press 'Cancel'"
//                                                         preferredStyle:UIAlertControllerStyleAlert];
//    
//    
//        UIAlertAction * ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
//                                 
//                                 [self.navigationController pushViewController:paypalController animated:YES];
//                             }];
//    
//        UIAlertAction * cancel = [UIAlertAction
//                             actionWithTitle:@"Cancel"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                
//                             }];
//        
//        [paymentSuccessfullyRecieved addAction:ok];
//    
//        [paymentSuccessfullyRecieved addAction:cancel];
//    
//        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
//
//}

-(void) profileButtonClicked: (UIButton *) button
{

    ProfileController *profileView=[[ProfileController alloc] init];

    [self.navigationController pushViewController:profileView animated:YES];

}

- (void) petsButtonClicked: (UIButton *) button
{
    PetListController *petProfile=[[PetListController alloc] init];
    
    [self.navigationController pushViewController : petProfile animated : YES];
    
}

- (void) referralsButtonClicked: (UIButton *) button
{
    PetReferralsController *petReferrals=[[PetReferralsController alloc] init];
    
    [self.navigationController pushViewController : petReferrals animated : YES];
    
}

- (void) logoutButtonClicked: (UIButton *) button {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    LoginController *login=[[LoginController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:login];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [navController setNavigationBarHidden:YES animated:YES];
    
    [UIView transitionWithView:appDelegate.window
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{ appDelegate.window.rootViewController = navController; }
                    completion:nil];
    
    [appDelegate.window makeKeyAndVisible];

}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];

}

//-(void)response:(id)data {
//    
//    if([data isKindOfClass:[NSDictionary class]])
//    {
//        if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
//            
//            ProfileController *profileView=[[ProfileController alloc] init];
//            
//            //profileView.dataDict = [data objectForKey:@"data"];
//            
//            [self.navigationController pushViewController:profileView animated:YES];
//            
//        }
//        
//        else {
//            
//            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Something went wrong" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            
//            [errorAlert show];
//            
//        }
//        
//    }
//    
//    else {
//        [[NSUserDefaults standardUserDefaults ] setObject:[[data objectAtIndex:0] objectForKey:@"id"] forKey:@"userId"];
//        
//        DashboardController *dashboard=[[DashboardController alloc] init];
//        
//        [self.navigationController pushViewController:dashboard animated:YES];
//    }
//    
//}

-(void)callButtonClicked:(UIButton*)button {
    
    NSString *annualPaymentPackageChecking = [[NSUserDefaults standardUserDefaults] objectForKey:AnnualSubscriptionPaid_KEY];
    
    NSLog(@"\n\nChecking payment : %@\n\n",annualPaymentPackageChecking);
    
    if([annualPaymentPackageChecking  isEqualToString:AnnualSubscriptionPaid_NOT_Cleared] ||  annualPaymentPackageChecking == nil) {
        
        self.calledByKeyValue = @"Call";
        
        [self paymentAuthentication];
        
    }
    else if ([annualPaymentPackageChecking isEqualToString:AnnualSubscriptionPaid_Cleared])
    {
        self.calledByKeyValue = @"SetExtraCheck";
        [self paymentAuthentication];
        [self callPortionCodeBlockWhenCallButtonClicked];
    }
}

-(void) callPortionCodeBlockWhenCallButtonClicked {
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        self.showSocketError= @"NO";
        
        //[self.delegate errorFromSocket:error];
    }
    
    else {
        
        self.showSocketError= @"YES";
        
        self.showBusyPopUp=@"YES";
        
        _manager=[SocketManager sharedInstance] ;
    
        NSLog(@"call button pressed: ");
        
        [_manager shouldRegisterCall]; // this will set shouldCallBoolean to FALSE
        
        // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [self setUpConnectionView];
        
        //  [self performSelector:@selector(showSocketErrorPopUp) withObject:nil afterDelay:16];
        
        [self connect];

    }
}

-(void) showSocketErrorPopUp{
    checkDoctorEveryThirtySeconds=FALSE;
    
    if ([self.showSocketError isEqualToString:@"YES"]) {
        
        //[MBProgressHUD hideHUDForView:self.view animated:NO];
        
       
       // [_manager checkSocket:@"stop"];
        
        //_manager=[SocketManager sharedInstance] ;
        
        NSLog(@"show socket Error");

       // [_manager callDeactivated];
        
    //    [self.connectionView removeFromSuperview];
//        
//        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        
//        if ([_appDel availableInternet]) {

        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Due to some technical reasons call cannot be initiated. Pleas try again after some time" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag=1;
            [alert show];
//
//        }
        
        
    }
}

-(void) failedToConnectToSocket {
    
    
    [_audioPlayer stop];
    
    
    checkDoctorEveryThirtySeconds=FALSE; // I think its not necessary because the above method gets called if socket does not connect means we have no doctors available yet
    
    
    
    self.isRinging=@"NO";
    
    //   [_manager checkSocket:@"stop"];
    
    [_manager callDeactivated];
    
    NSLog(@"close sockets and stop them as well");
    [_manager closeSocket];
    
    [_manager checkSocket:@"stop"];
    
    [_manager checkSocketConnectivity];


    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Due to some technical reasons call cannot be initiated. Pleas try again after some time" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
     self.showSocketError=@"NO";
    alert.tag=1;
    [alert show];

    
}
- (void) connect;
{
    _manager=[SocketManager sharedInstance] ;
    
    _manager.delegate=self;
   
    [_manager connect];
    
    //[_manager checkSocket:@"start"];

//    [self performSelector:@selector(reconnect) withObject:nil afterDelay:5.0];
    
    [self reconnect];
    
}
-(void) reconnect {
    
    _manager=[SocketManager sharedInstance] ;
    
    _manager.delegate=self;
    
    NSLog(@"go for socket Reconnectivity");

    [_manager checkSocket:@"start"];

}
-(void)setUpCallView:(double)callViewTimeOut {
    
    [self ring];
    
    self.isRinging=@"YES";
    
    NSLog(@" time out: %f",callViewTimeOut);
    
    double seconds=callViewTimeOut*60.0;
    
    if (callViewTimeOut>0) {
        
        checkDoctorEveryThirtySeconds=TRUE;
        
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryFortySeconds) object:nil];
        
        [self performSelector:@selector(checkAfterEveryFortySeconds) withObject:nil afterDelay:10.0];
    
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(closeRinger) object:nil];
    
    [self performSelector:@selector(closeRinger) withObject:nil afterDelay:seconds];
    
    
}

-(void)checkAfterEveryFortySeconds {
    
    if (checkDoctorEveryThirtySeconds) {
        
        [_manager pingForDoctorEveryFortySeconds];
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryFortySeconds) object:nil];
        
        [self performSelector:@selector(checkAfterEveryFortySeconds) withObject:nil afterDelay:10.0];
        
    }
    
    
    
}

-(void)declineButtonClicked:(UIButton*)button{
    
    // send data to server if required
    
    checkDoctorEveryThirtySeconds=FALSE;
    
    [self.connectionView removeFromSuperview];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
   
    [_audioPlayer stop];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callDisconnectedByPatientBeforeAnswer];

    [_manager callDeactivated];
    
    NSLog(@"decline button pressed");
    
   // [_manager closeSocket];
    
   // [_manager checkSocket:@"stop"]; //Reconnect if not connected
    
   // [_manager socketShouldNotReconnect];
    
  }

-(void)leaveCallScreen{
    
    checkDoctorEveryThirtySeconds=FALSE;
    
    [self.connectionView removeFromSuperview];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
    
    [_audioPlayer stop];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callDeactivated];
    
    NSLog(@"leave call screen");

    
}

-(void) ring{
    
    NSString *path=[[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
       _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    _audioPlayer.numberOfLoops=-1;
    
    [_audioPlayer play];
    
}


-(void)closeRinger{
    
    // will be called when User Call Timeouts
    if ([self.isRinging isEqualToString:@"YES"]) {
        
        self.isRinging=@"NO";
        
        checkDoctorEveryThirtySeconds=FALSE;
        
        _manager=[SocketManager sharedInstance];
        
        [_audioPlayer stop];
        
        [_manager callDisconnectedByPatientBeforeAnswer];
        
        [_manager callDeactivated];
        
        NSLog(@"self.ringing: %@",self.isRinging);
        NSLog(@"after time out interval \n\n");
        
        [self.connectionView removeFromSuperview];
        
        [_manager closeSocket];
        
        [_manager checkSocket:@"stop"];
        
        [_manager checkSocketConnectivity];
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"It seems due to some problem your call cannot initiated. It could be due to slow internet connection or some other reason" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [errorAlert show];
    }
    
    
}



-(void)setUpDeclineButton {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(50, 0, 50, 0);
    
    self.declineButton=[ControlsFactory getButton];
    
    [ self.declineButton setImage:[UIImage imageNamed:@"decline_call.png"] forState:UIControlStateNormal];
    
    [self.declineButton addTarget:self action:@selector(declineButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.connectionView addSubview: self.declineButton];
    
    [ self.declineButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.connectionView.mas_bottom).with.offset(-padding.bottom);
        
        make.centerX.mas_equalTo(self.connectionView.mas_centerX);
        
        make.height.mas_equalTo(80);
        
        make.width.mas_equalTo(80);
    }];
}

-(void)startOpenTokCall{
    
    checkDoctorEveryThirtySeconds=FALSE;
    
    OpenTokController *openToc=[[OpenTokController alloc] init];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
    
    [_audioPlayer stop];
    
    [self.connectionView removeFromSuperview];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callActivated];

    [self.navigationController pushViewController:openToc animated:YES];
}

-(void)responseFromSocket:(NSDictionary*)dataDict {
    
    self.showSocketError=@"NO";
    
    if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"call_activated"] ) {
        
        checkDoctorEveryThirtySeconds=FALSE;
        
        if (![dataDict objectForKey:@"session_id"]) {
            
            self.showBusyPopUp=@"NO";
            
            self.showSocketError=@"NO";
            
            self.isRinging=@"NO";
            
            [_audioPlayer stop];
            
            [self.connectionView removeFromSuperview];
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Server was not able to generate session. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alert show];
           
        }
        else {
        
            [self startOpenTokCall];
        
        }
    }
    else if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"busy"])
    {
        checkDoctorEveryThirtySeconds=FALSE;
        
        _manager=[SocketManager sharedInstance];
        
        [_audioPlayer stop];
        
        NSLog(@"USer is Busy");
        
        self.isRinging=@"NO";
        
     //   [_manager checkSocket:@"stop"];
        
        [_manager callDeactivated];
        
        [_manager closeSocket];
        
        [_manager checkSocket:@"stop"];
        
        [_manager checkSocketConnectivity];

        
        
        
        if ([self.showBusyPopUp isEqualToString:@"YES"]) {
            
        
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Busy" message:@"No Doctor available at the momment. Please try again after some time" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        alert.tag=0;
        [alert show];
            
        }

        
    }
    
    else if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"call"]){
        
//        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"]) {
//            
//            [[NSUserDefaults standardUserDefaults] setObject:[dataDict objectForKey:@"call_record_id"] forKey:@"call_record_id"];
//            
//            NSMutableDictionary *docsAndCallRecord= [[NSMutableDictionary alloc] init];
//            
//            [docsAndCallRecord setObject:[dataDict objectForKey:@"call_record_id"]  forKey:@"call_record_id"];
//            
//            [docsAndCallRecord setObject:[dataDict objectForKey:@"docsCount"]  forKey:@"docsCount"];
//            
//            [[NSUserDefaults standardUserDefaults] setObject:docsAndCallRecord forKey:DocsCountForCallRecordId];
//            
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            NSLog(@"show call with button");
//            
//            double value = [[dataDict objectForKey:@"docsCount"] doubleValue];
//            
//            [self setUpCallView:value];
//            
//            [self setUpDeclineButton];
//            
//            self.connecting.text=@"";
//
//        }
//        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"] isEqualToString:[dataDict objectForKey:@"call_record_id"] ]){
//            
//            if ([[dataDict objectForKey:@"docsCount"] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:DocsCountForCallRecordId] objectForKey:@"docsCount"]]) {
//                
//                NSLog(@"same call_record_id and same docsCount");
//            }
//            
//            else{
//                
//                NSLog(@"same call_record_id and but different docsCount cancel previous selector");
//                
//                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryThirtySeconds) object:nil];
//                
//                
//                NSMutableDictionary *docsAndCallRecord= [[NSMutableDictionary alloc] init];
//                
//                [docsAndCallRecord setObject:[dataDict objectForKey:@"call_record_id"]  forKey:@"call_record_id"];
//                
//                [docsAndCallRecord setObject:[dataDict objectForKey:@"docsCount"]  forKey:@"docsCount"];
//                
//                [[NSUserDefaults standardUserDefaults] setObject:docsAndCallRecord forKey:DocsCountForCallRecordId];
//                
//                [[NSUserDefaults standardUserDefaults] synchronize];
//                
//                
//            }
//        }
//        else
//        {
//            [[NSUserDefaults standardUserDefaults] setObject:[dataDict objectForKey:@"call_record_id"] forKey:@"call_record_id"];
        
            NSLog(@"show call with button");
            
            double value = [[dataDict objectForKey:@"docsCount"] doubleValue];
            
            [self setUpCallView:value];
            
            [self setUpDeclineButton];
            
            self.connecting.text=@"";

    
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LeaveCallScreen object:nil];
   

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex              {
    if(alertView.tag==0){
        
        if(buttonIndex == 0)//OK button pressed
        {
            NSLog(@"after busy is acknowledged remove connectionView");
            [self.connectionView removeFromSuperview];

//            if([self.callView isDescendantOfView:self.view]) {
//               
//                 NSLog(@"after busy is acknowledged remove callView");
//                
//                [self.callView removeFromSuperview];
//            }
//            else if ([self.connectionView isDescendantOfView:self.view]){
//               
//                 NSLog(@"after busy is acknowledged remove connectionView");
//                [self.connectionView removeFromSuperview];
//            }
        }
    }
    else   if (alertView.tag==1){
        
        if(buttonIndex == 0)//OK button pressed
        {
            
            [self.connectionView removeFromSuperview];
            
        }

    }
}

#pragma mark Connecting

-(void) setUpConnectionView{
    
    self.connectionView=[[UIView alloc] init];
    
    self.connectionView.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview: self.connectionView];
    
    [ self.connectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
    self.callViewBG=[ControlsFactory getImageView];
    
    [ self.callViewBG setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.connectionView addSubview: self.callViewBG];
    
    [ self.callViewBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.connectionView.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.connectionView.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.connectionView.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.connectionView.mas_right).with.offset(-padding.right);
    }];
    
    padding = UIEdgeInsetsMake(70, 0, 0, 0);
    
    self.avatarImage1=[ControlsFactory getImageView];
    
    [ self.avatarImage1 setImage:[UIImage imageNamed:@"incomingAvatar.jpg"]];
    
    [self.connectionView addSubview: self.avatarImage1];
    
    [ self.avatarImage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.connectionView.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.connectionView.mas_centerX);
        
        make.height.mas_equalTo(200);
        
        make.width.mas_equalTo(200);
    }];

    padding = UIEdgeInsetsMake(50, 0, 50, 0);
    
    self.connecting=[ControlsFactory getLabel];
    
    self.connecting.text=@"Dialling ...";
    
    self.connecting.textAlignment=NSTextAlignmentCenter;
    
    self.connecting.textColor=[UIColor whiteColor];
    
    [self.connectionView addSubview: self.connecting];
    
    [ self.connecting mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.connectionView.mas_bottom).with.offset(-padding.bottom);
        
        make.centerX.mas_equalTo(self.connectionView.mas_centerX);
        
        make.height.mas_equalTo(80);
        
        make.width.mas_equalTo(200);
    }];
    
}


-(void)errorFromSocket:(NSError*)error {
  
}

-(void)dismissAlert:(UIAlertView *) alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}


@end
