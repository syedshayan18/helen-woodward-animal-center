//
//  loginscreenViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/9/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"

@interface loginscreenViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl_username;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_username;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_password;
@property (weak, nonatomic) IBOutlet UIButton *btn_login;
@property (weak, nonatomic) IBOutlet UIButton *btn_register;
@property (weak, nonatomic) IBOutlet UIButton *btn_forget;
@property (weak, nonatomic) IBOutlet UIButton *btn_notmember;


@property (weak, nonatomic) IBOutlet UIView *subview;

@property (weak, nonatomic) IBOutlet UILabel *lbl_password;
@property (weak, nonatomic) IBOutlet UIButton *btn_loginn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_menu;
@property (weak, nonatomic) IBOutlet UIScrollView *Scrollview;


@property(weak, nonatomic) UIView *activeTextView;
@end
