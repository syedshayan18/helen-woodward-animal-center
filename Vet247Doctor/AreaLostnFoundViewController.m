//
//  AreaLostnFoundViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/11/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "AreaLostnFoundViewController.h"
#import "AreaLostnFoundCollectionViewCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MBProgressHUD.h"
#import "NetworkingClass.h"
#import <AFNetworking/AFNetworking.h>
#import "Lostnffoundmodel.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "Reachability.h"
#import "CheckInternet.h"
#import "PetProfilelostnfoundViewController.h"
#import "AppDelegate.h"
@interface AreaLostnFoundViewController ()<PlaceSearchTextFieldDelegate,UITextFieldDelegate,GMSMapViewDelegate>
@property AppDelegate *appDel;
@end

@implementation AreaLostnFoundViewController
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
 
    
    
    self.placestextfield.delegate=self;
    _radiusArray = [[NSMutableArray alloc]init];
  
    _lostfoundict = [[NSDictionary alloc]init];

  
   
    
    
    
   // [self.view addGestureRecognizer:taptohide];

    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy =
        kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    [self.locationManager startUpdatingLocation];

     [self.locationManager requestWhenInUseAuthorization];
    _placestextfield.placeSearchDelegate = self;
    _placestextfield.strApiKey = @"AIzaSyCgdljgsWF6H4ErAjz6AnCxwlQhMQGSbnc";
    _placestextfield.superViewOfList = self.view; // View, on which Autocompletion list should be appeared.
    _placestextfield.autoCompleteShouldHideOnSelection = true;
    _placestextfield.maximumNumberOfAutoCompleteRows = 5;
    _placestextfield.autoCompleteShouldHideOnSelection = true;
    _placestextfield.autoCompleteTableAppearsAsKeyboardAccessory = true;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 1)];
    _placestextfield.leftView = paddingView;
    _placestextfield.leftViewMode = UITextFieldViewModeAlways;
    
    [_radiusArray addObject:@"5 KM"];
    [_radiusArray addObject:@"10 KM"];
    [_radiusArray addObject:@"25 KM"];
    [_radiusArray addObject:@"50 KM"];
    [_radiusArray addObject:@"75 KM"];
    [_radiusArray addObject:@"100 KM"];
    
    
    self.radiuspicker = [[UIPickerView alloc]init];
    [_radiuspicker setDataSource:self];
    [_radiuspicker setDelegate:self];
    _radiuspicker.showsSelectionIndicator = YES;
    self.txt_radius.inputView = _radiuspicker;
    
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarButtonItemStylePlain;
    keyboardDoneButtonView.translucent = NO;
    keyboardDoneButtonView.tintColor = nil;
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                    style:UIBarButtonItemStylePlain target:self
                                                                   action:@selector(pickerDoneClicked:)];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    _txt_radius.inputAccessoryView = keyboardDoneButtonView;
  
    
    

   
}
-(BOOL)isInternetConnectionAvialable{
    BOOL isAvailable;
    isAvailable = [CheckInternet isInternetConnectionAvailable];
    if (isAvailable) {
        return isAvailable;
        
    }
    else {
        double delayInseconds = 15.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInseconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Internet Connection timeout" message:@"Please Check your Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                
                NSLog(@"ok");
                
                
                
                
                
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    return NO;
}


- (IBAction)pickerDoneClicked:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([_placestextfield.text isEqualToString:@""]){
        
        [self getallpetsrequest];
        
    }
    
    [textField resignFirstResponder];

    return YES;
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}


- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return _radiusArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_radiusArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _txt_radius.text = [_radiusArray objectAtIndex:row];
    [self networkrequest];
    [self.view endEditing:YES];


}




- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{

        CLLocation *curPos = locationManager.location;
    if (curPos){
        [locationManager stopUpdatingLocation];
        
    }
        
    _latitude = [[NSNumber numberWithDouble:locationManager.location.coordinate.latitude] stringValue];
    
    _longitude = [[NSNumber numberWithDouble:locationManager.location.coordinate.longitude] stringValue];
    

    
    [self getlocation];
        //from Google Map SDK
    
   
        
    }




- (void)getlocation{
    
    CLLocation *geolocation=[[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
    //from Google Map SDK
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
    GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
    {
        
        GMSAddress *address=response.firstResult;
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        
//        
//        
//        
//        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        
//        if (![_appDel availableInternet]) {
//            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            
//            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Internet Connection timeout" message:@"Please Check your Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                
//                
//                NSLog(@"ok");
        
                
                
                
                
          //  }];
            
            
           // [alert addAction:okAction];
            
            
          //  [self presentViewController:alert animated:YES completion:nil];

            
            
       // }
       // else {
           // [MBProgressHUD hideHUDForView:self.view animated:YES];

         if (address)
         {
        
                
                
                   _placestextfield.text = [NSString stringWithFormat:@"%@, %@,%@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
                _locationname = [NSString stringWithFormat:@"%@, %@, %@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
             
            }
    [self networkrequest];

       // }

        
           };
    [geocode reverseGeocodeCoordinate:geolocation.coordinate completionHandler:handler];
    
}






- (void) locationManager:(CLLocationManager *)manager
        didFailWithError:(NSError *)error
{
    NSLog(@"%@", @"Core location can't get a fix.");
    
}


- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];

    [locationManager stopUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _petData.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float width = (collectionView.frame.size.width/2-2);
   
    float height =  [UIScreen mainScreen].bounds.size.height * 0.4929577465;
    
    return CGSizeMake(width, height);
    

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AreaLostnFoundCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.layer.cornerRadius=5;
    cell.clipsToBounds=YES;
 
    
    if ([[[_petData objectAtIndex:indexPath.row]animaltag] isEqualToString:@"LOST"]){
        
        cell.lbl_tag.textColor = [UIColor redColor];
        cell.lbl_animalname.text =[[_petData objectAtIndex:indexPath.row]petname];
        cell.lbl_sex.text = [[_petData objectAtIndex:indexPath.row]petlostSex];
        cell.lbl_breed.text =[[_petData objectAtIndex:indexPath.row]petlostbreed];
        NSString *animalage=[NSString stringWithFormat:@"%@ Year Old",[[_petData objectAtIndex:indexPath.row]petage]];
        cell.lbl_animalage.text = animalage;
        cell.lbl_lostdate.text = [NSString stringWithFormat:@"Lost on:%@",[[_petData objectAtIndex:indexPath.row]expirydate]];;
        cell.lbl_tag.text=[[_petData objectAtIndex:indexPath.row]animaltag];
        
        NSURL *url = [[NSURL alloc]initWithString:[_petData[indexPath.row] petimagelost]];
        [cell.img_pet sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"post_bg_green"]];
        
        
        
    }
    else {
        
        cell.lbl_tag.text = @"FOUND";
        cell.lbl_animalname.text=nil;
        cell.lbl_animalage.text=nil;
        cell.lbl_lostdate.text=nil;
        cell.lbl_sex.text=nil;
        cell.lbl_breed.text=nil;
        NSURL *url = [[NSURL alloc]initWithString:[_petData[indexPath.row]petimagefound]];
        [cell.img_pet sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"post_bg_green"]];
        cell.lbl_tag.textColor = [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0];
        
       
        
    }
  
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    
    _index = indexPath.row;
    
    
    [self performSegueWithIdentifier:@"lostfounddetails" sender:nil];
}


-(void) placeSearchResponseForSelectedPlace:(GMSPlace *)responseDict{
    
     _locationname =_placestextfield.text;
    
  

   
    
    
    
        CLLocationCoordinate2D coordinates= responseDict.coordinate;
    
            _latitude = [[NSNumber numberWithDouble:coordinates.latitude] stringValue];
    
            _longitude = [[NSNumber numberWithDouble:coordinates.longitude] stringValue];
    
    

         [self networkrequest];
   
    
}



-(void)getallpetsrequest {
    
    
    
    
    _posturl =[NSString stringWithFormat:@"%@get_lost_found.php",BaseURL];
    

    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetworkingClass getlostnfound:_posturl completion:^(id finished, NSError *error) {
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error==nil){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
            NSLog(@"%@",finished);
            
            _petData = [[NSMutableArray alloc]init];
            
            // NSString *message = ;
            
            
            if([finished isKindOfClass:[NSDictionary class]]){
                if ([finished valueForKey:@"message"]){
                    
                    [_CollectionView reloadData];
                    
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Pet" message:@"No lost and found not found!" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        
                        NSLog(@"ok");
                        
                        
                        
                        
                        
                    }];
                    
                    
                    [alert addAction:okAction];
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    return ;
                }
            }
            else {
                
                for (int i=0;i<[finished count]; i++){
                    
                    
                    Lostnffoundmodel *model = [[Lostnffoundmodel alloc]initWithDictionary:finished[i]];
                    [_petData addObject:model];
                    
                }
                
                NSLog(@"%@",_petData);
                [_CollectionView reloadData];
                
                
                
            }
            
            
        }
        
        
        else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                
                NSLog(@"ok");
                
                
                
                
                
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            
            
        }
        
        
    }];
    
}


-(void)networkrequest {
    
    
    
    
        _posturl =[NSString stringWithFormat:@"%@get_lost_found.php?location=%@&location_latitude=%@&location_longitude=%@&location_distance=%@",BaseURL,_locationname,_latitude,_longitude,_txt_radius.text];
    
        NSString *url = [_posturl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [NetworkingClass getlostnfound:url completion:^(id finished, NSError *error) {
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (error==nil){
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                NSLog(@"%@",finished);
                
                _petData = [[NSMutableArray alloc]init];
               
               // NSString *message = ;
                
                
                if([finished isKindOfClass:[NSDictionary class]]){
                    if ([finished valueForKey:@"message"]){
                        
                        [_CollectionView reloadData];
                        
                        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Pet" message:@"No lost and found not found!" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            
                            NSLog(@"ok");
                            
                            
                            
                            
                            
                        }];
                        
                        
                        [alert addAction:okAction];
                        
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        return ;
                    }
                }
               else {
                    
                    for (int i=0;i<[finished count]; i++){
                        
                        
                        Lostnffoundmodel *model = [[Lostnffoundmodel alloc]initWithDictionary:finished[i]];
                        [_petData addObject:model];
                        
                    }
                    
                    NSLog(@"%@",_petData);
                    [_CollectionView reloadData];
                    
                    
                    
                }

                    
               }
                    
           
                           else {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
                
            }
            
            
        }];

}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PetProfilelostnfoundViewController *petprofile = (PetProfilelostnfoundViewController *)[segue destinationViewController];
    petprofile.petmodel=[_petData objectAtIndex:_index];
}


@end
