//
//  PetProfilelostnfoundViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lostnffoundmodel.h"

@interface PetProfilelostnfoundViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img_petprofile;
@property (weak, nonatomic) IBOutlet UITextView *textview_description;
@property (weak, nonatomic) IBOutlet UILabel *lbl_location;
@property (weak, nonatomic) IBOutlet UIImageView *img_location;
@property (weak, nonatomic) IBOutlet UILabel *lbl_species;
@property (weak, nonatomic) IBOutlet UILabel *lbl_breed;
@property (weak, nonatomic) IBOutlet UILabel *lbl_age;
@property (weak, nonatomic) IBOutlet UILabel *lbl_weight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sex;
@property (weak, nonatomic) IBOutlet UIButton *btn_chat;
@property (weak, nonatomic) IBOutlet UILabel *lbl_petaction;
@property (strong,nonatomic) UIImage *userprofileimagetobepassed;
@property (strong,nonatomic) UIImage *senderprofileimagetobepassed;
@property (strong,nonatomic) NSMutableArray *petprofiledict;
@property (strong,nonatomic) NSString *passingSenderName;
@property Lostnffoundmodel *petmodel;

@end
