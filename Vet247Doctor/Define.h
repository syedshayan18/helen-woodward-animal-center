//
//  Define.h
//  Vet247Doctor
//
//  Created by Malik on 7/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#ifndef Vet247Doctor_Define_h
#define Vet247Doctor_Define_h

#define isiPhone4 (self.view.frame.size.height == 480) ? YES : NO
#define isiPhone5 (self.view.frame.size.height == 568) ? YES : NO
#define isiPhone6 (self.view.frame.size.height == 667) ? YES : NO
#define isiPhone6Plus (self.view.frame.size.height == 736) ? YES : NO
#define IS_RETINA_DISPLAY() [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0f

// Network Connectivity
typedef NS_ENUM(NSInteger, MyErrorCodes) {
    InternetDiconnectivity= 195,
    MyErrorCodesInvalidURL,
    MyErrorCodesUnableToReachHost,
};

#endif
