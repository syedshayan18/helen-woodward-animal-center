//
//  ThirdViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "ThirdViewController.h"
#import "PetProfileDataManager.h"
#import "PetProfileKeys.h"
#import "AHContainerViewController.h"
#import "MyPetsViewController.h"

@interface ThirdViewController ()

@property BOOL radiobutton;
@property BOOL radiobutton1;
@property BOOL radiobutton2;
@property BOOL radiobutton3;
@property BOOL radiobutton4;
@property BOOL radiobutton5;


@end

@implementation ThirdViewController
    
    


- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fieldsunable:)
                                                 name:@"fieldsunable"
                                               object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(insuranceYes:)];
    
    [self.lbl_insuranceyes addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *tapGestureNo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(insuranceNo:)];
    
    [self.lbl_insuranceno addGestureRecognizer:tapGestureNo];
    
    UITapGestureRecognizer *tapGesturetrackerYes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trackerYes:)];
    
    [self.lbl_trackerYes addGestureRecognizer:tapGesturetrackerYes];
    
    UITapGestureRecognizer *tapGesturetrackerNo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trackerNo:)];
    
    [self.lbl_trackerNo addGestureRecognizer:tapGesturetrackerNo];
//
    UITapGestureRecognizer *tapGesturevertYes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vertYes:)];
    
    [self.lbl_vertYes addGestureRecognizer:tapGesturevertYes];
    
    UITapGestureRecognizer *tapGesturevertNo= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vertNo:)];
    
    [self.lbl_VertNo addGestureRecognizer:tapGesturevertNo];
    
    
    
    
    self.txt_phoneno.keyboardType =UIKeyboardTypePhonePad;
    NSAttributedString *insurance = [[NSAttributedString alloc] initWithString:@"INSURANCE COMPANY" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_insurance.attributedPlaceholder = insurance;
    NSAttributedString *policy = [[NSAttributedString alloc] initWithString:@"POLICY NUMBER" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_policyno.attributedPlaceholder = policy;
    NSAttributedString *tracker = [[NSAttributedString alloc] initWithString:@"TRACKER COMPANY" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_tracker.attributedPlaceholder = tracker;
    NSAttributedString *pin = [[NSAttributedString alloc] initWithString:@"PIN NUMBER" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_pin.attributedPlaceholder = pin;
    NSAttributedString *name = [[NSAttributedString alloc] initWithString:@"NAME" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_name.attributedPlaceholder = name;
    NSAttributedString *phone = [[NSAttributedString alloc] initWithString:@"PHONE NUMBER" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_phoneno.attributedPlaceholder = phone;
    
    //self.txt_pin.keyboardType =UIKeyboardTypeNumberPad;
    //self.txt_policyno.keyboardType=UIKeyboardTypeNumberPad;
    // Do any additional setup after loading the view.
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
//    {
    NSString *PETID=_thirdpet.PetID;
    if (PETID!=nil)
    {
        [self populateDataToView];
        
   // }
   
}
}

- (void)fieldsunable:(NSNotification *)note {
    
    _btn_insurranceyes.userInteractionEnabled=YES;
    _btn_insuuranceno.userInteractionEnabled=YES;
    _btn_trackerYes.userInteractionEnabled=YES;
    _btn_trackerNo.userInteractionEnabled=YES;
    _btn_vertYes.userInteractionEnabled=YES;
    _vertNo.userInteractionEnabled=YES;
    _txt_insurance.enabled=YES;
    _txt_policyno.enabled=YES;
    _txt_tracker.enabled=YES;
    _txt_pin.enabled=YES;
    _txt_name.enabled=YES;
    _txt_phoneno.enabled=YES;
    
    _lbl_insuranceyes.userInteractionEnabled=YES;
    _lbl_insuranceno.userInteractionEnabled=YES;
    _lbl_trackerYes.userInteractionEnabled=YES;
    _lbl_trackerNo.userInteractionEnabled=YES;
    _lbl_vertYes.userInteractionEnabled=YES;
    _lbl_VertNo.userInteractionEnabled=YES;

    
    
    
}


-(void)viewDidAppear:(BOOL)animated{
    AHContainerViewController *container = (AHContainerViewController*) self.parentViewController;
    MyPetsViewController *mypet = (MyPetsViewController*) container.parentViewController;
    
    if (mypet.btn_edit.isSelected==YES){
        
        _btn_insurranceyes.userInteractionEnabled=YES;
        _btn_insuuranceno.userInteractionEnabled=YES;
        _btn_trackerYes.userInteractionEnabled=YES;
        _btn_trackerNo.userInteractionEnabled=YES;
        _btn_vertYes.userInteractionEnabled=YES;
        _vertNo.userInteractionEnabled=YES;
        _txt_insurance.enabled=YES;
        _txt_policyno.enabled=YES;
        _txt_tracker.enabled=YES;
        _txt_pin.enabled=YES;
        _txt_name.enabled=YES;
        _txt_phoneno.enabled=YES;
        
        _lbl_insuranceyes.userInteractionEnabled=YES;
                _lbl_insuranceno.userInteractionEnabled=YES;
                _lbl_trackerYes.userInteractionEnabled=YES;
                _lbl_trackerNo.userInteractionEnabled=YES;
                _lbl_vertYes.userInteractionEnabled=YES;
                _lbl_VertNo.userInteractionEnabled=YES;

        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) checkInsuranceInfoTextField
{
    
    // Making view controller to handle error messages
   // UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    
    UIAlertController * insuranceInfoFieldsErrorAlert =   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [insuranceInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    //        UIAlertAction* cancel = [UIAlertAction
    //                                 actionWithTitle:@"Cancel"
    //                                 style:UIAlertActionStyleDefault
    //                                 handler:^(UIAlertAction * action)
    //                                 {
    //                                     [insuranceInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                                 }];
    //
    [insuranceInfoFieldsErrorAlert addAction:ok];
    //[insuranceInfoFieldsErrorAlert addAction:cancel];
    
    
    if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==NO) {
        
        [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Insurance YES or NO"];
        
        [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if(self.btn_insurranceyes.isSelected==YES)
    {
        
        if (self.txt_insurance.text && self.txt_insurance.text.length<1)
        {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Insurance Company's Name"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
        
        else if (self.txt_policyno.text && self.txt_policyno.text.length<1) {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Insurance Ploicy Number"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
    }
    
    if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==NO) {
        
        [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Tracker YES or NO"];
        
        [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if(self.btn_trackerYes.isSelected==YES)
    {
        
        if (self.txt_tracker.text && self.txt_tracker.text.length<1)
        {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter your Pet's Tracker, Company Name"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
        
        else if (self.txt_pin.text && self.txt_pin.text.length<1) {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter tracking Pin Number"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
    }
    
    if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==NO) {
        
        [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Veterinarian YES or NO"];
        
        [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if(self.btn_vertYes.isSelected==YES)
    {
        
        if (self.txt_name.text && self.txt_name.text.length<1)
        {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter your Pet's Veterinarian Name"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
        
        else if (self.txt_phoneno.text && self.txt_phoneno.text.length<1)
        {
            
            [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Pet's Veterinarian Phone Number"];
            
            [self presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
            
            return  NO;
        }
    }
    
    PetProfileDataManager * addInsuranceInfo = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    if(self.btn_insurranceyes.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_insurance.text : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_policyno.text : InsurancePolicyNumberKey];
        
    }
    else if(self.btn_insuuranceno.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsurancePolicyNumberKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsurancePolicyNumberKey];
        
    }
    
    if(self.btn_trackerYes.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_tracker.text :TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_pin.text :TrackingPinNumberKey];
        
    }
    else if(self.btn_trackerNo.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackingPinNumberKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackingPinNumberKey];
        
    }
    
    if(self.btn_vertYes.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_name.text :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.txt_phoneno.text :VeterinarianPhoneKey];
        
    }
    else if(self.vertNo.isSelected==YES)
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" :VeterinarianPhoneKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"No" :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"No" :VeterinarianPhoneKey];
    }
    
    return  YES;
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if ([[NSPetData objectForKey:PetInsuranceYesOrNoKey] isEqualToString:@"Yes"]) {
        
        [self.btn_insurranceyes setSelected:YES];
        
        self.txt_insurance.text = [NSPetData objectForKey:InsuranceCompanyKey];
        
        self.txt_policyno.text = [NSPetData objectForKey:InsurancePolicyNumberKey];
        
    }
    
    else if([[NSPetData objectForKey:PetInsuranceYesOrNoKey] isEqualToString:@"No"])
    {
        [self.btn_insuuranceno setSelected:NO];
    }
    
    if ([[NSPetData objectForKey:PetTrackerYesOrNoKey] isEqualToString:@"Yes"])
    {
        
        [self.btn_trackerYes setSelected:YES];
        
        self.txt_tracker.text = [NSPetData objectForKey:TrackerCompanyNameKey];
        
        if([NSPetData objectForKey:@"pin_number"] != nil)
        {
            self.txt_pin.text = [NSPetData objectForKey:@"pin_number"];
        }
        else
        {
            self.txt_pin.text = [NSPetData objectForKey:TrackingPinNumberKey];
        }
    }
    
    else if([[NSPetData objectForKey:PetTrackerYesOrNoKey] isEqualToString:@"No"])
    {
        [self.btn_trackerNo setSelected:YES];
    }
    
    if ([[NSPetData objectForKey:PetVeterinarianYesOrNoKey] isEqualToString:@"Yes"]) {
        
        [self.btn_vertYes setSelected:YES];
        
        self.txt_name.text = [NSPetData objectForKey:VeterinarianNameKey];
        
        self.txt_phoneno.text = [NSPetData objectForKey:VeterinarianPhoneKey];
        
    }
    
    else if([[NSPetData objectForKey:PetVeterinarianYesOrNoKey] isEqualToString:@"No"])
    {
        [self.vertNo setSelected:YES];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}




-(void)insuranceYes:(UITapGestureRecognizer *)sender {
    
    if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==NO)
    {
        [self.btn_insurranceyes setSelected:YES];
        
    }
    
    else if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==YES)
    {
        
        _insuranceviewcontraint.constant =80;
        [_txt_insurance setHidden:NO];
        
        [_txt_policyno setHidden:NO];
        [self.btn_insurranceyes setSelected:YES];
        
        [self.btn_insuuranceno setSelected:NO];
    }
    

}

-(void)insuranceNo:(UITapGestureRecognizer *)sender {
    if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==NO)
    {
        
        _insuranceviewcontraint.constant =0;
        [_txt_insurance setHidden:YES];
        [_txt_policyno setHidden:YES];
        
        [self.btn_insuuranceno setSelected:YES];
    }
    
    else if (self.btn_insuuranceno.isSelected==NO && self.btn_insurranceyes.isSelected==YES)
    {
        _insuranceviewcontraint.constant =0;
        [_txt_insurance setHidden:YES];
        [_txt_policyno setHidden:YES];
        [self.btn_insuuranceno setSelected:YES];
        
        [self.btn_insurranceyes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }

}




- (IBAction)btninsuranceYes:( UIButton *)sender {
    if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==NO)
    {
      [self.btn_insurranceyes setSelected:YES];
        
    }
    
    else if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==YES)
    {
        
       _insuranceviewcontraint.constant =80;
        [_txt_insurance setHidden:NO];
        
        [_txt_policyno setHidden:NO];
        [self.btn_insurranceyes setSelected:YES];
        
        [self.btn_insuuranceno setSelected:NO];
    }

    
    
}




- (IBAction)btn_insuranceNo:( UIButton *)sender {
    
    
    if (self.btn_insurranceyes.isSelected==NO && self.btn_insuuranceno.isSelected==NO)
    {
        
        _insuranceviewcontraint.constant =0;
        [_txt_insurance setHidden:YES];
        [_txt_policyno setHidden:YES];
     
        [self.btn_insuuranceno setSelected:YES];
    }
    
    else if (self.btn_insuuranceno.isSelected==NO && self.btn_insurranceyes.isSelected==YES)
    {
        _insuranceviewcontraint.constant =0;
        [_txt_insurance setHidden:YES];
        [_txt_policyno setHidden:YES];
        [self.btn_insuuranceno setSelected:YES];
        
        [self.btn_insurranceyes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }
    
    
    
}




-(void) trackerYes:(UITapGestureRecognizer *)sender{
    
    if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==NO)
    {
        [self.btn_trackerYes setSelected:YES];
    }
    
    else if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==YES)
    {
        
        
        _trackerviewheight.constant =80;
        [_txt_tracker setHidden:NO];
        
        [_txt_pin setHidden:NO];
        [self.btn_trackerYes setSelected:YES];
        
        [self.btn_trackerNo setSelected:NO];
    }
    

}




- (IBAction)btn_trackerYes: ( UIButton *)sender {
    if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==NO)
    {
        [self.btn_trackerYes setSelected:YES];
    }
    
    else if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==YES)
    {
        
        
        _trackerviewheight.constant =80;
        [_txt_tracker setHidden:NO];
        
        [_txt_pin setHidden:NO];
        [self.btn_trackerYes setSelected:YES];
        
        [self.btn_trackerNo setSelected:NO];
    }

    
    
        
    }



-(void) trackerNo:(UITapGestureRecognizer *)sender{
    if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==NO)
    {
        
        _trackerviewheight.constant =0;
        [_txt_pin setHidden:YES];
        [_txt_tracker setHidden:YES];
        [self.btn_trackerNo setSelected:YES];
    }
    
    else if (self.btn_trackerNo.isSelected==NO && self.btn_trackerYes.isSelected==YES)
    {
        _trackerviewheight.constant =0;
        [_txt_pin setHidden:YES];
        [_txt_tracker setHidden:YES];
        
        [self.btn_trackerNo setSelected:YES];
        
        [self.btn_trackerYes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }

}


- (IBAction)btn_TrackerNo:( UIButton *)sender {
    
    
    if (self.btn_trackerYes.isSelected==NO && self.btn_trackerNo.isSelected==NO)
    {
        
        _trackerviewheight.constant =0;
        [_txt_pin setHidden:YES];
        [_txt_tracker setHidden:YES];
        [self.btn_trackerNo setSelected:YES];
    }
    
    else if (self.btn_trackerNo.isSelected==NO && self.btn_trackerYes.isSelected==YES)
    {
        _trackerviewheight.constant =0;
        [_txt_pin setHidden:YES];
        [_txt_tracker setHidden:YES];

        [self.btn_trackerNo setSelected:YES];
        
        [self.btn_trackerYes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }

       // self.radiobutton3 = !self.radiobutton3;
        //[sender setSelected:self.radiobutton3];
}


-(void) vertYes:(UITapGestureRecognizer *)sender{
    if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==NO)
    {
        [self.btn_vertYes setSelected:YES];
    }
    
    else if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==YES)
    {
        
        _vertnviewheight.constant =80;
        [_txt_name setHidden:NO];
        
        [_txt_phoneno setHidden:NO];
        
        [self.btn_vertYes setSelected:YES];
        
        [self.vertNo setSelected:NO];
    }
    

    
}





- (IBAction)btn_VertYes:( UIButton *)sender {
            
    if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==NO)
    {
        [self.btn_vertYes setSelected:YES];
    }
    
    else if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==YES)
    {
        
        _vertnviewheight.constant =80;
        [_txt_name setHidden:NO];
        
        [_txt_phoneno setHidden:NO];

        [self.btn_vertYes setSelected:YES];
        
        [self.vertNo setSelected:NO];
    }

    
    // self.radiobutton = !self.radiobutton;
           // [sender setSelected:self.radiobutton];
            
            
        }



-(void) vertNo:(UITapGestureRecognizer *)sender{
    
    if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==NO)
    {
        _vertnviewheight.constant =0;
        [_txt_name setHidden:YES];
        
        [_txt_phoneno setHidden:YES];
        [self.vertNo setSelected:YES];
    }
    
    else if (self.vertNo.isSelected==NO && self.btn_vertYes.isSelected==YES)
    {
        _vertnviewheight.constant =0;
        [_txt_name setHidden:YES];
        
        [_txt_phoneno setHidden:YES];
        [self.vertNo setSelected:YES];
        
        [self.btn_vertYes setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }

}

- (IBAction)btn_VertNo:( UIButton *)sender {
    
    if (self.btn_vertYes.isSelected==NO && self.vertNo.isSelected==NO)
    {
        _vertnviewheight.constant =0;
        [_txt_name setHidden:YES];
        
        [_txt_phoneno setHidden:YES];
        [self.vertNo setSelected:YES];
    }
    
    else if (self.vertNo.isSelected==NO && self.btn_vertYes.isSelected==YES)
    {
        _vertnviewheight.constant =0;
        [_txt_name setHidden:YES];
        
        [_txt_phoneno setHidden:YES];
        [self.vertNo setSelected:YES];
        
        [self.btn_vertYes setSelected:NO];
    
    
       
            
        }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
