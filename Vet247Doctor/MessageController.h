//
//  MessageController.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatMessageCell.h"

@interface MessageController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@end
