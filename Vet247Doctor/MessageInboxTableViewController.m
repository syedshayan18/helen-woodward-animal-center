//
//  MessageInboxTableViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/21/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "MessageInboxTableViewController.h"
#import "PetdetailsViewController.h"
#import "MBProgressHUD.h"
#import "NetworkingClass.h"
#import "MessageInboxTVC.h"

@interface MessageInboxTableViewController ()

@end

@implementation MessageInboxTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getMessages];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) getMessages {
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [network getMessages:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
}
-(void)response:(id)data {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
        
        if([data isKindOfClass:[NSDictionary class]])
        {
            if ([[data objectForKey:@"status"] isEqualToString:@"failure"]) {
                
                UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [errorAlert show];
                
            }
            
            else if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
                
                
                self.dataFromServer=YES;
                
                [self.tableData removeAllObjects];
                
                
                _dataArray = [[NSArray alloc]init];
                self.dataArray=[data objectForKey:@"data"];
                
                //[self findRows];
                
                [self.tableView reloadData];
                
            
                
            }
            
        }
        
    });
    
    
}



-(IBAction)btn_message:(UIBarButtonItem *)sender {
 
    
 [self performSegueWithIdentifier:@"messageavet" sender:nil];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqual:@"messageavet"]){
        PetdetailsViewController *petdetails = (PetdetailsViewController *) [segue destinationViewController];
        petdetails.hideButtonformessage =YES;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageInboxTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    
//    cell.lbl_lastmessage.text = [_dataArray objectAtIndex:indexPath.row];
//    cell.lbl_doctoravailable.text=[_durationarray objectAtIndex:indexPath.row];
   // NSURL *imgurl = [[NSURL alloc]initWithString:[_imagearray objectAtIndex:indexPath.row]];
   // [cell.img_doctor sd_setImageWithURL:imgurl placeholderImage:[UIImage imageNamed:@"pet_list_image"]];
    cell.img_doctor.layer.cornerRadius =25;
    cell.img_doctor.clipsToBounds=YES;

    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
