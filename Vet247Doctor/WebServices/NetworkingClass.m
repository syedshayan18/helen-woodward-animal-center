//
//  NetworkingClass.m
//  MondoTalk
//
//  Created by Apple on 12/11/14.
//  Copyright (c) 2014 Eiconix. All rights reserved.
//

#import "NetworkingClass.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#define NAME @"name"
#define FIRSTNAME @"firstname"
#define LASTNAME @"lastname"
#define EMAIL @"email_address"
#define PHONE @"phone_no"
#define USERNAME @"username"
#define PASSWORD @"password"
#define USERID @"id"
#define ID @"user_id"
#define KEY @"key"
#define COMISSION @"mm_option"
#define PaymentMethod @"mm_method"
#define FRIENDS @"friends"
#define UDID @"udid"
#define REQUESTTO @"request_to"
#define REQUESTFROM @"request_from"
#define STATUS @"status"
#define SEARCH_TEXT @"text"
#define IMAGE_OR_VIDEO @"my_file"
#define K_NOTIFICATION @"notify"
#define K_IMAGE_OR_VIDEO @"img_video"

static NetworkingClass *sharedObj = nil;

@implementation NetworkingClass

#pragma mark Singelton

+(id) getSharedNetworkObject{
    
    if (sharedObj == nil) {
        sharedObj = [[NetworkingClass alloc] init];
    }
    
    return sharedObj;
}

#pragma mark SignUp

- (void)signUp:(NSMutableDictionary *)dataDict {
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet])
    {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",BaseURL,@"register.php",FIRSTNAME,[dataDict objectForKey:@"fName"],LASTNAME,[dataDict objectForKey:@"lName"],EMAIL,[dataDict objectForKey:@"email"],PHONE,[dataDict objectForKey:@"phonenumber"],PASSWORD,[dataDict objectForKey:@"password"],@"role",@"User"];

        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }else{
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
                
            });
        }];
        [getDataTask resume];
    }
    
    
}


#pragma mark SignIn

-(void)signIn:(NSString*)Username Password:(NSString*)paswrd
{
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
    
        NSString *urlString;

        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]) {
            
            urlString = [NSString stringWithFormat:@"%@%@?%@=%@&%@=%@&%@=%@&%@=%@",BaseURL,@"loginprocess.php",EMAIL,Username,PASSWORD,paswrd,@"dev_type",@"ios",@"device_token",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushtoken"]];
            
        }
        else
        {
            urlString = [NSString stringWithFormat:@"%@%@?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",BaseURL,@"loginprocess.php",EMAIL,Username,PASSWORD,paswrd,@"dev_type",@"ios",@"dev_id",[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],@"device_token",[[NSUserDefaults standardUserDefaults] objectForKey:@"pushtoken"]];
            NSLog(@"%@",urlString);
            
        }
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }else{
                        [self performSelector:@selector(showAlert)];
                        
                        NSLog(@"%@",error.description);
                        
                        [self.delegate error:error];
                    }
                }
                else {
                    
                    [self performSelector:@selector(showAlert)];
                    
                    NSLog(@"%@",error.description);
                    
                    [self.delegate error:error];
                }
            });
        }];
        
        [getDataTask resume];
        
    }
    
}

#pragma mark setProfile
-(void) setProfile:(NSData *)imageData dataDict:(NSMutableDictionary*)dataDict
{
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"update_profile.php"];
        
        [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"user_id"];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
        
        //Set Params
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:50];
        [request setHTTPMethod:@"POST"];
        
        NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
        
        NSString *intervalString = [NSString stringWithFormat:@"%f", today];
        //Create boundary, it can be anything
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
       
        [request setValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
       // [request setValue:@"http" forHTTPHeaderField: @"Content-Type"];
        
        NSString *string2=[intervalString stringByAppendingFormat:@"%@",@".jpg"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in dataDict) {
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [dataDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:imageData];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        // set URL
        [request setURL:[NSURL URLWithString:urlString]];
        
        
        NSURLSessionDataTask *postData = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    //        [NSURLConnection
                    //         sendAsynchronousRequest:request
                    //         queue:[NSOperationQueue mainQueue]
                    //         completionHandler:^(NSURLResponse *response,
                    //                             NSData *data,
                    //                             NSError *error)
                    //         {
                    //             NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    
                    if ([data length] > 0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }else{
                        [self performSelector:@selector(showAlert)];
                        
                        NSLog(@"%@",error.description);
                        
                        [self.delegate error:error];
                    }
                    
                    
                }
                else{
                    
                }
                
            });
            
        }];
        
        [postData resume];
        
    }
    
    
}

// set PetProfile Data networking function
-(void) savePetProfileInfo : (NSData *) petImageData :(NSMutableDictionary*) petData
{
    //NSLog(@"Animal Name in Networking class %@\n", [petData objectForKey:@"animalName"]);
    
    /////////////////////////////////////////////////////
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"insert_pet.php"];
    
    [petData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:50];
    [request setHTTPMethod:@"POST"];
    
    NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
    
    NSString *intervalString = [NSString stringWithFormat:@"%f", today];
    
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *string2=[intervalString stringByAppendingFormat:@"%@",@".jpg"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in petData)
    {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [petData objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSLog(@" setimage : %@ \n", [petData objectForKey:@"setimage"]);
    
    if([[petData objectForKey:@"setimage"] isEqualToString:@"Yes"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"%@",[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2]);
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:petImageData];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:urlString]];
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] > 0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 
                 
                 
                 
                 
                 
                 [self.delegate response:json];
                 
             }
             else
             {
                 
                 [self.delegate error:error];
             }
             
         }
         else
         {
             [self performSelector:@selector(showAlert)];
             
             NSLog(@"%@",error.description);
             
             [self.delegate error:error];
         }
     }];
    
    /////////////////////////////////////////////////////
}// -> end save petProfileInfo

-(void)getPets:(NSString*)userId
{
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    else
    {
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@",BaseURL,@"view_pet.php",@"userId",userId];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }
                        else
                        {
                            
                            [self.delegate error:error];
                        }
                        
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                    
                }
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
                
            });
            
            
            
        }];
        
        [getDataTask resume];
        
    }
    
    
    
    
    
}

+(void)historyrequest:(NSString*)deleteurl params:(NSDictionary*)parametersDictionary completion:(void (^)(id finished, NSError* error))completion {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
 
    manager.responseSerializer  = [AFHTTPResponseSerializer serializer];
    
    
    [manager POST:deleteurl parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
        
        NSLog(@"success!%@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        NSLog(@"error: %@", error);
        
      

        
        completion(NULL, error);
    }];
    
    
   
    
    
}


+(void)getprofileinfo:(NSString*)url completion:(void (^)(id finished, NSError* error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NULL, error);
    }];
    
}






-(void)reportlostpet:(NSString *)url param:(NSMutableDictionary*)paramDict completion:(void (^)(id finished,NSError *error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    //manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
[manager POST:url parameters:paramDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject,NULL);
        NSLog(@"success: %@",responseObject);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
         completion(NULL,error);
        
    }];
    
}

+(void)logout:(NSString *)url param:(NSMutableDictionary*)paramDict completion:(void (^)(id finished,NSError *error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    //manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:url parameters:paramDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject,NULL);
        NSLog(@"success: %@",responseObject);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        completion(NULL,error);
        
    }];
    
}



+(void)getlostnfound:(NSString*)url completion:(void (^)(id finished, NSError* error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NULL, error);
    }];
    
}


+(void)getuserlostnfound:(NSString *)url param:(NSMutableDictionary*)body completion:(void (^)(id finished,NSError *error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
//    
    [manager POST:url parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject,NULL);
        NSLog(@"success: %@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        
        completion(NULL,error);
        
        
    }];
    
}


+(void)deletemylostnfound:(NSString*)url completion:(void (^)(id finished, NSError* error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NULL, error);
    }];
    
}


+(void)getConversations:(NSString*)url  completion:(void (^)(id finished, NSURLSessionDataTask *resp, NSError* error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, task,NULL);
        NSLog(@"success: %@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        
        completion(NULL, task,error);
        
        
    }];
    
}

+(void)getchatmessages:(NSString *)url completion:(void (^)(id, NSError *))completion{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    

    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"Success: %@",responseObject);
        completion (responseObject,NULL);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        
        completion(NULL,error);
    }];
}


+(void)sendchatmessages:(NSString *)url param:(NSMutableDictionary*)body completion:(void (^)(id finished,NSError *error))completion{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    //
    [manager POST:url parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject,NULL);
        NSLog(@"success: %@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        
        completion(NULL,error);
        
        
    }];
    
}




+(void)request:(NSString*)url parameters:(NSDictionary*)parametersDictionary completion:(void (^)(id finished, NSError* error))completion {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer  = [AFHTTPResponseSerializer serializer];
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
              NSLog(@"success!%@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          NSLog(@"error: %@", error);
        completion(NULL, error);
    }];
    
    
    //    [operation start];
    
    
}


+(void)request2:(NSString*)url param:(NSDictionary*)paramDict completion:(void (^)(id finished, NSError* error))completion {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
   // AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
   // manager.requestSerializer = [AFHTTPRequestSerializer serializer];
     //manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    // [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
  //  manager.responseSerializer  = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
   // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
   // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
   
    [manager POST:url parameters:paramDict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
       
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, NULL);
        
        NSLog(@"success!%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        completion(NULL, error);
    }];
    
    
}

+(void)Userlatlongpostrequest:(NSString*)posturl params:(NSDictionary*)body completion:(void (^)(id finished, NSError* error))completion {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"text/json", @"application/json", @"text/javascript", nil];
    //
    [manager POST:posturl parameters:body progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject,NULL);
        NSLog(@"success: %@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        
        completion(NULL,error);
        
        
    }];
    
    
    
    
    
}

/*-(void)deletepet :(NSString *)petid{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"delete_pet.php"];
    
    //[petData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
   NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    
    NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
    
    NSString *intervalString = [NSString stringWithFormat:@"%f", today];
    
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"application/json; boundary=%@", boundary];
    
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
   // NSString *string2=[intervalString stringByAppendingFormat:@"%@",@".jpg"];
    
    // post body
   // NSMutableData *body = [NSMutableData data];
    
  //  for (NSString *param in petid)
  /*{
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [petData objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSLog(@" setimage : %@ \n", [petData objectForKey:@"setimage"]);
    
    if([[petData objectForKey:@"setimage"] isEqualToString:@"Yes"])
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"%@",[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2]);
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:petImageData];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 
    // setting the body of the post to the request
    NSDictionary *body = @{@"pet_id": petid};
    NSLog(@"%@",body);
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:body];
    [request setHTTPBody:data];
    
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
   
  //  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BaseURL,@"delete_pet.php"]];
    
   [request setURL:[NSURL URLWithString:urlString]];
    
  
   // NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //
        if (data) {
            NSHTTPURLResponse * httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            if (statusCode == 200) {
                
                NSError* error;
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                    
                    [self.delegate response:json];
                    
                }
                else
                {
                    
                    [self.delegate error:error];
                }

                
                
            }
        }else if (error)
        {
            [self performSelector:@selector(showAlert)];
            
            NSLog(@"%@",error.description);
            
            [self.delegate error:error];

        }
        
    }];
    [dataTask resume];
    
}*/

    // set URL
   //
   // [[NSURLSession alloc] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
   //
   // }];
   /* [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] > 0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }
             else
             {
                 
                 [self.delegate error:error];
             }
             
         }
         else
         {
             [self performSelector:@selector(showAlert)];
             
             NSLog(@"%@",error.description);
             
             [self.delegate error:error];
         }
     }];*/
    

    


-(void)getReferrals
{
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    else
    {
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"get_category.php"];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                        }
                        else
                        {
                            [self.delegate error:error];
                        }
                        
                    }
                    else{
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                    
                }
                else{
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
                
            });
            
        }];
        
        [getDataTask resume];
    }
    
}

-(void) getReferralsDetails
{

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    else
    {
        
        NSString *urlString = [NSString stringWithFormat:@"%@=%@",@"http://www.yourvetsnow.com/petappportal/app/webroot/petsapp/get_referrals.php?category_id",[[NSUserDefaults standardUserDefaults] objectForKey:@"GETReferralsPointsData"]];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                        }
                        else
                        {
                            [self.delegate error:error];
                        }
                        
                    }
                    else{
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                    
                }
                else{
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
                
                
            });
        }];
        [getDataTask resume];
    }
}

// Promotional Code checking

-(void) promotionalCodeChecking:(NSString *) userId :(NSString *) promotionalCode {

    NSLog(@"\nPromotional Code = %@\n\n", promotionalCode);
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    else
    {
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@&%@=%@",BaseURL,@"payment_check.php",@"userId",userId,@"pCode",promotionalCode];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(paymentResponse:)]) {
                            
                            [self.delegate paymentResponse:json];
                            
                        } else {
                            
                            [self.delegate error:error];
                        }
                        
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                    
                }
                
            });
            
        }];
        
        [getDataTask resume];
    }
}

-(void) paymentRecieved: (NSString *) userId: (NSString *) promotionalCode : (NSString*) selecetdPackageID {
    
    NSLog(@"\nPromotional Code = %@\n\n", promotionalCode);
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@&%@=%@&%@=%@",BaseURL,@"payment_received.php",@"userId",userId,@"pCode",promotionalCode, @"package_id",selecetdPackageID];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:30];
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                    
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
        
            });

        }];
        
        [getDataTask resume];
    }
}

-(void)paymentAuthentication: (NSString *) userId {

    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
    
        [self.delegate error:error];
    }
    else
    {
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@",BaseURL,@"payment_authentication.php",@"userId",userId];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:30];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(paymentResponse:)]) {
                            [self.delegate paymentResponse:json];
                        
                        } else {
                            
                            [self.delegate error:error];
                        }
                        
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
            });
        }];
        
        [getDataTask resume];
    }
}


- (void)postPictureMessage:(NSData *)imageData dataDict:(NSMutableDictionary*)dataDict{
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"insert_message.php"];
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
        
        //Set Params
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:50];
        [request setHTTPMethod:@"POST"];
        
        NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
        
        NSString *intervalString = [NSString stringWithFormat:@"%f", today];
        //Create boundary, it can be anything
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSString *string2=[intervalString stringByAppendingFormat:@"%@",@".jpg"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in dataDict) {
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [dataDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file",string2] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:imageData];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        // set URL
        [request setURL:[NSURL URLWithString:urlString]];
        NSURLSessionDataTask *postData = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }else{
                        [self performSelector:@selector(showAlert)];
                        
                        NSLog(@"%@",error.description);
                        
                        [self.delegate error:error];
                    }
                    
                }
                
                else {
                    
                    [self performSelector:@selector(showAlert)];
                    
                    NSLog(@"%@",error.description);
                    
                    [self.delegate error:error];
                    
                }
                
            });
        }];
        
        [postData resume];
        
    }
}

-(void)uploadImageOrVideo:(NSData *)imageData filename:(NSString *)filename videoOrImage:(NSString *)videoOrImage{
    //
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    // NSString *urlString = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",BaseURL,@"image_upload.php?user_id=",userId,@"&",K_IMAGE_OR_VIDEO,@"=",videoOrImage];
    //
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:120.0];
    //    [request setHTTPMethod:@"POST"];
    //    NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
    //
    //    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    //    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    //
    //    NSMutableData *body = [NSMutableData data];
    //    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //    [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n",filename]] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //    [body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [body appendData:[NSData dataWithData:imageData]];
    //    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [request setHTTPBody:body];
    //
    //    [NSURLConnection
    //     sendAsynchronousRequest:request
    //     queue:[NSOperationQueue mainQueue]
    //     completionHandler:^(NSURLResponse *response,
    //                         NSData *data,
    //                         NSError *error)
    //     {
    //         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    //
    //         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
    //         {
    //             NSError* error;
    //
    //             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    //
    //             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
    //
    //                 [self.delegate response:json];
    //
    //             }else{
    //
    //                 [self.delegate error:error];
    //             }
    //
    //         }else{
    //             [self performSelector:@selector(showAlert)];
    //             NSLog(@"%@",error.description);
    //             [self.delegate error:error];
    //         }
    //     }];
}
-(void)getProfileInfo:(NSString *)userId{
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@",BaseURL,@"get_profile.php",USERID,userId];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request setTimeoutInterval:25];
        
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                    
                }
                
            });
            
        }];
        
        [getDataTask resume];
    }
}

-(void)getMessages :(NSString *)userId {
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@",BaseURL,@"get_messages.php",@"userId",userId];
        
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        // request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
        
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }
                        else
                        {
                            
                            [self.delegate error:error];
                        }
                        
                    }
                    else
                    {
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                }
                
                else
                {
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                    
                    
                }
                
            });
            
        }];
        
        [getDataTask resume];
    }
    
    
}

-(void)postMessage :(NSDictionary*) messageDict {
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        NSError *error=[NSError errorWithDomain:@"" code:195 userInfo:nil];
        
        [self.delegate error:error];
    }
    
    else
    {
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        ;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        
        
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"insert_message.php"];
        
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
        
        request.HTTPMethod = @"POST";
        //setting header fields
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",@"messageId",[messageDict objectForKey:@"messageId"],@"userId",[messageDict objectForKey:@"userId"],@"messageText",[messageDict objectForKey:@"messageText"],@"messageBy",[messageDict objectForKey:@"messageBy"],@"petId",[messageDict objectForKey:@"petId"]];
        
        //Convert your data and set your request's HTTPBody property
        NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPBody = requestBodyData;
        //
        //    [NSURLConnection
        //     sendAsynchronousRequest:request
        //     queue:[NSOperationQueue mainQueue]
        //     completionHandler:^(NSURLResponse *response,
        //                         NSData *data,
        //                         NSError *error)
        //     {
        //         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        NSURLSessionDataTask *postMessagestask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
                    
                    
                    if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
                    {
                        NSError* error;
                        
                        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                            
                            [self.delegate response:json];
                            
                        }else{
                            
                            [self.delegate error:error];
                        }
                        
                    }else{
                        [self performSelector:@selector(showAlert)];
                        NSLog(@"%@",error.description);
                        [self.delegate error:error];
                    }
                    
                    
                }
                else{
                    [self performSelector:@selector(showAlert)];
                    NSLog(@"%@",error.description);
                    [self.delegate error:error];
                }
                
            });
            
        }];
        [postMessagestask resume];
    }
}

-(void)updateUserName:(NSString*)username {
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"profile_update.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    
    //setting header fields
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",USERNAME,username,USERID,userid];
    
    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}




-(void)getContacts{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"approved_friends.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //
    ////    DefaultsManager *manager = [DefaultsManager getSingleton];
    ////    NSString *userid = [manager getUserId];
    //
    //    //TODO:
    //  //  NSString *postData = [NSString stringWithFormat:@"%@=%@",USERID,userid];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}

-(void)getPendingPeople{
    
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"friend_request_pending.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    ////    DefaultsManager *manager = [DefaultsManager getSingleton];
    ////    NSString *userid = [manager getUserId];
    ////
    ////    //TODO:
    ////    NSString *postData = [NSString stringWithFormat:@"%@=%@",USERID,userid];
    ////
    ////    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}





-(void)searchUser:(NSString*)txtToSearch{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"search_user.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *strId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",SEARCH_TEXT,txtToSearch,USERID,strId];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}


-(void)forgotPassword:(NSString *)email {
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"recover_pass.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    
    //TODO:
    NSString *postData = [NSString stringWithFormat:@"%@=%@",EMAIL,[email lowercaseString]];
    
    //Convert your data and set your request's HTTPBody property
    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}





-(void)sendFriendRequest:(NSString *)toId{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"send_friend_request.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *fromId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",REQUESTTO,toId,REQUESTFROM,fromId];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}



-(void)updateName:(NSString *)name{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"profile_update.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",USERID,userId,NAME,name];
    
    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}



-(void)updateStatus:(NSString *)status
{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"status_update.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",USERID,userId,STATUS,status];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}










-(void)getSettings{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"settings_info.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@",USERID,userId];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}





-(void)getTimeLine{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"get_feeds.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@",USERID,userId];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    //    [NSURLConnection
    //     sendAsynchronousRequest:request
    //     queue:[NSOperationQueue mainQueue]
    //     completionHandler:^(NSURLResponse *response,
    //                         NSData *data,
    //                         NSError *error)
    //     {
    //         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    //
    //         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
    //         {
    //             NSError* error;
    //
    //             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    //
    //             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
    //
    //                 [self.delegate response:json];
    //
    //             }else{
    //
    //                 [self.delegate error:error];
    //             }
    //
    //         }else{
    //             [self performSelector:@selector(showAlert)];
    //             NSLog(@"%@",error.description);
    //             [self.delegate error:error];
    //         }
    //     }];
}






-(void)updateSettings:(NSString *)key value:(NSString *)value{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"profile_update.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",USERID,userId,key,value];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}





-(void)updateNotification:(NSString *)status{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"update_notification.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",USERID,userId,K_NOTIFICATION,status];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}


-(void)updateApproveDeclineRequest:(NSString*)requestFromId st_status:(NSString*)st_status{
    NSLog(@"HElloWorld3");
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"friend_approve_decline.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userid = [manager getUserId];
    //
    //    //TODO:
    //    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@",REQUESTTO,userid,REQUESTFROM,requestFromId,STATUS,st_status];
    //
    //    //Convert your data and set your request's HTTPBody property
    //    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    //    request.HTTPBody = requestBodyData;
    //
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             [self performSelector:@selector(showAlert)];
             NSLog(@"%@",error.description);
             [self.delegate error:error];
         }
     }];
}


-(void)uploadMultipleImagesFromProfile:(NSData *)thumbnail CoverPhoto:(NSData *)coverImage {
    
    //    DefaultsManager *manager = [DefaultsManager getSingleton];
    //    NSString *userId = [manager getUserId];
    //
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"cover_photo_file.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:100.0];
    
    NSMutableData* body = [NSMutableData data];
    
    NSString* boundary = @"---------------------------14737809831466499882746641449";
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding];
    
    // I use special simple class to distinguish file params
    
    [body appendData:boundaryData];
    
    // File upload
    [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n\r\n", @"thumb", @"imagefile"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData: thumbnail]; // It just return NSData with loaded file in it
    [body appendData: [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:boundaryData];
    
    [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n\r\n", @"cover", @"imagefile"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData: coverImage]; // It just return NSData with loaded file in it
    [body appendData: [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:boundaryData];
    
    // Regular param
    //   [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@\r\n", @"user_id", userId] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //  [request setValue:[NSString stringWithFormat:@"%d", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:body];
    
    
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
             //
             //                 [self.delegate response:json];
             //
             //             }else{
             //
             //                 [self.delegate error:error];
             //             }
             //
         }else{
             //             [self performSelector:@selector(showAlert)];
             //             NSLog(@"%@",error.description);
             //             [self.delegate error:error];
         }
     }];
}









#pragma mark UpDate Setting

-(void)settingButton:(NSString*)paymentMethod commission:(NSString*)com
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"payment/settings/set"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSString *id1=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    NSString *key1=[[NSUserDefaults standardUserDefaults] objectForKey:@"key"];
    
    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@",ID,id1,KEY,key1,PaymentMethod,paymentMethod,COMISSION,com];
    
    //Convert your data and set your request's HTTPBody property
    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             
             [self.delegate error:error];
         }
     }];
}

//Call on Welcome screen every time.

#pragma mark - Lunch Settings
-(void)launchSettings {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"account/settings"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    
    //setting header fields
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSString *id1=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    NSString *key1=[[NSUserDefaults standardUserDefaults] objectForKey:@"key"];
    
    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@",ID,id1,KEY,key1];
    
    //Convert your data and set your request's HTTPBody property
    NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             
             [self.delegate error:error];
         }
     }];
}

//https://api101.mondotalk.com/mondomate/v1/account/settings


#pragma mark - invite people manually
-(void)invitePeople:(NSString*)fname Phone:(NSString*) phoneNumber Email:(NSString*)email1
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"friend/add"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *personData = @{ NAME:fname, PHONE:phoneNumber, EMAIL:email1};
    NSArray *personDataArray = @[personData];
    NSDictionary *friendsDictionary = @{@"friends":personDataArray};
    
    NSError *error;
    NSMutableData *jsonData = [[NSJSONSerialization dataWithJSONObject:friendsDictionary
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error] mutableCopy];
    
    NSString *requestJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",requestJson);
    
    NSString *id1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    NSString *key1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"key"];
    
    NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@",@"friends",requestJson,ID,id1,KEY,key1];
    
    NSData *credentials = [postData dataUsingEncoding:NSUTF8StringEncoding];
    
    request.HTTPBody = credentials;
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             NSError* error;
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
                 
                 [self.delegate response:json];
                 
             }else{
                 
                 [self.delegate error:error];
             }
             
         }else{
             
             [self.delegate error:error];
         }
         
     }];
}


#pragma mark - invite people from contacts Button
-(void)invitePeopleFromContacts
{
    /*
     NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"payment/settings/set"];
     
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
     
     request.HTTPMethod = @"POST";
     //setting header fields
     [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
     
     NSString *id1=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
     NSString *key1=[[NSUserDefaults standardUserDefaults] objectForKey:@"key"];
     
     
     NSString *postData = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@",ID,id1,KEY,key1,PAYMENT,payment,COMMISSION,com];
     
     //Convert your data and set your request's HTTPBody property
     NSData *requestBodyData = [postData dataUsingEncoding:NSUTF8StringEncoding];
     request.HTTPBody = requestBodyData;
     
     [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response,
     NSData *data,
     NSError *error)
     {
     NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
     
     if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
     {
     NSError* error;
     
     id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
     
     if (self.delegate && [self.delegate respondsToSelector:@selector(response:)]) {
     
     [self.delegate response:json];
     
     }else{
     
     [self.delegate error:error];
     }
     
     }else{
     
     [self.delegate error:error];
     }
     }];
     
     */
    
}

-(void) showAlert
{
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Request Timed Out" message:@"Please check your internet connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [av show];
}



@end
