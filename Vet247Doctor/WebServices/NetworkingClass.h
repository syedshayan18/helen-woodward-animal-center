//
//  NetworkingClass.h
//  MondoTalk
//
//  Created by Apple on 12/11/14.
//  Copyright (c) 2014 Eiconix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@protocol NetworkDelegate <NSObject>

-(void)response:(id)data;
-(void)error:(NSError*)error;
-(void)paymentResponse:(id)data;

@end

@interface NetworkingClass : NSObject<NSURLConnectionDelegate>
//baseurl
//http://yourvetsnow.com/petappportal/petsapp/
#define TestURL @"http://yourvetsnow.com/petappportal/petsapp/"
#define BaseURL @"http://yourvetsnow.com/petappportal/petsapp/"

#define ImageResourcesURL @"http://yourvetsnow.com/petappportal/app/webroot/img/"

#define userprofileURL @"http://yourvetsnow.com/petappportal/app/webroot/img/profile/"

#define promotionalForPaymentURL @"http://yourvetsnow.com/petappportal/petsapp/payment_check.php?userId=%&pCode=%"

@property(nonatomic,weak) id delegate;

@property(nonatomic,retain) NSMutableData * responseData;

@property AppDelegate *appDel;

+(id)getSharedNetworkObject;

-(void)signUp:(NSMutableDictionary*)dataDict;

-(void)signIn:(NSString*)Email Password:(NSString*)paswrd;

-(void)getProfileInfo:(NSString *)userId;

-(void)setProfile:(NSData *)imageData dataDict:(NSDictionary*)dataDict;

-(void) savePetProfileInfo:(NSData *) petImageData:(NSMutableDictionary*) petData;
+(void)request:(NSString*)url parameters:(NSDictionary*)parametersDictionary completion:(void (^)(id finished, NSError* error))completion ;
+(void)request2:(NSString*)url param:(NSDictionary*)paramDict completion:(void (^)(id finished, NSError* error))completion ;
+(void)historyrequest:(NSString*)deleteurl params:(NSDictionary*)parametersDictionary completion:(void (^)(id finished, NSError* error))completion;
+(void)getuserlostnfound:(NSString *)url param:(NSMutableDictionary*)userID completion:(void (^)(id finished,NSError *error))completion;
+(void)deletemylostnfound:(NSString*)url completion:(void (^)(id finished, NSError* error))completion;
+(void)getlostnfound:(NSString*)url completion:(void (^)(id finished, NSError* error))completion;

+(void)logout:(NSString *)url param:(NSMutableDictionary*)paramDict completion:(void (^)(id finished,NSError *error))completion;
+(void)getConversations:(NSString*)url completion:(void (^)(id finished,  NSURLSessionDataTask *resp, NSError* error))completion;
+(void)getchatmessages:(NSString*)url completion:(void (^)(id finished, NSError* error))completion;
+(void)sendchatmessages:(NSString *)url param:(NSMutableDictionary*)body completion:(void (^)(id finished,NSError *error))completion;
-(void)reportlostpet:(NSString *)url param:(NSMutableDictionary*)paramDict completion:(void (^)(id finished,NSError *error))completion;

+(void)Userlatlongpostrequest:(NSString*)posturl params:(NSDictionary*)body completion:(void (^)(id finished, NSError* error))completion;
+(void)getprofileinfo:(NSString*)url completion:(void (^)(id finished, NSError* error))completion;
-(void)getMessages :(NSString *)userId;

-(void)deletepet :(NSString *)petid;

-(void)postMessage :(NSDictionary*) messageDict;

- (void)postPictureMessage:(NSData *)imageData dataDict:(NSMutableDictionary*)dataDict;

-(void)getPets:(NSString*)userId;

-(void)getReferrals;

-(void) getReferralsDetails;

-(void) paymentRecieved: (NSString *) userId: (NSString *) promotionalCode : (NSString*) selecetdPackageID;

-(void) paymentAuthentication: (NSString *) userId;

-(void) promotionalCodeChecking:(NSString *) userId: (NSString *) promotionalCode;

-(void)settingButton:(NSString*)payment commission:(NSString*)com;

-(void)invitePeople:(NSString*)fname Phone:(NSString*) phoneNumber Email:(NSString*)email1;

-(void)invitePeopleFromContacts;

-(void)updateUserName:(NSString*)username;

-(void)updateApproveDeclineRequest:(NSString*)requestFromId st_status:(NSString*)st_status;

-(void)getContacts;

-(void)getPendingPeople;

-(void)searchUser:(NSString*)txtToSearch;

-(void)sendFriendRequest:(NSString *)toId;

-(void)uploadImageOrVideo:(NSData *)imageData filename:(NSString *)filename videoOrImage:(NSString*)videoOrImage;

-(void)uploadMultipleImagesFromProfile:(NSData *)thumbnail CoverPhoto:(NSData *)coverImage;

-(void)updateName:(NSString *)name;

-(void)updateStatus:(NSString *)status;

-(void)getSettings;

-(void)getTimeLine;

-(void)updateSettings:(NSString*)key value:(NSString*)value;

-(void)updateNotification:(NSString *)status;

-(void)forgotPassword:(NSString *)email;

-(void)launchSettings;

@end
