//
//  ReferralsInfo.h
//  Vet247Doctor
//
//  Created by APPLE on 8/22/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

//#define CATEGORY_Name :@"category_name"

#define COMPANY_Name @"company_name"

#define COMPANY_Address @"address"

#define COMPANY_Phone @"phone_no"

#define COMPANY_Fax @"fax"

#define COMPANY_Email @"email"

#define COMPANY_Web @"web_address"

#define About_COMPANY @"about"

#define NewAbout_COMPANY @"new"

#define COMPANY_Profile @"profile_pic"

#define LATITUDE @"lat"

#define LONGITUDE @"long"

#define THUMBNAILLeft @"pic_one"

#define THUMBNAILRIGHT @"pic_second"

//"id": "86",
//"category_name": "Emergency Clinics ",
//"company_name": "Willowdale Animal Hospital Toronto",
//"address": "256 Sheppard Ave W, North York, ON M2N 1N3",
//"phone_no": "(416) 222-5409",
//"fax": "",
//"email": "",
//"web_address": "http://www.willowdaleanimalhospital.com/",
//"about": "",
//"new": "",
//"profile_pic": "http://yourvetsnow.com/petappportal/app/webroot/img/referral/",
//"lat": "43.758774",
//"long": "-79.423332"
//"pic_one": "http://yourvetsnow.com/petappportal/app/webroot/img/referral/"
//"pic_second":
//http://yourvetsnow.com/petappportal/app/webroot/img/referral/g

#import <Foundation/Foundation.h>

@interface ReferralsInfo : NSObject

@property (strong, nonatomic) NSString * compnayName;

@property (strong, nonatomic) NSString * companyAddress;

@property (strong, nonatomic) NSString * companyPhone;

@property (strong, nonatomic) NSString * companyFax;

@property (strong, nonatomic) NSString * companyEmail;

@property (strong, nonatomic) NSString * companyWeb;

@property (strong, nonatomic) NSString * aboutCompany;

@property (strong, nonatomic) NSString * whatNewAboutCompany;

@property (strong, nonatomic) NSString * profilePicture;

@property double longitude;

@property double latitude;

@property (strong, nonatomic) NSString * thumbNailLeft;

@property (strong, nonatomic) NSString * thumbNailRight;

@end
