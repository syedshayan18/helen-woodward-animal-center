//
//  AreaLostnFoundCollectionViewCell.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/11/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaLostnFoundCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_pet;
@property (weak, nonatomic) IBOutlet UILabel *lbl_animalname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tag;
@property (weak, nonatomic) IBOutlet UILabel *lbl_animalage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sex;
@property (weak, nonatomic) IBOutlet UILabel *lbl_breed;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lostdate;

@end
