//
//  ZZMainViewController.m
//  PayPal-iOS-SDK-Sample-App
//
//  Copyright (c) 2014, PayPal
//  All rights reserved.
//

#import "ZZMainViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DashboardController.h"
#import "AppDelegate.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "PromotionalCodeUtills.h"
#import "DashboardViewController.h"

// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.

#define kPayPalEnvironment PayPalEnvironmentProduction;

#define PermotionalBonusKEY @"))))))PermotionalBonusAvailable(((("

#define PermotionalBonusAvailable @"YES"

#define PermotionalBonusNotAvailable @"NO"

@interface ZZMainViewController ()

@property(nonatomic, strong, readwrite) IBOutlet UIButton *payNowButton;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payFutureButton;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@end

@implementation ZZMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    //livecleintid
    //AVtFn-Lyn9pjbys4PpxRuoJuTkYXaSjrhiLnZzSD_Wrj4SoHvTbBZElZBpDyxpdkKWzi8VThZgb0A8Ge
    
    //testcleintid
    //AegJE-R69_HSEcvDjfwZhQWkUvxUcLiAvMWmHxPS6qiiOekg8fgFsiXE6xKKKoxBCTi6gvZWyvPx7n3k
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AVtFn-Lyn9pjbys4PpxRuoJuTkYXaSjrhiLnZzSD_Wrj4SoHvTbBZElZBpDyxpdkKWzi8VThZgb0A8Ge"}];
    
    // use default environment, should be Production in real life
    
    self.environment = kPayPalEnvironment; // Dec 16 ????????????
    
    self.title = @"PayPal";
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    UIView *controllerView = [[UIView alloc] initWithFrame:screenRect];
    
    self.view = controllerView;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
    
    // SandBOX   AegJE-R69_HSEcvDjfwZhQWkUvxUcLiAvMWmHxPS6qiiOekg8fgFsiXE6xKKKoxBCTi6gvZWyvPx7n3k
    
    // LIVE AZNaXeyscmReIkXukmohX04c07vEYabyMKqtdlUo_eQH5WwK_pY_6wnDwt3W7zkQZ4yxbFkBKIFnpNZe
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    self.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    self.successView.hidden = YES;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    
    [self pay];
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];
}
-(void) viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    

}


- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

#pragma mark - Receive Single Payment

- (IBAction)pay
{
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    //       Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    
    // WE NEED TO SAVE THE PAYMENT DATE ON SERVER AND NEED TO CHECK ON DASHBOARD SCREEN FOR ANNUAL SUB SCRIPTION RENEW ....................
    
    NSDecimalNumber *total = nil;
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PermotionalBonusKEY] isEqual:PermotionalBonusAvailable])
//    {
//        total = [[NSDecimalNumber alloc] initWithString:@"98.45"];
//    }
//    else
//    {
//        total = [[NSDecimalNumber alloc] initWithString:@"107.4"];
//    }
    
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundUp
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    

    total = [[NSDecimalNumber decimalNumberWithString:[[NSUserDefaults standardUserDefaults] objectForKey:AmountToBePaidByUser]] decimalNumberByRoundingAccordingToBehavior:roundUp];
    
    
    NSLog(@"\nAmount to be paid::%@",total);
    
//    total = 
    
    NSString *currency = nil;
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedCountry"] isEqualToString:@"Canada"])
    {
        currency = @"CAD";
    }
    else
    {
        currency = @"USD";
    }
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = currency;
    payment.shortDescription = @"Annual Subscription";
    payment.items = nil;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfig delegate:self];
    
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    
    NSLog(@"PayPal Payment Success!");
    
    self.resultText = [completedPayment description];
    
    [self showSuccess];
    
    [self sendCompletedPaymentToServer:completedPayment];
    // Payment was processed successfully; send to server for verification and fulfillment
    
    [self paymentRecived];
    
  [self setDashBoardAsFirstController];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) paymentRecived {
    
    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AppDelegate* appDelegate= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isSelectedAnyPaymentPackage)
    {
        [sendPaymentRecivedNetworkObject paymentRecieved:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode] : [NSString stringWithFormat:@"%i",appDelegate.selectedPaymentPackageID]];
        
    }else
    {
        [sendPaymentRecivedNetworkObject paymentRecieved:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode] :@"0"];
    }
    
    appDelegate.isSelectedAnyPaymentPackage = NO;
    
    [PromotionalCodeUtills setPromotionalCode:@""];
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Message"
                                                         message:@"Payment Successfuly Paid"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"successfully recived"])
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self setDashBoardAsFirstController];
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
    else
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self performSelector:@selector(reCallPaymentRecived) withObject:nil afterDelay:5.0];
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Error"
                                                         message:@"Please Check your internet, We will try in few Seconds!"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                         }];
    
    [paymentSuccessfullyRecieved addAction:ok];
    
    [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    
    [self performSelector:@selector(reCallPaymentRecived) withObject:nil afterDelay:10.0];
}

-(void) reCallPaymentRecived
{
    [self paymentRecived];
}

-(void) setDashBoardAsFirstController
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_Cleared forKey:AnnualSubscriptionPaid_KEY];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLoggin"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    for (int i=0; i <self.navigationController.viewControllers.count; i++){
        if ([self.navigationController.viewControllers[i] isKindOfClass:DashboardViewController.class]){
            
            [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];

    
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    NSLog(@"PayPal Payment Canceled");
    
    //    UIAlertController *alertController = [UIAlertController
    //                                          alertControllerWithTitle:nil
    //                                          message:@"Please Try Again"
    //                                          preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* ok = [UIAlertAction
    //                         actionWithTitle:@"OK"
    //                         style:UIAlertActionStyleDefault
    //                         handler:^(UIAlertAction * action)
    //                         {
    //                             //Do some thing here
    //                             [alertController dismissViewControllerAnimated:YES completion:nil];
    //
    //                         }];
    //
    //    [alertController addAction:ok];
    //
    //    [self presentViewController:alertController animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}


#pragma mark - Authorize Future Payments

- (IBAction)getUserAuthorizationForFuturePayments:(id)sender {
    
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    [self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
    
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    [self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


#pragma mark - Helpers

- (void)showSuccess {
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}

#pragma mark - Flipside View Controller

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushSettings"]) {
        [[segue destinationViewController] setDelegate:(id)self];
    }
}

@end
