//
//  ChatController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/28/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ChatController.h"

#import "SDWebImage/UIImageView+WebCache.h"

#import "ControlsFactory.h"

#import "ChatMessage.h"

#import "NetworkingClass.h"

#import "Define.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import "MBProgressHUD.h"


#define ChatMessageCellIdentifier @"ChatMessageCell"

static float avatarWidth = 35.0f;

static float avatarHeight= 35.0f;

static float messageImageWidth=210.0f;

static float messageImageHeight= 150.0f;

static float offset=30.0f;

@interface ChatController ()


@property (strong, nonatomic) UIView *headerView;

@property (retain,nonatomic) UIView * fullImageView;

@property (strong,nonatomic) UIButton *fullImageBackButton;

@property (strong,nonatomic) UIImageView *fullImage;

@property (strong,nonatomic) UILabel *headerViewlabel;

@property (strong,nonatomic) UIButton *backButton;

@property (strong,nonatomic) UIImageView *footerImageView;

@property (strong,nonatomic) UITableView *chatTableView;

@property (strong,nonatomic) UIButton *sendButton, *cameraButton;

@property (strong,nonatomic) UITextView *messageText;

@property float keyboardHeight;

@property (strong,nonatomic) UITapGestureRecognizer *tapped;

@property CGPoint centerPoint;
@property (strong,nonatomic) NSString *petID;
@end

@implementation ChatController

- (void)viewDidLoad {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    _petID =_pet.PetID;
    [super viewDidLoad];
    
    self.centerPoint=self.view.center;
    
    self.tapped=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageFromDoctor:) name:@"messageFromDoctor" object:nil];

    [self addComponents];
}


- (void) addComponents
{
    [self setUpHeaderView];
    
    [self setUpBackButton];

    [self setupFooter];
    
    [self setupSendButton];
    
    [self setUpCameraButton];
    
    [self setUpTextView];
    
    [self setUpChatTableViewController];
}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];

    self.headerView.backgroundColor=[UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    //self.headerViewlabel.text=self.name;
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    ChatMessage *chatMessage=[self.chatData objectAtIndex:0];
    
    self.headerViewlabel.text=chatMessage.doctorName;
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(230);
        
    }];
    
}

-(void) setUpBackButton {
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);

    self.backButton=[ControlsFactory getButton];
    
    self.backButton.backgroundColor=[UIColor clearColor];
        
    [self.backButton setImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.backButton addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(54);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
    }];
    
}


-(void) setupFooter {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.footerImageView=[ControlsFactory getImageView];

    self.footerImageView.image= [UIImage imageNamed:@"footer.png"];
    
    [self.view addSubview:self.footerImageView];
    
    [self.footerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(padding.right);
        
        make.height.mas_equalTo(42);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
    }];

}

- (void) setupSendButton {
    
    self.sendButton=[ControlsFactory getButton];
    
    self.sendButton.backgroundColor=[UIColor clearColor];
    
    [self.sendButton setImage:[UIImage imageNamed:@"send.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:self.sendButton];
    
    [self.sendButton addTarget:self action:@selector(sendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets   padding = UIEdgeInsetsMake(0, 0, 0, 10);
    
    [self.view addSubview:self.sendButton];
    
    [self.sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.footerImageView.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(30);
        
        make.width.mas_equalTo(39);
        
        make.centerY.mas_equalTo(self.footerImageView.mas_centerY);
    }];
  
}

-(void)setUpCameraButton
{
    self.cameraButton=[ControlsFactory getButton];
    
    self.cameraButton.backgroundColor=[UIColor clearColor];
    
    [self.cameraButton setImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:self.cameraButton];
    
    [self.cameraButton addTarget:self action:@selector(cameraButtonCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets   padding = UIEdgeInsetsMake(10, 10, 0, 10);
    
    [self.view addSubview:self.cameraButton];
    
    [self.cameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.footerImageView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(30);
        
        make.centerY.mas_equalTo(self.footerImageView.mas_centerY);
    }];
    
    
}

-(void) setUpTextView {
    
    self.messageText=[ControlsFactory getTextView];
    
    self.messageText.backgroundColor=[UIColor clearColor];
    
    self.messageText.delegate=self;
    
    self.messageText.text=@"Type Here...";
    
    self.messageText.textColor=[UIColor lightGrayColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(5, 10, 0, 10);
    
    [self.view addSubview:self.messageText];
    
    [self.messageText mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.cameraButton.mas_right).with.offset(padding.left);
        
        make.right.equalTo(self.sendButton.mas_left).with.offset(-padding.right);
        
        make.height.mas_equalTo(35);
        
        make.centerY.mas_equalTo(self.footerImageView.mas_centerY);
    }];
    

}

-(void) setUpChatTableViewController {
    
    self.chatTableView=[[UITableView alloc] init];
    
    //[self.chatTableView addGestureRecognizer:self.tapped];
    
    self.chatTableView.delegate=self;
    
    self.chatTableView.dataSource=self;
    
    self.chatTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    [self.chatTableView registerClass:[ChatMessageCell class]   forCellReuseIdentifier:ChatMessageCellIdentifier];
    
    [self.view addSubview:self.chatTableView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 5, 0);
    
    [self.chatTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(padding.right);
        
        make.top.equalTo(self.headerView.mas_bottom);
        
        make.bottom.equalTo(self.footerImageView.mas_top).with.offset(-padding.bottom);
    }];
    
    self.chatTableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 20.0, 0.0);
    
}
-(void) showRecentMessages
{
    
    int lastRowNumber = [self.chatTableView numberOfRowsInSection:0] - 1;
    
    NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
    
    [self.chatTableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

-(void) backClicked : (UIButton*) button {
    
    [self.navigationController popViewControllerAnimated:YES];
   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"fromChatToMessages" object:self];
    
}

-(void) sendButtonClicked:(UIButton *) button
{
    [self.view endEditing:YES];
    
    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
    
    if ([self.messageText.text isEqualToString:@"Type Here..."]) {
        
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [self sendTextMessage];
    }
    
}

- (void) sendTextMessage
{
    NSMutableDictionary *messageData=[[NSMutableDictionary alloc] init];
    
    [messageData setObject:@"P" forKey:@"messageBy"];
    
    [messageData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
    [messageData setObject:self.messageText.text forKey:@"messageText"];
    
    if ([self.firstChat isEqualToString:@"NO"]) {
        
        [messageData setObject:[[self.chatData objectAtIndex:0] petId] forKey:@"petId"];
        
        [messageData setObject:[[self.chatData objectAtIndex:0] threadId] forKey:@"messageId"];
        
    }
    else{
        
        [messageData setObject:_petID forKey:@"petId"];
        
        [messageData setObject:@"-1" forKey:@"messageId"];
    }
    
    self.messageText.text = @"Type Here...";
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [network postMessage:messageData];
    
}

-(void) cameraButtonCLicked:(UIButton *) button
{
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"Photo Library",@"Camera", nil];
    
        [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    switch (buttonIndex) {
        case 0:
        {
            [self selectFromLibrary];
            
            //[self.messageData addPhotoMediaMessage];
            break;
        }
            
        case 1:
        {
            [self selectFromCamera];
            
            break;
            
        }
          
    }
    
}

- (void) selectFromLibrary {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void) selectFromCamera {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self sendPhotoMessage:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)sendPhotoMessage :(UIImage*) image {
    
    NSMutableDictionary *messageData=[[NSMutableDictionary alloc] init];
    
    [messageData setObject:@"P" forKey:@"messageBy"];
    
    [messageData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
    [messageData setObject:@"Picture Message" forKey:@"messageText"];
    
    if ([self.firstChat isEqualToString:@"NO"]) {
        
        [messageData setObject:[[self.chatData objectAtIndex:0] petId] forKey:@"petId"];
        
        [messageData setObject:[[self.chatData objectAtIndex:0] threadId] forKey:@"messageId"];
        
    }
    else{
        
        [messageData setObject:_petID forKey:@"petId"];
        
        [messageData setObject:@"-1" forKey:@"messageId"];
    }
    
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    NSData* data = UIImageJPEGRepresentation(image,0.4);
    
    [network postPictureMessage:data dataDict:messageData];
    
}



- (void)didReceiveMemoryWarning {
 
    [super didReceiveMemoryWarning];
 
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self showRecentMessages];
}
#pragma mark TextView Delegates
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.messageText.text=@"";
    
    self.messageText.textColor=[UIColor blackColor];
    
    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y-self.keyboardHeight-38.0f);
    
    //NSDictionary* keyboardInfo = [notification userInfo];
   // self.view.center=(self.centerPoint.x,);

}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([self.messageText.text isEqualToString:@""]) {
        
        self.messageText.text = @"Type Here...";
        
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    
    [textView resignFirstResponder];

    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
    
}

- (void) keyboardWillShow:(NSNotification *)note {
    
    NSDictionary *userInfo = [note userInfo];
    
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
   // NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    
    //self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y-kbSize.height-38.0f);
    
     self.keyboardHeight=kbSize.height;
    
    self.navigationController.view.backgroundColor=[UIColor whiteColor];

}


-(void) messageFromDoctor:(NSNotification *)note {
    
    NSDictionary *data=[note userInfo];
    
    if ([[data objectForKey:@"threadId"] isEqualToString:[[self.chatData objectAtIndex:0] threadId]]) {
        
        NSLog(@"MEssage Arrive at the right Screen");
        
        ChatMessage *chatMessage=[[ChatMessage alloc] init];
        
        chatMessage.messageId=[data objectForKey:@"messageId"];
        
        chatMessage.doctorName=[data objectForKey:@"docorName"];
        
        chatMessage.doctorImage=[data objectForKey:@"doctorImage"];
        
        chatMessage.message=[data objectForKey:@"message"];
        
        chatMessage.messageBy=[data objectForKey:@"messageBy"];
        
        chatMessage.messageDate=[data objectForKey:@"messageCreated"];
        
        chatMessage.messageImageLink=[data objectForKey:@"messageImage"];
        
        chatMessage.petId=[data objectForKey:@"petId"];
        
        chatMessage.threadId=[data objectForKey:@"threadId"];
        
        chatMessage.threadStatus=[data objectForKey:@"threadStatus"];
        
        [self.chatData addObject:chatMessage];
        
        [self.chatTableView reloadData];
        
        [self showRecentMessages];
    }
}

#pragma mark -
#pragma mark Table view delegates
static CGFloat padding = 20.0;


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.firstChat isEqualToString:@"YES"]) {
        
        return 1;
    
    }
    else
    {
    
        return [self.chatData count];
    
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.firstChat isEqualToString:@"YES"]) {
        
        ChatMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier forIndexPath:indexPath];
        
        return cell;

    }
    else
    {
        ChatMessage *messageData =  [self.chatData objectAtIndex:indexPath.row];
        
        //if ([[messageData messageImageLink] isEqualToString:@"http://invisionsolutions.ca/vet247/app/webroot/img/message/"])
        if ([[messageData messageImageLink] isEqualToString:[NSString stringWithFormat:@"%@%@",ImageResourcesURL,@"message/"]
             ])
        {
            ChatMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier forIndexPath:indexPath];
            
            cell=[self clearCell:cell];
            
            cell= [self createTextMessageCell:messageData messageCellForIndexPath:cell];
            
            return cell;
            
        }
     
        else
        {
            ChatMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier forIndexPath:indexPath];
            
            cell=[self clearCell:cell];
            
            cell=[self createPictureMessageCell:messageData messageCellForIndexPath:cell];
            
            return cell;
            
        }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.firstChat isEqualToString:@"YES"]) {
        
        return 50.0f;
    }
    else
    {
        
        ChatMessage *messageData =  [self.chatData objectAtIndex:indexPath.row];
        
       // if ([[messageData messageImageLink] isEqualToString:@"http://invisionsolutions.ca/vet247/app/webroot/img/message/"])
        if ([[messageData messageImageLink] isEqualToString:[NSString stringWithFormat:@"%@%@",ImageResourcesURL,@"message/"]
             ])

        {
            
            NSString *sender = [messageData messageBy];
            
            NSString *message = [messageData message];
            //    NSString *time = [s objectForKey:@"time"];
            
            UILabel *labelSize = [[UILabel alloc] init];
            
            [labelSize setFont:[UIFont systemFontOfSize:14.0]];
            
            labelSize.text = message;
            
            
            labelSize.numberOfLines = 0;
            
            CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-offset-avatarWidth, 9999); // this width will be as per your requirement
            
            CGSize size = [labelSize sizeThatFits:maximumLabelSize];
            
            size.width += (padding/2);
            
            UITextView *dummy=[[UITextView alloc] init];
            
            UIImageView *dummyImageView=[[UIImageView alloc] initWithFrame:CGRectZero];
            
            if ([sender isEqualToString:@"D"])
            {
                
                
                dummy.text=message;
                
                [dummy setFont:[UIFont systemFontOfSize:14]];
                
                [dummy setFrame:CGRectMake(avatarWidth + padding, padding*2, size.width, size.height)];
                
                
                
                [dummyImageView setFrame:CGRectMake( dummy.frame.origin.x - padding/2,
                                                    dummy.frame.origin.y - padding/4,
                                                    size.width+padding/2,
                                                    size.height+padding)];
                
//                NSLog(@"Content View Frame is : %@",NSStringFromCGRect(dummy.frame));
//                
//                NSLog(@"BG image  Frame is : %@",NSStringFromCGRect(dummyImageView.frame));
                
                
            }
            else{
                
                [dummy setFrame:CGRectMake(self.view.frame.size.width - size.width - padding,
                                           padding*2,
                                           size.width,
                                           size.height)];
                
               // NSLog(@"Content View Frame is : %@",NSStringFromCGRect(dummy.frame));
                
                [dummyImageView setFrame:CGRectMake(dummy.frame.origin.x - padding/2,
                                                    dummy.frame.origin.y - padding/4,
                                                    size.width+padding,
                                                    size.height+padding)];
                
               // NSLog(@"BG image  Frame is : %@",NSStringFromCGRect(dummyImageView.frame));
                
            }
            
            CGFloat height= dummyImageView.frame.size.height+dummyImageView.frame.origin.y;
            
            return  height;
            
        }
        
        return messageImageHeight+25+padding;

    }
    
        
       
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self.chatTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing: YES];
    
    ChatMessage * chatMessageData = (ChatMessage *) [self.chatData objectAtIndex:indexPath.row];
    
    NSLog(@"\n\nImage URL : %@\n\n", [chatMessageData messageImageLink]);
    
    if([[chatMessageData messageImageLink] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/message/"] || [chatMessageData messageImageLink] == nil)
    {
        return;
    }
    else
    {
        [self setUpShowFullImageView:[chatMessageData messageImageLink]];
    }
    
    
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

// show full imageView

-(void)setUpShowFullImageView:(NSString * ) fullImageUrlString
{
    self.fullImageView = [ControlsFactory getView];
    
    self.fullImageView.backgroundColor = [UIColor blackColor];
    
    UIEdgeInsets paddingForFullImageView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview : self.fullImageView];
    
    [self.fullImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForFullImageView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForFullImageView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForFullImageView.top);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(paddingForFullImageView.bottom);
        
    }];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.fullImage = [ControlsFactory getImageView];

    [self.fullImageView addSubview:self.fullImage];
    
    [self.fullImage setImageWithURL:[NSURL URLWithString:fullImageUrlString] placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    [self.fullImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.fullImageView.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.fullImageView.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.fullImageView.mas_top).with.offset(padding.top);
        
        make.bottom.equalTo(self.fullImageView.mas_bottom).with.offset(padding.bottom);
    }];
    
    self.fullImage.contentMode = UIViewContentModeScaleAspectFit;
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    self.fullImageBackButton = [ControlsFactory getButton];
    
    self.fullImageBackButton.backgroundColor = [UIColor clearColor];
    
    [self.fullImageBackButton setImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    
    self.fullImageBackButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.fullImageBackButton addTarget:self action:@selector(fullImageBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.fullImageView addSubview:self.fullImageBackButton];
    
    [self.fullImageBackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.fullImage.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(54);
        
        make.bottom.equalTo(self.fullImage.mas_top).with.offset(80);
    }];

}

-(void) fullImageBackButtonClicked:(UIButton *) button
{
    [self.fullImageView removeFromSuperview];
}

#pragma mark Creation of Cells


- (ChatMessageCell *)createTextMessageCell:(ChatMessage*)messageData messageCellForIndexPath:(ChatMessageCell*)cell
{
    NSString *sender = [messageData messageBy];
    
    NSString *message = [messageData message];
    
    UILabel *labelSize = [[UILabel alloc] init];
    
    [labelSize setFont:[UIFont systemFontOfSize:14]];
    
    labelSize.text = message;
    
    labelSize.numberOfLines = 0;
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-offset-avatarWidth, 9999); // this width will be as per your requirement
    
    CGSize size = [labelSize sizeThatFits:maximumLabelSize];
    
    size.width += (padding/2);
    
    cell.messageContentView.text = message;
    
    cell.messageContentView.textContainer.lineBreakMode=NSLineBreakByWordWrapping;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.userInteractionEnabled = NO;
    
    UIImage *bgImage = nil;
    
    [cell.messageContentView setFont:[UIFont systemFontOfSize:14]];
    
    if ([sender isEqualToString:@"D"]) { // left aligned
        
        cell.avatarImage.frame=CGRectZero;
        
        cell.avatarImage.image=nil;
        
        cell.avatarImage.image=[UIImage imageNamed:@"doctor.png"];
        
        bgImage = [[UIImage imageNamed:@"orange.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        
        [cell.messageContentView setFrame:CGRectMake(padding+avatarWidth, padding*2, size.width, size.height+20)];
        
        [cell.bgImageView setFrame:CGRectMake( cell.messageContentView.frame.origin.x - padding/2,
                                              cell.messageContentView.frame.origin.y - padding/8,
                                              size.width+padding/2,
                                              size.height+padding)];
        
        cell.avatarImage.frame=CGRectMake(cell.bgImageView.frame.origin.x-avatarWidth, cell.bgImageView.frame.size.height, avatarWidth, avatarHeight);
        
        
//        NSLog(@"Content View Frame is : %@",NSStringFromCGRect(cell.messageContentView.frame));
//        
//        NSLog(@"BG image  Frame is : %@",NSStringFromCGRect(cell.bgImageView.frame));
        
    } else {
        
        cell.avatarImage.frame=CGRectZero;
        
        cell.avatarImage.image=nil;
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]) {
         
            cell.avatarImage.image=[UIImage imageNamed:@"personIcon.png"];

        }
        
        else
        {
            
            cell.avatarImage.image=[UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]];

            cell.avatarImage.layer.cornerRadius=17.5;
            
            cell.avatarImage.layer.masksToBounds=YES;
        }
        
        

        
        bgImage = [[UIImage imageNamed:@"aqua.png"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
        
        [cell.messageContentView setFrame:CGRectMake(self.view.frame.size.width - size.width - avatarWidth-padding/2,
                                                     padding*2,
                                                     size.width,
                                                     size.height+20)];
        
        //NSLog(@"Content View Frame is : %@",NSStringFromCGRect(cell.messageContentView.frame));
        
        [cell.bgImageView setFrame:CGRectMake(cell.messageContentView.frame.origin.x -padding/4,
                                              cell.messageContentView.frame.origin.y - padding/8,
                                              size.width+padding/2,
                                              size.height+padding)];
        
        cell.avatarImage.frame=CGRectMake(cell.bgImageView.frame.size.width+cell.bgImageView.frame.origin.x, cell.bgImageView.frame.size.height, avatarWidth, avatarHeight);
        
       // NSLog(@"BG image  Frame is : %@",NSStringFromCGRect(cell.bgImageView.frame));
        
        
    }
    
    cell.senderAndTimeLabel.text=[messageData messageDate];
    
    cell.bgImageView.image = bgImage;
    
    return cell;
    
}

- (ChatMessageCell *)createPictureMessageCell:(ChatMessage*)messageData messageCellForIndexPath:(ChatMessageCell*)cell
{
    
    if ([messageData.messageBy isEqualToString:@"P"]) {
        
        cell.messageImage.frame=CGRectMake(self.view.frame.size.width-messageImageWidth-avatarWidth-2*padding, padding, messageImageWidth, messageImageHeight);
        
        cell.messageImage.contentMode=UIViewContentModeScaleToFill;
        
       // [cell.messageImage sd_setImageWithURL:[NSURL URLWithString:[messageData messageImageLink]]
                          //   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        [cell.messageImage setImageWithURL:[NSURL URLWithString:[messageData messageImageLink]] placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        
        cell.avatarImage.frame=CGRectMake(self.view.frame.size.width-avatarWidth-padding*.25, cell.messageImage.frame.size.height, avatarWidth, avatarHeight);
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]) {
            
            cell.avatarImage.image=[UIImage imageNamed:@"personIcon.png"];
            
        }
        
        else
        {
            
            cell.avatarImage.image=[UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]];
            
            cell.avatarImage.layer.cornerRadius=17.5;
            
            cell.avatarImage.layer.masksToBounds=YES;
        }

        
    }
    
    else if ([messageData.messageBy isEqualToString:@"D"])
    {
        cell.messageImage.frame=CGRectMake(2*padding+avatarWidth, padding, messageImageWidth, messageImageHeight);
        
        cell.messageImage.contentMode=UIViewContentModeScaleToFill;
        
        //[cell.messageImage sd_setImageWithURL:[NSURL URLWithString:[messageData messageImageLink]]
          //                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        [cell.messageImage setImageWithURL:[NSURL URLWithString:[messageData messageImageLink]] placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];

        
        cell.avatarImage.frame=CGRectMake(padding*.5, cell.messageImage.frame.size.height, avatarWidth, avatarHeight);
        
        cell.avatarImage.image=[UIImage imageNamed:@"doctor.png"];
    }
    
    cell.senderAndTimeLabel.text=[messageData messageDate];
    
    return cell;
}
- (ChatMessageCell *)clearCell:(ChatMessageCell*)cell{
    
    cell.senderAndTimeLabel.text=@"";
    
    cell.messageImage.image=nil;
    
    cell.bgImageView.image=nil;
    
    cell.avatarImage.image=nil;
    
    cell.messageContentView.text=@"";
    
    //cell.
    return cell;
}

#pragma mark Server Response

-(void)response:(id)data {
    
     [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
        
        if ([self.firstChat isEqualToString:@"YES"]) {
            
            self.chatData=[[NSMutableArray alloc] init];
        }
        self.firstChat=@"NO";
        
        ChatMessage *chatMessage=[[ChatMessage alloc] init];
        
        chatMessage.messageId=[data objectForKey:@"messageId"];
        
        chatMessage.doctorName=[data objectForKey:@"docorName"];
        
        chatMessage.doctorImage=[data objectForKey:@"doctorImage"];
        
        chatMessage.message=[data objectForKey:@"message"];
        
        chatMessage.messageBy=[data objectForKey:@"messageBy"];
        
        chatMessage.messageDate=[data objectForKey:@"messageCreated"];
        
        chatMessage.messageImageLink=[data objectForKey:@"messageImage"];
        
        chatMessage.petId=[data objectForKey:@"petId"];
        
        chatMessage.threadId=[data objectForKey:@"threadId"];
        
        chatMessage.threadStatus=[data objectForKey:@"threadStatus"];
        
        [self.chatData addObject:chatMessage];
        
        [self.chatTableView reloadData];
        
        [self showRecentMessages];
        
    }
    
}

-(void)error:(NSError*)error
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self.view endEditing:YES];
    
    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
}


-(void)hideKeyboard:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
