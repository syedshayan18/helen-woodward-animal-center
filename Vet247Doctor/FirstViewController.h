//
//  FirstViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "PropertyTrackerModel.h"
@interface FirstViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_animal;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_species;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_breed;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_color;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_age;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_weight;

@property (strong, nonatomic) NSArray *speciesFieldValues;
@property (weak, nonatomic) IBOutlet UIButton *btn_male;
@property (weak, nonatomic) IBOutlet UIButton *btn_female;
@property (weak, nonatomic) IBOutlet UIButton *btn_spayrd;
@property (weak, nonatomic) IBOutlet UIButton *btn_neutered;
@property (weak, nonatomic) IBOutlet UIButton *btn_tacked;

- (BOOL) checkGeneralInfoTextField;

@property (weak, nonatomic) IBOutlet UILabel *lbl_neutered;
@property (weak, nonatomic) IBOutlet UILabel *lbl_spayed;
@property PropertyTrackerModel *firstpet;
@property (weak, nonatomic) IBOutlet UILabel *lbl_male;
@property (weak, nonatomic) IBOutlet UILabel *lbl_female;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tacked;


@end
