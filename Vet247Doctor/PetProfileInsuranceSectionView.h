//
//  PetProfileInsuranceSectionView.h
//  Vet247Doctor
//
//  Created by APPLE on 7/31/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetProfileInsuranceSectionView : UIView <UITextFieldDelegate>


@property (strong, nonatomic) UIImageView *petInsuranceFieldBGImage;

@property (strong, nonatomic) UIButton *petInsuranceYesButton, *petInsuranceNoButton;

@property (strong, nonatomic) UILabel *petInsuranceYesLable, *petInsuranceNoLable;

@property (strong, nonatomic) UITextField *petInsuranceCompanyTextField;

@property (strong, nonatomic) UITextField *petInsurancePolicyNumberTextField;

@property (strong, nonatomic) UIImageView *petTrackerFieldBGImage;

@property (strong, nonatomic) UIButton *petTrackerYesButton, *petTrackerNoButton;

@property (strong, nonatomic) UILabel *petTrackerYesLable, *petTrackerNoLable;

@property (strong, nonatomic) UITextField *petTrackerCompanyTextField;

@property (strong, nonatomic) UITextField *petTrackerPinNumberTextField;

@property (strong, nonatomic) UIImageView *petVeterinarionFieldBGImage;

@property (strong, nonatomic) UIButton *petVeterinarianYesButton, *petVeterinarianNoButton;

@property (strong, nonatomic) UILabel *petVeterinarianYesLable, *petVeterinarianNoLable;

@property (strong, nonatomic) UITextField *petVeterinarianNameTextField;

@property (strong, nonatomic) UITextField *petVeterinarianPhoneNumberTextField;

@property (strong, nonatomic) UIImage *selectedImage;

@property (strong, nonatomic) UIImage *unSelectedImage;

- (void) addComponents;

-(BOOL) checkInsuranceInfoTextFields;

@end
