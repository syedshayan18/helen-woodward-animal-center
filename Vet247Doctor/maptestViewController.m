//
//  maptestViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "maptestViewController.h"
#import <GoogleMaps/GoogleMaps.h>
@interface maptestViewController ()
@property (nonatomic, retain) CLLocationManager *locationManager;
@end

@implementation maptestViewController
GMSMapView *mapView_;
BOOL firstLocationUpdate_;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    
    // Indicating the map frame bounds
    self.mapView_ = [GMSMapView mapWithFrame:self.mapview.bounds camera: camera];
    self.mapView_.myLocationEnabled = YES;
    
    // Add as subview the mapview
    [self.mapview addSubview: self.mapView_];
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
//                                                            longitude:151.2086
//                                                                 zoom:12];
//    
//    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
//    mapView_.settings.compassButton = YES;
//    mapView_.settings.myLocationButton = YES;
//    
//    
//    
//    // Listen to the myLocation property of GMSMapView.
//    [mapView_ addObserver:self
//               forKeyPath:@"myLocation"
//                  options:NSKeyValueObservingOptionNew
//                  context:NULL];
//    
//    self.mapview = mapView_;
//    
//    
//    
//    // Ask for My Location data after the map has already been added to the UI.
//    dispatch_async(dispatch_get_main_queue(), ^{
//        mapView_.myLocationEnabled = YES;
//    });
//}
//
//- (void)dealloc {
//    [mapView_ removeObserver:self
//                  forKeyPath:@"myLocation"
//                     context:NULL];
//}
//
//#pragma mark - KVO updates
//
//- (void)observeValueForKeyPath:(NSString *)keyPath
//                      ofObject:(id)object
//                        change:(NSDictionary *)change
//                       context:(void *)context {
//    if (!firstLocationUpdate_) {
//        // If the first location update has not yet been recieved, then jump to that
//        // location.
//        firstLocationUpdate_ = YES;
//        
//        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
//        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
//                                                         zoom:14];
//        GMSMarker *marker = [[GMSMarker alloc] init];
//        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
//        marker.appearAnimation = kGMSMarkerAnimationPop;
//        marker.icon = [UIImage imageNamed:@"user_pin"];
//        marker.map = mapView_;
//        
//    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
