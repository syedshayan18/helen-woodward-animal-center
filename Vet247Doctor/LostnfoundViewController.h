//
//  LostnfoundViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LostnfoundViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img_foundapet;
@property (weak, nonatomic) IBOutlet UIImageView *img_lostapet;
@property (weak, nonatomic) IBOutlet UIImageView *img_lostnfoundarea;
@property (weak, nonatomic) IBOutlet UIImageView *img_mylostandfoundpet;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImage *selectedimage;


@end
