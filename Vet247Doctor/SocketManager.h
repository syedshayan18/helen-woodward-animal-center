//
//  SocketManager.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 9/4/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

#import "AppDelegate.h"

@protocol SoccketManagerDelegate <NSObject>

-(void)responseFromSocket:(NSDictionary*)dataDict;

-(void)showSocketErrorPopUp;

-(void)errorFromSocket:(NSError*)error;

-(void)failedToConnectToSocket;

@end

@interface SocketManager : NSObject<SRWebSocketDelegate>

@property (nonatomic, weak) id<SoccketManagerDelegate> delegate;

@property (strong,nonatomic) AppDelegate *appDel;
@property (strong,nonatomic) NSString *petidcall;

//@property (strong,nonatomic) CallManager *callManager;
-(void) initiateOpenTokSession;

-(void) connect;

-(void) disconnect;

-(void)checkSocket:(NSString*)status;

-(void)callDisconnectedByPatientBeforeAnswer;

-(void)socketShouldNotReconnect;

-(void)closeSocket;

-(void)checkSocketConnectivity;

-(void)callActivated;

-(void)callDeactivated;

-(void)shouldRegisterCall;

-(void)pingForDoctorEveryFortySeconds;

-(BOOL)getCallStatus;

+(instancetype)sharedInstance;
@end
