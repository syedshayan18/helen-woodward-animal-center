//
//  TermsnconditionViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/22/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsnconditionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_accept;
@property (weak, nonatomic) IBOutlet UIButton *btn_decline;

@end
