//
//  ImagepreviewViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/18/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "ImagepreviewViewController.h"
#import "FoundpetdetailsViewController.h"

@interface ImagepreviewViewController ()

@end

@implementation ImagepreviewViewController

@synthesize dvImage;



- (void)viewDidLoad {
    [super viewDidLoad];
    _img_imagepreview.image = dvImage;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_NO:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)btn_YES:(UIButton *)sender{
    
    [self performSegueWithIdentifier:@"cameratoradius" sender:nil];
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[self navigationController]setNavigationBarHidden:YES ];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    FoundpetdetailsViewController *found = (FoundpetdetailsViewController *) [segue destinationViewController];
    found.petimage=dvImage;
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
