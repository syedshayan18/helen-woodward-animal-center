//
//  PetProfileKeys.h
//  Vet247Doctor
//
//  Created by APPLE on 8/8/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PetProfileKeys : NSObject

#define PetProfileConnectionType @"POST"

#define PetProfileConnectionURL @"http://vet247.invisionsolutions.ca/petsapp/insert_pet.php"

#define UserIdKey @"userId"

#define PetIdKey @"petId"

#define AnimalNameKey @"animalName"

#define AnimalSpeciesKey @"species"

#define AnimalBreadKey @"breed"

#define AnimalSexKey @"sex"

#define AnimalNeuteredOrSpayedKey @"type"

#define AnimalAgeKey @"age"

#define AnimalColorKey @"color"

#define AnimalWeightKey @"weight"

#define AnimalVaccinatedKey @"vaccinated"

#define PetVeterinarianYesOrNoKey @"veterinarian"

#define VeterinarianNameKey @"veterinarian_name"

#define VeterinarianPhoneKey @"veterinarian_phone"

#define AnimalIllnessHistoryKey @"history"

#define MedicalAlergiesKey @"allergies"

#define AnimalMedicationKey @"medication"

#define AnimalDietKey @"diet"

#define PetInsuranceYesOrNoKey @"petInsurance"

#define InsuranceCompanyKey @"insuranceCompany"

#define InsurancePolicyNumberKey @"policyNumber"

#define PetTrackerYesOrNoKey @"petTracker"

#define TrackerCompanyNameKey @"trackerCompany"

#define TrackingPinNumberKey @"pinNumber"

#define PetPictureYesOrNoKey @"setimage"

#define PetProfilePictureKey @"file"

@end
