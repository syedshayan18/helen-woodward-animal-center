//
//  signViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/9/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface signViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_email;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_password;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_confirmpass;
@property (weak, nonatomic) IBOutlet UIButton *btn_donee;
@property (weak, nonatomic) IBOutlet UIButton *btn_login;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_firstname;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_lastname;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_phone;
@property (assign) BOOL termsAndConditionsButtonClicked;
- (IBAction)btn_radio:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_agrement;

@end
