//
//  ProfileController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/8/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ProfileController.h"

#import "ControlsFactory.h"

#import "Masonry.h"

#import "NetworkingClass.h"

#import "AppDelegate.h"

#import "DashboardController.h"

#import "Define.h"

#import "PaymentController.h"

#import "MBProgressHUD.h"

#import "PackagesVC.h"

@interface ProfileController ()

@property (assign) BOOL imageExist;

@property  CGPoint centerPoint;

@property CGSize scrollSize;

@property (strong,nonatomic) UIImageView *backGroundImage,*profilePicImageView;

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *headerViewlabel,*maleLabel,*femaleLabel;

@property (strong,nonatomic) UIScrollView *scrollView;

@property (strong,nonatomic) UIImageView *fNameTextFieldBG, *lNameTextFieldBG,*mobileTextFieldBG,
*streetTextFieldBG,*cityTextFieldBG, *provinceTextFieldBG, *postalCodeTextFieldBG, *countryTextFieldBG,
*profilePicBG,*dateBG;

@property (strong,nonatomic) UITextField *fNameTextField,*lNameTextField,*mobileTextField,
*streetAddressTextField ,*cityTextField,*postalCodeTextField, *othersTextField;

@property (strong,nonatomic) UIButton *saveButton,*genderButtonMale, *genderButtonFemale, *dobButton,
*uploadButton,*changeButton, *removeButton,*backButton,*provinceButton, *countryButton;

@property (strong,nonatomic) UIImageView *dobBG, *genderBG;

@property (strong, nonatomic) UIDatePicker *datePicker;

@property (strong,nonatomic) UIPickerView *provincePickerView,*countryPickerView;

@property (strong,nonatomic) UIView *dateView;

@property (strong,nonatomic) NSMutableArray *provinceNames, *countryNames, *statesNames, *showProvinceOrStatesData;

@property (strong,nonatomic) NSString *gender,*setImage;

@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong,nonatomic) NetworkingClass *getProfile;

@property (strong,nonatomic) NSMutableDictionary *dataDict;

@property (strong,nonatomic) NSString *callFromObject;

@property (strong,nonatomic) UITapGestureRecognizer *gestureRecognizer;

@property (strong,nonatomic) UIButton *genderButtonMaleBG, *genderButtonFemaleBG;

@end

@implementation ProfileController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.datePicker =[[UIDatePicker alloc]init];
    
    self.provincePickerView= [[UIPickerView alloc]init];
    
    self.countryPickerView =[[UIPickerView alloc] init];

    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    self.centerPoint=self.view.center;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.dataDict =[[NSMutableDictionary alloc] init];
    
    [self getProfileInfo];
    
    [self addComponents];
    
    [self setUpOthersTextField];
    
}


- (void) updateView {
    
    if ([[self.dataDict objectForKey:@"sex"] length]>0) {
        
        self.gender=[self.dataDict objectForKey:@"sex"];
    }
    else
    {
        
        self.gender=@"Male";
    }
    
    self.fNameTextField.text=[self.dataDict objectForKey:@"firstName"];
    
    self.lNameTextField.text=[self.dataDict objectForKey:@"lastName"];
    
    self.mobileTextField.text=[self.dataDict objectForKey:@"phoneNo"];
    
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"dd-MM-yyyy"];
    
    if ([[self.dataDict objectForKey:@"birthday"] length]>0) {
        
        [self.dobButton setTitle:[df stringFromDate:[df dateFromString:[self.dataDict objectForKey:@"birthday"]]] forState:UIControlStateNormal];
        
        [self.dobButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    if ([[self.dataDict objectForKey:@"streetAddress"] length ]>0) {
        
        self.streetAddressTextField.text=[self.dataDict objectForKey:@"streetAddress"];
        
    }
    
    if ([[self.dataDict objectForKey:@"city"] length ]>0) {
        
        self.cityTextField.text=[self.dataDict objectForKey:@"city"];
    }
    
    if ([[self.dataDict objectForKey:@"province"] length] >0) {
        
        [self.provinceButton setTitle:[self.dataDict objectForKey:@"province"] forState:UIControlStateNormal];
        
        [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    if ([[self.dataDict objectForKey:@"country"] length]>0) {
        
        [self.countryButton setTitle:[self.dataDict objectForKey:@"country"] forState:UIControlStateNormal];
        
        [self.countryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    if ([[self.dataDict objectForKey:@"postalCode"] length ]> 0) {
        
        self.postalCodeTextField.text=[self.dataDict objectForKey:@"postalCode"];
        
    }
    if ([[self.dataDict objectForKey:@"sex"] length]>0) {
        
        if ([self.gender isEqualToString:@"Male"]) {
            
            [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
            
            self.gender=@"Male";
            
            [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
            
        }
        else {
            
            [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
            
            self.gender=@"Female";
            
            [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
        }
    }
    
    [self checkForProfilePic];
    
    self.setImage=@"Update";
}

- (void) addComponents{
    
    self.gender=@"Male";
    
    [self setUpBGImage];
    
    [self setUpHeaderView];
    
    [self setUpBackButton];
    
    [self setUpScrollView];
    
    [self setUpFNameTextField];
    
    [self setUpLNameTextField];
    
    [self setUpDateOfBirthField];
    
    [self setUpGenderField];
    
    [self setUpMobileTextField];
    
    [self setUpStreetTextFieldBG];
    
    [self setUpCityTextFieldBG];
    
    [self setUpCountryTextFieldBG];
    
    [self setUpPostalCodeTextFieldBG];

    [self setUpProvinceTextFieldBG];
    
    [self setUpProfilePicBG];
    
    [self setUpProfilePicImageAndButtons];
    
    [self setUpSaveButton];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.activityIndicator.backgroundColor=[UIColor lightGrayColor];
    
    self.activityIndicator.center = CGPointMake(self.profilePicBG.frame.size.width / 2.0, self.profilePicBG.frame.size.height / 2.0);
    //
    //[self.view addSubview: self.activityIndicator];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

- (void) getProfileInfo {
    
    self.callFromObject=@"getProfile";
    
    self.getProfile=[[NetworkingClass alloc] init];
    
    self.getProfile.delegate=self;
    
    [self.getProfile getProfileInfo:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
}

- (void) checkForProfilePic {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([[self.dataDict objectForKey:@"picturePath"] length]>0) {
        
        //        NSString *urlString=[self.dataDict objectForKey:@"picturePath"];
        //
        //        NSURL *url=[NSURL URLWithString:urlString];
        //
        //        NSURLRequest *request=[NSURLRequest requestWithURL:url];
        //
        //        NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
        //
        //        [connection start];
    if ([[self.dataDict objectForKey:@"picturePath"] isEqualToString:[NSString stringWithFormat:@"%@%@",ImageResourcesURL,@"profile/"]])
//        if ([[self.dataDict objectForKey:@"picturePath"] isEqualToString:@"http://invisionsolutions.//ca/vet247/app/webroot/img/profile/"])
        {
            
            self.imageExist=NO;
            
        }
        else
        {
            self.imageExist=YES;
            
            //self.profilePicImageView.image=image;
            
            [self.profilePicImageView setHidden:NO];
            
            [self.uploadButton setHidden:YES];
            
            [self.changeButton setHidden:NO];
            
            [self.removeButton setHidden:NO];
            
            
            
            [self downloadProfileImage];
            
            
        }
        
        
        
    }
    else
    {
        self.imageExist=NO;
        
    }
    
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"%@",response);
    
    [connection cancel];
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    
    int code = (int)[httpResponse statusCode];
    
    if (code == 200) {
        
        self.imageExist=YES;
        
        //self.profilePicImageView.image=image;
        
        [self.profilePicImageView setHidden:NO];
        
        [self.uploadButton setHidden:YES];
        
        [self.changeButton setHidden:NO];
        
        [self.removeButton setHidden:NO];
        
        [self downloadProfileImage];
        
    }
    else if(code == 404)
    {
        self.imageExist=NO;
        
    }
    else {
        
        self.imageExist=NO;
        
    }
    
    [self setUpOthersTextField];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.imageExist=NO;
    
}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}


- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text=@"User Profile";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(130);
        
    }];
    
}

-(void) setUpBackButton {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(40, 10, 15, 0);
    
    self.backButton=[ControlsFactory getButton];
    
    self.backButton.backgroundColor=[UIColor clearColor];
    
    //self.backButton.contentEdgeInsets= UIEdgeInsetsMake(0, 0, -10, 0);
    
    [self.backButton setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    //    self.backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
    //
    //    self.backButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    
    [self.backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        //make.top.mas_equalTo(self.headerView.mas_top).with.offset(padding.top);
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(30);
        
    }];
    
}


- (void) setUpScrollView{
    
    self.scrollView=[ControlsFactory getScrollView];
    
    self.scrollView.backgroundColor=[UIColor clearColor];
    
    self.scrollView.bounces=YES;
    
    self.scrollView.userInteractionEnabled=YES;
    
    self.scrollView.scrollEnabled=YES;
    
    self.scrollView.delegate = self;
    
    UIEdgeInsets padding=UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.scrollView];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.headerView.mas_bottom).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.left.mas_equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.mas_equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}

- (void) setUpFNameTextField
{
    self.fNameTextFieldBG=[ControlsFactory getImageView];
    
    self.fNameTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.fNameTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 20, 10);
    
    self.fNameTextField.delegate = self;
    
    [self.scrollView addSubview:self.fNameTextFieldBG];
    
    [self.fNameTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.scrollView.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        //make.right.mas_equalTo(self.scrollView.mas_right).with.offset(-padding.right);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.height.mas_equalTo(46);
    }];
    
    self.fNameTextField=[ControlsFactory getTextField];
    
    self.fNameTextField.backgroundColor=[UIColor clearColor];
    
    self.fNameTextField.delegate=self;
    
    self.fNameTextField.placeholder=@"First Name";
    
    self.fNameTextField.text= [self.dataDict objectForKey:@"firstName"];
    
    self.fNameTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.fNameTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.fNameTextField];
    
    [self.fNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.fNameTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.fNameTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.fNameTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
}

- (void) setUpLNameTextField{
    
    self.lNameTextFieldBG=[ControlsFactory getImageView];
    
    self.lNameTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.lNameTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 0, 10);
    
    [self.scrollView addSubview:self.lNameTextFieldBG];
    
    [self.lNameTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.fNameTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
    }];
    
    self.lNameTextField=[ControlsFactory getTextField];
    
    self.lNameTextField.backgroundColor=[UIColor clearColor];
    
    self.lNameTextField.delegate=self;
    
    self.lNameTextField.placeholder=@"Last Name";
    
    self.lNameTextField.text=[self.dataDict objectForKey:@"lastName"];
    
    self.lNameTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.lNameTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.lNameTextField];
    
    [self.lNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.lNameTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.lNameTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.lNameTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
}

- (void) setUpDateOfBirthField{
    
    self.dobBG=[ControlsFactory getImageView];
    
    self.dobBG.image=[UIImage imageNamed:@"dobBG.png"];
    
    self.dobBG.userInteractionEnabled=YES;
    
    self.dobBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 12, 0, 12);
    
    [self.scrollView addSubview:self.dobBG];
    
    [self.dobBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.lNameTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(92);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
    }];
    
    self.dateBG=[ControlsFactory getImageView];
    
    self.dateBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.dateBG.userInteractionEnabled=YES;
    
    self.dateBG.backgroundColor=[UIColor clearColor];
    
    padding=UIEdgeInsetsMake(27, 14, 10, 12);
    
    [self.scrollView addSubview:self.dateBG];
    
    [self.dateBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.dobBG.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.scrollView.mas_centerX);
        
        make.width.mas_equalTo(self.view.frame.size.width-40);
        
        make.height.mas_equalTo(46);
        
    }];
    
    self.dobButton=[ControlsFactory getButton];
    
    padding=UIEdgeInsetsMake(17, 60, 17, 60);
    
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"dd-MM-yyyy"];
    
    if ([[self.dataDict objectForKey:@"birthday"] length]>0) {
        
        //  NSDate *date = [dateFormat dateFromString:dateStr]
        [self.dobButton setTitle:[df stringFromDate:[df dateFromString:[self.dataDict objectForKey:@"birthday"]]] forState:UIControlStateNormal];
        
        [self.dobButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
    }
    else {
        
        [self.dobButton setTitle:[df stringFromDate:[NSDate date]] forState:UIControlStateNormal];
        
        [self.dobButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        
    }
    
    
    [self.dobButton addTarget:self action:@selector(dobButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.dobButton];
    
    [self.dobButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.dobBG.mas_centerX);
        
        make.top.mas_equalTo(self.dobBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.dobBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
}

- (void) setUpGenderField {
    
    self.genderButtonFemale.imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    
    self.genderBG=[ControlsFactory getImageView];
    
    self.genderBG.userInteractionEnabled=YES;
    
    self.genderBG.image=[UIImage imageNamed:@"genderBG.png"];
    
    self.genderBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 0, 10);
    
    [self.scrollView addSubview:self.genderBG];
    
    [self.genderBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.dobBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.height.mas_equalTo(72);
        
        
    }];
    padding=UIEdgeInsetsMake(26, 14, 15, 50);
    
    self.maleLabel =[ControlsFactory getLabel];
    
    self.maleLabel.text=@"Male";
    
    self.maleLabel.textColor=[UIColor whiteColor];
    
    self.maleLabel.textAlignment=NSTextAlignmentRight;
    
    self.maleLabel.userInteractionEnabled=YES;
    
    [self.scrollView addSubview:self.maleLabel];
    
    [self.maleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.width.mas_equalTo(70);
        
        make.height.mas_equalTo(20);
        
        make.right.mas_equalTo((self.genderBG.mas_centerX)).with.offset(-padding.right);
        
    }];
    
    padding=UIEdgeInsetsMake(26, 50, 10, 15);
    
    self.femaleLabel =[ControlsFactory getLabel];
    
    self.femaleLabel.text=@"Female";
    
    self.femaleLabel.textColor=[UIColor whiteColor];
    
    self.femaleLabel.userInteractionEnabled=YES;
    
    self.femaleLabel.textAlignment=NSTextAlignmentLeft;
    
    [self.scrollView addSubview:self.femaleLabel];
    
    [self.femaleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.width.mas_equalTo(70);
        
        make.height.mas_equalTo(20);
        
        make.left.mas_equalTo(self.genderBG.mas_centerX).with.offset(padding.left);
        
    }];
    
    padding=UIEdgeInsetsMake(15, 10, 1, 1);
    
    self.genderButtonMale=[ControlsFactory getButton];
    
    if ([self.gender isEqualToString:@"Male"]) {
        
        [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
        
        self.gender=@"Male";
        
        
    }
    else {
        
        [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
        
    }
    
    self.genderButtonMaleBG = [ControlsFactory getButton];
    
    self.genderButtonMaleBG.backgroundColor=[UIColor clearColor];
    
    [self.genderButtonMaleBG addTarget:self action:@selector(maleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.genderButtonMaleBG];
    
    [self.genderButtonMaleBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.right.mas_equalTo(self.genderBG.mas_centerX);
        
        make.height.mas_equalTo(50);
        
        make.width.mas_equalTo(120);
        
    }];
    
    
    padding=UIEdgeInsetsMake(26, 10, 1, 1);
    
    
    self.genderButtonMale.imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    [self.genderButtonMale addTarget:self action:@selector(maleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.genderButtonMale.backgroundColor=[UIColor clearColor];
    
    [self.scrollView addSubview:self.genderButtonMale];
    
    [self.genderButtonMale mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.right.mas_equalTo(self.genderBG.mas_centerX);
        
        make.height.mas_equalTo(25);
        
        make.width.mas_equalTo(120);
        
    }];
    
    
    self.genderButtonFemale=[ControlsFactory getButton];
    
    if ([self.gender isEqualToString:@"Female"]) {
        
        [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
        
        self.gender=@"Female";
        
    }
    else {
        
        [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
        
    }
    
    padding=UIEdgeInsetsMake(15, 10, 1, 1);
    
    self.genderButtonFemaleBG=[ControlsFactory getButton];
    
    [self.genderButtonFemale addTarget:self action:@selector(feMaleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.genderButtonFemale.backgroundColor=[UIColor clearColor];
    
    [self.genderButtonFemaleBG addTarget:self action:@selector(feMaleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.genderButtonFemaleBG.backgroundColor=[UIColor clearColor];
    
    
    [self.scrollView addSubview:self.genderButtonFemaleBG];
    
    [self.genderButtonFemaleBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.genderBG.mas_centerX);
        
        make.height.mas_equalTo(50);
        
        make.width.mas_equalTo(120);
        
    }];
    
    padding=UIEdgeInsetsMake(26, 10, 1, 1);
    
    [self.scrollView addSubview:self.genderButtonFemale];
    
    [self.genderButtonFemale mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderBG.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.genderBG.mas_centerX);
        
        make.height.mas_equalTo(25);
        
        make.width.mas_equalTo(120);
        
    }];
    
    padding=UIEdgeInsetsMake(0, 90, 0, 1);
    
    [self.genderButtonMale.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderButtonMale.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.genderButtonMale.mas_left).with.offset(padding.left);
        
        make.bottom.mas_equalTo(self.genderButtonMale.mas_bottom).with.offset(-padding.bottom);
        
        make.right.mas_equalTo(self.genderButtonFemale.mas_left).with.offset(padding.right);
        
        make.width.mas_equalTo(30);
        
    }];
    
    padding=UIEdgeInsetsMake(0, 0, 0, 90);
    
    [self.genderButtonFemale.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderButtonFemale.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.genderButtonMale.mas_right).with.offset(padding.left);
        
        make.right.mas_equalTo(self.genderButtonFemale.mas_right).with.offset(-padding.right);
        
        make.bottom.mas_equalTo(self.genderButtonFemale.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(30);
        
    }];
    
    
    
}

- (void) setUpMobileTextField{
    
    self.mobileTextFieldBG=[ControlsFactory getImageView];
    
    self.mobileTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.mobileTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 0, 10);
    
    [self.scrollView addSubview:self.mobileTextFieldBG];
    
    [self.mobileTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.genderBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        //make.right.mas_equalTo(self.scrollView.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
    }];
    
    self.mobileTextField =[ControlsFactory getTextField];
    
    self.mobileTextField.backgroundColor=[UIColor clearColor];
    
    self.mobileTextField.delegate=self;
    
    [self.mobileTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    // self.mobileTextField.returnKeyType=UIReturnKeyNext;
    
    self.mobileTextField.placeholder=@"Mobile No";
    
    self.mobileTextField.text=[self.dataDict objectForKey:@"phoneNo"];
    
    self.mobileTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.mobileTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.mobileTextField];
    
    [self.mobileTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.mobileTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.mobileTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.mobileTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
}

- (void) setUpStreetTextFieldBG {
    
    self.streetTextFieldBG=[ControlsFactory getImageView];
    
    self.streetTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.streetTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 0, 10);
    
    [self.scrollView addSubview:self.streetTextFieldBG];
    
    [self.streetTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mobileTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.height.mas_equalTo(46);
        
    }];
    
    self.streetAddressTextField=[ControlsFactory getTextField];
    
    self.streetAddressTextField.backgroundColor=[UIColor clearColor];
    
    self.streetAddressTextField.delegate=self;
    
    //self.streetAddressTextField.returnKeyType=UIReturnKeyNext;
    
    self.streetAddressTextField.placeholder=@"Street Address";
    
    self.streetAddressTextField.text=[self.dataDict objectForKey:@"streetAddress"];
    
    self.streetAddressTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.streetAddressTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.streetAddressTextField];
    
    [self.streetAddressTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.streetTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.streetTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.streetTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
}
- (void) setUpCityTextFieldBG{
    
    self.cityTextFieldBG=[ControlsFactory getImageView];
    
    self.cityTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.cityTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.scrollView addSubview:self.cityTextFieldBG];
    
    [self.cityTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.streetTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        // make.right.mas_equalTo(self.view.mas_right).with.offset(-padding.right);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.height.mas_equalTo(46);
        
    }];
    
    self.cityTextField=[ControlsFactory getTextField];
    
    self.cityTextField.backgroundColor=[UIColor clearColor];
    
    self.cityTextField.delegate=self;
    
    // self.cityTextField.returnKeyType=UIReturnKeyNext;
    
    self.cityTextField.placeholder=@"City";
    
    self.cityTextField.text=[self.dataDict objectForKey:@"city"];
    
    self.cityTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.cityTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.cityTextField];
    
    [self.cityTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.cityTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.cityTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.cityTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
    
}

-(void) setUpProvinceTextFieldBG {
    
    self.provinceNames = [[NSMutableArray alloc] initWithArray:@[@"Alberta", @"British Columbia", @"Manitoba",@"New Brunswick",@"Newfoundland",@"Nova Scotia",@"Ontario",@"Prince Edward Island",@"Quebec",@"Saskatchewan",@"Territory: Northwest Territories",@"Territory: Nunavut",@"Territory: Yukon"]];
    
    self.statesNames = [[NSMutableArray alloc] initWithArray:@[@"Alabama", @"Alaska", @"Arizona",@"Arkansas",@"California",@"Colorado",@"Connecticut",@"Delaware",@"District of Columbia",@"Florida",@"Georgia",@"Guam",@"Hawaii",@"Idaho",@"Illinois",@"Indiana",@"Iowa",@"Kansas",@"Kentucky",@"Louisiana",@"Maine",@"Maryland",@"Massachusetts",@"Michigan",@"Minnesota",@"Mississippi",@"Missouri",@"Montana",@"Nebraska",@"Nevada",@"Neew Hampshire",@"New Jersey",@"New Mexico",@"New York",@"North Carolina",@"North Dakota",@"Ohio",@"Oklahoma",@"Oregon",@"Pennsylvania",@"Rhode Island",@"South Carolina",@"South Dakota",@"Tennessee",@"Texas",@"Utah",@"Vermont",@"Virginia",@"Washington",@"West Virginia",@"Wisconsin",@"Wyoming",@"Yukon Territory"]];
    
    self.provinceTextFieldBG=[ControlsFactory getImageView];
    
    self.provinceTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.provinceTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.scrollView addSubview:self.provinceTextFieldBG];
    
    [self.provinceTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.postalCodeTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
    }];
    
    self.provinceButton=[ControlsFactory getButton];
    
    padding=UIEdgeInsetsMake(3, 60, 17, 60);
    
    if ([[self.dataDict objectForKey:@"province"] length] >0) {
        
        [self.provinceButton setTitle:[self.dataDict objectForKey:@"province"] forState:UIControlStateNormal];
        
        [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
//        NSString * country = self.countryButton.titleLabel.text;
//        
//        if([country isEqualToString:@"Other"])
//        {
//            self.provinceButton.hidden = YES;
//            
//            self.provinceButton.userInteractionEnabled = NO;
//            
//            self.othersTextField.hidden = NO;
//            
//            self.othersTextField.text = [self.dataDict objectForKey:@"province"];
//            
//            
//        }
        
    }
    else {
        
        [self.provinceButton setTitle:@"States" forState:UIControlStateNormal];
        
        [self.provinceButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
    }
    
    [self.provinceButton addTarget:self action:@selector(provinceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.provinceButton];
    
    [self.provinceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.provinceTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.provinceTextFieldBG.mas_top).with.offset(padding.top);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
        make.height.mas_equalTo(40);
        
    }];
}

-(void) setUpOthersTextField
{
    // - - - - - - - - - - - - - - - - - - - - // - - - - - - - - - - - - - - - - - - - - //
    
    self.othersTextField = [ControlsFactory getTextField];
    
    self.othersTextField.backgroundColor = [UIColor clearColor];
    
    self.othersTextField.delegate=self;
    
    self.othersTextField.placeholder=@"Others";
    
    self.othersTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.othersTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.scrollView addSubview:self.othersTextField];
    
    [self.othersTextField mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.left.mas_equalTo(self.provinceTextFieldBG.mas_left).with.offset(padding.left);
    
        make.top.mas_equalTo(self.provinceTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.provinceTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.right.mas_equalTo(self.provinceTextFieldBG.mas_right).with.offset(-padding.right);
    }];
    
    
    NSString * country = self.countryButton.titleLabel.text;
    
    if([country isEqualToString:@"Other"])
    {
        self.provinceButton.hidden = YES;
        
        self.provinceButton.userInteractionEnabled = NO;
        
        self.othersTextField.hidden = NO;
        
        self.othersTextField.text = [self.dataDict objectForKey:@"province"];
        
        self.othersTextField.textColor = [UIColor blackColor];
    }
    else
    {
        self.othersTextField.hidden = YES;
    }
}

- (void) setUpPostalCodeTextFieldBG {
    
    self.postalCodeTextFieldBG=[ControlsFactory getImageView];
    
    self.postalCodeTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.postalCodeTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.scrollView addSubview:self.postalCodeTextFieldBG];
    
    [self.postalCodeTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.countryTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
    }];
    
    self.postalCodeTextField=[ControlsFactory getTextField];
    
    self.postalCodeTextField.backgroundColor=[UIColor clearColor];
    
    self.postalCodeTextField.delegate=self;
    
    self.postalCodeTextField.placeholder=@"Post Code";
    
    self.postalCodeTextField.text=[self.dataDict objectForKey:@"postalCode"];
    
    self.postalCodeTextField.textAlignment=NSTextAlignmentCenter;
    
    [self.postalCodeTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    padding=UIEdgeInsetsMake(7, 60, 7, 60);
    
    [self.scrollView addSubview:self.postalCodeTextField];
    
    [self.postalCodeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        //make.left.mas_equalTo(self.fNameTextFieldBG.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.postalCodeTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.postalCodeTextFieldBG.mas_top).with.offset(padding.top);
        
        make.bottom.mas_equalTo(self.postalCodeTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
    }];
    
    
}

- (void) setUpCountryTextFieldBG{
    
    self.countryNames =[[NSMutableArray alloc] initWithArray:@[@"USA",@"Canada",@"Other"]];
    
    self.countryTextFieldBG=[ControlsFactory getImageView];
    
    self.countryTextFieldBG.image=[UIImage imageNamed:@"textField.png"];
    
    self.countryTextFieldBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.scrollView addSubview:self.countryTextFieldBG];
    
    [self.countryTextFieldBG mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.cityTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.view.frame.size.width - 20);
    
    }];
    
    self.countryButton = [ControlsFactory getButton];
    
    padding=UIEdgeInsetsMake(3, 60, 17, 60);
    
    if ([[self.dataDict objectForKey:@"country"] length]>0) {
        
        [self.countryButton setTitle:[self.dataDict objectForKey:@"country"] forState:UIControlStateNormal];
        
        [self.countryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self countryPickerSelects];
    }
    
    else{
        
        [self.countryButton setTitle:@"Country" forState:UIControlStateNormal];
        
        [self.countryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    
    [self.countryButton addTarget:self action:@selector(countryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.countryButton];
    
    [self.countryButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.countryTextFieldBG.mas_centerX);
        
        make.top.mas_equalTo(self.countryTextFieldBG.mas_top).with.offset(padding.top);
        
        // make.bottom.mas_equalTo(self.provinceTextFieldBG.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-120);
        
        make.height.mas_equalTo(40);
        
    }];
    
}

- (void) setUpProfilePicBG{
    
    self.profilePicBG=[ControlsFactory getImageView];
    
    self.profilePicBG.image=[UIImage imageNamed:@"profilePicBG"];
    
    self.profilePicBG.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.scrollView addSubview:self.profilePicBG];
    
    [self.profilePicBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.provinceTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        //make.right.mas_equalTo(self.scrollView.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(103);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
    }];
    
    padding=UIEdgeInsetsMake(35, 10, 35, 10);
    
    self.uploadButton=[ControlsFactory getButton];
    
    //[self.uploadButton setImage:[UIImage imageNamed:@"greenBG.png"] forState:UIControlStateNormal];
    
    [self.uploadButton setTitle:@"Upload" forState:UIControlStateNormal];
    
    [self.uploadButton setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greenBG.png"]]];
    
    [self.uploadButton addTarget:self action:@selector(uploadButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.uploadButton];
    
    [self.uploadButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.profilePicBG.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.profilePicBG.mas_centerX);
        
        make.height.mas_equalTo(33);
        
        make.width.mas_equalTo(120);
        
    }];
    
    //    [self.uploadButton setHidden:YES];
    
}

-(void) setUpProfilePicImageAndButtons{
    
    self.profilePicImageView=[ControlsFactory getImageView];
    
    [self.profilePicImageView setImage:[UIImage imageNamed:@"image.png"]];
    
    self.profilePicImageView.backgroundColor=[UIColor clearColor];
    
    [self.profilePicImageView setHidden:YES];
    
    self.removeButton=[ControlsFactory getButton];
    
    [self.removeButton setImage:[UIImage imageNamed:@"removeButtonBG.png"] forState:UIControlStateNormal];
    
    [self.removeButton addTarget:self action:@selector(removeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.removeButton setHidden:YES];
    
    self.changeButton=[ControlsFactory getButton];
    
    [self.changeButton setImage:[UIImage imageNamed:@"changeButtonBG.png"] forState:UIControlStateNormal];
    
    [self.changeButton addTarget:self action:@selector(changeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.changeButton setHidden:YES];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(22, 14, 15, 10);
    
    [self.scrollView addSubview:self.profilePicImageView];
    
    [self.profilePicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.profilePicBG.mas_top).with.offset(padding.top);
        
        make.left.mas_equalTo(self.profilePicBG.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(67);
        
        make.width.mas_equalTo(67);
        
    }];
    
    padding=UIEdgeInsetsMake(22, 14, 14, 10);
    
    [self.scrollView addSubview:self.changeButton];
    
    [self.changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(self.profilePicBG.mas_bottom).with.offset(-padding.bottom);
        
        make.left.mas_equalTo(self.profilePicImageView.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(88);
        
    }];
    
    padding=UIEdgeInsetsMake(22, 14, 14, 14);
    
    [self.scrollView addSubview:self.removeButton];
    
    [self.removeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(self.profilePicBG.mas_bottom).with.offset(-padding.bottom);
        
        make.left.mas_equalTo(self.changeButton.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(88);
        
    }];
    
}

- (void) setUpSaveButton{
    
    self.saveButton=[ControlsFactory getButton];
    
    [self.saveButton setImage:[UIImage imageNamed:@"saveButtonBG.png"] forState:UIControlStateNormal];
    
    self.saveButton.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding=UIEdgeInsetsMake(10, 10, 15, 10);
    
    [self.saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.scrollView addSubview:self.saveButton];
    
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.profilePicBG.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(43);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.left.mas_equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.bottom.mas_equalTo(self.scrollView.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

- (void) saveButtonClicked: (UIButton*) button
{
    
    if ([self checkFields]) {
        
        NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
        
        [dataDict setObject:self.fNameTextField.text forKey:@"firstName"];
        
        [dataDict setObject:self.lNameTextField.text forKey:@"lastName"];
        
        [dataDict setObject:self.dobButton.titleLabel.text forKey:@"birthday"];
        
        [dataDict setObject:self.gender forKey:@"sex"];
        
        [dataDict setObject:self.mobileTextField.text forKey:@"phoneNo"];
        
        [dataDict setObject:self.streetAddressTextField.text forKey:@"streetAddress"];
        
        [dataDict setObject:self.cityTextField.text forKey:@"city"];
        
        if(self.provinceButton.hidden)
        {
            [dataDict setObject:self.othersTextField.text forKey:@"province"];
        }
        else
        {
            [dataDict setObject:self.provinceButton.titleLabel.text forKey:@"province"];
        }
        
        [dataDict setObject:self.countryButton.titleLabel.text forKey:@"country"];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCountry"];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.countryButton.titleLabel.text forKey:@"selectedCountry"];
        
        [dataDict setObject:self.postalCodeTextField.text forKey:@"postalCode"];
        
        [dataDict setObject:self.setImage forKey:@"setimage"];
        
        NetworkingClass *network=[[NetworkingClass alloc] init];
        
        network.delegate=self;
        
        NSData* data = UIImageJPEGRepresentation(self.profilePicImageView.image,0.4);
        
        self.callFromObject=@"save";
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [network setProfile:data dataDict:dataDict];
        
    }
}

- (BOOL) checkFields{
    
    NSString * phoneRegEx = @"^\\+(?:[0-9] ?){6,14}[0-9]$";
    
    //NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    if ( self.fNameTextField.text && self.fNameTextField.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"First Name Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
    }
    
    else if ( self.lNameTextField.text && self.lNameTextField.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
    }
    
    else if (self.mobileTextField.text && self.mobileTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Mobile Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
    else if (self.streetAddressTextField.text && self.streetAddressTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Street Address missing" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
    else if (self.cityTextField.text && self.cityTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"City Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
    else if (self.postalCodeTextField.text && self.postalCodeTextField.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Postal Code Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    else if ([self.countryButton.titleLabel.text isEqualToString:@"Country"]){
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Select Country" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    else if (self.othersTextField.hidden == NO)
    {
        if(self.othersTextField.text && self.othersTextField.text.length<1)
        {
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter province in 'Others'" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [errorAlert show];
            
            return NO;
        }
    }
    else if ([self.provinceButton.titleLabel.text isEqualToString:@"Province"]){
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Select Province" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
    else if ([self.provinceButton.titleLabel.text isEqualToString:@"States"]){
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Select Your State" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
    }
    
    return YES;
}

- (void) dobButtonClicked: (UIButton*) button{
    
    [self.view endEditing:YES];
    
    //    [self.countryPickerView removeFromSuperview];
    //
    //    [self.provincePickerView removeFromSuperview];
    //
    [self.provincePickerView setHidden:YES];
    
    [self.countryPickerView setHidden:YES];
    
    //self.datePicker =[[UIDatePicker alloc]init];
    
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    
    self.datePicker.hidden=NO;
    
    self.datePicker.date=[NSDate date];
    
    self.datePicker.backgroundColor= [UIColor whiteColor];
    
    //self.datePicker.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.datePicker addTarget:self action:@selector(labelButton:) forControlEvents:UIControlEventValueChanged];
    
    [self.scrollView addSubview:self.datePicker];
    
    UIEdgeInsets  padding=UIEdgeInsetsMake(10, 4, 0, 12);
    
    [self.datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        //make.top.mas_equalTo(self.countryTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        make.height.mas_equalTo(162.0);
    }];
}


-(void) labelButton: (UIDatePicker*) picker {
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    [self.dobButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:self.datePicker.date]];
    
    self.dobButton.enabled=FALSE;
    
    [self.dobButton setTitle:str forState:UIControlStateNormal];
    
    
    self.dobButton.enabled=TRUE;
    
    //[self.datePicker set];
    //
    //    [self.datePicker resignFirstResponder];
}

-(void) uploadButtonClicked: (UIButton *) button {
    
    [self showActionSheet];
    
    //    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    //
    //    picker.delegate = self;
    //
    //    picker.allowsEditing = YES;
    //
    //    self.profilePicImageView.image=nil;
    //
    //    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //
    //    [self presentViewController:picker animated:YES completion:NULL];
    //
    //    [self.uploadButton setHidden:YES];
    //
    //    [self.removeButton setHidden:NO];
    //
    //    [self.changeButton setHidden:NO];
    //
    //    [self.profilePicImageView setHidden:NO];
}

- (void) changeButtonClicked: (UIButton *) button {
    
    [self showActionSheet];
    
}

- (void)showActionSheet {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select from Camera or Library"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Library",@"Camera", nil];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
            [self selectFromLibrary];
            
            break;
        case 1:
            
            [self selectFromCamera];
            
            break;
            
        default:
            
            break;
    }
}

- (void) selectFromLibrary {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    [self.uploadButton setHidden:YES];
    
    [self.removeButton setHidden:NO];
    
    [self.changeButton setHidden:NO];
    
    [self.profilePicImageView setHidden:NO];
    
}

- (void) selectFromCamera {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
        [self.uploadButton setHidden:YES];
        
        [self.removeButton setHidden:NO];
        
        [self.changeButton setHidden:NO];
        
        [self.profilePicImageView setHidden:NO];
        
    }
}
- (void) removeButtonClicked: (UIButton *) button {
    
    self.callFromObject=@"removeButton";
    
    self.setImage=@"No";
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userImage"];
    
    //    [self.uploadButton setHidden:NO];
    //
    //
    //
    //    [self.removeButton setHidden:YES];
    //
    //    [self.changeButton setHidden:YES];
    //
    //    [self.profilePicImageView setHidden:YES];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    [dataDict setObject:self.fNameTextField.text forKey:@"firstName"];
    
    [dataDict setObject:self.lNameTextField.text forKey:@"lastName"];
    
    [dataDict setObject:self.dobButton.titleLabel.text forKey:@"birthday"];
    
    [dataDict setObject:self.gender forKey:@"sex"];
    
    [dataDict setObject:self.mobileTextField.text forKey:@"phoneNo"];
    
    [dataDict setObject:self.streetAddressTextField.text forKey:@"streetAddress"];
    
    [dataDict setObject:self.cityTextField.text forKey:@"city"];
    
    [dataDict setObject:self.provinceButton.titleLabel.text forKey:@"province"];
    
    [dataDict setObject:self.countryButton.titleLabel.text forKey:@"country"];
    
    [dataDict setObject:self.postalCodeTextField.text forKey:@"postalCode"];
    
    [dataDict setObject:@"No" forKey:@"setimage"];
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    NSData* data = UIImagePNGRepresentation(self.profilePicImageView.image);
    
    [network setProfile:data dataDict:dataDict];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void) downloadProfileImage {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    
    dispatch_async(queue, ^{
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: [self.dataDict objectForKey:@"picturePath"]]];
        
        UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.profilePicImageView.image = image;
            
        });
        
    });
    
    //    UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: [self.dataDict objectForKey:@"picturePath"]]]];
    //    if (image) {
    ////
    //                                             [self setUpProfilePicBG];
    //
    //                                             [self setUpProfilePicImageAndButtons];
    //
    //                                             [self setUpSaveButton];
    //
    //                                             self.profilePicImageView.image=image;
    //
    //
    //
    //                                             [self.profilePicImageView setHidden:NO];
    //
    //                                             [self.uploadButton setHidden:YES];
    //
    //                                             [self.changeButton setHidden:NO];
    //
    //                                             [self.removeButton setHidden:NO];
    //
    // [self.activityIndicator stopAnimating];
    
    // [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    //                                         }
    //
    //    else
    //    {
    //
    //        [self setUpProfilePicBG];
    //
    //        [self setUpProfilePicImageAndButtons];
    //
    //        [self setUpSaveButton];
    
    // [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    //[self.activityIndicator stopAnimating];
    
    //}
    
}


- (void) backButtonClicked: (UIButton *) button {

    DashboardController *dashboard=[[DashboardController alloc] init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [navController setNavigationBarHidden:YES animated:YES];
    
    [UIView transitionWithView:appDelegate.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        appDelegate.window.rootViewController = navController;
                    }
                    completion:nil];
    
    [appDelegate.window makeKeyAndVisible];

}

- (void) maleButtonClicked: (UIButton *) button{
    
    [self.view endEditing:YES];
    
    [self.provincePickerView setHidden:YES];
    
    [self.countryPickerView setHidden:YES];
    
    [self.datePicker setHidden:YES];
    
    self.gender=@"Male";
    
    [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
    
    [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
    
}

-(void) feMaleButtonClicked: (UIButton *) button {
    
    [self.view endEditing:YES];
    
    [self.provincePickerView setHidden:YES];
    
    [self.countryPickerView setHidden:YES];
    
    [self.datePicker setHidden:YES];
    
    self.gender=@"Female";
    
    [self.genderButtonMale setImage:[UIImage imageNamed:@"genderButtonWhite.png"] forState:UIControlStateNormal];
    
    [self.genderButtonFemale setImage:[UIImage imageNamed:@"genderButtonGreen.png"] forState:UIControlStateNormal];
}

- (void) provinceButtonClicked: (UIButton *) button {
    
    [self.view endEditing:YES];
    
   
    
    self.provincePickerView.tag=1;
    
    [self.provincePickerView setHidden:NO];
    
    self.provincePickerView.delegate=self;
    
    self.provincePickerView.dataSource=self;
    
    [[UIPickerView appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [self.scrollView addSubview:self.provincePickerView];
    
    UIEdgeInsets  padding=UIEdgeInsetsMake(10, 4, 0, 12);
    
    [self.provincePickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        //make.top.mas_equalTo(self.countryTextFieldBG.mas_bottom).with.offset(padding.top);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        make.height.mas_equalTo(162.0);
        
    }];
    
}

- (void) countryButtonClicked: (UIButton *) button {
    
    [self.view endEditing:YES];
    
    
   // self.countryPickerView=
    
    [self.countryPickerView setHidden:NO];
    
    self.countryPickerView.delegate=self;
    
    self.countryPickerView.dataSource=self;
    
    [[UIPickerView appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [self.scrollView addSubview:self.countryPickerView];
    
    UIEdgeInsets  padding=UIEdgeInsetsMake(-1, 4, 0, 12);
    
    [self.countryPickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.view.frame.size.width-20);
        
        make.centerX.mas_equalTo(self.view.mas_centerX);
        
        make.height.mas_equalTo(162.0);
        
    }];
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if ([pickerView isEqual:self.provincePickerView]) {
        
        NSString * country = self.countryButton.titleLabel.text;
        
        if([country isEqualToString:@"USA"])
        {
            return self.statesNames.count;
        }
        else if([country isEqualToString:@"Canada"])
        {
            return self.provinceNames.count;
        }
        return self.provinceNames.count;
        
    }
    else if([pickerView isEqual:self.countryPickerView]) {
        
        NSLog(@"countryNames = %li", (long)self.countryNames.count);
        return self.countryNames.count;
        
    }
    else
    {
        
        return 0;
        
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.provincePickerView]) {
        
        NSString * country = self.countryButton.titleLabel.text;
        
        if([country isEqualToString:@"USA"])
        {
            [self.provinceButton setTitle:self.statesNames[row] forState:UIControlStateNormal];
           
            [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
//            [self countryPickerSelects];
            
            return self.statesNames[row];
        }
        else if([country isEqualToString:@"Canada"])
        {
            [self.provinceButton setTitle:self.provinceNames[row] forState:UIControlStateNormal];
            
            [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
//            [self countryPickerSelects];
            
            return self.provinceNames[row];
        }
        else
        {
            [self.provinceButton setTitle:self.provinceNames[row] forState:UIControlStateNormal];
            
            [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
//            [self countryPickerSelects];
            
            return self.provinceNames[row];
        }
        
    }
    else
    {
        [self.countryButton setTitle:self.countryNames[row] forState:UIControlStateNormal];
            
        [self.countryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        NSLog(@"row = %li", (long)row);
        return self.countryNames[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if ([pickerView isEqual:self.provincePickerView]) {
        
        NSString * country = self.countryButton.titleLabel.text;
        
        if([country isEqualToString:@"USA"])
        {
            [self.provinceButton setTitle:self.statesNames[row] forState:UIControlStateNormal];
            
            [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else if([country isEqualToString:@"Canada"])
        {
            [self.provinceButton setTitle:self.provinceNames[row] forState:UIControlStateNormal];
            
            [self.provinceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else
        {
        
        }
        
        [self.countryPickerView setHidden:YES];
        
        [self.datePicker setHidden:YES];
    
    }
    
    else if([pickerView isEqual:self.countryPickerView]) {
        
        [self.countryButton setTitle:self.countryNames[row] forState:UIControlStateNormal];
        
        [self.countryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        //        [self.countryPickerView removeFromSuperview];
        //
        [self.provincePickerView setHidden:YES];
        //
        [self.datePicker setHidden:YES];
        
        [self countryPickerSelects];
        
    }
    
}

-(void) countryPickerSelects
{
    NSString * country = self.countryButton.titleLabel.text;
    
    if([country isEqualToString:@"USA"])
    {
        [self.provinceButton setTitle:@"States" forState:UIControlStateNormal];
//        self.provinceButton.titleLabel.text = @"States";
        
        self.provinceButton.hidden = NO;
        
        self.provinceButton.userInteractionEnabled = YES;
    
        self.othersTextField.hidden = YES;
        
    }
    else if([country isEqualToString:@"Canada"])
    {
//        self.provinceButton.titleLabel.text = @"Province";
        
        [self.provinceButton setTitle:@"Province" forState:UIControlStateNormal];
        
        self.provinceButton.hidden = NO;
        
        self.othersTextField.hidden = YES;
        
        self.provinceButton.userInteractionEnabled = YES;
    }
    else if([country isEqualToString:@"Other"])
    {
        self.provinceButton.hidden = YES;
        
        self.provinceButton.userInteractionEnabled = NO;
        
        self.othersTextField.hidden = NO;
    }
    
    [self.provinceButton setTitleColor:[UIColor colorWithRed:0.6667 green:0.6667 blue:0.6667 alpha:1.0] forState:UIControlStateNormal];
    
    self.provinceButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
//    self.provinceButton.titleLabel.textAlignment = [NS];
    
    [self.countryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    self.scrollSize=self.scrollView.contentSize;

    if(textField == self.postalCodeTextField || textField == self.othersTextField)
    {
        if (isiPhone4 ||isiPhone5) {
        
            self.scrollView.contentOffset = CGPointMake(0, 385);
        
        }
        if (isiPhone6 || isiPhone6Plus) {
            
            self.scrollView.contentOffset = CGPointMake(0, 300);
            
        }
        
    }
    
//    self.scrollView.contentOffset = CGPointMake(0, 100);
    
//    if (isiPhone4 ||isiPhone5) {
//        
//        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width , self.scrollView.contentSize.height*1.25);
//        
//    }
//    
//    if (isiPhone6 || isiPhone6Plus) {
//        
//        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width , self.scrollView.contentSize.height*1.75);
//    }
    
    [self.provincePickerView setHidden:YES];
    
    [self.countryPickerView setHidden:YES];
    
    [self.datePicker setHidden:YES];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.scrollView.contentSize=self.scrollSize;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    
    
    self.view.center=self.centerPoint;
    
    return NO;
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.setImage=@"Yes";
    
    // UIImagePNGRepresentation
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(chosenImage) forKey:@"userImage"];
    
    self.profilePicImageView.image = chosenImage;
    
    self.callFromObject=@"save";
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)response:(id)data {
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"])
        {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid Credentials" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        else if  ([[data objectForKey:@"status"] isEqualToString:@"success"])
        {
            if ([self.callFromObject isEqualToString:@"getProfile"]) {
                
                self.callFromObject=@"others";
                
                self.dataDict=[data objectForKey:@"data"];
                
                [self updateView];
                
            }
            else{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                //[[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
                
                if ([self.callFromObject isEqualToString:@"removeButton"]) {
                    
                    [self.uploadButton setHidden:NO];
                    
                    [self.removeButton setHidden:YES];
                    
                    [self.changeButton setHidden:YES];
                    
                    [self.profilePicImageView setHidden:YES];
                    UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [errorAlert show];
                    
                }
                
                else if([self.callFromObject isEqualToString:@"save"]) {
                    
                UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Success" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [errorAlert show];
                }
                
            }
            //
            
        }
        
    }
    
    [self.othersTextField removeFromSuperview];
    
    [self setUpOthersTextField];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
    
    [self.countryPickerView setHidden:YES];
    
    [self.provincePickerView setHidden:YES];
    
    [self.datePicker setHidden:YES];
}

- (void) tapGesture:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    [self.countryPickerView setHidden:YES];
    
    [self.provincePickerView setHidden:YES];
    
    [self.datePicker setHidden:YES];
    
    [self.countryPickerView resignFirstResponder];
    
    [self.countryPickerView removeFromSuperview];
    
    [self.datePicker resignFirstResponder];
    
    [self.datePicker removeFromSuperview];
    
    [self.provincePickerView resignFirstResponder];
    
    [self.provincePickerView removeFromSuperview];
    
}

-(void) setDashBoardAsFirstController
{
    
    NSString *isNewUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"];
    
    if ([isNewUser isEqual:@"NO"]) {
        
        DashboardController *dashboard=[[DashboardController alloc] init];
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        [navController setNavigationBarHidden:YES animated:YES];
        
        [UIView transitionWithView:appDelegate.window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            appDelegate.window.rootViewController = navController;
                        }
                        completion:nil];
        
        [appDelegate.window makeKeyAndVisible];
        
    }else{
    
        PackagesVC *dashboard=[[PackagesVC alloc] init];
        
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        [navController setNavigationBarHidden:YES animated:YES];
        
        [UIView transitionWithView:appDelegate.window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            appDelegate.window.rootViewController = navController;
                        }
                        completion:nil];
        
        [appDelegate.window makeKeyAndVisible];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:AnnualSubscriptionPaid_KEY] isEqualToString:AnnualSubscriptionPaid_NOT_Cleared])
//    {
//        PaymentController * payController = [[PaymentController alloc] init];
//        
//        [self.navigationController pushViewController:payController animated:YES];
//    }
//    else
//    {
    [self setDashBoardAsFirstController];
//    }
}

-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    
//    [textField resignFirstResponder];
//    
//    return NO;
//}


@end
