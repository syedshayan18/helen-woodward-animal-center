//
//  ViewController.h
//  Vet247Doctor
//
//  Created by Malik on 7/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController<UITextFieldDelegate>

@property (strong,nonatomic) UIImageView *viewBgImage;

@property (strong,nonatomic) UIView *fieldsView;


@end

