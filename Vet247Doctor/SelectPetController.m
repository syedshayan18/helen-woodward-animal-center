//
//  setPetController.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/31/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "SelectPetController.h"

#import "Masonry.h"

#import "ControlsFactory.h"

#import "ChatController.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

static float cellHeight=71.5f;

@interface SelectPetController ()

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *headerViewlabel;

@property (strong,nonatomic) UIButton *crossButton;

@property (strong,nonatomic) UIButton *crossButtonBG;

@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) NSMutableArray *petData;

@end

@implementation SelectPetController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self fetchPets];
    
   // self.petData =[[NSMutableArray alloc] initWithObjects:@"Annabelle",@"Dalila",@"Mavice", nil];
    
    [self addComponents];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchPets
{
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [network getPets:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
}
-(void) addComponents {
    
    [self setUpHeaderView];
    
}
- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(60);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text=@"Choose Pet";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(130);
        
    }];
    
    self.crossButton=[ControlsFactory getButton];
    
    [self.crossButton setBackgroundImage:[UIImage imageNamed:@"crossButton.png"] forState:UIControlStateNormal];
    
    [self.crossButton addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 10);
    
    [self.view addSubview:self.crossButton];
    
    [self.crossButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(22);
        
        make.width.mas_equalTo(21);
    
    }];
    
    self.crossButtonBG=[ControlsFactory getButton];
    
    self.crossButtonBG.backgroundColor=[UIColor clearColor];
    
    [self.crossButtonBG addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
     paddingForAddButton=UIEdgeInsetsMake(0, 0, 15, 0);
    
    [self.view addSubview:self.crossButtonBG];
    
    [self.crossButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForAddButton.right);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForAddButton.bottom);
        
        make.height.mas_equalTo(45);
        
        make.width.mas_equalTo(60);
        
    }];
    
}

-(void) setUpTableView {
    
    self.tableView =[ControlsFactory getTableView];
    
    self.tableView.delegate=self;
    
    self.tableView.dataSource=self;
    
    self.tableView.backgroundColor=[UIColor whiteColor];
    
    //[self.tableView registerClass:[MessageCell class]   forCellReuseIdentifier:MessageCellIdentifier];
    
    [self.view addSubview:self.tableView];
    
    UIEdgeInsets paddingforTableView =UIEdgeInsetsMake(0, 0, 2, 0);
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingforTableView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingforTableView.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingforTableView.top);
        
        //make.height.mas_equalTo(self.petData.count*cellHeight);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(paddingforTableView.bottom);
        
    }];

    

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void) crossButtonClicked:(UIButton *)crossButton {
    
    [self willMoveToParentViewController:nil];
    
    [self.view removeFromSuperview];
    
    [self removeFromParentViewController];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"crossButton"
     object:self];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.petData.count) {
        
        return 1;
    
    }
    else
    {
    
        return self.petData.count;
    
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    //  Cleanning Old Data!
    UILabel *temp_name_lbl = (UILabel*) [cell viewWithTag:1001];
    UIImageView *temp_imageView = (UIImageView*) [cell viewWithTag:2001];
    
    [temp_imageView removeFromSuperview];
    [temp_name_lbl removeFromSuperview];
    
    if (!self.petData.count) {
    }
    else
    {
        NSDictionary *cellData=[self.petData objectAtIndex: indexPath.row];
        
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 55, 55)];
        
        imgView.tag = 2001;
        
        imgView.backgroundColor=[UIColor clearColor];
        
        [imgView.layer setCornerRadius:27.5f];
        
        [imgView.layer setMasksToBounds:YES];
        
        if([[cellData objectForKey:@"image"] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/pet/"])
        {
            
            [imgView setImage:[UIImage imageNamed:@"dog1.jpg"]];
            
        }
        else
        {
            
            [imgView sd_setImageWithURL:[NSURL URLWithString:[cellData objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            
        }
        
        [cell.contentView addSubview:imgView];
        
        UILabel *nameLable = [[UILabel alloc] initWithFrame:CGRectMake(85, 15, self.view.frame.size.width-100 , 40)];
        
        nameLable.tag = 1001;
        
        nameLable.text=[cellData objectForKey:@"animalName"];
        
        nameLable.textColor = [UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0];
        
        [cell.contentView addSubview:nameLable];
        
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self willMoveToParentViewController:nil];
    
    [self.view removeFromSuperview];
    
    [self removeFromParentViewController];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"moveToChat" object:nil userInfo:[self.petData objectAtIndex: indexPath.row]];
    
//    [[NSNotificationCenter defaultCenter]
//     
//     postNotificationName:@"moveToChat"
//     
//     object:self];
    
    

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([data isKindOfClass:[NSArray class]])
    {
        self.petData=data;
        
        [self setUpTableView];
        
    }
    else if ([data objectForKey:@"msg"])
    {
       // [self setUpTableView];

        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [errorAlert show];
        
    }

}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
   // [self setUpTableView];
    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
    {
        
        [self willMoveToParentViewController:nil];
        
        [self.view removeFromSuperview];
        
        [self removeFromParentViewController];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"crossButton"
         object:self];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
