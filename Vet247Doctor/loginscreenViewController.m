//
//  loginscreenViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/9/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "loginscreenViewController.h"
#import "Masonry.h"

#import "Define.h"

#import "DashboardController.h"

#import "NetworkingClass.h"


#import "profileViewController.h"

#import "DashboardViewController.h"

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"

@interface loginscreenViewController () <UITextFieldDelegate>{

}
@end

@implementation loginscreenViewController{
  
    UITextField *activeField;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   

    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    NSString *ab=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSLog(@"userid%@",ab);
  
    
    NSString *a =[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"];
    NSLog(@"firstlogin%@",a);
    //r35 g147 b51 green
    //r255 g139 b0 yellow
    
    _btn_loginn.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_loginn.clipsToBounds = YES;
    
    
    
    _btn_register.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_register.clipsToBounds = YES;
   
    
    NSAttributedString *user = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_username.attributedPlaceholder = user;
    NSAttributedString *password = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_password.attributedPlaceholder = password;
//_lbl_username.textColor=[UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0];
//    _lbl_password.textColor=[UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0];

//    self.logInGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLoginGesture:)];
//    
//    self.logInGestureRecognizer.numberOfTouchesRequired = 1;
//    
//    [self.view addGestureRecognizer:self.logInGestureRecognizer];

    
    self.txt_username.delegate = self;
    self.txt_password.delegate = self;
    
    
  
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWasShown:)
//                                                 name:UIKeyboardDidShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillBeHidden:)
//                                                 name:UIKeyboardWillHideNotification object:nil];
//    
//    
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]!=NULL)
//    {
//        
//        
//        
//            
//               if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
//            
//             
//               [self performSegueWithIdentifier:@"profile" sender:nil];
//            
//            
//            
//            
//            
//               }
//                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"NO"]) {
//                    
//                    [self performSegueWithIdentifier:@"dashboard" sender:nil];
//                                                  }
//            
//        
//               else {
//                                      
//              }
//        
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
//        
//        
//        
//        [self performSegueWithIdentifier:@"profile" sender:nil];
//        
//        
//        
//    }
//    else
//    {
//        NSLog(@"dashboard");
//        
//        [self performSegueWithIdentifier:@"dashboard" sender:nil];
//        
//        
//        
//        //
//    }
    }
    // Do any additional setup after loading the view.
- (IBAction)unwindToContainerVC:(UIStoryboardSegue *)segue {
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]!=NULL) {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
            
            [self performSegueWithIdentifier:@"profile" sender:nil];
            
            
            
            
            
            
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"NO"]) {
            [self performSegueWithIdentifier:@"dashboard" sender:nil];
            //                DashboardViewController *dash = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"dashboard"]; //or the homeController
            //                [self presentViewController:dash animated:YES completion:nil];
        }
        else {
            
            
            
            
            //              loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"]; //or the homeController
            //                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:logincontroller];
            //             self.window.rootViewController = navController;
            
            
        }
    }
    else {
        
        //             loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"]; //or the homeController
        //            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:logincontroller];        self.window.rootViewController = navController;
        
    }

}





- (void)viewWillAppear:(BOOL)animated {

    
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
   
    self.txt_username.text=@"";
    
    self.txt_password.text=@"";
    
    [self.view endEditing:YES];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // save the text view that is being edited
    self.activeTextView = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // release the selected text view as we don't need it anymore
    self.activeTextView = nil;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)setBtn_login:(UIButton *)sender {
         //[self.view endEditing:NO];
      
    if ( self.txt_username.text && self.txt_username.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Username Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        [errorAlert show];
        
    }
    
    else if (self.txt_password.text && self.txt_password.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        
    }
    else
    {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
        
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if ([emailTest evaluateWithObject:self.txt_username.text] == NO) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Valid Email Address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        
        else {
            
            NetworkingClass *network = [[NetworkingClass alloc] init];
            
            network.delegate = self;
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            
            [network signIn:self.txt_username.text Password:self.txt_password.text];
            
        }
        
    }

    
}






-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([data isKindOfClass:[NSArray class]])
    {
        
        // NSLog(@"OK");
        
    }
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if([[data objectForKey:@"status"] isEqualToString:@"failure"] && [[data objectForKey:@"msg"] isEqualToString:@"block user"])
        {
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"We're sorry to inform you that,\nThis user is been blocked by the Admin for some reasone." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
            return;
            
        }
        
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"]) {
            
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid Credentials" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        
    }
    
    else {
        
        [[NSUserDefaults standardUserDefaults ] setObject:[[data objectAtIndex:0] objectForKey:@"id"] forKey:@"userId"];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
//            SWRevealViewController *reveal = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swreveal"]; //or the homeController
//                            
//            AppDelegate *app = (AppDelegate *)([UIApplication sharedApplication].delegate);
//            [app.window setRootViewController:reveal];
//
            
            
        [self performSegueWithIdentifier:@"profile" sender:nil];
            
            
           
    
            
        }
        else
        {
            
//            SWRevealViewController *reveal = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swreveal"]; //or the homeController
//            
//            AppDelegate *app = (AppDelegate *)([UIApplication sharedApplication].delegate);
//            [app.window setRootViewController:reveal];

            NSLog(@"dashboard");
            
            [self performSegueWithIdentifier:@"dashboard" sender:nil];
            
            
            
//
        }
    }
    
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
/*
- (void)keyboardWillShow:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.25 animations:^
     {
         
    
         
         NSDictionary *userInfo = [aNotification userInfo];
         // Get the origin of the keyboard when it's displayed.
         NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
         // Get the top of the keyboard as the y coordinate of its origin in self's view's coordinate system. The bottom of the text view's frame should align with the top of the keyboard's final position.
         CGRect keyboardRect = [aValue CGRectValue];
         
         
         //self.view.frame.origin.y -= 50;
         
         CGRect newFrame = [self.view frame];
         newFrame.origin.y -= 100; // tweak here to adjust the moving position
       //  [self.view setFrame:biew];
         
     }completion:^(BOOL finished)
     {
         
     }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.25 animations:^
     {
         CGRect newFrame = [self.view frame];
         newFrame.origin.y += height; // tweak here to adjust the moving position
        // [self.view setFrame:newFrame];
         
     }completion:^(BOOL finished)
     {
         
     }];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}





- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    height = MIN(keyboardSize.height,keyboardSize.width);
    width = MAX(keyboardSize.height,keyboardSize.width);
    
    //your other code here..........
}*/
#pragma mark - Keyboard Delegate
//- (void)keyboardWasShown:(NSNotification*)aNotification
//{
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
////    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
////    self.Scrollview.contentInset = contentInsets;
////    self.Scrollview.scrollIndicatorInsets = contentInsets;
//    CGRect frame = self.view.frame;
//    frame.origin.y -= 1000;
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
 //   CGRect aRect = self.view.frame;
 //   aRect.size.height -= kbSize.height;
//    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
//        [self.Scrollview scrollRectToVisible:activeField.frame animated:YES];
//    }
//}

// Called when the UIKeyboardWillHideNotification is sent
//- (void)keyboardWillBeHidden:(NSNotification*)aNotification
//{
////    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
////    self.Scrollview.contentInset = contentInsets;
////    self.Scrollview.scrollIndicatorInsets = contentInsets;
//    
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    CGRect frame = self.view.frame;
//    frame.origin.y += 1000;
//    
//}
#pragma mark - TextField Delegates
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    activeField = textField;
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    activeField = nil;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
