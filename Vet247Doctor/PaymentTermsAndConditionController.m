//
//  PaymentController.m
//  Vet247Doctor
//
//  Created by APPLE on 9/12/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PaymentController.h"

#import "ControlsFactory.h"

#import "Masonry.h"

#import "AppDelegate.h"

#import "DashboardController.h"

#import "Define.h"

#import "PaymentTermsAndConditionController.h"

#import "ZZMainViewController.h"

//#import "PayPalViewController.h"

@interface PaymentTermsAndConditionController ()

@property (strong, nonatomic) UIView * headerView;

@property (strong, nonatomic) UIImageView *backGroundImage;

@property (strong, nonatomic) UILabel * headerViewlabel;

@property (strong, nonatomic) UIButton *  previousButton;

@property (strong, nonatomic) UIScrollView * termsAndConditionsScrollView;

@property (strong, nonatomic) UILabel * termsAndConditionsInfoLabel;

@property (strong, nonatomic) UILabel * termsAndConditionsInfoLabelPart2;

@property (strong, nonatomic) UIButton * cancelButton;

@property (strong, nonatomic) UIButton * iAgreeButton;

@end

@implementation PaymentTermsAndConditionController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addComponents];
    
}

-(void) addComponents {
    
    [self setUpBGImage];
    
    [self setUpHeaderView];
   
    [self setUpScrollView];
    
    [self setUpAgreeAndCancelButtons];
    
//    [self bringAllFieldsAndButtonsToFront];
}

- (void) setUpBGImage {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text=@"Terms & Conditions";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:18.5];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX).with.offset(20);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(180);
        
    }];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(40, 10, 10, 0);
    
    self.previousButton=[ControlsFactory getButton];
    
    self.previousButton.backgroundColor=[UIColor clearColor];
    
    [self.previousButton setImage:[UIImage imageNamed:@"backButtonBG.png"] forState:UIControlStateNormal];
    
    self.previousButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.previousButton addTarget:self action:@selector(previousButton1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.previousButton];
    
    [self.previousButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(31);
        
        make.width.mas_equalTo(55);
        
    }];
    
}

-(void) setUpScrollView
{
    self.termsAndConditionsScrollView = [ControlsFactory getScrollView];
    
    self.termsAndConditionsScrollView.backgroundColor=[UIColor clearColor];
    
    self.termsAndConditionsScrollView.clipsToBounds=YES;
    
    self.termsAndConditionsScrollView.scrollEnabled = YES;
    
    self.termsAndConditionsScrollView.layer.cornerRadius=8.0f;
    
    self.termsAndConditionsScrollView.layer.masksToBounds=YES;
    
    self.termsAndConditionsScrollView.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    self.termsAndConditionsScrollView.layer.borderWidth= 2.0f;
    
    self.termsAndConditionsScrollView.userInteractionEnabled = YES;
    
    self.termsAndConditionsScrollView.canCancelContentTouches = NO;
    
    self.termsAndConditionsScrollView.bounces = NO;

    [self.view addSubview:self.termsAndConditionsScrollView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(5, 5, 15, 5);
    
    [self.termsAndConditionsScrollView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        make.bottom.mas_equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
    // setting termsAndConditionsInfoLabel
    
    self.termsAndConditionsInfoLabel = [ControlsFactory getLabel];
    
    self.termsAndConditionsInfoLabel.backgroundColor = [UIColor clearColor];
    
    [self.termsAndConditionsInfoLabel setFont:[UIFont systemFontOfSize:14]];
    
    self.termsAndConditionsInfoLabel.text = @"ets Plus More (“Vets Plus More”, “we”, “us”, or “our”) operates the website located at www.VetsPlusMore.com and other related websites and mobile applications with links to these Terms and Conditions (collectively, the “Site”). We offer online services (the “Services”) enabling our members (“Members”) to report their pet history and engage veterinarian services (“Veterinarians”) to obtain non-emergency general medical or psychiatric advice and prescriptions for non-controlled substances (if necessary as determined by the veterinarian servicer and where consistent with applicable law and standards of care) and other general medicine or acute care advice. The following terms and conditions form a binding agreement between you and us, whether or not you register and become a Member (each, a “Registered User”) or simply browse the Site as a “Visitor”, where “you” or “your” refers to the person accessing or using the Site or Services. Registered Users and Visitors may be referred to collectively as “Users”.\n    PLEASE READ THESE TERMS AND CONDITIONS OF USE CAREFULLY. BY ACCESSING THE SITE OR USING THE SERVICES YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS. IF YOU DO NOT WISH TO BE BOUND BY THESE TERMS AND CONDITIONS, YOU MAY NOT ACCESS OR USE THIS SITE OR THE SERVICES.\n We may at our sole discretion change, add, or delete portions of these Terms and Conditions at any time on a going- forward basis. It is your responsibility to check these Terms and Conditions for changes prior to use of the Site or Services, and in any event your continued use of the Site or Services following the posting of changes to these Terms and Conditions constitutes your acceptance of any changes. We will notify you of any such material changes by posting notice of the changes on the Site, and/or, in our sole discretion, by email. Certain services available through our Site may have their own terms and conditions that apply to your use of those services. These Terms and Conditions do not alter in any way the terms or conditions of any of those other written or online terms and conditions or agreements you may have or will have with Vets Plus More (“Separate Vets Plus More Agreements”). To the extent that there is any conflict between these Terms and Conditions and any Separate Vets Plus More Agreements, the terms of the Separate Vets Plus More Agreements will govern.\n The Site is not directed to children and children are not eligible to use our Services. Protecting the privacy of children is very important to us. We do not collect or maintain personal information from people we actually know are under 18 years old, and no part of the Site or Services is designed to attract people under 18 years old. If we later obtain actual knowledge that a user is under 18 years old, we will take steps to remove that user’s personal information from our databases. By using our Site, you represent that you are at least 18 years old.\n  1. USERS. You promise that all information you provide to Vets Plus More is true, accurate, current and complete, and you agree to maintain and promptly update such information to keep it true, accurate, current and complete. If we have reasonable grounds to suspect that such information is not true, accurate, current or complete, we may deny or terminate your access to the Site or Services (or any portion thereof).\n Certain Services are only available to Registered Users who are required to set up an account prior to accessing such Services (“Account”) and execute a Separate Vets Plus More Agreement. When you set up an Account, you are required to enter your name, email address, password (“Password”) and certain other information collected by Vets Plus More depending on whether you are a Member. You may not transfer or share your Password or Account (collectively, the “Account Information”) with anyone. You are responsible for maintaining the confidentiality of your Account Information and for all activities that occur under your Password or Account, including use by others to whom you have given your Account Information. You agree to immediately notify us upon becoming aware of any unauthorized use of your Account Information or any other breach of security. You are responsible for any and all use of your Account. Without limiting any rights which Vets Plus More may otherwise have, Vets Plus More reserves the right to take any and all action, as it deems necessary or reasonable, regarding the security of the Site and your Account, including without limitation terminating your Account, changing your Password, or requesting additional information to authorize transactions on your Account. Notwithstanding the above, Vets Plus More may rely on the authority of anyone accessing your Account or using your Password and in no event and under no circumstances shall Vets Plus More be held liable to you for any liabilities or damages resulting from or arising out of your use of the Site, your use of the Account Information or your release of the Account Information to a third party. You may not use anyone else’s Account at any time.\n 2. ACCESS RIGHTS. We hereby grant to you a limited, non-exclusive, nontransferable right to access the Site and use the Services solely for your personal non-commercial use only as permitted under these Terms and Conditions and any Separate Vets Plus More Agreements you may have entered into with us (“Access Rights”).\n You agree that you will not, and will not attempt to: (a) interfere in any manner with the operation of the Services or Site, or the hardware and network used to operate the Services or Site; (b) distribute, sell, lease, rent, sublicense, assign, export, or transfer in any other manner any of your rights under this Agreement or otherwise use the Services or Site for the benefit of a third party or to operate a service bureau; (c) modify, copy or make derivative works based on any part of the Services, the Site or any underlying software, technology or other information, including any printed materials of the same; (d) create Internet “links” to or from the Services or Site, or “frame” or “mirror” any of Vets Plus More content which forms part of the Services or Site; or (e) otherwise use the Services or Site in any manner that exceeds the scope of use granted above.\n Any use of third party software provided in connection with the Site or Services will be governed by such third parties’ licenses and not by these Terms and Conditions. You agree to be responsible for any act or omission of any users that access the Site or Services under your Account or using your Password that, if undertaken by you, would be a violation of these Terms and Conditions, and that such act or omission shall be deemed a violation of these Terms and Conditions by you. We reserve the right, in our sole discretion, to deny use of the Site or Services to anyone for any reason.\n 3. YOUR RESPONSIBILITIES AND ACCEPTABLE USE OF THE SITE AND SERVICE. You agree not to use the Site or Services to: (a) violate any local, state, national or international law; (b) stalk, harass or harm another individual; (c) collect or store personal data about other users; (d) impersonate any person or entity, or otherwise misrepresent your affiliation with a person or entity; or (e) interfere with or disrupt the Site or Services or servers or networks connected to the Site or Services, or disobey any requirements, procedures, policies or regulations of networks connected to the Site or Services. You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Site or Services, use of the Site or Services or access to the Site or Services for any purposes other than for which the Site or Services are being provided to you. You may not reverse engineer, disassemble, decompile, or translate any components of the Site or Services, attempt to derive the source code of any components of the Site or Services, or authorize or assist any third party to do any of the foregoing. Without our written consent, you may not (i) allow, enable, or otherwise support the transmission of mass unsolicited, commercial advertising or solicitations via e-mail (SPAM); (ii) use any high volume, automated, or electronic means to access the Services (including without limitation robots, spiders or scripts); or (iii) frame the Site, place pop-up windows over its pages, or otherwise affect the display of its pages.\n  Use of the Site requires that you comply with certain acceptable use policies we may establish from time-to- time. As part of your responsibilities as a Visitor or a Registered User of the Site, you agree that you will not:\n    (a) use the Site or any Service in a manner that is unlawful, harmful to minors, threatening, harassing, abusive, defamatory, slanderous, vulgar, gratuitously violent, obscene, pornographic, indecent, lewd, libelous, invasive of another’s privacy, or racially, ethnically or otherwise offensive, hateful or abusive;\n (b) infringe someone else’s patent, trademark, trade secret, copyright or other intellectual property or other rights;\n (c) use the Site or any Service for unsolicited or unauthorized advertising, junk or bulk e-mail (SPAM), chain letters, letters relating to a pyramid scheme or any other unsolicited commercial or non-commercial communication;\n    (d) interfere with others using the Site;\n (e) use the Site in any manner that uploads or otherwise spreads any software viruses, worms, time bombs, corrupted files, Trojan horses or any other computer code, files, or programs that are designed or intended to disrupt, damage, overburden, impair or limit the functioning of any software, hardware, network, server or communications systems or equipment;\n (f) disrupt, interfere or inhibit any other user from enjoying the Site or other affiliated or linked websites, material, contents, products and/or services;\n (g) use any robot, spider, or other such programmatic or automatic device, inducing but not limited to automated dial-in or inquiry devices, to obtain information from the Site or otherwise monitor or copy any portion of the Site and/or Services;\n (h) create a false identity for the purpose of misleading others;\n (i) prepare, compile, use, download or otherwise copy any user information and/or usage information for any portion thereof, or transmit, provide or otherwise distribute (whether or not for a fee) such information to any third party;\n (j) attempt to disable, bypass, modify, defeat or otherwise circumvent any security related tools incorporated into the Services and/or the Site;\n (k) reproduce, duplicate, copy, sell, or exploit for any commercial purposes, any portion of the Site or access to the Site; or\n (l) systematically collect or use any content from the Site or Service, including through the use of any data mining, or similar data gathering and extraction methods.\n    4. MODIFICATIONS TO TERMS AND CONDITIONS. We may change these Terms and Conditions from time to time. If you object to any such changes, your sole recourse will be to cease using the Site and/or Services. Continued use of the Site and/or Services following notice of any such changes will indicate your acknowledgement of such changes and agreement to be bound by the revised Terms and Conditions, inclusive of such changes. Such notice may be comprised of an email to your registered email address or a notice posted at the Site.\n 5. Fees and Purchase Terms. If you are a Member, the following terms apply: You agree to pay all fees or charges to your Account in accordance with the fees, charges, and billing terms in effect at the time a fee or charge is due and payable. You must provide Vets Plus More with valid credit card (Visa, MasterCard, or any other issuer accepted by us) (“Payment Provider”) as a condition to receiving the Services. Your Payment Provider agreement governs your use of the designated credit card, and you must refer to that agreement and not this Agreement to determine your rights and liabilities. By providing Vets Plus More with your credit card number or PayPal account and associated payment information, you agree that Vets Plus More is authorized to immediately invoice your Account for all fees and charges due and payable to Vets Plus More hereunder and that no additional notice or consent is required. You agree to immediately notify Vets Plus More of any change in your billing address or the credit card used for payment hereunder. Vets Plus More reserves the right at any time to change its prices and billing methods, either immediately upon posting on the Site or by e-mail delivery to you. Vets Plus More’s fees are net of any applicable sales tax. If any Services, or payments for any goods or services, under this Agreement are subject to sales tax in any jurisdiction, you will be responsible for payment of such sales tax, and any related penalties or interest and will indemnify Vets Plus More for any liability or expense Vets Plus More may incur in connection with such sales taxes. For purposes of this Agreement, “Sales Tax” shall mean any sales or use tax, and any other tax measured by sales proceeds, that Vets Plus More is permitted to pass to you that is\n   (a) the functional equivalent of a sales tax and\n (b) the applicable taxiing jurisdiction does not otherwise impose a sales or use tax. Vets Plus More may automatically charge and withhold such taxes for orders to be delivered to addresses within any jurisdictions that it deems is required. Unless otherwise agreed to by Vets Plus More in writing, all fees paid are non- refundable.\n  6. DISCONTINUATION OF OR MODIFICATIONS TO THE SITE OR SERVICES. We reserve the right to modify or discontinue the Site or Services with or without notice to you. We will not be liable to you or any third party should we exercise our right to modify or discontinue the Site or Services. If you object to any such changes, your sole recourse will be to cease using the Site or Services. Continued use of the Site or Services following notice of any such changes will indicate your acknowledgement of such changes and satisfaction with the Site or Services as so modified.\n 7. PRIVACY. As part of the login and registration process, you will be asked to provide certain personal information to us. All uses of your personal information will be treated in accordance with the Separate Vets Plus More Agreements you executed with Vets Plus More.\n 8. ACCURACY OF INFORMATION. Vets Plus More does not warrant that any information, pictures or graphic depictions, descriptions or other content of the Site are accurate, complete, reliable, updated, current, or error-free. You agree to notify Vets Plus More immediately if you become aware of any errors or inconsistencies in the information or content provided through the Site and comply with any corrective action taken by Vets Plus More.\n 9. THIRD PARTY CONTENT AND MONITORING. Parties other than Vets Plus More may offer and provide products and services on or through the Site. Except for Vets Plus More branded information, products or services that are identified as being offered by Vets Plus More, Vets Plus More does not operate, control, or\n    endorse any information, products, or services on the Site or accessible through the Site in any way. Vets Plus More is not responsible for examining or evaluating, and Vets Plus More does not warrant the offerings of, any of these businesses or individuals or the content of their websites. Vets Plus More does not assume any responsibility or liability for the actions, product, and content of all these and any other third parties. You should carefully review their privacy statements and other conditions of use.\n 10. LINKS. Our provision of a link to any other website or location is for your convenience and does not signify our endorsement of such other site or location or its contents. We have no control over, do not review, and cannot be responsible for, these outside websites or their content. Access to any other websites linked to the Site is at your own risk. When leaving the Site, you should carefully review the applicable terms and policies, including privacy and data gathering practices, of that third-party website. WE WILL NOT BE LIABLE FOR ANY INFORMATION, SOFTWARE, OR LINKS FOUND AT ANY OTHER WEBSITE, INTERNET LOCATION, OR SOURCE OF INFORMATION, NOR FOR YOUR USE OF SUCH INFORMATION, SOFTWARE OR LINKS, NOR FOR THE ACTS OR OMISSIONS OF ANY SUCH WEBSITES OR THEIR RESPECTIVE OPERATORS.\n 11. Ownership. The software, code, proprietary methods and systems used to provide the Site or Services (“Our Technology”) and the content of the Site and Services (“Our Content”) are\n (1) copyrighted by Vets Plus More and/or its licensors under United States and international copyright laws, (2) subject to other intellectual property and proprietary rights and laws, and\n (3) owned by Vets Plus More or its licensors. Neither Our Content nor Our Technology may be copied, modified, reproduced, republished, posted, transmitted, sold, offered for sale, or redistributed in any way without our prior written permission and the prior written permission of our applicable licensors. You must abide by all copyright notices, information, or restrictions contained in or attached to any of Our Content or Our Technology and you may not remove or alter any such notice, information or restriction. Your use of our Content and Technology must at all times comply with these Terms and Conditions and any additional restrictions in any Separate Vets Plus More Agreements you may have entered into with Vets Plus More. Nothing in these Terms and Conditions grants you any right to receive delivery of a copy of Our Technology or to obtain access to Our Technology except as generally and ordinarily permitted through the Site according to these Terms and Conditions.\n12.TERMINATION.YoumaydeleteyourAccountandendyourregistrationatanytime,foranyreasonby sending an email to support@VetsPlusMore.com. Vets Plus More may terminate your use of the Site, your Account and/or registration for any reason at any time. You understand that termination of your agreement with Vets Plus More pursuant to these Terms and Conditions and your Account may involve deletion of your information from our live databases as well as any content that you uploaded to the Site using such Account. YOU AGREE THAT WE WILL NOT BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY TERMINATION OF YOUR ACCESS TO THE SITE OR SERVICES OR DELETION OF YOUR ACCOUNT OR CONTENT UPLOADED BY YOU.\n YOUR SEPARATE VETS PLUS MORE AGREEMENT MAY CONTAIN DIFFERENT TERMINATION PROVISIONS FOR A GIVEN SERVICE. IN SUCH CASES THE TERMINATION PROVISIONS WITHIN THE SEPARATE VETS PLUS MORE AGREEMENT FOR SPECIFIC SERVICES SHALL GOVERN THE TERMINATION OF THOSE SERVICES.";
    
    self.termsAndConditionsInfoLabel.numberOfLines = 0;
    
    [self.termsAndConditionsInfoLabel sizeToFit];
    
    self.termsAndConditionsInfoLabel.textAlignment = NSTextAlignmentLeft;
    
    self.termsAndConditionsInfoLabel.userInteractionEnabled = YES;
    
    self.termsAndConditionsInfoLabel.textColor = [UIColor whiteColor];
    
    [self.termsAndConditionsScrollView addSubview:self.termsAndConditionsInfoLabel];
    
    [self.termsAndConditionsInfoLabel mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.termsAndConditionsScrollView.mas_top).with.offset(5);
        
        make.left.equalTo(self.termsAndConditionsScrollView.mas_left).with.offset(10);
        
        make.width.mas_equalTo(self.view.frame.size.width - 30);
        
    }];
    
    // setting termsAndConditionsInfoLabelPart2
    
    self.termsAndConditionsInfoLabelPart2 = [ControlsFactory getLabel];
    
    self.termsAndConditionsInfoLabelPart2.backgroundColor = [UIColor clearColor];
    
    [self.termsAndConditionsInfoLabelPart2 setFont:[UIFont systemFontOfSize:14]];
    
    self.termsAndConditionsInfoLabelPart2.text = @"13. DISCLAIMEROFWARRANTIES.YOUEXPRESSLYAGREETHATUSEOFTHESITEORSERVICESIS AT YOUR SOLE RISK. BOTH THE SITE AND SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. VETS PLUS MORE EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE, NON-INFRINGEMENT, TITLE, OPERABILITY, CONDITION, QUIET ENJOYMENT, VALUE, ACCURACY OF DATA AND SYSTEM INTEGRATION. VETS PLUS MORE MAKES NO WARRANTY THAT THE SITE OR SERVICES WILL MEET YOUR REQUIREMENTS, OR THAT THE SITE OR SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR FREE; NOR DOES VETS PLUS MORE MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SITE OR SERVICES, OR THAT DEFECTS IN THE SITE OR SERVICES WILL BE CORRECTED. YOU UNDERSTAND AND AGREE THAT ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SITE OR SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF SUCH MATERIAL AND/OR INFORMATION. NO ADVICE OR INFORMATION, WHETHER ORAL OR\n    WRITTEN, OBTAINED BY YOU FROM VETS PLUS MORE OR THROUGH THE SITE OR SERVICES WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN.\n  14. LIMITATION OF LIABILITY. YOU UNDERSTAND THAT TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, IN NO EVENT WILL VETS PLUS MORE OR ITS OFFICERS, EMPLOYEES, DIRECTORS, PARENTS, SUBSIDIARIES, AFFILIATES, AGENTS OR LICENSORS BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF REVENUES, PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF SUCH PARTIES WERE ADVISED OF, KNEW OF OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES, AND NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY), ARISING OUT OF OR RELATED TO YOUR USE OF THE SITE OR THE SERVICES, REGARDLESS OF WHETHER SUCH DAMAGES ARE BASED ON CONTRACT, TORT (INCLUDING NEGLIGENCE AND STRICT LIABILITY), WARRANTY, STATUTE OR OTHERWISE. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THIS SITE OR THE SERVICES, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USE OF THE SITE OR THE SERVICES. THE AGGREGATE LIABILITY OF VETS PLUS MORE TO YOU FOR ALL CLAIMS ARISING FROM OR RELATED TO THE SITE OR THE SERVICES IS LIMITED TO THE LESSER OF (I) THE AMOUNT OF FEES ACTUALLY PAID BY YOU FOR USE OF THE SERVICES OR (II) ONE HUNDRED DOLLARS (U.S. $100.00). Furthermore, Vets Plus More shall not be liable or responsible for any negligence or malpractice of any veterinary providing service to you under this agreement. You agree to hold harmless Vets Plus More for any negligence or malpractice claim against any veterinary performed services under Vets Plus More. Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. Accordingly, some of the above limitations and disclaimers may not apply to you. To the extent that we may not, as a matter of applicable law, disclaim any implied warranty or limit its liabilities, the scope and duration of such warranty and the extent of our liability will be the minimum permitted under such applicable law.\n  15. INDEMNIFICATION. You agree to indemnify, defend and hold harmless Vets Plus More, its parents, subsidiaries, affiliates, licensors, co-branders, suppliers and other contract relationship, and the officers, directors, employees, consultants, and agents of each, and other Registered Users and Visitors, from and against any and all third-party claims, liabilities, damages, losses, costs, expenses, fees (including reasonable attorneys’ fees and court costs) that such parties may incur as a result of or arising from\n  (1) User Content and any information you submit, post or transmit through the Site or Services, (2) your use of the Site or Services,\n        (3) your violation of these Terms and Conditions,\n  (4) your violation of any rights of any other person or entity or\n   (5) any viruses, trojan horses, worms, time bombs, cancelbots or other similar harmful or deleterious programming routines input by you into the Services.\n   16. TRADEMARKS.Certainofthenames,logos,andothermaterialsdisplayedontheSiteorintheServices may constitute trademarks, trade names, service marks or logos (“Marks“) of Vets Plus More or other entities. You are not authorized to use any such Marks. Ownership of all such Marks and the goodwill associated therewith remains with us or those other entities.\n   17. GEOGRAPHICAL RESTRICTIONS. Vets Plus More makes no representation that all products, services and/or material described on the Site, or the Services available through the Site, are appropriate or available for use in locations outside the United States or all territories within the United States, Canada, United Kingdom, South Africa, Australia New Zealand and European Union. Registered Users and Visitors access our Site and the Services on their own initiative and are responsible for compliance with local laws. Certain companies affiliated with Vets Plus More provide services and operate websites which may be linked to our Site and which are governed by their own terms of use and not these Terms and Conditions, and may be subject to laws of other local or international jurisdictions.\n   18. MISCELLANEOUS.ExceptforanySeparateVetsPlusMoreAgreements,authorizationsandconsents between you and Vets Plus More, these Terms and Conditions constitute the entire and exclusive and final statement of the agreement between you and us with respect to the subject matter hereof, and governs your access to the Site and your use of the Services, superseding any prior agreements or negotiations between you and us with respect to the subject matter hereof. The validity, interpretation, construction and performance of these Terms and Conditions will be governed by the laws of the Country of Bahamas, without giving effect to the principles of conflict of laws. The parties agree to the personal and subject matter\n  jurisdiction and venue of the courts located in Nassau, Bahamas, for any action related to these Terms and Conditions. Our failure to exercise or enforce any right or provision of these Terms and Conditions will not constitute a waiver of such right or provision. If any provision of these Terms and Conditions is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties’ intentions as reflected in the provision, and that the other provisions of these Terms and Conditions remain in full force and effect. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Site or Services or these Terms and Conditions must be filed within one (1) year after such claim or cause of action arose or be forever barred. The section titles in these Terms and Conditions are for convenience only and have no legal or contractual effect. You are not an employee, partner, joint venturer, independent contractor or agent of or with Vets Plus More, and you have no authority to act on behalf of or bind Vets Plus More in any way. You may not assign your rights under these Terms and Conditions without our prior written consent, and any attempted assignment will be null and void.\n  19. SURVIVAL. The terms of Sections 6 through 20, and any other limitations on liability explicitly set forth herein will survive the expiration or earlier termination of the agreement pursuant to these Terms and Conditions for any reason. Our (and our licensors’) proprietary rights (including any and all intellectual property rights) in and to Our Content, Our Technology and the Site or Services will survive the expiration or earlier termination of the agreement pursuant to these Terms and Conditions for any reason.\n  20. NETWORKCONNECTIVITY.VetsPlusMoredevotesconsiderableefforttooptimizingsignalstrengthand diagnosis deficiencies but is not responsible for the internet or data bandwidth and signal of your mobile device.\n   21. CUSTOMER CONDUCT. You agree to refrain from abusive language when with veterinarian during a consult. You agree to refrain from contacting or seeking to contact the veterinarian for telemedicine care outside of the platform. This is done to protect the veterinarians and to ensure clinical care is delivered in a reliable, continuous and controlled platform. Vets Plus More is not responsible for any interactions with veterinarian not conducted on the Vets Plus More platform.\n    22.\tVIOLATIONS.PleasereportanyviolationsoftheseTermsandConditionstoinfo@VetsPlusMore.com.\nThe Vets Plus More® App is not intended to replace a traditional appointment with a veterinarian and a complete physical exam. This service is only meant to be informative and supplemental. Your local veterinarian is the only person who can provide a definitive diagnosis or medical treatment plan for your pet.";
   
    self.termsAndConditionsInfoLabelPart2.numberOfLines = 0;
    
    [self.termsAndConditionsInfoLabelPart2 sizeToFit];
    
    self.termsAndConditionsInfoLabelPart2.textAlignment = NSTextAlignmentLeft;
    
    self.termsAndConditionsInfoLabelPart2.userInteractionEnabled = YES;
    
    self.termsAndConditionsInfoLabelPart2.textColor = [UIColor whiteColor];
    
    [self.termsAndConditionsScrollView addSubview:self.termsAndConditionsInfoLabelPart2];
    
    [self.termsAndConditionsInfoLabelPart2 mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.termsAndConditionsInfoLabel.mas_bottom).with.offset(-3);
        
        make.left.equalTo(self.termsAndConditionsScrollView.mas_left).with.offset(10);
        
        make.width.mas_equalTo(self.view.frame.size.width - 30);
        
    }];
    
}

-(void) setUpAgreeAndCancelButtons
{
    // setUp iAgreeButton
    
    self.iAgreeButton = [ControlsFactory getButton];
    
    self.iAgreeButton.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    [self.iAgreeButton setTitle:@"I Agree" forState:UIControlStateNormal];
    
    [self.iAgreeButton addTarget:self action:@selector(iAgreeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.iAgreeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    self.iAgreeButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    self.iAgreeButton.contentEdgeInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    
    [self.iAgreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.termsAndConditionsScrollView addSubview:self.iAgreeButton];
    
    [self.iAgreeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.termsAndConditionsScrollView.mas_left).with.offset(10);
        
        make.right.mas_equalTo(self.view.mas_centerX).with.offset(20);
        
        make.height.mas_equalTo(45);
        
        make.top.equalTo(self.termsAndConditionsInfoLabelPart2.mas_bottom).with.offset(5);
        
        make.bottom.equalTo(self.termsAndConditionsScrollView.mas_bottom).with.offset(-5);
        
    }];
    
    // setUp cancelButton

    self.cancelButton = [ControlsFactory getButton];
    
    self.cancelButton.backgroundColor = [UIColor whiteColor];
    
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    
    self.cancelButton.userInteractionEnabled = YES;
    
    [self.cancelButton setTitleColor:[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0] forState:UIControlStateNormal];
    
    [self.cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    self.cancelButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    self.cancelButton.contentEdgeInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    
    [self.termsAndConditionsScrollView addSubview:self.cancelButton];
    
    [self.view bringSubviewToFront:self.cancelButton];
    
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.termsAndConditionsInfoLabelPart2.mas_bottom).with.offset(5);
        
        make.left.mas_equalTo(self.view.mas_centerX).with.offset(30);
        
        make.right.mas_equalTo(self.view.mas_right).with.offset(-15);
        
        make.height.mas_equalTo(45);
    }];
}

-(void) bringAllFieldsAndButtonsToFront
{
    
//    [self.view bringSubviewToFront:self.iAgreeButton];
//    
//    [self.view bringSubviewToFront:self.cancelButton];
    
}

-(void) previousButton1Clicked:(UIButton *) btn
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) cancelButtonClicked:(UIButton *) btn
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void) iAgreeButtonClicked:(UIButton *) btn
{
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    
    [self.navigationController pushViewController:paypalController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
