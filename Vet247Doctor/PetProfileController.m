//
//  PetProfileController1.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/11/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfileController.h"

#import "PetListController.h"

#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "ProfileController.h"

#import "LoginController.h"

#import "PetProfileGeneralInfoSectionView.h"

#import "PetProfileHistorySectionView.h"

#import "PetProfileInsuranceSectionView.h"

#import "PetProfilePictureSectionView.h"

#import "PetProfileDataManager.h"

#import "NetworkingClass.h"

@interface PetProfileController ()

@property (strong,nonatomic) UIImageView *backGroundImage;

@property (strong,nonatomic) UILabel *headerViewlabel;

@property (strong,nonatomic) UIView *headerView,*footerView;

@property (strong,nonatomic) UIButton *nextButton, *backButton;

@property (strong,nonatomic) UIImageView *currentScreenImageView;

@property (strong,nonatomic) UIScrollView *scrollView;

@property (strong, nonatomic) UIButton * previousButton,*previousButtonBG;

@property (strong,nonatomic) PetProfileGeneralInfoSectionView *generalInfoView;

@property (strong,nonatomic) PetProfileHistorySectionView *historySectionView;

@property (strong,nonatomic) PetProfileInsuranceSectionView *insuranceSectionView;

@property (strong,nonatomic) PetProfilePictureSectionView *pictureSectionView;

@property int currentlyVisibleSectionNumber;

@property (strong, nonatomic) NSMutableDictionary *petProfileDictionaryData;

@property (strong,nonatomic) UITapGestureRecognizer *gestureRecognizer;

@end


@implementation PetProfileController

- (void)viewDidLoad {
    
    // Initializing currently Visible Selection number
    _currentlyVisibleSectionNumber = 0;
    
    self.view.userInteractionEnabled = YES;
    
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    [self addComponents];
    
    self.petProfileDictionaryData = [[NSMutableDictionary alloc] init];
    
}

//NSLog(@"Section Number %d", _sectionNumber);


- (void) addComponents{
    
    [self setUpBGImage];
    
    [self setUpHeaderView];
    
    [self setUpFooterView];
    
    [self setUpScrollView];
    
    // - - General Info Section OK - -
    [self setUpGeneralInfoView];
    
//    // - - History Section OK - -
//    [self setUpHistoryInfoView];
//    
//    // - - Insurance Section OK - -
//    [self setUpInsuranceInfoView];
//    
//    // - - Upload Picture Section OK - -
//    [self setUpPictureLoadView];
//    
//    self.historySectionView.hidden = YES;
//    
//    self.insuranceSectionView.hidden = YES;
//    
//    self.pictureSectionView.hidden = YES;
}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}


- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text=@"Pet Profile";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(130);
        
    }];

    UIEdgeInsets padding = UIEdgeInsetsMake(40, 10, 15, 0);
    
    self.previousButton=[ControlsFactory getButton];
    
    self.previousButton.backgroundColor=[UIColor clearColor];
    
    //self.backButton.contentEdgeInsets= UIEdgeInsetsMake(0, 0, -10, 0);
    
    [self.previousButton setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    
    self.previousButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    //    self.backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
    //
    //    self.backButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    
    [self.previousButton addTarget:self action:@selector(previousButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.previousButton];
    
    [self.previousButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        //make.top.mas_equalTo(self.headerView.mas_top).with.offset(padding.top);
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(30);
        
    }];
    
    padding = UIEdgeInsetsMake(40, 10, 5, 0);
    
    self.previousButtonBG=[ControlsFactory getButton];
    
    self.previousButtonBG.backgroundColor=[UIColor clearColor];
    
    [self.previousButtonBG addTarget:self action:@selector(previousButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.previousButtonBG];
    
    [self.previousButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        //make.top.mas_equalTo(self.headerView.mas_top).with.offset(padding.top);
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(50);
        
        make.width.mas_equalTo(50);
        
    }];
    
    

}

-(void) previousButtonClicked: (UIButton *) btn
{
 
    [PetProfileDataManager removePetProfileInfo];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"UpdatePET"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Reload_PETDATA"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) setUpFooterView{
    
    self.footerView =[ControlsFactory getView];
    
    self.footerView.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.footerView];
    
    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(40);
        
    }];
    
    self.nextButton =[ControlsFactory getButton];
    
    [self.nextButton setImage:[UIImage imageNamed:@"nextButtonBG.png"] forState:UIControlStateNormal];
    
    [self.nextButton addTarget:self action:@selector(nextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(0, 0, 5, 10);
    
    [self.footerView addSubview:self.nextButton];
    
    [self.nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(29);
        
        make.width.mas_equalTo(72);
        
    }];
    
    self.currentScreenImageView= [ControlsFactory getImageView];
    
    self.currentScreenImageView.image=[UIImage imageNamed:@"screen1.png"];
    
    padding = UIEdgeInsetsMake(0, 0, 5, 10);
    
    [self.footerView addSubview:self.currentScreenImageView];
    
    [self.currentScreenImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.footerView.mas_centerX);
        
        make.centerY.mas_equalTo(self.footerView.mas_centerY);
        
        make.height.mas_equalTo(11);
        
        make.width.mas_equalTo(70);
        
    }];
    
    self.backButton =[ControlsFactory getButton];
    
    [self.backButton setImage:[UIImage imageNamed:@"backButtonBG.png"] forState:UIControlStateNormal];
    
    padding = UIEdgeInsetsMake(0, 10, 5, 10);
    
    [self.footerView addSubview:self.backButton];
    
    [self.backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(29);
        
        make.width.mas_equalTo(72);
        
    }];
    
    self.backButton.hidden = YES;
}

// Setting UIScrollView Section

- (void) setUpScrollView {
    
    self.scrollView=[ControlsFactory getScrollView];
    
    self.scrollView.backgroundColor=[UIColor clearColor];
    
    self.scrollView.clipsToBounds=YES;
    
    self.scrollView.scrollEnabled = YES;
    
    self.scrollView.canCancelContentTouches = NO;
    
    self.scrollView.bounces = NO;
    
    [self.view addSubview:self.scrollView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(5, 5, 5, 5);
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.footerView.mas_top).with.offset(-padding.bottom);
        
    }];
    
}

// Setting  Pet Profile's General info view section

- (void) setUpGeneralInfoView {
    
    self.generalInfoView = [[PetProfileGeneralInfoSectionView alloc] init];
    
    [self.scrollView addSubview:self.generalInfoView];
    
    [self.generalInfoView addComponents];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 5, 0);
    
    [self.generalInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.scrollView.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.scrollView.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.scrollView.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

// Setting  Pet Profile's History info section view

-(void) setUpHistoryInfoView{
    
    self.historySectionView = [[PetProfileHistorySectionView alloc] init];
    
    [self.scrollView addSubview:self.historySectionView];
    
    [self.historySectionView addComponents];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 5, 0);
    
    [self.historySectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width-10);
        
        make.top.equalTo(self.scrollView.mas_top).with.offset(padding.top);
        
        make.bottom.equalTo(self.scrollView.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

// Setting  Pet Profile's Insurance info view section

- (void) setUpInsuranceInfoView {
    
    self.insuranceSectionView=[[PetProfileInsuranceSectionView alloc] init];
    
    [self.scrollView addSubview:self.insuranceSectionView];
    
    [self.insuranceSectionView addComponents];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 5, 0);
    
    [self.insuranceSectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.scrollView.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width - 10);
        
        make.top.equalTo(self.scrollView.mas_top).with.offset(padding.top);
        
        make.bottom.equalTo(self.scrollView.mas_bottom).with.offset(-padding.bottom);
        
    }];
    
}

// Setting  Pet Profile's Picture Profile view section

- (void) setUpPictureLoadView {
    
    self.pictureSectionView=[[PetProfilePictureSectionView alloc] init];
    
    [self.scrollView addSubview:self.pictureSectionView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 5, 0);
    
    [self.pictureSectionView mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.left.equalTo(self.scrollView.mas_left).with.offset(padding.left);
         
         make.width.mas_equalTo(self.view.frame.size.width-10);
         
         make.top.equalTo(self.scrollView.mas_top).with.offset(padding.top);
         
         make.bottom.equalTo(self.footerView.mas_top).with.offset(-padding.bottom);
         
     }];
    
    
    [self.pictureSectionView addComponents];
    
}

-(void) nextButtonClicked:(UIButton *) nxtButton{
    
    if (_currentlyVisibleSectionNumber > 2) {
        
        return;
    }
    
    switch (_currentlyVisibleSectionNumber) {
            
        case 0:
            
            if([self.generalInfoView checkGeneralInfoTextFields])
            {
                [self.pictureSectionView removeFromSuperview];
                [self.insuranceSectionView removeFromSuperview];
                [self.historySectionView removeFromSuperview];
                [self.generalInfoView removeFromSuperview];

                _currentlyVisibleSectionNumber ++;
            
                [self setUpHistoryInfoView];
            
                self.currentScreenImageView.image = [UIImage imageNamed:@"screen2.png"];
            
                self.backButton.hidden = NO;
                
                self.previousButton.hidden = YES;
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"Reload_PETDATA"];
                
            }
            break;
            
        case 1:
            
            if([self.historySectionView checkHistoryInfoTextFields])
            {
                [self.pictureSectionView removeFromSuperview];
                [self.insuranceSectionView removeFromSuperview];
                [self.historySectionView removeFromSuperview];
                [self.generalInfoView removeFromSuperview];

                _currentlyVisibleSectionNumber ++;
            
                [self setUpInsuranceInfoView];
            
                self.currentScreenImageView.image = [UIImage imageNamed:@"screen3.png"];
                
                self.previousButton.hidden = YES;
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"Reload_PETDATA"];
            }
            
            break;
            
        case 2:
            
            if([self.insuranceSectionView checkInsuranceInfoTextFields])
            {
                [self.pictureSectionView removeFromSuperview];
                [self.insuranceSectionView removeFromSuperview];
                [self.historySectionView removeFromSuperview];
                [self.generalInfoView removeFromSuperview];

                _currentlyVisibleSectionNumber ++;
                
                [self setUpPictureLoadView];
                
                self.currentScreenImageView.image = [UIImage imageNamed:@"screen4.png"];
                
                self.nextButton.hidden = YES;
                
                self.previousButton.hidden = YES;
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"Reload_PETDATA"];
            }
            
            break;
            
        default:
            
            break;
    }
    
    return;
    
}

-(void) backButtonClicked:(UIButton *) bckButton{
    
    if (_currentlyVisibleSectionNumber < 1) {
        
        return;
    }
    
    [self.pictureSectionView removeFromSuperview];
    [self.insuranceSectionView removeFromSuperview];
    [self.historySectionView removeFromSuperview];
    
    switch (_currentlyVisibleSectionNumber) {
            
        case 3:
            
            [self.pictureSectionView removeFromSuperview];
            
            [self setUpInsuranceInfoView];
            
            self.nextButton.hidden = NO;
            
            self.previousButton.hidden = YES;
            
            self.currentScreenImageView.image = [UIImage imageNamed:@"screen3.png"];
  
            _currentlyVisibleSectionNumber --;
            
            //[[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Reload_PETDATA"];
            
            break;
            
        case 2:
            
            [self.insuranceSectionView removeFromSuperview];
            
            [self setUpHistoryInfoView];
            
            self.previousButton.hidden = YES;
            
            self.currentScreenImageView.image = [UIImage imageNamed:@"screen2.png"];
            
            _currentlyVisibleSectionNumber --;
            
            break;
            
        case 1:
            
            [self.historySectionView removeFromSuperview];
            
            [self setUpGeneralInfoView];
            
            self.previousButton.hidden = NO;
            
            self.currentScreenImageView.image = [UIImage imageNamed:@"screen1.png"];
            
            _currentlyVisibleSectionNumber --;
            
            self.backButton.hidden = YES;
            
            break;
            
        default:
            
            break;
    }
    
    return;
    
}

- (void) tapGesture:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    [self.generalInfoView.speciesFieldPickerView removeFromSuperview];
    
    if(self.historySectionView != nil)
    {
        [self.historySectionView changeTextViewStatus];
    }
    
//    
//    [self.countryPickerView setHidden:YES];
//    
//    [self.provincePickerView setHidden:YES];
//    
//    [self.datePicker resignFirstResponder];
//    
//    [self.datePicker removeFromSuperview];
//    
//    [self.datePicker setHidden:YES];
}


-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
//-(void) viewDidAppear:(BOOL)animated
//{
//    
//    NSLog(@" VIEW FRAME %@",NSStringFromCGRect(self.view.frame));
//    
//    NSLog(@"SCROLL VIEW FRAME %@",NSStringFromCGRect(self.scrollView.frame));
//    
//    NSLog(@" VIEW FRAME %@",NSStringFromCGRect(self.generalInfoView.frame));
//    
//}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
