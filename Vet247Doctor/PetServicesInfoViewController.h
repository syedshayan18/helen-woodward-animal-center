//
//  PetServicesInfoViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/3/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReferralsInfo.h"

@interface PetServicesInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img_Servicesimg;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (strong,nonatomic) ReferralsInfo * loadReferralDetails;
-(void) getReferralsData:(ReferralsInfo *) info;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_back;
@property (strong,nonatomic) NSMutableArray *refpetdetails;
@end
