//
//  Chatgettermodel.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/6/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Chatgettermodel : NSObject

-(id)initWithDictionary:(NSDictionary *)dictionary;
-(id)initWithDict:(NSDictionary *)dictionary;


@property (strong,nonatomic)NSDictionary *chatdict;
@property (strong,nonatomic) NSString *chatmessages,*userimage,*lastmessage,*isread,*msgid,*sendername,*msgsender;



@property (strong,nonatomic)NSDictionary *lastmsgDict,*ReceiverDict,*SenderDict;
@property (strong,nonatomic) NSString *lastmsg,*datecreated,*isreadlastmsg,*Sender_ID,*Report_ID,*Receiver_ID;

@property (strong,nonatomic) NSString *senderimagepath,*Senderfirstname,*Senderlastname,*msgtype;





@end
