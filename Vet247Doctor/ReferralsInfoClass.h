//
//  ReferralsInfoClass.h
//  Vet247Doctor
//
//  Created by APPLE on 8/22/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReferralsInfoClass : NSObject

@property (nonatomic,retain) NSMutableDictionary * dataContainer;

+(ReferralsInfoClass*) getPetReferralsDataManagerSharedInstance;

//-(void) insertPetProfileInfo:(NSString*)value :(NSString*)NsKey;

-(NSMutableDictionary *) getPetReferralsInfo;

//-(void) setPetData:(NSMutableDictionary*) petData;

+(void) removePetReferralsInfo;

@end
