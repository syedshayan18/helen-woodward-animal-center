//
//  ReferralsDetailController.m
//  Vet247Doctor
//
//  Created by APPLE on 8/18/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "Masonry.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "ControlsFactory.h"

#import "ReferralsDetailController.h"

#import "ReferralsMapViewController.h"

#import "PetProfileDataManager.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import "SDWebImage/UIImageView+WebCache.h"

//#import "ReferralsInfo.h"

@interface ReferralsDetailController ()

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *messagesLbl;

@property (strong,nonatomic) UIButton *backBtn;

@property (strong, nonatomic) UIScrollView * referralViewScroller;

@property (strong, nonatomic) UIButton * companyIconButton;

@property (strong, nonatomic) UILabel * companyNameLable;

@property (strong, nonatomic) UILabel * companyNameValueLbl;

@property (strong, nonatomic) UILabel * phoneLable;

@property (strong, nonatomic) UITextView * phoneValueLbl;

@property (strong, nonatomic) UILabel * faxLable;

@property (strong, nonatomic) UILabel * faxValueLbl;

@property (strong, nonatomic) UILabel * emailLable;

@property (strong, nonatomic) UITextView * emailValueLbl;

@property (strong, nonatomic) UILabel * webLable;

@property (strong, nonatomic) UITextView * webValueLbl;

@property (strong, nonatomic) UILabel * addressLable;

@property (strong, nonatomic) UILabel * addressValueLbl;

@property (strong, nonatomic) UILabel * aboutLable;

@property (strong, nonatomic) UILabel * aboutValueLbl;

@property (strong, nonatomic) UILabel * whatIsNewLable;

@property (strong, nonatomic) UILabel * whatIsNewValueLbl;

@property (strong, nonatomic) UIImageView * thumbNailImageLeft;

@property (strong, nonatomic) UIImageView * thumbNailImageRight;

@property (strong,nonatomic) ReferralsInfo * loadReferralDetails;

@property (strong, nonatomic) UITapGestureRecognizer * phoneValueLableTapped;

@end

@implementation ReferralsDetailController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addComponents];
    
    [self updateReferralsDetail];
    
}

-(void) getReferralsData:(ReferralsInfo *) info
{
    
    self.loadReferralDetails = info;
    
}

-(void) addComponents
{
    [self setUpHeaderView];
    
    [self setUpReferralsDetailsView];
}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        //make.top.equalTo(self.view.mas_top);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    self.backBtn=[ControlsFactory getButton];
    
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    
    [self.backBtn addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(54);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
        
    }];
    
    UIEdgeInsets paddingForMessagesLabel = UIEdgeInsetsMake(0, 0, 15, 0);
    
    self.messagesLbl =[ControlsFactory getLabel];
    
    self.messagesLbl.text=@"Company Name";
    
    [self.messagesLbl setTextAlignment:NSTextAlignmentCenter];
    
    self.messagesLbl.font= [UIFont systemFontOfSize:20.0];
    
    self.messagesLbl.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.messagesLbl];
    
    [self.messagesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.left.mas_equalTo(self.backBtn.mas_right).with.offset(10);
        
        make.right.mas_equalTo(self.headerView.mas_right).with.offset(-10);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForMessagesLabel.bottom);
        
    }];
}

-(void) setUpReferralsDetailsView
{
    // setting up Referrals View Scroller
    
    self.referralViewScroller = [ControlsFactory getScrollView];
    
    self.referralViewScroller.backgroundColor = [UIColor clearColor];
    
    self.referralViewScroller.clipsToBounds=YES;
    
    self.referralViewScroller.scrollEnabled = YES;
    
    self.referralViewScroller.canCancelContentTouches = NO;
    
    self.referralViewScroller.bounces = NO;
    
    self.referralViewScroller.delegate = self;
    
    [self.view addSubview:self.referralViewScroller];
    
    UIEdgeInsets scrollerPadding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.referralViewScroller mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(scrollerPadding.top);
        
        make.left.equalTo(self.view.mas_left);
        
        make.right.equalTo(self.view.mas_right);
        
        make.bottom.equalTo(self.view.mas_bottom);
        
    }];
    
    // setting up Company Icon Image Button
    
    self.companyIconButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.companyIconButton setBackgroundImage:[UIImage imageNamed:@"petFood.png"] forState:UIControlStateNormal];
    
    self.companyIconButton.layer.cornerRadius = 5;
    
    [self.companyIconButton.layer setMasksToBounds:YES];
    
    self.companyIconButton.layer.cornerRadius = self.companyIconButton.bounds.size.width/2 / 2.0;
    
    [self.companyIconButton addTarget:self action:@selector(companyIconButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.companyIconButton.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(20, 10, 15, 0);
    
    [self.referralViewScroller addSubview:self.companyIconButton];
    
    [self.companyIconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.referralViewScroller.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(100);
        
        make.width.mas_equalTo(100);
        
        make.top.equalTo(self.referralViewScroller.mas_top).with.offset(padding.top);
        
    }];
    
    // setting Lable for Company Name
    
    self.companyNameLable =[ControlsFactory getLabel];
    
    self.companyNameLable.text=@"Company Name";
    
    self.companyNameLable.backgroundColor = [UIColor clearColor];
    
    [self.companyNameLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.companyNameLable setTextAlignment:NSTextAlignmentLeft];
    
    self.companyNameLable.font= [UIFont systemFontOfSize:17.0];
    
    [self.referralViewScroller addSubview:self.companyNameLable];
    
    padding = UIEdgeInsetsMake(15, 3, 15, 0);
    
    [self.companyNameLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.referralViewScroller.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.companyIconButton.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(20);
        
    }];
    
    // setting lable for Compnay Name value
    
    self.companyNameValueLbl =[ControlsFactory getLabel];
    
    self.companyNameValueLbl.text=@"Pet Foodie";
    
    self.companyNameValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.companyNameValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.companyNameValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.companyNameValueLbl.font= [UIFont systemFontOfSize:14.0];
    
    [self.referralViewScroller addSubview:self.companyNameValueLbl];
    
    padding = UIEdgeInsetsMake(3, 2, 2, 0);
    
    [self.companyNameValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.companyNameLable.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.companyNameLable.mas_left).with.offset(padding.left - 2);
        
        make.height.mas_equalTo(17);
        
        make.width.mas_equalTo(190);
        
    }];
    
    // setting Lable for Phone
    
    self.phoneLable =[ControlsFactory getLabel];
    
    self.phoneLable.text=@"Phone";
    
    self.phoneLable.backgroundColor = [UIColor clearColor];
    
    [self.phoneLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.phoneLable setTextAlignment:NSTextAlignmentLeft];
    
    self.phoneLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.phoneLable];
    
    padding = UIEdgeInsetsMake(10, 3, 15, 0);
    
    [self.phoneLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.companyNameValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.companyIconButton.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
        make.width.mas_equalTo(90);
        
    }];
    
    // setting lable for Phone value
    
    self.phoneValueLbl =[ControlsFactory getTextView];
    
    self.phoneValueLbl.text=@"3251-882-56";
    
    self.phoneValueLbl.backgroundColor = [UIColor clearColor];
    
//    [self.phoneValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.phoneValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.phoneValueLbl.font= [UIFont systemFontOfSize:14.0];
    
    self.phoneValueLbl.editable = NO;
    
    self.phoneValueLbl.scrollEnabled = NO;
    
//    self.phoneValueLbl.userInteractionEnabled = YES;
    
    self.phoneValueLbl.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
   
    [self.phoneValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.referralViewScroller addSubview:self.phoneValueLbl];
    
    padding = UIEdgeInsetsMake(0, 0, 2, 0);
    
    [self.phoneValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.phoneLable.mas_bottom).with.offset(-5);
        
        make.left.equalTo(self.phoneLable.mas_left).with.offset(-4);
        
        make.height.mas_equalTo(25);
        
        make.width.mas_equalTo(95);
        
    }];
    
    // setting Lable for Fax
    
    self.faxLable =[ControlsFactory getLabel];
    
    self.faxLable.text=@"Fax";
    
    self.faxLable.backgroundColor = [UIColor clearColor];
    
    [self.faxLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.faxLable setTextAlignment:NSTextAlignmentLeft];
    
    self.faxLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.faxLable];
    
    padding = UIEdgeInsetsMake(10, 15, 15, 5);
    
    [self.faxLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.companyNameValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.phoneValueLbl.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
        make.width.mas_equalTo(90);
        
    }];
    
    // setting lable for Fax value
    
    self.faxValueLbl =[ControlsFactory getLabel];
    
    self.faxValueLbl.text=@"3251-882-56";
    
    self.faxValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.faxValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.faxValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.faxValueLbl.font= [UIFont systemFontOfSize:14.0];
    
    self.faxValueLbl.adjustsFontSizeToFitWidth = YES;
    
    [self.referralViewScroller addSubview:self.faxValueLbl];
    
    padding = UIEdgeInsetsMake(5, 0, 2, 0);
    
    [self.faxValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.faxLable.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.faxLable.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(90);
        
        make.height.mas_equalTo(15);
        
    }];
    
    // setting Lable for Email
    
    self.emailLable =[ControlsFactory getLabel];
    
    self.emailLable.text=@"Email";
    
    self.emailLable.backgroundColor = [UIColor clearColor];
    
    [self.emailLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.emailLable setTextAlignment:NSTextAlignmentLeft];
    
    self.emailLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.emailLable];
    
    padding = UIEdgeInsetsMake(10, 3, 15, 0);
    
    [self.emailLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.phoneValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.companyIconButton.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
        make.width.mas_equalTo(90);
        
    }];
    
    // setting lable for Email value
    
    self.emailValueLbl = [ControlsFactory getTextView];
    
    self.emailValueLbl.text = @"info@test.com";
    
//    self.emailValueLbl.numberOfLines = 2;
    
    self.emailValueLbl.editable = NO;
    
    self.emailValueLbl.scrollEnabled = NO;
    
//    self.emailValueLbl.dataDetectorTypes = UIDataDetectorTypeAll;
    
    self.emailValueLbl.dataDetectorTypes = UIDataDetectorTypeLink ;//UIDataDetectorTypeAddress // UIDataDetectorTypeLink
    
    self.emailValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.emailValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.emailValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.emailValueLbl.font = [UIFont systemFontOfSize:14.0];
    
//    self.emailValueLbl.adjustsFontSizeToFitWidth = YES;
    
    [self.referralViewScroller addSubview:self.emailValueLbl];
    
    padding = UIEdgeInsetsMake(0, 0, 2, 0);
    
    [self.emailValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.emailLable.mas_bottom).with.offset(-5);
        
        make.left.equalTo(self.emailLable.mas_left).with.offset(-4);
        
        make.width.mas_equalTo(95);
        
        make.height.mas_equalTo(25);
        
    }];
    
    // setting Lable for Web
    
    self.webLable =[ControlsFactory getLabel];
    
    self.webLable.text=@"Web";
    
    self.webLable.backgroundColor = [UIColor clearColor];
    
    [self.webLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.webLable setTextAlignment:NSTextAlignmentLeft];
    
    self.webLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.webLable];
    
    padding = UIEdgeInsetsMake(10, 15, 15, 5);
    
    [self.webLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.phoneValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.emailLable.mas_right).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
        make.width.mas_equalTo(90);
        
    }];
    
    // setting lable for Web value
    
    self.webValueLbl =[ControlsFactory getTextView];
    
    self.webValueLbl.text=@"www.test.ca";
    
    self.webValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.webValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.webValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.webValueLbl.editable = NO;
    
    self.webValueLbl.scrollEnabled = NO;
    
//    self.webValueLbl.dataDetectorTypes = UIDataDetectorTypeAll;
    
    self.webValueLbl.dataDetectorTypes = UIDataDetectorTypeLink;
    
    self.webValueLbl.font= [UIFont systemFontOfSize:14.0];
    
    [self.referralViewScroller addSubview:self.webValueLbl];
    
    padding = UIEdgeInsetsMake(0, 0, 2, 0);
    
    [self.webValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.webLable.mas_bottom).with.offset(-5);
        
        make.left.equalTo(self.webLable.mas_left).with.offset(-4);
        
        make.right.equalTo(self.view.mas_right).with.offset(-5);
        
        make.height.mas_equalTo(25);
    
    }];
    
    // setting Lable for Address
    
    self.addressLable =[ControlsFactory getLabel];
    
    self.addressLable.text=@"Address";
    
    self.addressLable.backgroundColor = [UIColor clearColor];
    
    [self.addressLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.addressLable setTextAlignment:NSTextAlignmentLeft];
    
    self.addressLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.addressLable];
    
    padding = UIEdgeInsetsMake(20, 3, 15, 5);
    
    [self.addressLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.emailLable.mas_bottom).with.offset(45);
        
        make.left.equalTo(self.companyIconButton.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
    }];
    
    // setting lable for Address value
    
    self.addressValueLbl =[ControlsFactory getLabel];
    
    self.addressValueLbl.text=@"2300, test address, CA, address, testing, post:2300";
    
    self.addressValueLbl.backgroundColor = [UIColor clearColor];
    
    self.addressValueLbl.numberOfLines = 0;
    
    [self.addressValueLbl sizeToFit];
    
    [self.addressValueLbl setTextColor:[UIColor colorWithRed:0.4169 green:0.4169 blue:0.4169 alpha:1.0]];
    
    [self.addressValueLbl setTextAlignment:NSTextAlignmentLeft];
    
    self.addressValueLbl.font= [UIFont systemFontOfSize:14.0];
    
    [self.referralViewScroller addSubview:self.addressValueLbl];
    
    padding = UIEdgeInsetsMake(5, 0, 2, 20);
    
    [self.addressValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.addressLable.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.addressLable.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width - 25);
        
    }];
    
    // setting Lable for About
    
    self.aboutLable =[ControlsFactory getLabel];
    
    self.aboutLable.text=@"About";
    
    self.aboutLable.backgroundColor = [UIColor clearColor];
    
    [self.aboutLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.aboutLable setTextAlignment:NSTextAlignmentLeft];
    
    self.aboutLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.aboutLable];
    
    padding = UIEdgeInsetsMake(20, 3, 15, 5);
    
    [self.aboutLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.addressValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.companyIconButton.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
    }];
    
    // setting TextView for About value
    
    self.aboutValueLbl = [ControlsFactory getLabel];
    
    self.aboutValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.aboutValueLbl setFont:[UIFont systemFontOfSize:14]];
    
    self.aboutValueLbl.text = @"Lorem ipsum dolor sit amet, consectetur adpiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus,ac";
    
    self.aboutValueLbl.textAlignment = NSTextAlignmentLeft;
    
    self.aboutValueLbl.numberOfLines = 0;
    
    [self.aboutValueLbl sizeToFit];
    
    self.aboutValueLbl.userInteractionEnabled = NO;
    
    self.aboutValueLbl.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    [self.referralViewScroller addSubview:self.aboutValueLbl];
    
    padding = UIEdgeInsetsMake(3, 0, 0 , 0);
    
    [self.aboutValueLbl mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.aboutLable.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.aboutLable.mas_left).with.offset(-padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width - 20);
        
    }];
    
    // setting Lable for What's new
    
    self.whatIsNewLable =[ControlsFactory getLabel];
    
    self.whatIsNewLable.text=@"What's New";
    
    self.whatIsNewLable.backgroundColor = [UIColor clearColor];
    
    [self.whatIsNewLable setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
    
    [self.whatIsNewLable setTextAlignment:NSTextAlignmentLeft];
    
    self.whatIsNewLable.font= [UIFont systemFontOfSize:16.0];
    
    [self.referralViewScroller addSubview:self.whatIsNewLable];
    
    padding = UIEdgeInsetsMake(20, 3, 15, 5);
    
    [self.whatIsNewLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.aboutValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.companyIconButton.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(18);
        
    }];
    
    // setting TextView for About value
    
    self.whatIsNewValueLbl = [ControlsFactory getLabel];
    
    self.whatIsNewValueLbl.backgroundColor = [UIColor clearColor];
    
    [self.whatIsNewValueLbl setFont:[UIFont systemFontOfSize:14]];
    
    self.whatIsNewValueLbl.text = @"Default Data:Lorem ipsum dolor sit amet, consectetur adpiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus,ac";
    
    self.whatIsNewValueLbl.numberOfLines = 0;
    
    [self.whatIsNewValueLbl sizeToFit];
    
    self.whatIsNewValueLbl.textAlignment = NSTextAlignmentLeft;
    
    self.whatIsNewValueLbl.userInteractionEnabled = NO;
    
    self.whatIsNewValueLbl.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    [self.referralViewScroller addSubview:self.whatIsNewValueLbl];
    
    padding = UIEdgeInsetsMake(3, 0, 000 , 0);
    
    [self.whatIsNewValueLbl mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.whatIsNewLable.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.whatIsNewLable.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.view.frame.size.width - 20);
        
//        make.bottom.mas_equalTo(self.referralViewScroller.mas_bottom).with.offset(-10);
        
    }];
    
}

-(void) updateReferralsDetail
{
    
    self.messagesLbl.text = self.loadReferralDetails.compnayName;
    
    self.companyNameValueLbl.text = self.loadReferralDetails.compnayName;
    
    self.phoneValueLbl.text = self.loadReferralDetails.companyPhone;
    
    self.faxValueLbl.text = self.loadReferralDetails.companyFax;
    
    self.emailValueLbl.text = self.loadReferralDetails.companyEmail;
    
    self.webValueLbl.text = self.loadReferralDetails.companyWeb;
    
    self.addressValueLbl.text = self.loadReferralDetails.companyAddress;
    
    self.aboutValueLbl.text = self.loadReferralDetails.aboutCompany;
    
    self.whatIsNewValueLbl.text = self.loadReferralDetails.whatNewAboutCompany;
    
    if([self.loadReferralDetails.profilePicture isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/referral/"])
    {
        // http://yourvetsnow.com/petappportal/app/webroot/img/referral/
        
        [self.companyIconButton setBackgroundImage:[UIImage imageNamed:@"petFood.png"] forState:UIControlStateNormal];
       
    }
    //@"petFood.png",@"petVet.png",@"petClinic.png"
    else
    {
        UIImageView * imgForButton = [[UIImageView alloc] init];
        
        [imgForButton sd_setImageWithURL:[NSURL URLWithString:self.loadReferralDetails.profilePicture] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        [self.companyIconButton setBackgroundImage:imgForButton.image forState:UIControlStateNormal];
    }
    
    if([self.loadReferralDetails.thumbNailLeft  isEqualToString:@"0"] || [self.loadReferralDetails.thumbNailRight  isEqualToString:@"0"])
    {
        [self.whatIsNewValueLbl mas_makeConstraints:^(MASConstraintMaker * make){
            
            make.bottom.mas_equalTo(self.referralViewScroller.mas_bottom).with.offset(-10);
            
        }];
    }
    else
    {
        [self setUpTwoThumbNailsOnBottom];
    }
}

-(void) setUpTwoThumbNailsOnBottom
{
    
    self.thumbNailImageLeft =[ControlsFactory getImageView];
    
    self.thumbNailImageLeft.backgroundColor = [UIColor lightGrayColor];
    
    [self.thumbNailImageLeft setImageWithURL:[NSURL URLWithString:self.loadReferralDetails.thumbNailLeft] placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(10, 0, 0, 0);
    
    [self.referralViewScroller addSubview:self.thumbNailImageLeft];
    
    [self.thumbNailImageLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.whatIsNewValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.referralViewScroller.mas_left).with.offset(5);
        
        make.bottom.mas_equalTo(self.referralViewScroller.mas_bottom).with.offset(-10);
        
        make.right.equalTo(self.referralViewScroller.mas_centerX).with.offset(-3);
        
        make.height.mas_equalTo(120);
        
    }];

    self.thumbNailImageLeft.contentMode = UIViewContentModeScaleAspectFit;
    
    self.thumbNailImageRight =[ControlsFactory getImageView];
    
    self.thumbNailImageRight.backgroundColor = [UIColor lightGrayColor];
    
    [self.thumbNailImageRight setImageWithURL:[NSURL URLWithString:self.loadReferralDetails.thumbNailRight] placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    [self.referralViewScroller addSubview:self.thumbNailImageRight];
    
    [self.thumbNailImageRight mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.whatIsNewValueLbl.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.thumbNailImageLeft.mas_right).with.offset(6);
        
        make.bottom.mas_equalTo(self.referralViewScroller.mas_bottom).with.offset(-10);
        
        make.right.equalTo(self.view.mas_right).with.offset(-5);
        
//        make.height.mas_equalTo(120);
    }];
    
    self.thumbNailImageRight.contentMode = UIViewContentModeScaleAspectFit;

}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
//    if (sender.contentOffset.x != 0)
//    {
//        
//        CGPoint offset = sender.contentOffset;
//        
//        offset.x = 0;
//        
//        sender.contentOffset = offset;
//        
//    }
}

-(void) phoneValueLableGesture:(UITapGestureRecognizer *) recognizer
{
   if(self.phoneValueLbl.text.length > 0 && self.phoneValueLbl.text != nil)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", self.phoneValueLbl.text]]];
    }
}


//- (void) tapLoginGesture:(UITapGestureRecognizer *)recognizer
//{
//    [self.view endEditing:YES];
//}

-(void) backButtonClicked: (UIButton *) btn
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) companyIconButtonClicked: (UIButton *) iconButton
{
    
    //    ReferralsMapViewController * mapViewController = [[ReferralsMapViewController alloc] init];
    //
    //    [self.navigationController pushViewController:mapViewController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
