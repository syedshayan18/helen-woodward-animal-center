//
//  MessageInboxTVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/21/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageInboxTVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_doctoravailable;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastmessage;
@property (weak, nonatomic) IBOutlet UIImageView *img_doctor;

@end
