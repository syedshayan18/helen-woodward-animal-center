//
//  LostreportViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/16/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"
#import <UITextView_Placeholder/UITextView+Placeholder.h>
@import GoogleMaps;
@interface LostreportViewController : UIViewController <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btn_sendlostreport;
@property (weak, nonatomic) IBOutlet UITextView *txtview_details;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *PlacesTextField;
@property (weak, nonatomic) IBOutlet UIView *subview;
@property (strong,nonatomic) NSString *passedpetid;
@property (strong,nonatomic) NSMutableDictionary *lostdict;
@property (strong,nonatomic) GMSMarker *marker;
@property (strong,nonatomic) NSString *Markerlocationname;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property BOOL firstLocationUpdate;
@property int radius;
@property (strong,nonatomic) NSString *locationname;
@property (strong,nonatomic) NSString *userid;
@property (weak,nonatomic) UITextField *globaltextfield;

@end
