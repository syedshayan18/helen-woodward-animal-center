//  This is my solution to the SO question "MKAnnotationView - Lock custom annotation view to pin on location updates":
//  http://stackoverflow.com/questions/6392931/mkannotationview-lock-custom-annotation-view-to-pin-on-location-updates
//
//  CalloutAnnotationView based on the work at: 
//  http://blog.asolutions.com/2010/09/building-custom-map-annotation-callouts-part-1/
//  
//  The Example* classes represent things you will probably change in your own project to fit your needs.  Consider CalloutAnnotationView abstract - it must be subclassed (here it's subclass is ExampleCalloutView), and linked with a xib connecting the IBOutlet for contentView.  The callout should resize to fit whatever view you supply as contentView.

#import "CalloutAnnotationView.h"
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>

#define CalloutMapAnnotationViewBottomShadowBufferSize 6.0f
#define CalloutMapAnnotationViewContentHeightBuffer 8.0f
#define CalloutMapAnnotationViewHeightAboveParent 13.0f
#define CalloutMapAnnotationViewInset 4.0f

@interface CalloutAnnotationView()

@property (nonatomic, readonly) CGFloat yShadowOffset;
@property (nonatomic) BOOL animateOnNextDrawRect;
@property (nonatomic) CGRect endFrame;

- (void)prepareFrameSize;
- (void)prepareOffset;
- (CGFloat)relativeParentXPosition;
- (void)adjustMapRegionIfNeeded;

@end


@implementation CalloutAnnotationView

@synthesize parentAnnotationView = _parentAnnotationView;
@synthesize mapView = _mapView;
@synthesize contentView = _contentView;
@synthesize animateOnNextDrawRect = _animateOnNextDrawRect;
@synthesize endFrame = _endFrame;
@synthesize yShadowOffset = _yShadowOffset;

- (id)initWithAnnotation:(id<MKAnnotation>)annotation;
{
    [NSException raise:NSInternalInconsistencyException 
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (id) initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier Map:(MKMapView *)map{
	if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])) {
		self.enabled = NO;
		self.backgroundColor = [UIColor clearColor];
        self.mapView = map;        
	}
	return self;
}

- (void)setAnnotation:(id <MKAnnotation>)annotation {
	[super setAnnotation:annotation];
    
    [self prepareFrameSize];
	[self prepareOffset];
	[self adjustMapRegionIfNeeded];
	[self setNeedsDisplay];
}

- (void)setAnnotationAndAdjustMap:(id <MKAnnotation>)annotation {
	[super setAnnotation:annotation];
    
    self.parentAnnotationView.hidden = YES;
    NSLog(@"%@",self.parentAnnotationView);
    
	[self prepareFrameSize];
	[self prepareOffset];
	[self adjustMapRegionIfNeeded];
	[self setNeedsDisplay];
}

- (void)prepareFrameSize {

//	CGRect frame = self.frame;
//	CGFloat height = self.contentView.frame.size.height +
//	CalloutMapAnnotationViewContentHeightBuffer +
//	CalloutMapAnnotationViewBottomShadowBufferSize + 14 + (2*CalloutMapAnnotationViewInset);
//	
//	frame.size = CGSizeMake(self.contentView.frame.size.width + 14 + (2*CalloutMapAnnotationViewInset), height);
//	self.frame = frame;
    
	CGRect frame = self.frame;
    
	CGFloat height = 128.0;
    CGFloat width = 164.0;
    
#if  defined (Zwijndrecht)  || defined (Rijksvastgoedbedrijf)

     height = 128.0;
     width = 237.0;
    
#endif
	
	frame.size = CGSizeMake(width,height);
	self.frame = frame;
    
}

- (void)prepareOffset {
    
//  self.centerOffset = CGPointMake(5 , -40);
    self.draggable = NO;

}

- (void)adjustMapRegionIfNeeded {
}

- (CGFloat)xTransformForScale:(CGFloat)scale {
	CGFloat xDistanceFromCenterToParent = self.endFrame.size.width / 2 - [self relativeParentXPosition];
	return (xDistanceFromCenterToParent * scale) - xDistanceFromCenterToParent;
}

- (CGFloat)yTransformForScale:(CGFloat)scale {
	CGFloat yDistanceFromCenterToParent = (((self.endFrame.size.height) / 2) + CalloutMapAnnotationViewBottomShadowBufferSize + CalloutMapAnnotationViewHeightAboveParent);
	return yDistanceFromCenterToParent - yDistanceFromCenterToParent * scale;
}

- (void)animateIn {
	self.endFrame = self.frame;
	CGFloat scale = 0.001f;
	self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
	[UIView beginAnimations:@"animateIn" context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationDuration:0.075];
	[UIView setAnimationDidStopSelector:@selector(animateInStepTwo)];
	[UIView setAnimationDelegate:self];
	scale = 1.1;
	self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
	[UIView commitAnimations];
}

- (void)animateInStepTwo {
	[UIView beginAnimations:@"animateInStepTwo" context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.1];
	[UIView setAnimationDidStopSelector:@selector(animateInStepThree)];
	[UIView setAnimationDelegate:self];
	
	CGFloat scale = 0.95;
	self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
	
	[UIView commitAnimations];
}

- (void)animateInStepThree {
	[UIView beginAnimations:@"animateInStepThree" context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.075];
	
	CGFloat scale = 1.0;
	self.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, [self xTransformForScale:scale], [self yTransformForScale:scale]);
	
	[UIView commitAnimations];
}

- (void)didMoveToSuperview {
	[self adjustMapRegionIfNeeded];
	[self animateIn];
}

- (CGFloat)yShadowOffset
{
	if (!_yShadowOffset)
    {
		float osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
		if (osVersion >= 3.2) {
			_yShadowOffset = 6;
		} else {
			_yShadowOffset = -6;
		}
		
	}
	return _yShadowOffset;
}

- (CGFloat)relativeParentXPosition {
	return self.bounds.size.width / 2;
}

- (void)dealloc {
//    [_parentAnnotationView release];
//    _parentAnnotationView = nil;
//    [_mapView release];
//    _mapView = nil;
//	[_contentView release];
//    _contentView = nil;
//    [super dealloc];
}

- (void)didSelectAnnotationViewInMap:(MKMapView *)mapView{
    
}

- (void)didDeselectAnnotationViewInMap:(MKMapView *)mapView{
    
    self.parentAnnotationView.hidden = NO;
}

- (void)setContentView:(UIView *)newContentView{
    
    
    UIView* oldView = _contentView;
    [newContentView retain];
    
    _contentView = newContentView;

    [oldView removeFromSuperview];
    [oldView release];
    
    [self addSubview:newContentView];
}

@end