
#import <Foundation/Foundation.h>
#import "CustomCalloutProtocols.h"
#import "ExampleCalloutView.h"
//#import "ShapeMarkerData.h"

@interface ExampleCalloutAnnotation : NSObject
<MKAnnotation, CustomAnnotationProtocol>
{
    CLLocationCoordinate2D _coordinate;
    ExampleCalloutView* calloutView;
}

@property (nonatomic, assign) ShapeMarkerData* calloutDataInfo;
@property (nonatomic, assign) MKMapView* mapView;

- (id) initWithLat:(CGFloat)latitute lon:(CGFloat)longitude;

@end