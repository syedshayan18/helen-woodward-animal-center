//
//  AppDelegate.h
//  Vet247Doctor
//
//  Created by Malik on 7/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//  USER/CLient

#import <UIKit/UIKit.h>

#import "Reachability.h"

#define AnnualSubscriptionPaid_KEY @"AnnualSubscriptionPaid_KEY"

#define AnnualSubscriptionPaid_Cleared @"YES"

#define AnnualSubscriptionPaid_NOT_Cleared @"NO"

static NSString *const AppWentToBackGround = @"appWentToBackground";

static NSString *const LeaveCallScreen = @"leaveCallScreen";



@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Reachability* wifiReach;
    BOOL iswifiReach;
    BOOL isinternetReach;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *fullMessage;
@property (assign, nonatomic) int selectedPaymentPackageID;
@property (assign, nonatomic) BOOL isSelectedAnyPaymentPackage;


-(BOOL)availableInternet;
-(BOOL)availableInternetWithoutMessage;


@end

