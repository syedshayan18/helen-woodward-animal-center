//
//  ReferralsInfo.m
//  Vet247Doctor
//
//  Created by APPLE on 8/22/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ReferralsInfo.h"

@implementation ReferralsInfo

@synthesize profilePicture;

@synthesize compnayName;

@synthesize companyPhone;

@synthesize companyFax;

@synthesize companyEmail;

@synthesize companyWeb;

@synthesize companyAddress;

@synthesize aboutCompany;

@synthesize whatNewAboutCompany;

@synthesize longitude;

@synthesize latitude;

@synthesize thumbNailLeft;

@synthesize thumbNailRight;

@end
