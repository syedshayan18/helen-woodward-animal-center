//
//  PackagesViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/14/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackagesViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_coupon;
@property (weak, nonatomic) IBOutlet UIView *view_1day;
@property (weak, nonatomic) IBOutlet UIView *view_1month;
@property (weak, nonatomic) IBOutlet UIView *view_3months;
@property (weak, nonatomic) IBOutlet UIView *view_1year;
@property (weak, nonatomic) IBOutlet UIView *view_free;
@property (weak, nonatomic) IBOutlet UIView *view_6months;
@property (weak, nonatomic) IBOutlet UIButton *btn_1day;
@property (weak, nonatomic) IBOutlet UIButton *btn_1month;
@property (weak, nonatomic) IBOutlet UIButton *btn_3months;
@property (weak, nonatomic) IBOutlet UIButton *btn_6months;
@property (weak, nonatomic) IBOutlet UIButton *btn_1year;
@property (weak, nonatomic) IBOutlet UIButton *btn_accessfree;

@property (weak, nonatomic) IBOutlet UIImageView *img_line;

@end
