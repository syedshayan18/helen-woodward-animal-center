//
//  ReferralsMapViewController.m
//  Vet247Doctor
//
//  Created by APPLE on 8/20/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

//#import "GoogleMaps"

@import GoogleMaps;

#import "ReferralsMapViewController.h"

#import "MapViewLocatorObject.h"

#import "Masonry.h"

#import "ControlsFactory.h"

#import "MBProgressHUD.h"

#import "ReferralsInfo.h"

#import "MapViewLocatorObject.h"

#import "NetworkingClass.h"

#import "ReferralsDetailController.h"

#import "CustomCallOutView.h"

#import "MapAnnotationView.h"

#import "CustomMapView.h"

#import "PetServicesInfoViewController.h"
int indexingButtonNumber = 0;


@interface ReferralsMapViewController ()

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *messagesLbl;

@property (strong,nonatomic) UIButton *backBtn;

@property (strong, nonatomic) UIView * searchView;

@property (strong, nonatomic) UITextField * txtFld;

@property (strong, nonatomic) UIImageView * txtFldImgView;

@property (strong, nonatomic) UIImageView * searchIcon;

@property (strong, nonatomic) CustomMapView * referralsMapView;

@property (strong,nonatomic)  CustomCallOutView *customView;

@property (nonatomic, strong) SMCalloutView *calloutView;

@property (strong,nonatomic)MKAnnotationView *viewOfannotation;

@property (strong,nonatomic) NSMutableArray * referralsArray, * filteredMapObjectsArray;

@property (strong,nonatomic) UIButton *customAnotationButton;

@property (strong,nonatomic) UILabel *callOutLabel;

@property (strong,nonatomic) NSString *indexNumber;



@property (assign) BOOL searching;

@property (assign) CLLocationCoordinate2D userCurrentLocation;

@property (strong, nonatomic) MVPlaceSearchTextField * autoCompletTextField;

@property (assign) GMSPlacesClient * placesClient;

@end

@implementation ReferralsMapViewController
@synthesize headertitle;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    // Auto Complete settings
    self.autoCompletTextField = [[MVPlaceSearchTextField alloc] init];
    
    self.autoCompletTextField.placeSearchDelegate = self;
    
    self.autoCompletTextField.strApiKey = @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM";
    
    self.autoCompletTextField.superViewOfList = self.view;  // View, on which Autocompletion list should be appeared.
    
    self.autoCompletTextField.autoCompleteShouldHideOnSelection = YES;
    
    self.autoCompletTextField.maximumNumberOfAutoCompleteRows = 100;
    //-- auto complete text "ends"
    
    self.userCurrentLocation = CLLocationCoordinate2DMake(0, 0);
    
    [self.referralsMapView setShowsUserLocation:YES];
    
    self.referralsMapView.layer.cornerRadius = 5;
    
    self.referralsMapView.delegate = self;
    
    self.userCurrentLocation = CLLocationCoordinate2DMake(0, 0);
    
    self.calloutView = [SMCalloutView new];
    
    self.calloutView.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
    self.referralsArray = [[NSMutableArray alloc] init];
    
    self.filteredMapObjectsArray = [[NSMutableArray alloc] init];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addComponents];
    
    [self getReferralsData];
    
    [self focusOnUserOwnLocation];
}

-(void)viewDidAppear:(BOOL)animated{
    
    //Optional Properties
    self.autoCompletTextField.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
    
    self.autoCompletTextField.autoCompleteBoldFontName = @"HelveticaNeue";
    
    self.autoCompletTextField.autoCompleteTableCornerRadius = 0.0;
    
    self.autoCompletTextField.autoCompleteRowHeight = 35;
    
    self.autoCompletTextField.autoCompleteTableCellTextColor = [UIColor colorWithWhite:0.131 alpha:1.000];
    
    self.autoCompletTextField.autoCompleteFontSize = 12;
    
    self.autoCompletTextField.autoCompleteTableBorderWidth = 0.5;
    
    self.autoCompletTextField.showTextFieldDropShadowWhenAutoCompleteTableIsOpen = YES;
    
    self.autoCompletTextField.autoCompleteShouldHideOnSelection = YES;
    
    self.autoCompletTextField.autoCompleteShouldHideClosingKeyboard = YES;
    
    self.autoCompletTextField.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    
    self.autoCompletTextField.autoCompleteTableFrame = CGRectMake((self.view.frame.size.width-self.autoCompletTextField.frame.size.width)*0.5 - 10 , self.autoCompletTextField.frame.size.height+90.0, self.autoCompletTextField.frame.size.width+20, 200.0);
    
//    [self.view addSubview:_autoCompletTextField.autoCompleteTableView];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnButtonClicked:) name:@"fromCallOutView" object:nil];
    
    // self.searching = FALSE;
    //self.txtFld.text=@"";
    //[self.filteredMapObjectsArray removeAllObjects];
}

-(void) setUpCustomView {
    
    self.customView=[[CustomCallOutView alloc] init];
    
    [self.customView setUpCustomView];
}

-(void) customAnotationButtonClicked:(UIButton*) button{
    NSLog(@"OK");
}

-(void) addComponents
{
    [self setUpCustomView];
    
    [self setUpHeaderView];
    
    [self setUpMapView];
    
    [self.referralsMapView setShowsUserLocation:YES];
}


- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:255.0/255 green:156.0/255 blue:0.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    self.backBtn=[ControlsFactory getButton];
    
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    [self.backBtn addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(20);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
    }];
    
    UIEdgeInsets paddingForMessagesLabel = UIEdgeInsetsMake(0, 0, 15, 0);
    
    self.messagesLbl =[ControlsFactory getLabel];
    
    self.messagesLbl.text=headertitle;
    
    [self.messagesLbl setTextAlignment:NSTextAlignmentCenter];
    
    self.messagesLbl.font= [UIFont systemFontOfSize:20.0];
    
    self.messagesLbl.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.messagesLbl];
    _messagesLbl.numberOfLines = 2;
    
    [self.messagesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForMessagesLabel.bottom);
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(200);

        
    }];
    
    self.searchView=[ControlsFactory getView];
    
    self.searchView.backgroundColor=[UIColor whiteColor];
    
    UIEdgeInsets paddingForSearchVIew= UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.searchView];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.height.mas_equalTo(70);
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForSearchVIew.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForSearchVIew.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForSearchVIew.top);
        
    }];
    
    self.txtFldImgView=[ControlsFactory getImageView];
    
    self.txtFldImgView.image=[UIImage imageNamed:@"searchRectangle.png"];
    
    UIEdgeInsets paddingForTxtFldImg = UIEdgeInsetsMake(15, 15, 15, 15);
    
    [self.searchView addSubview:self.txtFldImgView];
    
    [self.txtFldImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.searchView.mas_left).with.offset(paddingForTxtFldImg.left);
        
        make.right.equalTo(self.searchView.mas_right).with.offset(-paddingForTxtFldImg.right);
        
        // make.top.equalTo(self.searchView.mas_bottom);
        
        make.top.equalTo(self.searchView.mas_top).with.offset(paddingForTxtFldImg.top);
        
        make.bottom.equalTo(self.searchView.mas_bottom).with.offset(-paddingForTxtFldImg.bottom);
        
    }];
    
    self.searchIcon=[ControlsFactory getImageView];
    
    self.searchIcon.image=[UIImage imageNamed:@"searchIcon.png"];
    
    UIEdgeInsets paddingForSearchIcon= UIEdgeInsetsMake(10, 10, 10, 15);
    
    [self.txtFldImgView addSubview:self.searchIcon];
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForSearchIcon.right);
        
        make.centerY.mas_equalTo(self.txtFldImgView.mas_centerY);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(18);
        
    }];
    
//    self.txtFld = [ControlsFactory getTextField];
//    
//    self.txtFld.backgroundColor=[UIColor clearColor];
//    
//    self.txtFld.placeholder=@"Search Location";
//    
//    [self.txtFld setValue:[UIColor colorWithRed:255.0/255 green:157.0/255 blue:59.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
//    
//    self.txtFld.delegate = self;
//
//    UIEdgeInsets paddingForTxtFld= UIEdgeInsetsMake(0, 25, 0, 0);
//    
//    [self.txtFldImgView addSubview:self.txtFld];
//    
//    self.txtFldImgView.userInteractionEnabled = YES;
//    
//    [self.txtFld mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.equalTo(self.txtFldImgView.mas_left).with.offset(paddingForTxtFld.left);
//        
//        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForTxtFld.right);
//        
//        make.top.equalTo(self.txtFldImgView.mas_top).with.offset(paddingForTxtFld.top);
//        
//        make.bottom.equalTo(self.txtFldImgView.mas_bottom).with.offset(-paddingForTxtFld.bottom);
//        
//    }];
    
    self.autoCompletTextField.backgroundColor = [UIColor clearColor];
    
    self.autoCompletTextField.placeholder = @"Search Location";
    
    [self.autoCompletTextField setValue:[UIColor colorWithRed:255.0/255 green:157.0/255 blue:59.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.autoCompletTextField.placeSearchDelegate = self;
    
    self.autoCompletTextField.delegate = self;
    
    [self.autoCompletTextField awakeFromNib];
    
    UIEdgeInsets paddingForTxtFld= UIEdgeInsetsMake(0, 15, 0, 0);
    
    [self.txtFldImgView addSubview:self.autoCompletTextField];
    
    self.txtFldImgView.userInteractionEnabled = YES;
    
    [self.autoCompletTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.txtFldImgView.mas_left).with.offset(paddingForTxtFld.left);
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForTxtFld.right-25);
        
        make.top.equalTo(self.txtFldImgView.mas_top).with.offset(paddingForTxtFld.top);
        
        make.bottom.equalTo(self.txtFldImgView.mas_bottom).with.offset(-paddingForTxtFld.bottom);
    }];
}

-(void) setUpMapView
{
    self.referralsMapView = [[CustomMapView alloc] init];
    
    self.referralsMapView.delegate = self;
    
    self.referralsMapView.showsUserLocation = YES;
    
    self.referralsMapView.delegate = self;
    
    [self.view addSubview:self.referralsMapView];
    
    UIEdgeInsets paddingForMapView = UIEdgeInsetsMake(10, 0, 0, 0);
    
    [self.referralsMapView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForMapView.left);
        
        make.top.equalTo(self.txtFldImgView.mas_bottom).with.offset(paddingForMapView.top);
        
        make.right.equalTo(self.view.mas_right).with.offset(paddingForMapView.right);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(paddingForMapView.bottom);
        
    }];
}

-(void) getReferralsData
{
    
    NetworkingClass * bringReferralsData = [[NetworkingClass alloc] init];
    
    bringReferralsData.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [bringReferralsData getReferralsDetails];
    
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSDictionary * saveReferralsData;
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        saveReferralsData = [data objectForKey:@"data"];
        
        int i = 0;
        
        for (id obj in saveReferralsData) {
            
            ReferralsInfo * info = [[ReferralsInfo alloc] init];
            
            info.profilePicture = [obj objectForKey:COMPANY_Profile];
            
            info.compnayName = [obj objectForKey:COMPANY_Name];
            
            info.companyPhone = [obj objectForKey:COMPANY_Phone];
            
            info.companyFax = [obj objectForKey:COMPANY_Fax];
            
            info.companyEmail = [obj objectForKey:COMPANY_Email];
            
            info.companyWeb = [obj objectForKey:COMPANY_Web];
            
            info.companyAddress = [obj objectForKey:COMPANY_Address];
            
            info.aboutCompany = [obj objectForKey:About_COMPANY];
            
            info.whatNewAboutCompany = [obj objectForKey:NewAbout_COMPANY];
            
            info.longitude = [[obj objectForKey:LONGITUDE] doubleValue];
            
            info.latitude = [[obj objectForKey:LATITUDE] doubleValue];
            
            if([[obj objectForKey:THUMBNAILLeft] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/referral/"]) {
                
                info.thumbNailLeft = @"0";
            }
            else {
                info.thumbNailLeft = [obj objectForKey:THUMBNAILLeft];
            }
            
            if([[obj objectForKey:THUMBNAILRIGHT] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/referral/"]) {
                
                info.thumbNailRight = @"0";
            }
            else {
                info.thumbNailRight = [obj objectForKey:THUMBNAILRIGHT];
            }
            
            [self.referralsArray insertObject:info atIndex:i];
            
            i++;
            
        }
        
        if(self.referralsArray.count == 0)
        {
          // UIAlertView *noDataAlert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"No record found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
           // [noDataAlert show];
        }
        else
        {
            [self loadDataToMap];
        }
    }
    
    else if ([data objectForKey:@"msg"])
    {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [errorAlert show];
    }
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void) loadDataToMap
{
    [self.referralsMapView removeAnnotations:self.referralsMapView.annotations];
    
    CLLocationCoordinate2D longLat;
    
    ReferralsInfo * data;
    
    if(!self.searching)
    {
        for (int i = 0 ; i < self.referralsArray.count ; i ++)
        {
            data = (ReferralsInfo *) [self.referralsArray objectAtIndex:i];
            
            longLat.longitude = data.longitude;
            
            longLat.latitude = data.latitude;
            
            MapViewLocatorObject * myMapObject = [[MapViewLocatorObject alloc] initWithTitle:data.compnayName Location:longLat Index:i];
            
            [self.referralsMapView addAnnotation:myMapObject];
        }
        
        if (self.userCurrentLocation.latitude == 0 || self.userCurrentLocation.longitude == 0) {
            
            [self focusOnAnnotationsRegion:self.referralsMapView.annotations];
        }else{
        
            [self focusOnUserOwnLocation];
        }
    }
    else if(self.searching)
    {
        for (int i = 0 ; i < self.filteredMapObjectsArray.count ; i ++)
        {
            data = (ReferralsInfo *) [self.filteredMapObjectsArray objectAtIndex:i];
            
            longLat.longitude = data.longitude;
            
            longLat.latitude = data.latitude;
            
            MapViewLocatorObject * myMapObject = [[MapViewLocatorObject alloc] initWithTitle:data.compnayName Location:longLat Index:i];
            
            [self.referralsMapView addAnnotation:myMapObject];
        }
        
        [self focusOnAnnotationsRegion:self.referralsMapView.annotations];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    string = [textField.text stringByReplacingCharactersInRange:range withString:string];

    NSLog(@"\n\n Should Change String Length is :: %i\n\n", (int)string.length);
    
    if(string.length == 0)
    {
        [self focusOnAnnotationsRegion:self.referralsMapView.annotations];
    }
    
//    if (string.length == 0) {
//        
//        self.searching=false;
//        
//        [self.filteredMapObjectsArray removeAllObjects];
//        
//        [self loadDataToMap];
//        
//        self.txtFld.text = @"";
//        
//        return YES;
//    }
//    
//    if (self.txtFld.text.length==0)
//    {
//        self.searching = TRUE;
//        
//        [self.filteredMapObjectsArray removeAllObjects];
//        
//        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF.companyAddress contains[c] %@",string];
//        
//        self.filteredMapObjectsArray  =[NSMutableArray arrayWithArray:[self.referralsArray filteredArrayUsingPredicate:predicate]];
//    }
//    else
//    {
//        self.searching=TRUE;
//        
//        [self.filteredMapObjectsArray removeAllObjects];
//        
//        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF.companyAddress contains[c] %@",string];
//        
//        self.filteredMapObjectsArray  =[NSMutableArray arrayWithArray:[self.referralsArray filteredArrayUsingPredicate:predicate]];
//        
//    }
//    
//    [self loadDataToMap];
//    
    return YES;
}


//- (void)placeAutocomplete {
//    
//    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
//    filter.type = kGMSPlacesAutocompleteTypeFilterCity;
//    
//    [_placesClient autocompleteQuery:@"Sydney Oper"
//                              bounds:nil
//                              filter:filter
//                            callback:^(NSArray *results, NSError *error) {
//                                if (error != nil) {
//                                    NSLog(@"Autocomplete error %@", [error localizedDescription]);
//                                    return;
//                                }
//                                
//                                for (GMSAutocompletePrediction* result in results) {
//                                    NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
//                                }
//                            }];
//}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    if ([annotation isKindOfClass:[MapViewLocatorObject class]]) {
        
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"MapViewLocatorObject"];
        
        MapViewLocatorObject * myyLocation = (MapViewLocatorObject *) annotation;
        
        if([myyLocation.title isEqualToString:@"demoB123"])
        {
            if (annotationView == nil)
            {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotationViewReuseIdentifier"];
            }
            
            // here you can assign your friend's image
            annotationView.image = [UIImage imageNamed:@"friend_image.png"];
            annotationView.annotation = annotation;
            
            // add below line of code to enable selection on annotation view
            annotationView.canShowCallout = YES;
            
            return annotationView;
        }
        else
        {
            self.calloutView.contentView = self.customView;
            
            NSLog(@"\n User Location \n %@", [self deviceLocation]);
            
            
            if(annotationView == nil)
                
                annotationView = myyLocation.annotaionView;
            
            else
                
                annotationView.annotation = annotation;
            
            //NSLog(@"Indexing Number : %d\n", myLocation.button.tag);
            
            annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height/2);
            
            return annotationView;
            
        }
    }
    
    else
        return  nil;
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    [self.calloutView dismissCalloutAnimated:YES];
    
    [self calloutViewDidDisappear:nil];
    
    [self.view endEditing:YES];
}

-(void) calloutViewDidDisappear:(SMCalloutView *)calloutView
{
    for (id<MKAnnotation> a in [self.referralsMapView annotations])
    {
        [self.referralsMapView deselectAnnotation:a animated:NO];
    }
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if([view.annotation isKindOfClass:[MKUserLocation class]])
    {
        return;
    }
    
    if ([view.annotation isKindOfClass:[MapViewLocatorObject class]])
    {
        
        NSLog(@"%@",((MapViewLocatorObject*)view.annotation).indexNumber);
        
        self.indexNumber = ((MapViewLocatorObject*)view.annotation).indexNumber;
        
        self.calloutView.contentView = self.customView;
        
        self.calloutView.calloutOffset = view.calloutOffset;
        
        self.calloutView.contentView.frame = self.customView.frame;
        
        self.calloutView.frame = self.customView.frame;
        
        //  self.calloutView.frame = CGRectMake(self.calloutView.frame.origin.x, self.calloutView.frame.origin.y, 200, 40);
        
        [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.referralsMapView animated:YES];
        
        //        [self.calloutView presentCalloutFromRect:self.viewOfannotation.bounds
        //                                          inView:self.viewOfannotation
        //                               constrainedToView:self.map
        //                        permittedArrowDirections:SMCalloutArrowDirectionAny
        //                                        animated:YES];
        //
        self.referralsMapView.calloutView = self.calloutView;
        
        self.customView.callOutLabel.text = [view.annotation title];
    }
    
    //[mapView deselectAnnotation:view.annotation animated:YES];
    //    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 70)];
    //
    //    customView.backgroundColor = [UIColor clearColor];
    //
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 0, 200, 60)];
    //
    //    imageView.image = [UIImage imageNamed:@"popup-icon.png"];
    //
    //    imageView.userInteractionEnabled = YES;
    //
    //    self.customView.center = CGPointMake(view.bounds.size.width*0.5f,-15);
    //
    //    [self.customView addSubview:imageView];
    //
    //    UIButton *anoButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 0, 200, 50)];
    //
    //    [anoButton setBackgroundImage:[UIImage imageNamed:@"backBtn"] forState:UIControlStateNormal];
    //
    //    [anoButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //   // [anoButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    //anoButton.backgroundColor=[UIColor redColor];
    //
    //    [self.customView addSubview:anoButton];
    //
    //    [self.customView bringSubviewToFront:anoButton];
    //
    //    [view addSubview:self.customView];
    //
    //    [self.view bringSubviewToFront:anoButton];
}

- (void) backButtonClicked: (UIButton *) button
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) btnButtonClicked:(NSNotification *)note
{
    [self.view endEditing:YES];
    
    //    [self.txtFld resignFirstResponder];
    
    
    PetServicesInfoViewController *ref = [[UIStoryboard storyboardWithName:@"Main" bundle:nil ]instantiateViewControllerWithIdentifier:@"petservicesinfo"];
    //ReferralsDetailController * populateReferalsDetailController = [[ReferralsDetailController alloc] init];
    
    //NSLog(@"Calling button's Tag : %d\n",btn1.tag);
    
    if(!self.searching)
    {
        [ref getReferralsData:[self.referralsArray objectAtIndex:[self.indexNumber intValue]]];
    }
    else
    {
        [ref getReferralsData:[self.filteredMapObjectsArray objectAtIndex:[self.indexNumber intValue]]];
    }
    //[self presentViewController:ref animated:YES completion:nil];
    [self.navigationController pushViewController:ref animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)deviceLocation
{
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
    
    //NSString *tmp = [[NSString alloc] initWithFormat:@"%f",self.locationManager.location.coordinate.latitude];
    
    //NSLog(tmp);
    
    
    return theLocation;
}

//- (void)mapTapped {
//    [self.customView removeFromSuperview];
//}

//-(CLLocationCoordinate2D *)getUserLocation
//{
//    CLLocationCoordinate2D * newLocation; //= [[CLLocationcoordinate2D alloc] init];
//
//    newLocation->longitud
//
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"fromCallOutView" object:nil];
}

-(void)focusOnUserOwnLocation
{
    if (self.searching == NO)
    {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.userCurrentLocation, 1000*20, 1000*20);
        
        [self.referralsMapView setRegion:[self.referralsMapView regionThatFits:region] animated:YES];
    }
}

-(void) focusOnAnnotationsRegion:(NSArray *)annotations
{
    if(annotations.count > 0)
    {
        double minLat=90.0f, maxLat=-90.0f;
        double minLon=180.0f, maxLon=-180.0f;
        
        for (id<MKAnnotation> mka in annotations) {
            if ( mka.coordinate.latitude  < minLat ) minLat = mka.coordinate.latitude;
            if ( mka.coordinate.latitude  > maxLat ) maxLat = mka.coordinate.latitude;
            if ( mka.coordinate.longitude < minLon ) minLon = mka.coordinate.longitude;
            if ( mka.coordinate.longitude > maxLon ) maxLon = mka.coordinate.longitude;
        }
        
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
        MKCoordinateSpan span = MKCoordinateSpanMake(maxLat-minLat, maxLon-minLon);
        MKCoordinateRegion region = MKCoordinateRegionMake (center, span);
        
        BOOL isValidRegion = CLLocationCoordinate2DIsValid(region.center);
        
        if (isValidRegion) {
            
            [self.referralsMapView setRegion:region animated:NO];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    self.userCurrentLocation = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    
    //    // Add an annotation
    //    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    //
    //    point.coordinate = userLocation.coordinate;
    //
    //    [self.referralsMapView addAnnotation:point];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //Locations is guaranteed to have at least one object.
    CLLocation* loc = [locations lastObject];
    
    float latitude = loc.coordinate.latitude;
    
    float longitude = loc.coordinate.longitude;
    
    self.userCurrentLocation = CLLocationCoordinate2DMake(latitude, longitude);
    
//    NSLog(@"%.8f",latitude);
//    
//    NSLog(@"%.8f",longitude);
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"\n\nString Length is :: %i\n\n", (int)self.autoCompletTextField.text.length);
 
    if(self.autoCompletTextField.text.length == 0)
    {
        [self focusOnAnnotationsRegion:self.referralsMapView.annotations];
    }
    
//    [self.view endEditing:YES];

    [self.autoCompletTextField endEditing:YES];
    
//    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Place search Textfield Delegates
-(void)placeSearchResponseForSelectedPlace:(GMSPlace*)responseDict{
    
    [self.view endEditing:YES];
    
    [self focusONAutoSelectedAdressPoint:responseDict.coordinate];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
}

-(void) focusONAutoSelectedAdressPoint:(CLLocationCoordinate2D) choosenPoint {

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(choosenPoint, 10*5, 10*5);
    
    [self.referralsMapView setRegion:[self.referralsMapView regionThatFits:region] animated:YES];

}

-(void)placeSearchWillShowResult{
    
}
-(void)placeSearchWillHideResult{
    
}
-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

@end



