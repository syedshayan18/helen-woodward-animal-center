
//  Created by Shayan Ali on 8/15/16.
//  Copyright © 2016 shayanali. All rights reserved.


#import "CheckInternet.h"
#import "Reachability.h"

@implementation CheckInternet

+ (BOOL) isInternetConnectionAvailable
{
    Reachability *internet = [Reachability reachabilityWithHostName: @"http://appinhand.net/LiveApplications/quotes/getquotes.php"];
    NetworkStatus netStatus = [internet currentReachabilityStatus];
    bool netConnection = false;
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"Access Not Available");
            netConnection = false;
            break;
        }
        case ReachableViaWWAN:
        {
            netConnection = true;
            break;
        }
        case ReachableViaWiFi:
        {
            netConnection = true;
            break;
        }
    }
    return netConnection;
}

@end
