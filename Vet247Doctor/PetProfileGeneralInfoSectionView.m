//
//  GeneralInfoView.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/11/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfileGeneralInfoSectionView.h"

#import "ControlsFactory.h"

#import "Define.h"

#import "Masonry.h"

#import "PetProfileKeys.h"

#import "PetProfileKeys.h"

#import "PetProfileDataManager.h"

@implementation PetProfileGeneralInfoSectionView

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
    }
    return self;
    
}

- (void) addComponents {
    
    self.speciesFieldValues = [[NSArray alloc] init];
    
    _speciesFieldValues = @[@"Dog", @"Cat",
                            @"Bird", @"Snake", @"Lion"];
    
    self.selectedImage = [[UIImage alloc] init];
    
    self.selectedImage = [UIImage imageNamed:@"selectedButton.png"];
    
    self.unSelectedImage = [[UIImage alloc] init];
    
    self.unSelectedImage = [UIImage imageNamed:@"unSelectedButton.png"];
    
    self.speciesSelectedvalue  = [[NSString alloc] init];
    
    self.backgroundColor=[UIColor clearColor];
    
    [self setUpAnimalNameTextField];
    
    [self setUpSpeciesPickerViewBGImage];
    
    [self SetUpUnhideSpeciesFieldViewButton];
    
    [self setUpBreadTextView];
    
    [self setUpSexSelectionField];
    
    [self setUpNeuteredOrSpFieldView];
    
    [self setUpAgeAndColorFieldView];
    
    [self setUpWeightTextField];
    
    //  [self setUpPickerViewFieldPicker];
    
    self.speciesFieldPickerView.hidden = YES;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
    {
        
        [self populateDataToView];
        
    }
    
}

- (void) setUpAnimalNameTextField {
    
    self.animalNameTextField = [ControlsFactory getTextField];
    
    self.animalNameTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.animalNameTextField setReturnKeyType:UIReturnKeyNext];
    
    self.animalNameTextField.placeholder = @"Animal Name";
    
    self.animalNameTextField.textAlignment = NSTextAlignmentCenter;
    
    self.animalNameTextField.delegate = self;
    
    [self.animalNameTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self addSubview:self.animalNameTextField];
    
    UIEdgeInsets padding = UIEdgeInsetsMake (10, 7.5, 0 , 7.5);
    
    [self.animalNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
    }];
    
}

-(void) setUpSpeciesPickerViewBGImage{
    
    //Species Text Field BackGroung Image
    
    self.speciesFieldBGImage = [ControlsFactory getImageView];
    
    self.speciesFieldBGImage.image = [UIImage imageNamed:@"speciesBG.png"];
    
    self.speciesFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.speciesFieldBGImage];
    
    [self.speciesFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.animalNameTextField.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(81);
        
    }];
}

- (UIViewController *)getContainerController
{
    /// Finds the view's view controller.
    
    // Take the view controller class object here and avoid sending the same message iteratively unnecessarily.
    Class vcc = [UIViewController class];
    
    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass: vcc])
            return (UIViewController *)responder;
    
    // If the view controller isn't found, return nil.
    return nil;
}

-(void) setUpPickerViewFieldPicker{
    
    // Species selection field Picker view
    
    self.speciesFieldPickerView = [ControlsFactory getPickerView];
    
    self.speciesFieldPickerView.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.speciesFieldPickerView.delegate = self;
    
    self.speciesFieldPickerView.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(21, 0, 5 , 0);
    
    [self addSubview:self.speciesFieldPickerView];
    
    [self.speciesFieldPickerView mas_makeConstraints:^(MASConstraintMaker * make){
        
        //make.top.equalTo(self.weightTextField.mas_bottom).with.offset(padding.top);
        
        UIViewController *parentController = [self getContainerController];
        
        if(parentController.view.frame.size.height == 480)
        {
            make.bottom.equalTo(self.mas_bottom).with.offset(-(padding.bottom + 95));
        }
        
        else
        {
            make.bottom.equalTo(self.mas_bottom).with.offset(-padding.bottom);
        }
        make.left.equalTo(self.speciesFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.speciesFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(110);
        
    }];
}

-(void) SetUpUnhideSpeciesFieldViewButton{
    
    self.unhideSpeciesFieldPickerViewButton = [ControlsFactory getButton];
    
    [self.unhideSpeciesFieldPickerViewButton setTitle:@"   --Select--" forState:UIControlStateNormal];
    
    [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forState:UIControlStateNormal];
    
    self.unhideSpeciesFieldPickerViewButton.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.unhideSpeciesFieldPickerViewButton addTarget:self action:@selector(unhideSpeciesFieldPickerViewButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.speciesFieldPickerView.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(21, 7.5, 0 , 7.5);
    
    [self addSubview:self.unhideSpeciesFieldPickerViewButton];
    
    [self.unhideSpeciesFieldPickerViewButton mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.speciesFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.speciesFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.speciesFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(46);
        
    }];
    
    self.speciesFieldPickerView.hidden = YES;
    
}

- (void) setUpBreadTextView {
    
    self.breadTextField = [ControlsFactory getTextField];
    
    self.breadTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.breadTextField.placeholder = @"Breed";
    
    self.breadTextField.textAlignment = NSTextAlignmentCenter;
    
    self.breadTextField.delegate = self;
    
    [self.breadTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self addSubview:self.breadTextField];
    
    UIEdgeInsets padding = UIEdgeInsetsMake (10, 7.5, 000 , 7.5);
    
    [self.breadTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.top.equalTo(self.speciesFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
    }];
    
}


-(void) setUpSexSelectionField{
    
    // Animal's sex selection Text Field BackGroung Image
    
    self.sexFieldBGImage = [ControlsFactory getImageView];
    
    self.sexFieldBGImage.image = [UIImage imageNamed:@"sexFieldBG.png"];
    
    self.sexFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.sexFieldBGImage];
    
    [self.sexFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.breadTextField.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.height.mas_equalTo(63);
        
    }];
    
    // setting up sex seletion field lables and button
    
    
    // setting mail selection button
    
    self.maleButton = [ControlsFactory getButton];
    
    self.maleButton.backgroundColor = [UIColor clearColor];
    
    [self.maleButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.maleButton addTarget:self action:@selector(maleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25, 40, 5, 7.5);
    
    [self addSubview:self.maleButton];
    
    [self.maleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.sexFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.sexFieldBGImage.mas_centerX).with.offset(-70);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting mail selection buttonBG
    
    self.maleButtonBG = [ControlsFactory getButton];
    
    self.maleButtonBG.backgroundColor = [UIColor clearColor];
    
    [self.maleButtonBG addTarget:self action:@selector(maleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(20, 40, 5, 7.5);
    
    [self addSubview:self.maleButtonBG];
    
    [self.maleButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        //make.left.equalTo(self.sexFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.sexFieldBGImage.mas_centerX).with.offset(-50);
        
        make.width.mas_equalTo(60);
        
        make.height.mas_equalTo(35);
        
    }];
    
    // setting male button lable
    
    self.maleLable = [ControlsFactory getLabel];
    
    self.maleLable.backgroundColor = [UIColor clearColor];
    
    self.maleLable.text = @"Male";
    
    self.maleLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 30, 5, 7.5);
    
    [self addSubview:self.maleLable];
    
    [self.maleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.sexFieldBGImage.mas_centerX).with.offset(-35);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(46);
        
    }];
    
    // setting female selection button
    
    self.femaleButton = [ControlsFactory getButton];
    
    self.femaleButton.backgroundColor = [UIColor clearColor];
    
    [self.femaleButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.femaleButton addTarget:self action:@selector(femaleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25,15,0,0);
    
    [self addSubview:self.femaleButton];
    
    [self.femaleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.maleLable.mas_right).with.offset(25);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting female selection buttonBG
    
    self.femaleButtonBG = [ControlsFactory getButton];
    
    self.femaleButtonBG.backgroundColor = [UIColor clearColor];
    
    [self.femaleButtonBG addTarget:self action:@selector(femaleButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(20,15,0,0);
    
    [self addSubview:self.femaleButtonBG];
    
    [self.femaleButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.maleLable.mas_right).with.offset(50);
        
        make.width.mas_equalTo(75);
        
        make.height.mas_equalTo(35);
        
    }];
    
    
    // setting female button's Lable
    
    self.femaleLable = [ControlsFactory getLabel];
    
    self.femaleLable.backgroundColor = [UIColor clearColor];
    
    self.femaleLable.text = @"Female";
    
    self.femaleLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 40, 5, 7.5);
    
    [self addSubview:self.femaleLable];
    
    [self.femaleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.sexFieldBGImage.mas_centerX).with.offset(62);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
}

-(void) setUpNeuteredOrSpFieldView{
    
    // Animal is Neutered or Spayed verifying BackGround Image
    
    self.neuteredOrSpayedBGImage = [ControlsFactory getImageView];
    
    self.neuteredOrSpayedBGImage.image = [UIImage imageNamed:@"neuteredOrSpayedBG.png"];
    
    self.neuteredOrSpayedBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.neuteredOrSpayedBGImage];
    
    [self.neuteredOrSpayedBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sexFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.height.mas_equalTo(63);
        
    }];
    
    // setting up Neutered or Spayed YES/NO seletion field lables and button
    
    
    // setting YES buttonBG
    self.neuteredOrSpYesButton = [ControlsFactory getButton];
    
    self.neuteredOrSpYesButton.backgroundColor = [UIColor clearColor];
    
    [self.neuteredOrSpYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.neuteredOrSpYesButton addTarget:self action:@selector(neuteredOrSpYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25, 40, 5, 7.5);
    
    [self addSubview:self.neuteredOrSpYesButton];
    
    [self.neuteredOrSpYesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.neuteredOrSpayedBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.neuteredOrSpayedBGImage.mas_centerX).with.offset(-60);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting YES button
    self.neuteredOrSpYesButtonBG = [ControlsFactory getButton];
    
    self.neuteredOrSpYesButtonBG.backgroundColor = [UIColor clearColor];
    
    //[self.neuteredOrSpYesButtonBG setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.neuteredOrSpYesButtonBG addTarget:self action:@selector(neuteredOrSpYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(45, 40, 5, 7.5);
    
    [self addSubview:self.neuteredOrSpYesButtonBG];
    
    [self.neuteredOrSpYesButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.neuteredOrSpayedBGImage.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(76);
        
        make.height.mas_equalTo(49);
        
    }];
    
    // setting YES Lable
    
    self.neuteredOrSpYesLable = [ControlsFactory getLabel];
    
    self.neuteredOrSpYesLable.backgroundColor = [UIColor clearColor];
    
    self.neuteredOrSpYesLable.text = @"Yes";
    
    self.neuteredOrSpYesLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 30, 5, 7.5);
    
    [self addSubview:self.neuteredOrSpYesLable];
    
    [self.neuteredOrSpYesLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.neuteredOrSpayedBGImage.mas_centerX).with.offset(-28);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(35);
        
    }];
    
    // setting NO Button
    
    self.neuteredOrSpNoButton = [ControlsFactory getButton];
    
    self.neuteredOrSpNoButton.backgroundColor = [UIColor clearColor];
    
    [self.neuteredOrSpNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.neuteredOrSpNoButton addTarget:self action:@selector(neuteredOrSpNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25,15,0,0);
    
    [self addSubview:self.neuteredOrSpNoButton];
    
    [self.neuteredOrSpNoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.neuteredOrSpYesLable.mas_right).with.offset(30);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting NO ButtonBG
    self.neuteredOrSpNoButtonBG = [ControlsFactory getButton];
    
    self.neuteredOrSpNoButtonBG.backgroundColor = [UIColor clearColor];
    
    //[self.neuteredOrSpNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.neuteredOrSpNoButtonBG addTarget:self action:@selector(neuteredOrSpNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(10,15,0,0);
    
    [self addSubview:self.neuteredOrSpNoButtonBG];
    
    [self.neuteredOrSpNoButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.neuteredOrSpNoButton.mas_left).with.offset(3);
        
        make.width.mas_equalTo(76);
        
        make.height.mas_equalTo(39);
        
    }];
    
    // setting NO button's Lable
    
    self.neuteredOrSpNoLable = [ControlsFactory getLabel];
    
    self.neuteredOrSpNoLable.backgroundColor = [UIColor clearColor];
    
    self.neuteredOrSpNoLable.text = @"No";
    
    self.neuteredOrSpNoLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 40, 5, 7.5);
    
    [self addSubview:self.neuteredOrSpNoLable];
    
    [self.neuteredOrSpNoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.neuteredOrSpayedBGImage.mas_centerX).with.offset(72);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
}

-(void) setUpAgeAndColorFieldView{
    
    // setting Age Text Field
    self.ageTextField = [ControlsFactory getTextField];
    
    self.ageTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.ageTextField.placeholder = @"Age";
    
    self.ageTextField.textAlignment = NSTextAlignmentCenter;
    
    self.ageTextField.delegate = self;
    
    [self.ageTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self addSubview:self.ageTextField];
    
    [self.ageTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    UIEdgeInsets padding = UIEdgeInsetsMake (10, 7.5, 000 , 7.5);
    
    [self.ageTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(140);
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(45);
        
    }];
    
    // setting Color Text Field
    
    self.colorTextField = [ControlsFactory getTextField];
    
    self.colorTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.colorTextField.placeholder = @"Color";
    
    self.colorTextField.textAlignment = NSTextAlignmentCenter;
    
    self.colorTextField.delegate = self;
    
    [self.colorTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self addSubview:self.colorTextField];
    
    padding = UIEdgeInsetsMake (10, 7.5, 000 , 8);
    
    [self.colorTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.width.mas_equalTo(140);
        
        make.top.equalTo(self.neuteredOrSpayedBGImage.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(46);
        
    }];
    
}


-(void) setUpWeightTextField{
    
    self.weightTextField = [ControlsFactory getTextField];
    
    self.weightTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.weightTextField.placeholder = @"Weight Lbs";
    
    self.weightTextField.textAlignment = NSTextAlignmentCenter;
    
    self.weightTextField.delegate = self;
    
    [self.weightTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.weightTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    [self addSubview:self.weightTextField];
    
    UIEdgeInsets padding = UIEdgeInsetsMake (10, 7.5, 000 , 7.5);
    
    [self.weightTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.top.equalTo(self.ageTextField.mas_bottom).with.offset(padding.top);
        
        make.height.mas_equalTo(46);
        
        make.width.mas_equalTo(self.frame.size.width - 20);
        
        make.bottom.equalTo(self.mas_bottom).with.offset(-5);
        
    }];
    
}

// Check Text Fields

- (BOOL) checkGeneralInfoTextFields
{
    
    //NSString *phoneRegEx = @"[0-9]{1,11}";
    
    //NSPredicate *ageTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    
    UIAlertController * checkGeneralInfoFieldsErrorAlert =   [UIAlertController
                                                              alertControllerWithTitle:@"Error"
                                                              message:@""
                                                              preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [checkGeneralInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [checkGeneralInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [checkGeneralInfoFieldsErrorAlert addAction:ok];
    [checkGeneralInfoFieldsErrorAlert addAction:cancel];
    
    if ( self.animalNameTextField.text && self.animalNameTextField.text.length<1) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Name"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.speciesSelectedvalue && self.speciesSelectedvalue.length<1) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please select Pet's specie"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.breadTextField.text && self.breadTextField.text.length<1) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Breed"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if ([self.femaleButton.currentImage isEqual:self.unSelectedImage] && [self.maleButton.currentImage isEqual:self.unSelectedImage]) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please select sex"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if ([self.neuteredOrSpNoButton.currentImage isEqual:self.unSelectedImage] && [self.neuteredOrSpYesButton.currentImage isEqual:self.unSelectedImage]) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please answer neutered or spayed"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    else if (self.ageTextField.text && self.ageTextField.text.length < 1)
    {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Age"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    //    else if ([ageTest evaluateWithObject:self.ageTextField.text] == NO) {
    //
    //        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter valid age"];
    //
    //        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    else if (self.colorTextField.text && self.colorTextField.text.length<1)
    {
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Color"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.weightTextField.text && self.weightTextField.text.length < 1)
    {
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's weight"];
        
        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    //    else if ([ageTest evaluateWithObject:self.weightTextField.text] == NO) {
    //
    //        [checkGeneralInfoFieldsErrorAlert setMessage:@"Value of Pet's Weight should be in Numerics"];
    //
    //        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return NO;
    //    }
    
    // adding data to NSMutable dictionary for ultimately sending over server
    
    [PetProfileDataManager removePetProfileObjectForKey:@"selectSpecies"];
    
    PetProfileDataManager * generalInfoDictionary = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    [generalInfoDictionary insertPetProfileInfo:self.animalNameTextField.text :AnimalNameKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.speciesSelectedvalue :AnimalSpeciesKey];
    
    [generalInfoDictionary insertPetProfileInfo: self.breadTextField.text :AnimalBreadKey];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"]) {
        
        [generalInfoDictionary insertPetProfileInfo:[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePETId"] :PetIdKey];
        
    }
    
    else
    {
        [generalInfoDictionary insertPetProfileInfo:@"-1" :PetIdKey];
    }
    
    if([self.maleButton.currentImage isEqual:self.selectedImage])
    {
        [generalInfoDictionary insertPetProfileInfo:@"M" :AnimalSexKey];
    }
    else
    {
        [generalInfoDictionary insertPetProfileInfo:@"F" :AnimalSexKey];
    }
    
    if([self.neuteredOrSpYesButton.currentImage isEqual:self.selectedImage])
    {
        [generalInfoDictionary insertPetProfileInfo:@"Yes" :AnimalNeuteredOrSpayedKey];
    }
    else
    {
        [generalInfoDictionary insertPetProfileInfo:@"No" :AnimalNeuteredOrSpayedKey];
    }
    
    [generalInfoDictionary insertPetProfileInfo:self.ageTextField.text :AnimalAgeKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.colorTextField.text :AnimalColorKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.weightTextField.text :AnimalWeightKey];
    
    return  YES;
    
    
}//end Check Fields value


-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    self.animalNameTextField.text = [NSPetData objectForKey:AnimalNameKey];
    
    //NSLog(@"\n%d\n",(int)[NSPetData objectForKey:@"selectSpecies"]);
    
    int speciesFieldIndex;
    
    if ([[NSPetData objectForKey:@"selectSpecies"] isEqualToString:@"0"] || [[NSPetData objectForKey:AnimalSpeciesKey] isEqualToString:@"0"])
    {
        speciesFieldIndex = 0;
    }
    
    else if ([[NSPetData objectForKey:@"selectSpecies"] isEqualToString:@"1"] || [[NSPetData objectForKey:AnimalSpeciesKey] isEqualToString:@"1"])
    {
        speciesFieldIndex = 1;
    }
    
    else if ([[NSPetData objectForKey:@"selectSpecies"] isEqualToString:@"2"] || [[NSPetData objectForKey:AnimalSpeciesKey] isEqualToString:@"2"])
    {
        speciesFieldIndex = 2;
    }
    
    else if ([[NSPetData objectForKey:@"selectSpecies"] isEqualToString:@"3"] || [[NSPetData objectForKey:AnimalSpeciesKey] isEqualToString:@"3"])
    {
        speciesFieldIndex = 3;
    }
    
    else if([[NSPetData objectForKey:@"selectSpecies"] isEqualToString:@"4"] || [[NSPetData objectForKey:AnimalSpeciesKey] isEqualToString:@"4"])
    {
        speciesFieldIndex = 4;
    }
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
    {
        [NSPetData removeObjectForKey:@"selectSpecies"];
        
        [NSPetData removeObjectForKey:@"id"];
    }
    switch (speciesFieldIndex)
    {
            
        case 0:
            
            [self.unhideSpeciesFieldPickerViewButton setTitle:@"Dog" forState:UIControlStateNormal];
            
            [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            self.speciesSelectedvalue = @"0";
            
            break;
            
        case 1:
            
            [self.unhideSpeciesFieldPickerViewButton setTitle:@"Cat" forState:UIControlStateNormal];
            
            [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            self.speciesSelectedvalue = @"1";
            
            break;
            
        case 2:
            
            [self.unhideSpeciesFieldPickerViewButton setTitle:@"Bird" forState:UIControlStateNormal];
            
            [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            self.speciesSelectedvalue = @"2";
            
            
            break;
            
        case 3:
            
            [self.unhideSpeciesFieldPickerViewButton setTitle:@"Snake" forState:UIControlStateNormal];
            
            [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            self.speciesSelectedvalue = @"3";
            
            break;
            
        case 4:
            
            [self.unhideSpeciesFieldPickerViewButton setTitle:@"Lion" forState:UIControlStateNormal];
            
            [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            self.speciesSelectedvalue = @"4";
            
            break;
        default:
            break;
    }
    
    self.breadTextField.text = [NSPetData objectForKey:AnimalBreadKey];
    
    if ([[NSPetData objectForKey:AnimalSexKey] isEqualToString:@"M"]) {
        
        [self.maleButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([[NSPetData objectForKey:AnimalSexKey] isEqualToString:@"F"])
    {
        [self.femaleButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    if ([[NSPetData objectForKey:AnimalNeuteredOrSpayedKey] isEqualToString:@"Yes"]) {
        
        [self.neuteredOrSpYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([[NSPetData objectForKey:AnimalNeuteredOrSpayedKey] isEqualToString:@"No"])
    {
        [self.neuteredOrSpNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    self.ageTextField.text = [NSPetData objectForKey:AnimalAgeKey];
    
    self.colorTextField.text = [NSPetData objectForKey:AnimalColorKey];
    
    self.weightTextField.text = [NSPetData objectForKey:AnimalWeightKey];
    
}



-(void) maleButtonClicked:(UIButton *) maleButton
{
    [self endEditing:YES];
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    if ([self.maleButton.currentImage isEqual:self.unSelectedImage] && [self.femaleButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.maleButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.maleButton.currentImage isEqual:self.unSelectedImage] && [self.femaleButton.currentImage isEqual:self.selectedImage])
    {
        [self.maleButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.femaleButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) femaleButtonClicked:(UIButton *) femaleButton
{
    [self endEditing:YES];
    
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    if ([self.femaleButton.currentImage isEqual:self.unSelectedImage] && [self.maleButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.femaleButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.femaleButton.currentImage isEqual:self.unSelectedImage] && [self.maleButton.currentImage isEqual:self.selectedImage])
    {
        [self.femaleButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.maleButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) neuteredOrSpYesButtonClicked:(UIButton *) neuterOrSpYesButton
{
    [self endEditing:YES];
    
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    if ([self.neuteredOrSpYesButton.currentImage isEqual:self.unSelectedImage] && [self.neuteredOrSpNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.neuteredOrSpYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.neuteredOrSpYesButton.currentImage isEqual:self.unSelectedImage] && [self.neuteredOrSpNoButton.currentImage isEqual:self.selectedImage])
    {
        [self.neuteredOrSpYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.neuteredOrSpNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) neuteredOrSpNoButtonClicked:(UIButton *) neuterOrSpNoButton
{
    [self endEditing:YES];
    
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    if ([self.neuteredOrSpNoButton.currentImage isEqual:self.unSelectedImage] && [self.neuteredOrSpYesButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.neuteredOrSpNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.neuteredOrSpNoButton.currentImage isEqual:self.unSelectedImage] && [self.neuteredOrSpYesButton.currentImage isEqual:self.selectedImage])
    {
        [self.neuteredOrSpNoButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.neuteredOrSpYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
}

-(void) unhideSpeciesFieldPickerViewButtonClicked:(UIButton *) unhideSpecies
{
    [self.speciesFieldPickerView removeFromSuperview];
    
    [self setUpPickerViewFieldPicker];
    
    [self.unhideSpeciesFieldPickerViewButton setTitle:self.speciesFieldValues[0] forState:UIControlStateNormal];
    
    [self endEditing:YES];
    
    //[self.];
    
    [self.unhideSpeciesFieldPickerViewButton setTitle:self.speciesFieldValues[0] forState:UIControlStateNormal];
    
    self.speciesSelectedvalue = self.speciesFieldValues[0];
    
    [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self addSubview:self.speciesFieldPickerView];
    
    self.speciesFieldPickerView.hidden = NO;
    
}

//#pragma mark -
//#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return self.speciesFieldValues.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.speciesFieldValues[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    [self.unhideSpeciesFieldPickerViewButton setTitle:_speciesFieldValues[row] forState:UIControlStateNormal];
    
    [self.unhideSpeciesFieldPickerViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.speciesSelectedvalue = [NSString stringWithFormat:@"%d", (int)row];
    
    // NSLog(@"\n\n%@\n\n", self.speciesSelectedvalue);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self endEditing:YES];
    
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
////    if(self.superview.frame.size.width == 310)
////    {
//        if(!([textField isEqual:self.animalNameTextField] || [textField isEqual:self.breadTextField]))
//        {
//            if(self.superview.frame.size.width == 310)
//            {
//                self.center = CGPointMake(self.center.x, 60);
//            }
//            else
//            {
//                self.center = CGPointMake(self.center.x, 100);
//            }
//        }
////    }
    self.speciesFieldPickerView.hidden = YES;
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    [self.speciesFieldPickerView removeFromSuperview];
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
//    
//    if([self.ageTextField isFirstResponder] || [self.colorTextField isFirstResponder])
//    {
//        return;
//    }
//    
//    if(!([textField isEqual:self.animalNameTextField] || [textField isEqual:self.breadTextField]))
//        {
//            UIViewController * setCenterYController = [self getContainerController];
//            if(self.superview.frame.size.width == 310)
//            {
//                self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -70);
//            }
//            else
//            {
//                self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -95);
//            }
//        }
    
}

//- (UIViewController *)getContainerController
//{
//    /// Finds the view's view controller.
//
//    // Take the view controller class object here and avoid sending the same message iteratively unnecessarily.
//    Class vcc = [UIViewController class];
//
//    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
//    UIResponder *responder = self;
//    while ((responder = [responder nextResponder]))
//        if ([responder isKindOfClass: vcc])
//            return (UIViewController *)responder;
//
//    // If the view controller isn't found, return nil.
//    return nil;
//}
//- (void)textFieldDidEndEditing:(UITextField *)textField{
//
//    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//
//    if (isiPhone4 ||isiPhone5) {
//
//        self.view.center=CGPointMake(self.centerPoint.x, 100);
//
//    }
//
//}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
