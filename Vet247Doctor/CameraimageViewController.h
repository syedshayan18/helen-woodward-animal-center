//
//  CameraimageViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/11/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraimageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img_camera;
@property (strong,nonatomic) NSString *imagestring;

@end
