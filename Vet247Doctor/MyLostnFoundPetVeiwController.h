//
//  MyLostnFoundPetVeiwController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLostnFoundPetVeiwController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (strong,nonatomic) NSMutableArray *petData;
@property NSUInteger index;
@property NSUInteger buttontag;
@end
