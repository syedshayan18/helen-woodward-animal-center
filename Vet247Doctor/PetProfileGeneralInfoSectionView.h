//
//  GeneralInfoView.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/11/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetProfileGeneralInfoSectionView : UIView <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (strong,nonatomic) UITextField *animalNameTextField;

@property (strong, nonatomic) UIImageView *speciesFieldBGImage;

@property (strong, nonatomic) UIPickerView *speciesFieldPickerView;

@property (strong, nonatomic) UITextField *breadTextField;

@property (strong, nonatomic) UIImageView *sexFieldBGImage;

@property (strong, nonatomic) UIButton *maleButton, *maleButtonBG, *femaleButton, *femaleButtonBG;

@property (strong, nonatomic) UILabel *maleLable, *femaleLable;

@property (strong, nonatomic) UIImage *selectedImage;

@property (strong, nonatomic) UIImage *unSelectedImage;

@property (strong, nonatomic) UIImageView *neuteredOrSpayedBGImage;

@property (strong, nonatomic) UIButton *neuteredOrSpYesButton, *neuteredOrSpYesButtonBG, *neuteredOrSpNoButton, *neuteredOrSpNoButtonBG;

@property (strong, nonatomic) UILabel *neuteredOrSpYesLable, *neuteredOrSpNoLable;

@property (strong, nonatomic) UITextField *ageTextField;

@property (strong, nonatomic) UITextField *colorTextField;

@property (strong, nonatomic) UITextField *weightTextField;

@property (strong, nonatomic) UIButton *unhideSpeciesFieldPickerViewButton;

@property (strong, nonatomic) NSArray *speciesFieldValues;

@property (strong, nonatomic) NSString *speciesSelectedvalue;

-(void) addComponents;

-(BOOL) checkGeneralInfoTextFields;

@end
