//
//  profileViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/13/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "profileViewController.h"

#import "Masonry.h"

#import "NetworkingClass.h"

#import "AppDelegate.h"

#import "DashboardViewController.h"

#import "Define.h"

#import "PaymentController.h"

#import "MBProgressHUD.h"

#import "PackagesVC.h"
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "Vet247Doctor-Swift.h"
#import "UIButton+WebCache.h"
@interface profileViewController (){
    NSDateFormatter *dateformat;
    
}

@end

@implementation profileViewController

- (void)viewDidLoad {
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stacktapmale:)];

    [self.stack_male addGestureRecognizer:tapGesture];
    _btn_profile.layer.cornerRadius = 60; // this value vary as per your desire
    _btn_profile.clipsToBounds = YES;
    
    UITapGestureRecognizer *tapGesturefemale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stacktapfemale:)];
    
    [self.stack_female addGestureRecognizer:tapGesturefemale];
    
    _btn_donee.layer.cornerRadius = 15; // this value vary as per your desire
       _btn_donee.clipsToBounds = YES;
    self.dataDict =[[NSMutableDictionary alloc] init];
    

    
    
 // [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.provincePickerView= [[UIPickerView alloc]init];
    
    self.countryPickerView =[[UIPickerView alloc] init];
    province = [[UIPickerView alloc] init];
  
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture1:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    // Setting Provinces and States Hardcoded
    self.provinceNames = [[NSMutableArray alloc] initWithArray:@[@"Alberta", @"British Columbia", @"Manitoba",@"New Brunswick",@"Newfoundland",@"Nova Scotia",@"Ontario",@"Prince Edward Island",@"Quebec",@"Saskatchewan",@"Territory: Northwest Territories",@"Territory: Nunavut",@"Territory: Yukon"]];
    
    self.statesNames = [[NSMutableArray alloc] initWithArray:@[@"Alabama", @"Alaska", @"Arizona",@"Arkansas",@"California",@"Colorado",@"Connecticut",@"Delaware",@"District of Columbia",@"Florida",@"Georgia",@"Guam",@"Hawaii",@"Idaho",@"Illinois",@"Indiana",@"Iowa",@"Kansas",@"Kentucky",@"Louisiana",@"Maine",@"Maryland",@"Massachusetts",@"Michigan",@"Minnesota",@"Mississippi",@"Missouri",@"Montana",@"Nebraska",@"Nevada",@"Neew Hampshire",@"New Jersey",@"New Mexico",@"New York",@"North Carolina",@"North Dakota",@"Ohio",@"Oklahoma",@"Oregon",@"Pennsylvania",@"Rhode Island",@"South Carolina",@"South Dakota",@"Tennessee",@"Texas",@"Utah",@"Vermont",@"Virginia",@"Washington",@"West Virginia",@"Wisconsin",@"Wyoming",@"Yukon Territory"]];
    
    
//[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.txt_dob.delegate=self;
    [self.txt_phonenum setKeyboardType:UIKeyboardTypeNumberPad];

    dateformat = [[NSDateFormatter alloc] init];
    
    dateformat.dateFormat = @"dd-MM-yyyy";
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];
    [self.txt_dob setInputView:datePicker];
    
 
    dayPicker = [[UIPickerView alloc] init];
    dayPicker.delegate = self;
    dayPicker.dataSource = self;
   // dayPicker.showsSelectionIndicator = YES;
    [self.txt_Country setInputView:dayPicker];
    self.txt_Province.delegate = self;
    
    province.delegate = self;
    province.dataSource = self;
    
    [self getProfileInfo];
    
   _profilebg = self.btn_profile.currentBackgroundImage;
    
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.activityIndicator.backgroundColor=[UIColor lightGrayColor];
    
    self.activityIndicator.center = CGPointMake(_profilebg.size.width / 2.0, _profilebg.size.height / 2.0);
    //
    //[self.view addSubview: self.activityIndicator];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Do any additional setup after loading the view.
    
    

}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"%@",response);
    
    [connection cancel];
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    
    int code = (int)[httpResponse statusCode];
    
    if (code == 200) {
        
        self.imageExist=YES;
        
        //self.profilePicImageView.image=image;
        
    
        
        [self downloadProfileImage];
        
    }
    else if(code == 404)
    {
        self.imageExist=NO;
        
    }
    else {
        
        self.imageExist=NO;
        
    }
    
   // [self setUpOthersTextField];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.imageExist=NO;
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    _txt_Province.placeholder = @"PROVINCE";

    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"text Country = %@", _txt_Country.text);
    if ([_txt_Country.text isEqualToString:@"USA" ]||[_txt_Country.text isEqualToString:@"Canada"])
    {
        province.showsSelectionIndicator = YES;
        [self.txt_Province setInputView:province];
        
    }
    if ([_txt_Country.text isEqualToString:@"Others"]){
        _txt_Province.placeholder = @"Others";
        [self.txt_Province setInputView:nil];
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}
- (void) getProfileInfo {
    
    self.callFromObject=@"getProfile";
    
    self.getProfile=[[NetworkingClass alloc] init];
    
    self.getProfile.delegate=self;
    
    [self.getProfile getProfileInfo:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == province){
        if([self.txt_Country.text  isEqual: @"USA"]){
            return self.statesNames.count;
        }else if([self.txt_Country.text  isEqual: @"Canada"]){
            return self.provinceNames.count;
        }
    }
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView == province){
        if([self.txt_Country.text  isEqual: @"USA"]){
//            self.txt_Province.text = self.statesNames[row];
            return self.statesNames[row];
        }else if([self.txt_Country.text  isEqual: @"Canada"]){
//            self.txt_Province.text = self.provinceNames[row];
            return self.provinceNames[row];
        }
    }
    switch (row)
    {
        case 0:
            self.txt_Country.text = @"USA";
            return @"USA";
        case 1:
            self.txt_Country.text = @"Canada";
            return @"Canada";
        case 2:
            self.txt_Country.text = @"Others";
            return @"Others";
      
        default: return nil;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView == province){
        if([self.txt_Country.text  isEqual: @"USA"]){
            self.txt_Province.text = self.statesNames[row];
        }else if([self.txt_Country.text  isEqual: @"Canada"]){
            self.txt_Province.text = self.provinceNames[row];
        }
    }else{
        switch (row)
        {
            
                
        }
    }
}









- (void) tapGesture1:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
  
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updateTextField:(UIDatePicker *)sender
{
    self.txt_dob.text = [dateformat stringFromDate:sender.date];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}


- (IBAction)someTextFieldTouchDown:(UITextField *)sender
{
    if (self.txt_dob.inputView == nil)
    {
        UIDatePicker *datePicker = [[UIDatePicker alloc] init];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(updateTextField:)
             forControlEvents:UIControlEventValueChanged];
        [self.txt_dob setInputView:datePicker];
    }
}


- (void) updateview {
    
    NSLog(@"%@",_dataDict);
    self.txt_firstname.text=[self.dataDict objectForKey:@"firstName"];
    
    self.txt_lastname.text=[self.dataDict objectForKey:@"lastName"];
    
    self.txt_phonenum.text=[self.dataDict objectForKey:@"phoneNo"];
    
   // NSDateFormatter * df = [[NSDateFormatter alloc] init];
    
   // [df setDateFormat:@"dd-MM-yyyy"];
    
    if ([[self.dataDict objectForKey:@"birthday"] length]>0) {
        
       
        self.txt_dob.text = [self.dataDict objectForKey:@"birthday"];
      // self.dataDict[@"birthday"] = self.txt_dob.text;
//        [self.tx setTitle:[df stringFromDate:[df dateFromString:[self.dataDict objectForKey:@"birthday"]]] forState:UIControlStateNormal];
//        
//        [self.dobButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    if ([[self.dataDict objectForKey:@"streetAddress"] length ]>0) {
        
        self.txt_streetadd.text=[self.dataDict objectForKey:@"streetAddress"];
        
    }
    
    if ([[self.dataDict objectForKey:@"city"] length ]>0) {
        
        self.txt_city.text=[self.dataDict objectForKey:@"city"];
    }
    
    if ([[self.dataDict objectForKey:@"province"] length] >0) {
        
        
        self.txt_Province.text =[_dataDict objectForKey:@"province"];
        
        
       
    }
    
    if ([[self.dataDict objectForKey:@"country"] length]>0) {
        
        _txt_Country.text = [_dataDict objectForKey:@"country"];
    }
    
    if ([[self.dataDict objectForKey:@"postalCode"] length ]> 0) {
        
        self.txt_postcode.text=[self.dataDict objectForKey:@"postalCode"];
        
    }
    if ([[self.dataDict objectForKey:@"sex"] length]>0) {
        
        if ([[self.dataDict objectForKey:@"sex"] isEqualToString:@"Male"]) {
            
            [self.btn_male setSelected:YES];
            self.gender=@"Male";
            
            [self.btn_female setSelected:NO];
            
        }
       if ([[self.dataDict objectForKey:@"sex"] isEqualToString:@"Female"]) {
            
           [_btn_female setSelected:YES];
            
            self.gender=@"Female";
            
            [_btn_male setSelected:NO];
        }
        
        
        
       
    }
    
    
    
    [self checkForProfilePic];
    
    self.setImage=@"Yes";
}

- (void) checkForProfilePic {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([[self.dataDict objectForKey:@"picturePath"] length]>0) {
        
        //        NSString *urlString=[self.dataDict objectForKey:@"picturePath"];
        //
        //        NSURL *url=[NSURL URLWithString:urlString];
        //
        //        NSURLRequest *request=[NSURLRequest requestWithURL:url];
        //
        //        NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
        //
        //        [connection start];
        if ([[self.dataDict objectForKey:@"picturePath"] isEqualToString:[NSString stringWithFormat:@"%@%@",ImageResourcesURL,@"profile/"]])
            //        if ([[self.dataDict objectForKey:@"picturePath"] isEqualToString:@"http://invisionsolutions.//ca/vet247/app/webroot/img/profile/"])
        {
            
            self.imageExist=NO;
            
        }
        else
        {
            self.imageExist=YES;
            
            //self.profilePicImageView.image=image;
            
//            [self.profilePicImageView setHidden:NO];
//            
//            [self.uploadButton setHidden:YES];
//            
//            [self.changeButton setHidden:NO];
//            
//            [self.removeButton setHidden:NO];
            
            
           // NSURL *url =[self.dataDict objectForKey:@"picturePath"];
             [self downloadProfileImage];
//            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: [self.dataDict objectForKey:@"picturePath"]]];
//                   UIImage *image = [UIImage imageWithData:data];
//
//            [mypet.img_petprofile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"profile_bg_green.png"]];
            
            // [_btn_profile sd_setImageWithURL:url forState:UIControlStateNormal];
            
           
            
            
        }
        
        
        
    }
    else
    {
        self.imageExist=NO;
        
    }
    
    
}

-(void) downloadProfileImage {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    
    dispatch_async(queue, ^{
        
        NSString *imageurl =[self.dataDict objectForKey:@"picturePath"];
        
       
       // UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
         [_btn_profile sd_setImageWithURL:[NSURL URLWithString:imageurl] forState:UIControlStateNormal];
            
        
            
        });
        
    });
}


-(void)response:(id)data {
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"])
        {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid Credentials" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        else if  ([[data objectForKey:@"status"] isEqualToString:@"success"])
        {
            if ([self.callFromObject isEqualToString:@"getProfile"]) {
                
                self.callFromObject=@"others";
                
                self.dataDict=[data objectForKey:@"data"];
                
                [self updateview];
                
            }
            else{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                //[[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
               
                
                 if([self.callFromObject isEqualToString:@"save"]) {
                    
                    UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Success" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [errorAlert show];
                }
                
            }
            //
            
        }
        
    }
    
  
}




-(IBAction)donebtn:(UIButton *) sender {
   
   
    
    
    
    if ([self checkFields]) {
        
        
        NSString *userid=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
       
        
        NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
        NSString *b= self.txt_firstname.text;
        [dataDict setObject:userid forKey:@"user_id"];
        [dataDict setObject:self.txt_firstname.text forKey:@"firstName"];
        
        [dataDict setObject:self.txt_lastname.text forKey:@"lastName"];
        
        [dataDict setObject:self.txt_dob.text forKey:@"birthday"];
        
      
        
        [dataDict setObject:self.txt_phonenum.text forKey:@"phoneNo"];
        
        [dataDict setObject:self.txt_streetadd.text forKey:@"streetAddress"];
        
        [dataDict setObject:self.txt_city.text forKey:@"city"];
        
        
        
//        if(self.provinceButton.hidden)
//        {
            [dataDict setObject:self.txt_Province.text forKey:@"province"];
//        }
//        else
//        {
//            [dataDict setObject:self.provinceButton.titleLabel.text forKey:@"province"];
//        }
        
        [dataDict setObject:self.txt_Country.text forKey:@"country"];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedCountry"];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.txt_Country.text forKey:@"selectedCountry"];
        
        [dataDict setObject:self.txt_postcode.text forKey:@"postalCode"];
        
        if (_btn_male.isSelected==YES) {
             [dataDict setObject:@"Male" forKey:@"sex"];
        }
        if (_btn_female.isSelected==YES){
            [dataDict setObject:@"Female" forKey:@"sex"];
        }
        
        [dataDict setObject:self.setImage forKey:@"setimage"];
        
        NetworkingClass *network=[[NetworkingClass alloc] init];
        
        network.delegate=self;
        
      
        
       NSData* data = UIImageJPEGRepresentation(self.chosenimage,0.4);
       
        
        
        NSString* image = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        
        
        
        
        //[dataDict setObject:data forKey:@"imagePath"];
        
        self.callFromObject=@"save";
        
        NSLog(@"%@",dataDict);
        
        
      
        
        network.delegate=self;
        
        
        self.callFromObject=@"save";
         NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"update_profile.php"];
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        ImageDataUpload *uploadTask = [[ImageDataUpload alloc] init];
        
     //   [uploadTask myImageUploadRequestWithParams:dataDict url:urlString image:self.chosenimage];
        [uploadTask myImageUploadRequestWithParams:dataDict url:urlString image:self.chosenimage completion:^(NSDictionary* data, NSError* error) {
        
            
         
            if (error == nil){
                NSLog(@"%@",data);
                
                _imageurl =[data objectForKey:@"profileLink"];
                NSLog(@"%@",_imageurl);
               
                [dataDict setObject:_imageurl forKey:@"imagePath"];
                
                
              
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{

                
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"User Profile Updated"
                                             message:@"Profile has been updated"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    
                    
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]&&[self checkFields])
                        
                    {
                        //[[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"firstLoggin" ];
                        [self performSegueWithIdentifier:@"dashboard" sender:nil];
                        
                    }
                    
                    
                    
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
        
            
            });
            
            
            
        }];
        
      // [network setProfile:data dataDict:dataDict];

        
       
        // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        
        
        
        
//        [NetworkingClass request2:urlString param:dataDict completion:^(id finished,NSError *error){
//            
//            NSString * str =  [[NSString alloc] initWithData:finished encoding:NSUTF8StringEncoding];
//            NSDictionary *jsonDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:finished];
//            //NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:finished options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"Dictionary from data %@",str);
//            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//
//            
//        }];
////
    
//        [NetworkingClass request:urlString parameters:dataDict completion:^(id finished, NSError *error) {
//            
//            NSLog(@"%@",finished);
//      //   NSError* error;
//            
//            
//            
//            NSString * str =  [[NSString alloc] initWithData:finished encoding:NSUTF8StringEncoding];
//            NSDictionary *jsonDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:finished];
//            //NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:finished options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"Dictionary from data %@",str);
//            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            
//            
//            //   NSDictionary* json = [NSJSONSerialization JSONObjectWithData:finished
//            //  options:kNilOptions
//            //   error:&error];
//            
//            //  NSArray* latestLoans = [json objectForKey:@"loans"];
//            
//            // NSLog(@"loans: %@", latestLoans);
//            
//            
//        }];
//
        
       
//   
//        
   }
//
    
    
   
    
    
    
}

-(IBAction)profilebtn:(UIButton *) sender{
    [self showActionSheet];
    
}

- (void)showActionSheet {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select from Camera or Library"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Library",@"Camera", nil];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
            [self selectFromLibrary];
            
            break;
        case 1:
            
            [self selectFromCamera];
            
            break;
            
        default:
            
            break;
    }
}

- (void) selectFromLibrary {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
        
}

- (void) selectFromCamera {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    _chosenimage = info[UIImagePickerControllerEditedImage];
    
    self.setImage=@"Yes";
    
    // UIImagePNGRepresentation
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(_chosenimage) forKey:@"userImage"];
    
    [_btn_profile setImage:_chosenimage  forState:UIControlStateNormal];
   // [_btn_profile setimage:_chosenimage forState:UIControlStateNormal];
   // self.profilePicImageView.image = chosenImage;
    
    self.callFromObject=@"save";
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}




- (BOOL) checkFields{
    
    
    
    //NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    if ( self.txt_firstname.text && self.txt_firstname.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"First Name Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
    }
    
    else if ( self.txt_lastname.text && self.txt_lastname.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
    }
//    else if (self.btn_male.isSelected==NO && self.btn_female.isSelected==NO)
//    {
//        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Select Sex" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [errorAlert show];
//        
//        return NO;
//    }
    
  
    
 
    
    else if (self.txt_city.text && self.txt_city.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"City Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
   
    else if (self.txt_Country.text && self.txt_Country.text.length<1){
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Select Country" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
  
    
  
  
    else if ([self.btn_profile.currentImage isEqual:[UIImage imageNamed:@"avatar"]]) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Select image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        return NO;
        
    }
    
    
 

  
    
    return YES;
}


-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void) stacktapmale:(UITapGestureRecognizer *)sender {
    
  
    
    if (self.btn_male.isSelected==NO && _btn_female.isSelected==NO)
    {
        [self.btn_male setSelected:YES];
    }
    
    else if (self.btn_male.isSelected==NO && self.btn_female.isSelected==YES)
    {
        
        
        
        [self.btn_male setSelected:YES];
        
        [self.btn_female setSelected:NO];
    }
    
}




-(IBAction)btnmale:(UIButton *)sender {
    
    if (self.btn_male.isSelected==NO && _btn_female.isSelected==NO)
    {
        [self.btn_male setSelected:YES];
    }
    
    else if (self.btn_male.isSelected==NO && self.btn_female.isSelected==YES)
    {
        
        
        
        [self.btn_male setSelected:YES];
        
        [self.btn_female setSelected:NO];
    }

    
}

-(void) stacktapfemale:(UITapGestureRecognizer *)sender {
    
    if (self.btn_male.isSelected==NO && self.btn_female.isSelected==NO)
    {
        
        [self.btn_female setSelected:YES];
    }
    
    else if (self.btn_female.isSelected==NO && self.btn_male.isSelected==YES)
    {
        
        [self.btn_female setSelected:YES];
        
        [self.btn_male setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }
}




-(IBAction)btnfemale:(UIButton *)sender {
    
    if (self.btn_male.isSelected==NO && self.btn_female.isSelected==NO)
    {
        
        [self.btn_female setSelected:YES];
    }
    
    else if (self.btn_female.isSelected==NO && self.btn_male.isSelected==YES)
    {
        
        [self.btn_female setSelected:YES];
        
        [self.btn_male setSelected:NO];
        
        
        //  self.radiobutton5 = !self.radiobutton5;
        //  [sender setSelected:self.radiobutton5];
        
    }



}
    
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
