
#import <Foundation/Foundation.h>
#import "CalloutAnnotationView.h"
//#import "ShapeMarkerData.h"

@interface ExampleCalloutView : CalloutAnnotationView <AnnotationBtnPressed>

@property (retain, nonatomic) IBOutlet UILabel *referralAddressLabel;

//@property (retain, nonatomic) IBOutlet UILabel *gebied;

@property (retain, nonatomic) IBOutlet UIButton *showReferralDetailButton;

//@property (retain, nonatomic) IBOutlet UIImageView *bgImage;

//@property (retain, nonatomic) IBOutlet UILabel *schouwBtnLbl;

@property (nonatomic, assign) ShapeMarkerData* calloutDataInfo;

@property (nonatomic,assign) id delegate;

- (void)populateCalloutView:(ShapeMarkerData *)data;

- (IBAction) startSchouwAction:(id)sender;

- (id) initWithAnnotation:(id<MKAnnotation>)annotation Map:(MKMapView *)map;

-(id) instantiateObject;

@end