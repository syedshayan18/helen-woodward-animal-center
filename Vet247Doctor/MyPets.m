//
//  MyPets.m
//  Vet247Doctor
//
//  Created by Muhammad Raza on 30/03/2017.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "MyPets.h"

@interface MyPets ()
@property (strong, nonatomic) IBOutlet UIView *contentView;
@end

@implementation MyPets

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit{
   
   

    [[NSBundle mainBundle] loadNibNamed:@"MyPets" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
