//
//  ChatlostnfoundViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/1/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessage.h>
#import  <JSQMessagesViewController/JSQMessagesBubbleImage.h>

@interface ChatlostnfoundViewController : JSQMessagesViewController
@property (nonatomic) NSMutableArray* messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (strong,nonatomic) NSString *reciever_id;
@property (strong,nonatomic) NSString *report_id;
@property (strong,nonatomic) NSString *sender_id;
@property (strong,nonatomic) NSString *SenderID,*UserID;
@property (strong,nonatomic) NSString *sendername;
@property (strong,nonatomic) NSMutableArray *Chatarray;
@property (strong,nonatomic) UIImage *avatarimage;
@property (strong,nonatomic) UIImage *avatarimage2;
@property (strong,nonatomic) NSString *ReID,*myimage,*senderimage;
@property (strong,nonatomic) UIImage *avatarimagered;
@property (strong,nonatomic) UIImage *avatarimagegreen;
@property (strong,nonatomic) UIImage *chosenimage;
@property (strong,nonatomic) UIImage *downloadedimage;
@property (strong,nonatomic) NSString *type;
@property (strong,nonatomic) NSDictionary *messagedict;
@property (strong,nonatomic) NSString *SenderNamePassed;
@property (strong,nonatomic) NSString *listeningmsg, *listentingmsgtype,*listeningmsgsenderID,*listeningimagepath;

- (void)addPhotoMediaMessage;
@end
