//
//  MessageInboxTableViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/21/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageInboxTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_message;
@property (assign) BOOL  dataFromServer;

@property (strong,nonatomic) NSArray *dataArray;

@property (strong,nonatomic) NSMutableArray *tableData, *dataToBeProcessed,*doctorNames,*filteredDoctorNames,*filteredtableData;
@end
