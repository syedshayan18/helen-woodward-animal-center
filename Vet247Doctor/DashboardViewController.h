//
//  DashboardViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/13/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PetdetailsViewController.h"
@import GoogleMaps;

@interface DashboardViewController : UIViewController<PetdetailsViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *ui_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_logout;

@property (weak, nonatomic) IBOutlet UIButton *btn_mypets;
@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (strong,nonatomic) NSMutableArray *petdata;
@property (weak, nonatomic) IBOutlet UIButton *btn_callvet;
@property (strong,nonatomic) NSMutableDictionary *iddict;
@property (strong,nonatomic) NSDictionary *chatIDs;
@property (weak, nonatomic) IBOutlet UIButton *btn_perservices;
@property (strong,nonatomic) NSString * userid,*latitude,*longitude;
@property (nonatomic, retain) CLLocationManager *locationManager;
@end
