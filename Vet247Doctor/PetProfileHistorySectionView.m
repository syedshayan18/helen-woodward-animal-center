//
//  PetProfileHistorySectionView.m
//  Vet247Doctor
//
//  Created by APPLE on 7/30/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfileHistorySectionView.h"

#import "ControlsFactory.h"

#import "Define.h"

#import "Masonry.h"

#import "PetProfileKeys.h"

#import "PetProfileDataManager.h"

@implementation PetProfileHistorySectionView


// Constructor

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void) addComponents{
    
    self.backgroundColor = [UIColor clearColor];
    
    self.selectedImage = [[UIImage alloc] init];
    
    self.selectedImage = [UIImage imageNamed:@"selectedButton.png"];
    
    self.unSelectedImage = [[UIImage alloc] init];
    
    self.unSelectedImage = [UIImage imageNamed:@"unSelectedButton.png"];
    
    self.userInteractionEnabled = YES;
    
    [self setUpHistoryField];
    
    [self setUpAllergicField];
    
    [self setUpMedicationFieldView];
    
    [self setUpDietFieldView];
    
    [self setUpVaccinatedField];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
    {
        
        [self populateDataToView];
        
    }
}

-(void) setUpHistoryField{
    
    self.historyFieldBGImage = [ControlsFactory getImageView];
    
    self.historyFieldBGImage.image = [UIImage imageNamed:@"historyFieldBG.png"];
    
    self.historyFieldBGImage.userInteractionEnabled = YES;
    
    self.historyFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 8.5, 5, 8.5);
    
    [self addSubview:self.historyFieldBGImage];
    
    [self.historyFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.mas_width).with.offset(-18);
        
        make.right.equalTo(self.mas_right).with.offset(padding.right);
        
        make.height.mas_equalTo(98);
        
    }];
    
    self.historyTextView = [ControlsFactory getTextView];
    
    self.historyTextView.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.historyTextView setFont:[UIFont systemFontOfSize:16]];
    
    [self.historyTextView setReturnKeyType:UIReturnKeyNext];
    
    self.historyTextView.text = @"\n History of illness?";
    
    self.historyTextView.textAlignment = NSTextAlignmentCenter;
    
    self.historyTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    self.historyTextView.delegate = self;
    
    self.historyTextView.tag = 1;
    
    [self addSubview:self.historyTextView];
    
    [self bringSubviewToFront:self.historyTextView];
    
    padding = UIEdgeInsetsMake(23, 7.5, 000 , 7.5);
    
    [self addSubview:self.historyTextView];
    
    [self bringSubviewToFront:self.historyTextView];
    
    [self.historyTextView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.historyFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.historyFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.historyFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.width.mas_equalTo(self.frame.size.width - 20);
        
        make.height.mas_equalTo(67);
    }];
}

-(void) setUpAllergicField{
    
    self.allergiFieldBGImage = [ControlsFactory getImageView];
    
    self.allergiFieldBGImage.image = [UIImage imageNamed:@"allergiesFieldBG.png"];
    
    self.allergiFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 8.5, 5, 8.5);
    
    [self addSubview:self.allergiFieldBGImage];
    
    [self.allergiFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.historyFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.mas_right).with.offset(padding.right);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-18);
        
        make.height.mas_equalTo(98);
    }];
    
    self.allergiTextView = [ControlsFactory getTextView];
    
    self.allergiTextView.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.allergiTextView setFont:[UIFont systemFontOfSize:16]];
    
    self.allergiTextView.text = @"Does your pet have any known drug allergies?";
    
    self.allergiTextView.editable = YES;
    
    self.allergiTextView.tag = 1;
    
    self.allergiTextView.textAlignment = NSTextAlignmentCenter;
    
    self.allergiTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    padding = UIEdgeInsetsMake(23, 7.5, 000 , 7.5);
    
    self.allergiTextView.delegate = self;
    
    [self addSubview:self.allergiTextView];
    
    [self.allergiTextView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.allergiFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.allergiFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.allergiFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(67);
    }];
}

-(void) setUpMedicationFieldView{
    
    self.medicationFieldBGImage = [ControlsFactory getImageView];
    
    self.medicationFieldBGImage.image = [UIImage imageNamed:@"medicationFieldBG.png"];
    
    self.medicationFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 8.5, 5, 8.5);
    
    [self addSubview:self.medicationFieldBGImage];
    
    [self.medicationFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.allergiFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-18);
        
        make.right.equalTo(self.mas_right).with.offset(padding.right);
        
        make.width.mas_equalTo(self.frame.size.width - 20);
        
        make.height.mas_equalTo(98);
    }];
    
    self.medicationTextView = [ControlsFactory getTextView];
    
    self.medicationTextView.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.medicationTextView.text = @"Is your pet recieving any medication?";
    
    [self.medicationTextView setFont:[UIFont systemFontOfSize:16]];
    
    self.medicationTextView.editable = YES;
    
    self.medicationTextView.tag = 1;
    
    self.medicationTextView.textAlignment = NSTextAlignmentCenter;
    
    self.medicationTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    padding = UIEdgeInsetsMake(23, 7.5, 000 , 7.5);
    
    self.medicationTextView.delegate = self;
    
    [self addSubview:self.medicationTextView];
    
    [self.medicationTextView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.medicationFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.medicationFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.medicationFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(67);
    }];
}

-(void) setUpDietFieldView{
    
    self.dietFieldBGImage = [ControlsFactory getImageView];
    
    self.dietFieldBGImage.image = [UIImage imageNamed:@"dietFieldBg.png"];
    
    self.dietFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 8.5, 5, 8.5);
    
    [self addSubview:self.dietFieldBGImage];
    
    [self.dietFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.medicationFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-18);
        
        make.right.equalTo(self.mas_right).with.offset(padding.right);
        
        make.width.mas_equalTo(292);
        
        make.height.mas_equalTo(98);
        
    }];
    
    self.dietTextView = [ControlsFactory getTextView];
    
    self.dietTextView.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    self.dietTextView.text = @"\n What is your pet's diet?";
    
    [self.dietTextView setFont:[UIFont systemFontOfSize:16]];
    
    self.dietTextView.textAlignment = NSTextAlignmentCenter;
    
    self.dietTextView.editable = YES;
    
    self.dietTextView.tag = 1;
    
    self.dietTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
    
    padding = UIEdgeInsetsMake(23, 7.5, 000 , 7.5);
    
    self.dietTextView.delegate = self;
    
    [self addSubview:self.dietTextView];
    
    [self bringSubviewToFront:self.dietTextView];
    
    [self.dietTextView mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.dietFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.dietFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.dietFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(67);
        
    }];
    
}

-(void) setUpVaccinatedField{
    
    // Animal is vaccinted verification's field BackGround Image
    
    self.vaccinatedFieldBGImage = [ControlsFactory getImageView];
    
    self.vaccinatedFieldBGImage.image = [UIImage imageNamed:@"vaccinationFIeldBG.png"];
    
    self.vaccinatedFieldBGImage.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 10);
    
    [self addSubview:self.vaccinatedFieldBGImage];
    
    [self.vaccinatedFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.dietFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(68);
        
        make.bottom.equalTo(self.mas_bottom).with.offset(-5);
        
    }];
    
    // setting up Vaccinated YES/NO seletion field lables and button
    
    // setting YES button
    
    self.vaccinatedYesButton = [ControlsFactory getButton];
    
    self.vaccinatedYesButton.backgroundColor = [UIColor clearColor];
    
    [self.vaccinatedYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.vaccinatedYesButton addTarget:self action:@selector(vaccinatedYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(30, 40, 5, 7.5);
    
    [self addSubview:self.vaccinatedYesButton];
    
    [self.vaccinatedYesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.vaccinatedFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.vaccinatedFieldBGImage.mas_centerX).with.offset(-60);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting YES buttonBG
    
    self.vaccinatedYesButtonBG = [ControlsFactory getButton];
    
    self.vaccinatedYesButtonBG.backgroundColor = [UIColor clearColor];
    
    [self.vaccinatedYesButtonBG addTarget:self action:@selector(vaccinatedYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(23, 0, 5, 7.5);
    
    [self addSubview:self.vaccinatedYesButtonBG];
    
    [self.vaccinatedYesButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.right.equalTo(self.vaccinatedYesButton.mas_right).with.offset(-padding.left);
        
        make.width.mas_equalTo(59);
        
        make.height.mas_equalTo(35);
        
    }];
    
    // setting YES Button's Lable
    
    self.vaccinatedYesLable = [ControlsFactory getLabel];
    
    self.vaccinatedYesLable.backgroundColor = [UIColor clearColor];
    
    self.vaccinatedYesLable.text = @"Yes";
    
    self.vaccinatedYesLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(22, 30, 5, 7.5);
    
    [self addSubview:self.vaccinatedYesLable];
    
    [self.vaccinatedYesLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.vaccinatedFieldBGImage.mas_centerX).with.offset(-28);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(35);
        
    }];
    
    // setting NO Button
    
    self.vaccinatedNoButton = [ControlsFactory getButton];
    
    self.vaccinatedNoButton.backgroundColor = [UIColor clearColor];
    
    [self.vaccinatedNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.vaccinatedNoButton addTarget:self action:@selector(vaccinatedNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(30,15,0,0);
    
    [self addSubview:self.vaccinatedNoButton];
    
    [self.vaccinatedNoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.vaccinatedYesLable.mas_right).with.offset(30);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting NO ButtonBG
    
    self.vaccinatedNoButtonBG = [ControlsFactory getButton];
    
    self.vaccinatedNoButtonBG.backgroundColor = [UIColor clearColor];
    
    [self.vaccinatedNoButtonBG addTarget:self action:@selector(vaccinatedNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(23,15,0,0);
    
    [self addSubview:self.vaccinatedNoButtonBG];
    
    [self.vaccinatedNoButtonBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.vaccinatedYesLable.mas_right).with.offset(55);
        
        make.width.mas_equalTo(60);
        
        make.height.mas_equalTo(35);
        
    }];
    
    // setting NO button's Lable
    
    self.vaccinatedNoLable = [ControlsFactory getLabel];
    
    self.vaccinatedNoLable.backgroundColor = [UIColor clearColor];
    
    self.vaccinatedNoLable.text = @"No";
    
    self.vaccinatedNoLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(22, 40, 5, 7.5);
    
    [self addSubview:self.vaccinatedNoLable];
    
    [self.vaccinatedNoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.vaccinatedFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.vaccinatedFieldBGImage.mas_centerX).with.offset(72);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
}

// testing History section Fileds

-(BOOL) checkHistoryInfoTextFields
{
    //NSString *phoneRegEx = @"[0-9]{1,11}";
    
    //NSPredicate *ageTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    // Making view controller to handle error messages
    //    UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    //
    //
    //    UIAlertController * checkHistoryInfoFieldsErrorAlert =   [UIAlertController
    //                                                              alertControllerWithTitle:@"Error"
    //                                                              message:@""
    //                                                              preferredStyle:UIAlertControllerStyleAlert];
    //    UIAlertAction* ok = [UIAlertAction
    //                         actionWithTitle:@"OK"
    //                         style:UIAlertActionStyleDefault
    //                         handler:^(UIAlertAction * action)
    //                         {
    //                             [checkHistoryInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                         }];
    //    UIAlertAction* cancel = [UIAlertAction
    //                             actionWithTitle:@"Cancel"
    //                             style:UIAlertActionStyleDefault
    //                             handler:^(UIAlertAction * action)
    //                             {
    //                                 [checkHistoryInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                             }];
    //
    //    [checkHistoryInfoFieldsErrorAlert addAction:ok];
    //    [checkHistoryInfoFieldsErrorAlert addAction:cancel];
    //
    //    if ( self.historyTextView.tag == 1 || (self.historyTextView.text && self.historyTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Illness History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.allergiTextView.tag == 1 || (self.allergiTextView.text && self.allergiTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Medical Allergic History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.allergiTextView.tag == 1 || (self.medicationTextView.text && self.medicationTextView.text.length<1)) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Kindly enter Medication regarding History. If no history, enter 'NO'. "];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //    else if (self.dietTextView.tag == 1 || (self.dietTextView.text && self.dietTextView.text.length < 1))
    //    {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Please enter Pet's Diet"];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    //
    //    else if ([self.vaccinatedYesButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedNoButton.currentImage isEqual:self.unSelectedImage]) {
    //
    //        [checkHistoryInfoFieldsErrorAlert setMessage:@"Kindly select Vaccinated YES or NO"];
    //
    //        [errorShowViewController presentViewController:checkHistoryInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    
    PetProfileDataManager * addHistoryInfo = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    if(self.historyTextView.tag !=1)
    {
        [addHistoryInfo insertPetProfileInfo: self.historyTextView.text :AnimalIllnessHistoryKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalIllnessHistoryKey];
    }
    
    if(self.allergiTextView.tag != 1)
    {
        [addHistoryInfo insertPetProfileInfo: self.allergiTextView.text :MedicalAlergiesKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :MedicalAlergiesKey];
    }
    
    if(self.medicationTextView.tag !=1)
    {
        [addHistoryInfo insertPetProfileInfo: self.medicationTextView.text :AnimalMedicationKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalMedicationKey];
    }
    
    if(self.dietTextView.tag != 1)
    {
        [addHistoryInfo insertPetProfileInfo: self.dietTextView.text :AnimalDietKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalDietKey];
    }
    
    if([self.vaccinatedYesButton.currentImage isEqual:self.selectedImage])
    {
        [addHistoryInfo insertPetProfileInfo: @"Yes" :AnimalVaccinatedKey];
    }
    else if([self.vaccinatedNoButton.currentImage isEqual:self.selectedImage])
    {
        [addHistoryInfo insertPetProfileInfo: @"No" :AnimalVaccinatedKey];
    }
    else
    {
        [addHistoryInfo insertPetProfileInfo: @"" :AnimalVaccinatedKey];
    }
    
    return  YES;
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if([[NSPetData objectForKey:AnimalIllnessHistoryKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalIllnessHistoryKey] == nil)
    {
        //        self.historyTextView.text = [NSPetData objectForKey:AnimalIllnessHistoryKey];
        //        self.historyTextView.textColor = [UIColor blackColor];
        //        self.historyTextView.tag = 0;
    }
    else
    {
        self.historyTextView.text = [NSPetData objectForKey:AnimalIllnessHistoryKey];
        self.historyTextView.textColor = [UIColor blackColor];
        self.historyTextView.tag = 0;
    }
    
    if([[NSPetData objectForKey:MedicalAlergiesKey] isEqualToString:@""] || [NSPetData objectForKey:MedicalAlergiesKey] == nil)
    {}
    
    else
    {
        self.allergiTextView.text = [NSPetData objectForKey:MedicalAlergiesKey];
        self.allergiTextView.textColor = [UIColor blackColor];
        self.allergiTextView.tag = 0;
    }
    
    if([[NSPetData objectForKey:AnimalMedicationKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalMedicationKey] == nil)
    {}
    else
    {
        self.medicationTextView.text = [NSPetData objectForKey:AnimalMedicationKey];
        self.medicationTextView.textColor = [UIColor blackColor];
        self.medicationTextView.tag = 0;
    }
    
    if([[NSPetData objectForKey:AnimalDietKey] isEqualToString:@""] || [NSPetData objectForKey:AnimalMedicationKey] == nil)
    {}
    else
    {
        self.dietTextView.text = [NSPetData objectForKey:AnimalDietKey];
        self.dietTextView.textColor = [UIColor blackColor];
        self.dietTextView.tag = 0;
    }
    
    if ([[NSPetData objectForKey:AnimalVaccinatedKey] isEqualToString:@"Yes"]) {
        
        [self.vaccinatedYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if([[NSPetData objectForKey:AnimalVaccinatedKey] isEqualToString:@"No"])
    {
        [self.vaccinatedNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
}



- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    [self changeTextViewStatus];
    
    if(textView.tag == 1)
    {
        textView.text = @"";
        textView.tag = 0;
    }
    
//    if([textView isEqual:self.medicationTextView] || [textView isEqual:self.dietTextView])
//    {
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center = CGPointMake(self.center.x, 80);
//        }
//        else
//        {
//            self.center = CGPointMake(self.center.x, 100);
//        }
//    }
    
    textView.textColor = [UIColor blackColor];
    
    return YES;
}

-(void) changeTextViewStatus {
    
    if(self.allergiTextView.text.length == 0)
    {
        self.allergiTextView.text = @"Does your pet have any known drug allergies?";
        self.allergiTextView.tag = 1;
        self.allergiTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
        return;
    }
    
    if (self.historyTextView.text.length == 0)
    {
        self.historyTextView.text = @"\n History of illness?";
        self.historyTextView.tag = 1;
        self.historyTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
        return;
    }
    
    if (self.medicationTextView.text.length == 0)
    {
        self.medicationTextView.text = @"Is your pet recieving any medication?";
        self.medicationTextView.tag = 1;
        self.medicationTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
        return;
    }
    
    if (self.dietTextView.text.length == 0)
    {
        self.dietTextView.text = @"\n What is your pet's diet?";
        self.dietTextView.tag = 1;
        self.dietTextView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
        return;
    }
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0];
        
        if(textView == self.allergiTextView)
        {
            textView.text = @"Does your pet have any known drug allergies?";
            textView.tag = 1;
            [textView resignFirstResponder];
            //[self.medicationTextView becomeFirstResponder];
        }
        
        else if (textView == self.historyTextView)
        {
            textView.text = @"\n History of illness?";
            textView.tag = 1;
            [textView resignFirstResponder];
            //[self.allergiTextView becomeFirstResponder];
        }
        
        else if (textView == self.medicationTextView)
        {
            textView.text = @"Is your pet recieving any medication?";
            textView.tag = 1;
            [textView resignFirstResponder];
            //[self.dietTextView becomeFirstResponder];
        }
        
        else if (textView == self.dietTextView)
        {
            textView.text = @"\n What is your pet's diet??";
            textView.tag = 1;
            [textView resignFirstResponder];
        }
        
        [textView resignFirstResponder];
        
    }
    //
    //    else
    //    {
    //        if(textView == self.allergiTextView)
    //        {
    ////            textView.text = @"Does your pet have any known drug allergies?";
    ////            textView.tag = 1;
    //            [textView resignFirstResponder];
    //            [self.medicationTextView becomeFirstResponder];
    //
    //        }
    //
    //        else if (textView == self.historyTextView)
    //        {
    ////            textView.text = @"\n History of illness?";
    ////            textView.tag = 1;
    //            [textView resignFirstResponder];
    //            [self.allergiTextView becomeFirstResponder];
    //        }
    //
    //        else if (textView == self.medicationTextView)
    //        {
    ////            textView.text = @"Is your pet recieving any medication?";
    ////            textView.tag = 1;
    //            [textView resignFirstResponder];
    //            [self.dietTextView becomeFirstResponder];
    //        }
    //
    //        else if (textView == self.dietTextView)
    //        {
    ////            textView.text = @"\n What is your pet's diet??";
    ////            textView.tag = 1;
    //            [textView resignFirstResponder];
    //        }
    //        [textView resignFirstResponder];
    //    }
}

-(void) textViewDidEndEditing:(UITextView *)textView
{
//    if([textView isEqual:self.medicationTextView] || [textView isEqual:self.dietTextView])
//    {
//        UIViewController * setCenterYController = [self getContainerController];
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -70);
//        }
//        else
//        {
//            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -95);
//        }
//    }
}

- (UIViewController *)getContainerController
{
    /// Finds the view's view controller.
    
    // Take the view controller class object here and avoid sending the same message iteratively unnecessarily.
    Class vcc = [UIViewController class];
    
    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass: vcc])
            return (UIViewController *)responder;
    
    // If the view controller isn't found, return nil.
    return nil;
}

-(void)layoutSubviews{
    
    
    NSLog(@"layoutSubviews");
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//
//    if([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//
//    return YES;
//}

-(void) vaccinatedNoButtonClicked:(UIButton *) vacNoButton{
    
    if ([self.vaccinatedNoButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedYesButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.vaccinatedNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.vaccinatedNoButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedYesButton.currentImage isEqual:self.selectedImage])
    {
        [self.vaccinatedNoButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.vaccinatedYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) vaccinatedYesButtonClicked:(UIButton *) vacYesButton{
    
    if ([self.vaccinatedNoButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedYesButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.vaccinatedYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.vaccinatedYesButton.currentImage isEqual:self.unSelectedImage] && [self.vaccinatedNoButton.currentImage isEqual:self.selectedImage])
    {
        [self.vaccinatedYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.vaccinatedNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

@end
