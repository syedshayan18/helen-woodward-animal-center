//
//  ThirdViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "PropertyTrackerModel.h"
@interface ThirdViewController : UIViewController
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_insurance;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_policyno;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_tracker;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_pin;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_name;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_phoneno;
@property (weak, nonatomic) IBOutlet UIButton *btn_insurranceyes;
@property (weak, nonatomic) IBOutlet UIButton *btn_insuuranceno;
@property (weak, nonatomic) IBOutlet UIButton *btn_trackerYes;
@property (weak, nonatomic) IBOutlet UIButton *btn_trackerNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_vertYes;
@property (weak, nonatomic) IBOutlet UIButton *vertNo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_havepet;
@property (weak, nonatomic) IBOutlet UIView *insuranceview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *insuranceviewcontraint;
-(BOOL) checkInsuranceInfoTextField;
@property (weak, nonatomic) IBOutlet UIView *trackerview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vertnviewheight;
@property (weak, nonatomic) IBOutlet UIView *veternview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackerviewheight;
@property PropertyTrackerModel *thirdpet;
@property (weak, nonatomic) IBOutlet UILabel *lbl_insuranceyes;
@property (weak, nonatomic) IBOutlet UILabel *lbl_insuranceno;
@property (weak, nonatomic) IBOutlet UILabel *lbl_trackerYes;
@property (weak, nonatomic) IBOutlet UILabel *lbl_trackerNo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_vertYes;
@property (weak, nonatomic) IBOutlet UILabel *lbl_VertNo;


@end
