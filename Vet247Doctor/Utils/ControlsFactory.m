//
//  ControlsFactory.m
//  Vet247Doctor
//
//  Created by Malik on 7/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ControlsFactory.h"

@interface ControlsFactory ()

@end

@implementation ControlsFactory

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
+(UIButton*)getButton
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    
    return btn;
}

+(UITextField*)getTextField
{
    UITextField *txtFld=[[UITextField alloc]init];
    
    return txtFld;
}

+(UILabel*)getLabel
{
    UILabel *label=[[UILabel alloc]init];
    
    return label;
}

+(UIImageView*)getImageView
{
    UIImageView *imageView=[[UIImageView alloc]init];
    
    return imageView;
}

+(UIView*)getView
{
    UIView *view=[[UIView alloc]init];
    
    return view;
}

+(UIScrollView*)getScrollView
{
    UIScrollView *scroll=[[UIScrollView alloc]init];
    
    return scroll;
}

+(UITableView*)getTableView
{
    UITableView *tableView=[[UITableView alloc] init];
    
    return tableView;
}

+(UITextView*) getTextView
{
    UITextView *textView=[[UITextView alloc] init];
    
    return textView;
}

+(UIPickerView*)getPickerView
{
    UIPickerView * pickerView = [[UIPickerView alloc] init];
    
    return pickerView;
}

@end
