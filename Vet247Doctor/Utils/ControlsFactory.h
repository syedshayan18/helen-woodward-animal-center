//
//  ControlsFactory.h
//  Vet247Doctor
//
//  Created by Malik on 7/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ControlsFactory : UIViewController

+(UIButton*)getButton;

+(UITextField*)getTextField;

+(UILabel*)getLabel;

+(UIImageView*)getImageView;

+(UIView*)getView;

+(UIScrollView*)getScrollView;

+(UITableView*)getTableView;

+(UITextView*) getTextView;

+(UIPickerView*) getPickerView;


@end
