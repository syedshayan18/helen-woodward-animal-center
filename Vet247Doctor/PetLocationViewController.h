//
//  PetLocationViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;
@interface PetLocationViewController : UIViewController
@property (strong,nonatomic) NSString *lng,*lat;
@property (strong,nonatomic) NSString *pettag;
@property (weak, nonatomic) IBOutlet GMSMapView *Mapview;

@end
