//
//  PetServicesinfoTVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/3/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetServicesinfoTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Infotitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_details;
@property (weak, nonatomic) IBOutlet UITextView *txt_about;

@end
