//
//  Lostnffoundmodel.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/26/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "Lostnffoundmodel.h"

@implementation Lostnffoundmodel

-(id)initWithDictionary:(NSDictionary *)lostnfounddata{
    
    self = [super init];
    
    _datadict = [[NSDictionary alloc]init];
    if (self){
        
        
        
        
          _datadict = lostnfounddata[@"data"];
        
        self.petname = [self.datadict objectForKey:@"animal_name"];
        self.petimagefound=lostnfounddata[@"pet_image"];
        self.petimagelost=[_datadict objectForKey:@"imagePath"];
        self.animaltag= lostnfounddata[@"tag"];
        self.petlostSex = [self.datadict objectForKey:@"sex"];
        self.petfoundsex = lostnfounddata [@"pet_sex"];
        self.petfoundSpecies = lostnfounddata[@"specie"];
        self.petlostSpecies = [self.datadict objectForKey:@"species"];
        self.petdescription =lostnfounddata[@"detail"];
        self.petage = [self.datadict objectForKey:@"age"];
        self.expirydate = lostnfounddata[@"date"];
        self.locationname = lostnfounddata[@"location"];
        self.latitude = lostnfounddata[@"latitude"];
        self.longitude = lostnfounddata[@"longitude"];
        self.petlostcolor = [_datadict objectForKey:@"color"];
        self.petfoundcolor = lostnfounddata[@"pet_color"];
        self.petlostbreed=[self.datadict objectForKey:@"breed"];
        self.petfoundbreed=lostnfounddata[@"breed"];
        self.petlostWeight=[_datadict objectForKey:@"weight"];
        self.petfoundweight= lostnfounddata[@"pet_weight"];
        self.petaction = lostnfounddata[@"pet_action"];
           self.userID= lostnfounddata[@"user_id"];
        self.reportID = lostnfounddata[@"id"];
        
    }
    return self;
}


@end
