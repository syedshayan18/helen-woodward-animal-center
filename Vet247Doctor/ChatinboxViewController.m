//
//  ChatinboxViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/5/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "ChatinboxViewController.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "ChatinboxTVC.h"
#import "Chatgettermodel.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ChatlostnfoundViewController.h"
@interface ChatinboxViewController ()

@end

@implementation ChatinboxViewController
@synthesize senderavatarimagetobepassed,myavatarimagetobepassed;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString *imagepath = [[NSUserDefaults standardUserDefaults]objectForKey:@"userimage"];
    NSURL *imageurl = [NSURL URLWithString:imagepath];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    
    [manager downloadImageWithURL:imageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
     
     {
         
         
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
         
         if(image){
             myavatarimagetobepassed = image;
         }
     }];

    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [self getconversations];
}


-(void)getconversations {
    
    
    NSString *userid = [[NSUserDefaults  standardUserDefaults]objectForKey:@"userId"];
    
    NSString *posturl = [NSString stringWithFormat:@"%@conversation.php?user_id=%@&report_id=%@",BaseURL,userid,_report_ID];
    NSString *url = [posturl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",url);
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [NetworkingClass getConversations:url completion:^(id finished, NSURLSessionDataTask *resp, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse*) resp.response;
        
        
        if (error==nil || responseCode.statusCode == 200){
            NSLog(@"%@",finished);
            
            if ([finished count]>0){
                
                _chatinboxarray = [[NSMutableArray alloc]init];
                
                for (int i =0;i<[finished count]; i++){
                    
                    Chatgettermodel *inboxgetter = [[Chatgettermodel alloc]initWithDict:finished[i]];
                    [_chatinboxarray addObject:inboxgetter];
                    
                    
                    
                }
                
                
                
                NSLog(@"%@",_chatinboxarray);
                [self.tableview reloadData];
            }
            
            else {
                
                
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Chat" message:@"No Chat found!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];

                
                
            }
            
            
        }
        else {
            
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                
                NSLog(@"ok");
                
                
                
                
                
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }];
    
    

}



    






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_chatinboxarray count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatinboxTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    _nameconcat = [NSString stringWithFormat:@"%@ %@",[_chatinboxarray[indexPath.row]Senderfirstname],[_chatinboxarray[indexPath.row]Senderlastname]];

    cell.lbl_username.text =_nameconcat;
    
    if ([[_chatinboxarray[indexPath.row]msgtype] isEqualToString:@"0"]){
        
         cell.lbl_lastmessage.text = [_chatinboxarray[indexPath.row]lastmsg];
    }
    else {
         cell.lbl_lastmessage.text = @"picture message";
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",userprofileURL,[_chatinboxarray[indexPath.row]senderimagepath]];
    NSURL *imagepathurl = [[NSURL alloc]initWithString:url];
                           
    [cell.img_chatuserimage sd_setImageWithURL:imagepathurl placeholderImage:[UIImage imageNamed:@"menu_avatar"]];
   
    cell.img_chatuserimage.layer.cornerRadius = 25;
    cell.img_chatuserimage.clipsToBounds =YES;
    
    
    
    
    
   

    
    
    
    
    
    return cell;
    
    
    
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete){
        
        
    }
        
        

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatinboxTVC *cell = [tableView cellForRowAtIndexPath:indexPath];
    senderavatarimagetobepassed =cell.img_chatuserimage.image;
    _passingSenderName = cell.lbl_username.text;
    _selectedindex = indexPath.row;
    
    [self performSegueWithIdentifier:@"chatscreen" sender:nil];
    
   
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    ChatlostnfoundViewController *chatscreen = (ChatlostnfoundViewController *)[segue destinationViewController];
    chatscreen.report_id =[_chatinboxarray[_selectedindex]Report_ID];
    chatscreen.SenderID=[_chatinboxarray[_selectedindex]Sender_ID];
    chatscreen.reciever_id=[_chatinboxarray[_selectedindex]Receiver_ID];
    chatscreen.avatarimagegreen = senderavatarimagetobepassed;
    chatscreen.avatarimagered = myavatarimagetobepassed;
    NSLog(@"Received:%@",[_chatinboxarray[_selectedindex]Receiver_ID]);
    NSLog(@"sender%@",[_chatinboxarray[_selectedindex]Sender_ID]);
    NSLog(@"report%@",[_chatinboxarray[_selectedindex]Report_ID]);
    chatscreen.SenderNamePassed =_passingSenderName;
    
}


@end
