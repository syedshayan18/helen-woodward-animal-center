
//
//  PetdetailsViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/29/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PetdetailsViewController.h"
#import "iCarousel.h"
#import "MyPets.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "PropertyTrackerModel.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "DashboardViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MyPetsViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "PetProfileDataManager.h"
#import "SecondViewController.h"
#import "AHContainerViewController.h"
#import "CallScreenViewController.h"
#import "AppDelegate.h"
#import "SocketManager.h"
#import <AVFoundation/AVFoundation.h>
#import "SocketManager.h"
#import "ChatController.h"
#import "LostreportViewController.h"
#pragma clang diagnostic ignored "-Wgnu"
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdouble-promotion"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPADDEVICE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)

#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)

#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE_7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)


  AVAudioPlayer *_audioPlayer;

static int dataCount;

@implementation PetdetailsViewController




@synthesize hideButton;
@synthesize  hideButtonformessage;
@synthesize  hideButtonforlostfound;




SocketManager *_manager;
static bool checkDoctorEveryThirtySeconds=FALSE;
AHContainerViewController *containerview;
CallScreenViewController * callscreen;
MyPets *mypet;


-(void)viewWillAppear:(BOOL)animated {
    
    
    
   [[self navigationController] setNavigationBarHidden:NO animated:YES];
    if (hideButton==NO&&hideButtonformessage==NO&&hideButtonforlostfound==NO){
        
        [self fetchPets];
        
        
        
    }
    self.showBusyPopUp=@"NO";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (hideButtonformessage==YES){
        self.navigationItem.rightBarButtonItem=nil;
        [self fetchPets];
    }
    
    else if (hideButton==YES){
        [self fetchPets];
         self.navigationItem.rightBarButtonItem = nil;
    }

    else if (hideButtonforlostfound==YES){
        [self fetchPets];
        self.navigationItem.rightBarButtonItem=nil;
        
    }
    

    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leaveCall) name:LeaveCallScreen object:nil];
[[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    

    
    
    
       NSLog(@"%f",[[UIScreen mainScreen] bounds].size.height)  ;
    
    
    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    _carousel.delegate = self;
    _carousel.dataSource=self;
    _wrap = NO;
    //[self fetchPets];
    [self.carousel reloadData];
     _petData=[[NSMutableArray alloc] init];
    
   
   
    
    
//    _btn_editview.layer.cornerRadius = 15; // this value vary as per your desire
//    _btn_editview.clipsToBounds = YES;
//    
//    _btn_delete.layer.cornerRadius = 15; // this value vary as per your desire
//    _btn_delete.clipsToBounds = YES;
//iCarouselTypeInvertedCylinder
     _carousel.type = iCarouselTypeLinear;

   // petview.type = iCarouselTypeLinear;
    // Do any additional setup after loading the view.
}



-(void)viewDidAppear:(BOOL)animated {
    
     //[[self navigationController] setNavigationBarHidden:NO animated:YES];
   
   

}

- (void) fetchPets
{
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate=self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [network getPets:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
      
    
}


-(IBAction)addpet:(UIBarButtonItem *)sender{
    

    [self performSegueWithIdentifier:@"addpet" sender:nil];
   
  
    
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        
        if ([containerview.currentViewController isKindOfClass:SecondViewController.self])
        {
            
            SecondViewController *secondVC = (SecondViewController *) containerview.currentViewController;
            secondVC.txt_history.enabled=YES;
            secondVC.txt_Medical.enabled=YES;
            secondVC.txt_medicalallergies.enabled=YES;
            secondVC.txt_diet.enabled=YES;
            secondVC.btn_vacineYes.userInteractionEnabled=YES;
            secondVC.btn_vacineNo.userInteractionEnabled=YES;
            
            
        }
        if ([containerview.currentViewController isKindOfClass:FirstViewController.self]){
            FirstViewController *firstVC = (FirstViewController *) containerview.currentViewController;
            firstVC.txt_age.enabled =YES;
            firstVC.txt_species.enabled=YES;
            firstVC.txt_breed.enabled=YES;
            firstVC.txt_color.enabled=YES;
            firstVC.txt_weight.enabled=YES;
            firstVC.txt_animal.enabled = YES;
            firstVC.btn_male.userInteractionEnabled=YES;
            firstVC.btn_female.userInteractionEnabled=YES;
            firstVC.btn_spayrd.userInteractionEnabled=YES;
            firstVC.btn_neutered.userInteractionEnabled=YES;
        }
        
        if ([containerview.currentViewController isKindOfClass:ThirdViewController.self]){
            ThirdViewController *thirdVC = (ThirdViewController *) containerview.currentViewController;
            thirdVC.btn_insurranceyes.userInteractionEnabled=YES;
            thirdVC.btn_insuuranceno.userInteractionEnabled=YES;
            thirdVC.btn_trackerYes.userInteractionEnabled=YES;
            thirdVC.btn_trackerNo.userInteractionEnabled=YES;
            thirdVC.btn_vertYes.userInteractionEnabled=YES;
            thirdVC.vertNo.userInteractionEnabled=YES;
            thirdVC.txt_insurance.enabled=YES;
            thirdVC.txt_policyno.enabled=YES;
            thirdVC.txt_tracker.enabled=YES;
            thirdVC.txt_pin.enabled=YES;
            thirdVC.txt_name.enabled=YES;
            thirdVC.txt_phoneno.enabled=YES;
        }
        
        
        
        
    });
    

    
    

}

-(void)response:(id)data {
    
      [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([data isKindOfClass:[NSArray class]])
    {
        self.petdictionary = [[NSMutableDictionary alloc]init];
        _petdictionary =data;
        NSLog(@"%@",_petdictionary);
        self.petData = [NSMutableArray new];
     // self.petData=data;
        NSLog(@"%@",_petData);
        
        for (int i = 0 ; i < [data count]; i++){
       PropertyTrackerModel *model =  [[PropertyTrackerModel alloc] initWithDictionary:data[i]];
        [_petData addObject:model];
            NSLog(@"PET:%@",model.animalname);
             NSLog(@"PET:%ld",(long)model.age);
            
        }
        [_carousel reloadData];
        NSLog(@"%@",_petData);
    }
    else if ([data objectForKey:@"msg"])
    {
        // [self setUpTableView];
        
        if ([self.navigationController.topViewController isKindOfClass:PetdetailsViewController.self]){
        [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"norecord" object:self];
            
          
        
        }
    }
    
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return _petData.count;
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    
    //MyPets *mypet = [[MyPets alloc] initWithFrame:CGRectMake(0,0,240,450)];
     //MyPets *mypet = [[MyPets alloc] init];
   
   // if (IS_IPHONE_6_PLUS){
        CGRect  frame = [UIScreen mainScreen].bounds;
        float height = (frame.size.height / 3) * 2;
    
        float width = (frame.size.width / 3) + (frame.size.width / 2);
        MyPets *mypet = [[MyPets alloc] initWithFrame:CGRectMake(0,0,width-30,height+64)];
    
        [mypet.btn_delete addTarget:self
                             action:@selector(btndelete:)
                   forControlEvents:UIControlEventTouchUpInside];
        [mypet.btn_editview addTarget:self
                               action:@selector(btneditview:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [mypet.btn_callpet addTarget:self
                           action:@selector(btn_callpet:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [mypet.btn_message addTarget:self
                          action:@selector(btn_msg:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [mypet.btn_report addTarget:self
                          action:@selector(btn_report:)
                forControlEvents:UIControlEventTouchUpInside];
    
        NSLog(@"petdata:%@",_petData);
        
        mypet.lbl_sex.text=[[_petData objectAtIndex:index] Sex];
        mypet.lbl_animalname.text=[[_petData objectAtIndex:index] animalname];
        mypet.lbl_breed.text=[[_petData objectAtIndex:index] breed];
        
        NSURL *url = [[NSURL alloc]initWithString:[_petData[index] profileimage]];
        NSLog(@"%@",url);
        [mypet.img_petprofile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"profile_bg_green.png"]];
        mypet.btn_delete.layer.cornerRadius=15;
        mypet.btn_delete.clipsToBounds = YES;
        mypet.btn_editview.layer.cornerRadius=15;
        mypet.btn_editview.clipsToBounds = YES;
        mypet.btn_callpet.layer.cornerRadius=15;
        mypet.btn_callpet.clipsToBounds=YES;
    
    mypet.btn_message.layer.cornerRadius=15;
    mypet.btn_message.clipsToBounds=YES;
    mypet.btn_report.layer.cornerRadius=15;
    mypet.btn_report.clipsToBounds=YES;
    
    
    if (hideButton==YES){
        
       
        
        [mypet.btn_callpet setAlpha:1.0f];
        [mypet.btn_delete setAlpha:0.0f];
        [mypet.btn_editview setAlpha:0.0f];
    }
    else if (hideButtonformessage==YES){
        [mypet.btn_callpet setAlpha:0.0f];
        [mypet.btn_delete setAlpha:0.0f];
        [mypet.btn_editview setAlpha:0.0f];
        [mypet.btn_message setAlpha:1.0f];
        
    }
    
    else if (hideButtonforlostfound==YES){
        [mypet.btn_report setAlpha:1.0f];
        [mypet.btn_delete setAlpha:0.0f];
    }
    
    
        mypet.btn_editview.tag =index;
        mypet.btn_delete.tag =index;
    mypet.btn_callpet.tag=index;
    mypet.btn_report.tag=index;
        _arrayID = [[NSMutableArray alloc]init];
        for (int i=0;i<_petData.count;i++){
            
            [_arrayID insertObject:[_petData[i]ID] atIndex:i];
            NSLog(@"%@",_arrayID);
        }
   
        NSString* curl = [NSString stringWithFormat:@"%@ Year Old",[[_petData objectAtIndex:index] stringage]];
        
        mypet.lbl_age.text=curl;
        
        //mypet.img_petprofile.=[[_petData objectAtIndex:0] image];
        
        
        if([[_petData[index]profileimage ] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/pet/"])
        {
            
            [mypet.img_petprofile setImage:[UIImage imageNamed:@"profile_bg"]];
            
        }
        else
        {
            
            [mypet.img_petprofile sd_setImageWithURL:[NSURL URLWithString:[_petData[index]profileimage ]] placeholderImage:[UIImage imageNamed:@"profile_bg"]];
            
        }
        
        
        
        return mypet;
        

       
        //[mypet setFrame:CGRectMake(0, 0, 240, 480)];
        
        
    
    
 //   }
 

  
    return view;
    

}

-(IBAction)btn_msg:(UIButton *)sender{
    
   
    
        
        ChatController *chatController=[[ChatController alloc] init];
        
        [self addChildViewController:chatController];
        
        chatController.chatData=nil;
        
        chatController.firstChat=@"YES";
        
    chatController.pet = [_petData objectAtIndex:sender.tag];
    
        [self.navigationController pushViewController:chatController animated:YES];
        
    

    
    
}


-(IBAction)btn_callpet:(UIButton *)sender{
    
    
    
    
  
   
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]) {
        
        self.showSocketError= @"NO";
        
        //[self.delegate errorFromSocket:error];
    }
    
    else {
        
        self.showSocketError= @"YES";
        
        self.showBusyPopUp=@"YES";
        
        _manager=[SocketManager sharedInstance] ;
        
        
        _petidindex = sender.tag;
        NSString *callpetid = [_arrayID objectAtIndex:_petidindex];
        _manager.petidcall =callpetid;
          [[self navigationController] setNavigationBarHidden:YES animated:YES];
        NSLog(@"call button pressed: ");
        
        [_manager shouldRegisterCall]; // this will set shouldCallBoolean to FALSE
        
        // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        callscreen =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"callscreen"];
      
        [self.view addSubview:callscreen.view];
        
        callscreen.img_callavatar.layer.cornerRadius = 75;
        callscreen.img_callavatar.clipsToBounds = YES;
       
        
        
     
//
//        [viewController.btn_decline addTarget:self
//                              action:@selector(declinebtn:)
//                    forControlEvents:UIControlEventTouchUpInside];
        
        //[s]
       // [self setUpConnectionView];
        
        //  [self performSelector:@selector(showSocketErrorPopUp) withObject:nil afterDelay:16];
        
        [self connect];
        
    }
 
    // CallScreenViewController * viewController =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"callscreen"];
   // [self presentViewController:viewController animated:YES completion:nil];

}

-(IBAction)btn_report:(UIButton *)sender{
    
    
    
    _PetID =[_arrayID objectAtIndex:sender.tag];
    [self performSegueWithIdentifier:@"reportpet" sender:nil];
}
 
-(IBAction)btneditview:(UIButton *)sender{
    
    
    
    
    
    
    
    _petdataindex =sender.tag;
    NSLog(@"%ld",_petdataindex);
    
    
    
    
    
    
    
    [self performSegueWithIdentifier:@"detailspet" sender:nil];
    
    
    
}



-(IBAction)btndelete:(UIButton *)sender
{
 
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Pet Delete"
                                 message:@"Are you sure you want to delete this pet?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
        
        _PetID =[_arrayID objectAtIndex:sender.tag];
        MyPetsViewController *passingid = [[MyPetsViewController alloc]init];
        passingid.petIDfirfirst =_PetID;
        
        // NSInteger b = [_PetID integerValue];
        //NSNumber *numbe = [NSNumber numberWithInteger:b];
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseURL,@"delete_pet.php"];
        
        NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
        body[@"pet_id"] = _PetID;
        NSLog(@"%@",body);
        /*
         NSError *error;
         
         NSData *postData = [NSKeyedArchiver archivedDataWithRootObject:body];
         //    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
         
         NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://yourvetsnow.com/petappportal/petsapp/delete_pet.php"]
         cachePolicy:NSURLRequestUseProtocolCachePolicy
         timeoutInterval:10.0];
         [request setHTTPMethod:@"POST"];
         
         [request setHTTPBody:postData];
         
         NSURLSession *session = [NSURLSession sharedSession];
         NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         if (error) {
         NSLog(@"%@", error);
         } else {
         
         NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
         NSLog(@"Dictionary from data %@",jsonDictionary);
         
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
         NSLog(@"%@", httpResponse);
         }
         }];
         [dataTask resume];
         */
        [NetworkingClass request:urlString parameters:body completion:^(id finished, NSError *error) {
            
            NSLog(@"%@",finished);
            // NSError* error;
            
            NSString * str =  [[NSString alloc] initWithData:finished encoding:NSUTF8StringEncoding];
            NSDictionary *jsonDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:finished];
            //NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:finished options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"Dictionary from data %@",str);
            
            
            
            //   NSDictionary* json = [NSJSONSerialization JSONObjectWithData:finished
            //  options:kNilOptions
            //   error:&error];
            
            //  NSArray* latestLoans = [json objectForKey:@"loans"];
            NSString *item = [_petData objectAtIndex:_petdataindex];
            [self.petData removeObject:item];
            if ([_petData count] == 0){
           [delegate petdetailsViewControllerDidFinishDelegate:0];
             //   [PetdetailsViewController setDataCount:0];
            }

            // NSLog(@"loans: %@", latestLoans);
            
            [_carousel reloadData];
            
        
        }];

        
        
        
        
        
        NSLog(@"ok");
        
        
        
        
        
    }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:okAction];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
   

}


- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}


- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0, 0.0, 1.0, 0.0);
    return CATransform3DTranslate(transform, 0.0, 0.0, offset * self.carousel.itemWidth);
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.10;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
//    NSNumber *item = (self.items)[(NSUInteger)index];
//    NSLog(@"Tapped view number: %@", item);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSLog(@"at segue method%@",_petData);
        if ([segue.identifier isEqual:@"detailspet"]){
        MyPetsViewController *petdetails = (MyPetsViewController *) [segue destinationViewController];
        petdetails.pet = [self.petData objectAtIndex:self.petdataindex];
        
        
            
//            if ([segue.identifier isEqual:@""]){
//                CallScreenViewController *callscreen = ()
//            }
////
            
        
            
           
        }
    
    if ([segue.identifier isEqual:@"reportpet"]){
        LostreportViewController *lostpet = (LostreportViewController *)[segue destinationViewController];
        lostpet.passedpetid =_PetID;
        
    }
}
+(void)setDataCount:(int)setValue{
    dataCount = setValue;
    
}


+(int) getDataCount{
    return dataCount;
}






    


-(void) showSocketErrorPopUp{
    checkDoctorEveryThirtySeconds=FALSE;
    
    if ([self.showSocketError isEqualToString:@"YES"]) {
        
        //[MBProgressHUD hideHUDForView:self.view animated:NO];
        
        
        // [_manager checkSocket:@"stop"];
        
        //_manager=[SocketManager sharedInstance] ;
        
        NSLog(@"show socket Error");
        
        // [_manager callDeactivated];
        
        //    [self.connectionView removeFromSuperview];
        //
        //        _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //
        //        if ([_appDel availableInternet]) {
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Due to some technical reasons call cannot be initiated. Pleas try again after some time" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=1;
        [alert show];
        //
        //        }
        
        
    }
}

-(void) failedToConnectToSocket {
    
    
    [_audioPlayer stop];
    
    
    checkDoctorEveryThirtySeconds=FALSE; // I think its not necessary because the above method gets called if socket does not connect means we have no doctors available yet
    
    
    
    self.isRinging=@"NO";
    
    //   [_manager checkSocket:@"stop"];
    
    [_manager callDeactivated];
    
    NSLog(@"close sockets and stop them as well");
    [_manager closeSocket];
    
    [_manager checkSocket:@"stop"];
    
    [_manager checkSocketConnectivity];
    
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Due to some technical reasons call cannot be initiated. Pleas try again after some time" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    self.showSocketError=@"NO";
    alert.tag=1;
    [alert show];
    
    
}
- (void) connect;
{
    _manager=[SocketManager sharedInstance] ;
    
    _manager.delegate=self;
    
    [_manager connect];
    
    //[_manager checkSocket:@"start"];
    
    //    [self performSelector:@selector(reconnect) withObject:nil afterDelay:5.0];
    
    [self reconnect];
    
}
-(void) reconnect {
    
    _manager=[SocketManager sharedInstance] ;
    
    _manager.delegate=self;
    
    NSLog(@"go for socket Reconnectivity");
    
    [_manager checkSocket:@"start"];
    
}
-(void)setUpCallView:(double)callViewTimeOut {
    
    [self ring];
    
    self.isRinging=@"YES";
    
    NSLog(@" time out: %f",callViewTimeOut);
    
    double seconds=callViewTimeOut*60.0;
    
    if (callViewTimeOut>0) {
        
        checkDoctorEveryThirtySeconds=TRUE;
        
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryFortySecond) object:nil];
        
        [self performSelector:@selector(checkAfterEveryFortySecond) withObject:nil afterDelay:10.0];
        
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(closeRinger) object:nil];
    
    [self performSelector:@selector(closeRinger) withObject:nil afterDelay:seconds];
    
    
}

-(void)checkAfterEveryFortySecond {
    
    if (checkDoctorEveryThirtySeconds) {
        
        [_manager pingForDoctorEveryFortySeconds];
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryFortySecond) object:nil];
        
        [self performSelector:@selector(checkAfterEveryFortySecond) withObject:nil afterDelay:10.0];
        
    }
    
    
    
}

-(void)declinebtn:(UIButton*)sender{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // send data to server if required
    
    checkDoctorEveryThirtySeconds=FALSE;
    
    [callscreen.view removeFromSuperview];
    
   // [self.connectionView removeFromSuperview];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
    
    [_audioPlayer stop];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callDisconnectedByPatientBeforeAnswer];
    
    [_manager callDeactivated];
    
    NSLog(@"decline button pressed");
    
    // [_manager closeSocket];
    
    // [_manager checkSocket:@"stop"]; //Reconnect if not connected
    
    // [_manager socketShouldNotReconnect];
    
}

-(void)leaveCall{
    
    checkDoctorEveryThirtySeconds=FALSE;
    [callscreen.view removeFromSuperview];
  //  [self.connectionView removeFromSuperview];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
    
    [_audioPlayer stop];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callDeactivated];
    
    NSLog(@"leave call screen");
    
    
}

-(void) ring{
    
    NSString *path=[[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    _audioPlayer.numberOfLoops=-1;
    
    [_audioPlayer play];
    
}


-(void)closeRinger{
    
    // will be called when User Call Timeouts
    if ([self.isRinging isEqualToString:@"YES"]) {
        
        self.isRinging=@"NO";
        
        checkDoctorEveryThirtySeconds=FALSE;
        
        _manager=[SocketManager sharedInstance];
        
        [_audioPlayer stop]; 
        
        [_manager callDisconnectedByPatientBeforeAnswer];
        
        [_manager callDeactivated];
        
        NSLog(@"self.ringing: %@",self.isRinging);
        NSLog(@"after time out interval \n\n");
        
        
         [callscreen.view removeFromSuperview];
        //[self.connectionView removeFromSuperview];
        
        [_manager closeSocket];
        
        [_manager checkSocket:@"stop"];
        
        [_manager checkSocketConnectivity];
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"It seems due to some problem your call cannot initiated. It could be due to slow internet connection or some other reason" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [errorAlert show];
    }
    
    
}



-(void)startOpenTokCall{
    
    checkDoctorEveryThirtySeconds=FALSE;
    
    OpenTokController *openToc=[[OpenTokController alloc] init];
    
    self.showBusyPopUp=@"NO";
    
    self.showSocketError=@"NO";
    
    self.isRinging=@"NO";
    
    [_audioPlayer stop];
  
     [callscreen.view removeFromSuperview];
  //  [self.connectionView removeFromSuperview];
    
    _manager=[SocketManager sharedInstance];
    
    [_manager callActivated];
    
    [self.navigationController pushViewController:openToc animated:YES];
}

-(void)responseFromSocket:(NSDictionary*)dataDict {
    
    self.showSocketError=@"NO";
    
    if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"call_activated"] ) {
        
        checkDoctorEveryThirtySeconds=FALSE;
        
        if (![dataDict objectForKey:@"session_id"]) {
            
            self.showBusyPopUp=@"NO";
            
            self.showSocketError=@"NO";
            
            self.isRinging=@"NO";
            
            [_audioPlayer stop];
            
             [callscreen.view removeFromSuperview];
         //   [self.connectionView removeFromSuperview];
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Server was not able to generate session. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alert show];
            
        }
        else {
            
            [self startOpenTokCall];
            
        }
    }
    else if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"busy"])
    {
        checkDoctorEveryThirtySeconds=FALSE;
        
        _manager=[SocketManager sharedInstance];
        
        [_audioPlayer stop];
        
        NSLog(@"USer is Busy");
        
        self.isRinging=@"NO";
        
        //   [_manager checkSocket:@"stop"];
        
        [_manager callDeactivated];
        
        [_manager closeSocket];
        
        [_manager checkSocket:@"stop"];
        
        [_manager checkSocketConnectivity];
        
        
        
        
        if ([self.showBusyPopUp isEqualToString:@"YES"]) {
            
            
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Busy" message:@"No Doctor available at the momment. Please try again after some time" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            alert.tag=0;
            [alert show];
            
        }
        
        
    }
    
    else if ([[dataDict objectForKey:@"msg_type"] isEqualToString:@"call"]){
        
        //        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"]) {
        //
        //            [[NSUserDefaults standardUserDefaults] setObject:[dataDict objectForKey:@"call_record_id"] forKey:@"call_record_id"];
        //
        //            NSMutableDictionary *docsAndCallRecord= [[NSMutableDictionary alloc] init];
        //
        //            [docsAndCallRecord setObject:[dataDict objectForKey:@"call_record_id"]  forKey:@"call_record_id"];
        //
        //            [docsAndCallRecord setObject:[dataDict objectForKey:@"docsCount"]  forKey:@"docsCount"];
        //
        //            [[NSUserDefaults standardUserDefaults] setObject:docsAndCallRecord forKey:DocsCountForCallRecordId];
        //
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //
        //            NSLog(@"show call with button");
        //
        //            double value = [[dataDict objectForKey:@"docsCount"] doubleValue];
        //
        //            [self setUpCallView:value];
        //
        //            [self setUpDeclineButton];
        //
        //            self.connecting.text=@"";
        //
        //        }
        //        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"] isEqualToString:[dataDict objectForKey:@"call_record_id"] ]){
        //
        //            if ([[dataDict objectForKey:@"docsCount"] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:DocsCountForCallRecordId] objectForKey:@"docsCount"]]) {
        //
        //                NSLog(@"same call_record_id and same docsCount");
        //            }
        //
        //            else{
        //
        //                NSLog(@"same call_record_id and but different docsCount cancel previous selector");
        //
        //                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkAfterEveryThirtySeconds) object:nil];
        //
        //
        //                NSMutableDictionary *docsAndCallRecord= [[NSMutableDictionary alloc] init];
        //
        //                [docsAndCallRecord setObject:[dataDict objectForKey:@"call_record_id"]  forKey:@"call_record_id"];
        //
        //                [docsAndCallRecord setObject:[dataDict objectForKey:@"docsCount"]  forKey:@"docsCount"];
        //
        //                [[NSUserDefaults standardUserDefaults] setObject:docsAndCallRecord forKey:DocsCountForCallRecordId];
        //
        //                [[NSUserDefaults standardUserDefaults] synchronize];
        //
        //
        //            }
        //        }
        //        else
        //        {
        //            [[NSUserDefaults standardUserDefaults] setObject:[dataDict objectForKey:@"call_record_id"] forKey:@"call_record_id"];
        
        NSLog(@"show call with button");
        
        double value = [[dataDict objectForKey:@"docsCount"] doubleValue];
        
        [self setUpCallView:value];
        
        [callscreen.btn_decline addTarget:self
                                      action:@selector(declinebtn:)
                            forControlEvents:UIControlEventTouchUpInside];
        //[self setUpDeclineButton];
        [callscreen.lbl_calling setAlpha:0];
        [callscreen.lbl_connecting setAlpha:1.0];
        
        
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
   
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LeaveCallScreen object:nil];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex              {
    if(alertView.tag==0){
        
        if(buttonIndex == 0)//OK button pressed
        {
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            NSLog(@"after busy is acknowledged remove connectionView");
            [callscreen.view removeFromSuperview];
           // [self.connectionView removeFromSuperview];
            
            //            if([self.callView isDescendantOfView:self.view]) {
            //
            //                 NSLog(@"after busy is acknowledged remove callView");
            //                
            //                [self.callView removeFromSuperview];
            //            }
            //            else if ([self.connectionView isDescendantOfView:self.view]){
            //               
            //                 NSLog(@"after busy is acknowledged remove connectionView");
            //                [self.connectionView removeFromSuperview];
            //            }
        }
    }
    else   if (alertView.tag==1){
        
        if(buttonIndex == 0)//OK button pressed
        {
            [self.navigationController setNavigationBarHidden:NO animated:YES];
             [callscreen.view removeFromSuperview];
          //  [self.connectionView removeFromSuperview];
            
        }
        
    }
}


-(void)errorFromSocket:(NSError*)error {
    
}

-(void)dismissAlert:(UIAlertView *) alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}



@end
