//
//  SecondViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "PropertyTrackerModel.h"
@interface SecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_history;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_Medical;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_medicalallergies;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_diet;
@property (weak, nonatomic) IBOutlet UIButton *btn_vacineYes;
@property (weak, nonatomic) IBOutlet UIButton *btn_vacineNo;
-(BOOL) checkHistoryInfoTextField;
@property (weak, nonatomic) IBOutlet UILabel *lbl_vaccineYes;
@property (weak, nonatomic) IBOutlet UILabel *lbl_VacineNo;
@property BOOL secondunable;


@property PropertyTrackerModel *secondpet;
@end
