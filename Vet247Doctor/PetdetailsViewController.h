//
//  PetdetailsViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/29/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "AppDelegate.h"
#import "OpenTokController.h"
#import <AVFoundation/AVFoundation.h>
@protocol PetdetailsViewControllerDelegate
@required


- (void)petdetailsViewControllerDidFinishDelegate:(int)count;


@end



id <PetdetailsViewControllerDelegate> delegate;

@interface PetdetailsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>




    




@property (strong, nonatomic) NSMutableArray *animals;
@property (strong, nonatomic) NSMutableArray *descriptions;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (strong,nonatomic) NSMutableArray *petData;
@property (strong,nonatomic) NSMutableArray *arrayID;
@property (strong,nonatomic) NSMutableDictionary *petdictionary;
@property NSInteger petdataindex;
@property NSInteger petidindex;
@property NSString *PetID;
@property (nonatomic) BOOL wrap;


@property (nonatomic) BOOL hideButton;
@property (nonatomic) BOOL hideButtonformessage;
@property (nonatomic) BOOL hideButtonforlostfound;
@property (strong, nonatomic) AppDelegate *appDel;
@property (strong,nonatomic) NSString *isRinging,*showSocketError;
@property (strong,nonatomic) NSString *showBusyPopUp;
+(void)setDataCount:(int)setValue;
 

+(int) getDataCount;
 
@end


