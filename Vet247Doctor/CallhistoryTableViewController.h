//
//  CallhistoryTableViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallhistoryTableViewController : UITableViewController
@property (strong,nonatomic) NSString *useridhistory;
@property (strong,nonatomic) NSMutableDictionary *iddict;
@property (strong,nonatomic) NSMutableDictionary *callhistroy;
@property (strong,nonatomic) NSMutableDictionary *filteredhistory;
@property (strong,nonatomic) NSDictionary *doctorinfo;

@property (strong,nonatomic) NSDictionary *petinfo;
@property (strong,nonatomic) NSDictionary *userdict;
@property (strong,nonatomic) NSMutableArray *durationarray;
@property (strong,nonatomic) NSMutableArray *datearray;
@property (strong,nonatomic) NSMutableArray *imagearray;
@property (strong,nonatomic) NSMutableArray *currentdate;
@property (strong,nonatomic) NSMutableArray *historyid;
@property BOOL check;
@end
