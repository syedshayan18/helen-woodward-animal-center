//
//  PaymentController.m
//  Vet247Doctor
//
//  Created by APPLE on 9/12/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PaymentController.h"

#import "ControlsFactory.h"

#import "Masonry.h"

#import "AppDelegate.h"

#import "DashboardController.h"

#import "Define.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "PaymentTermsAndConditionController.h"

#import "PromotionalCodeUtills.h"

#define PermotionalBonusKEY @"))))))PermotionalBonusAvailable(((("

#define PermotionalBonusAvailable @"YES"

#define PermotionalBonusNotAvailable @"NO"


@interface PaymentController ()

@property (strong, nonatomic) UIView * headerView;

@property (strong, nonatomic) UIImageView *backGroundImage;

@property (strong, nonatomic) UILabel * headerViewlabel;

@property (strong, nonatomic) UIButton *  previousButton;

@property (strong, nonatomic) UIView * paymentFieldsView;

@property (strong, nonatomic) UIButton * insuranceButton;

@property (strong, nonatomic) UILabel * insuranceButtonConditionLable;

@property (strong, nonatomic) UITextField * promotionalCodeTextField;

@property (strong, nonatomic) UIButton * payNowButton;

@property (strong, nonatomic) UILabel * currencyInfoLable;

@property (strong, nonatomic) UITapGestureRecognizer * gestureRecognizer;

@end

@implementation PaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.userInteractionEnabled = YES;
    
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    [self addComponents];
    
}

-(void) addComponents
{
    [self setUpBGImage];

   
    
    [self setUpPaymentFieldsView];
}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}


-(void) setUpPaymentFieldsView
{
    self.paymentFieldsView=[ControlsFactory getView];
    
    self.paymentFieldsView.backgroundColor=[UIColor clearColor];
    
    UIEdgeInsets paddingForHeaderView;
    
    if(isiPhone4)
    {
        paddingForHeaderView = UIEdgeInsetsMake(80, 20, 0, 20);
    }
    
    else if(isiPhone5)
    {
        paddingForHeaderView = UIEdgeInsetsMake(110, 20, 0, 20);
    }
    
    else if(isiPhone6)
    {
        paddingForHeaderView = UIEdgeInsetsMake(145, 20, 0, 20);
    }
    
    else if(isiPhone6Plus)
    {
        paddingForHeaderView = UIEdgeInsetsMake(180, 20, 0, 20);
    }
    
    else
    {
        paddingForHeaderView = UIEdgeInsetsMake(80, 20, 0, 20);
    }
    
    [self.view addSubview:self.paymentFieldsView];
    
    [self.paymentFieldsView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForHeaderView.top);
        
        make.bottom.equalTo(self.paymentFieldsView.mas_top).with.offset(240);
        
    }];
    
    // setUp insuranceButton
    
    self.insuranceButton = [ControlsFactory getButton];
    
    self.insuranceButton.backgroundColor = [UIColor colorWithRed:0.6588 green:0.8039 blue:0.7020 alpha:1.0];
    
    [self.insuranceButton setTitle:@"Insurance" forState:UIControlStateNormal];
    
    self.insuranceButton.userInteractionEnabled = YES;
    
    [self.insuranceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.insuranceButton addTarget:self action:@selector(insuranceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.paymentFieldsView addSubview:self.insuranceButton];
    
    [self.insuranceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.paymentFieldsView.mas_left).with.offset(5);
        
        make.right.mas_equalTo(self.paymentFieldsView.mas_right).with.offset(-5);
        
        make.height.mas_equalTo(45);
        
        make.top.equalTo(self.paymentFieldsView.mas_top).with.offset(0);
        
    }];
    
    // setUp insuranceButtonConditionLable
    
    self.insuranceButtonConditionLable = [ControlsFactory getLabel];
    
    self.insuranceButtonConditionLable.backgroundColor = [UIColor clearColor];
    
    self.insuranceButtonConditionLable.text = @"If you belongs to an insurance company";
    
    [self.insuranceButtonConditionLable setTextColor:[UIColor whiteColor]];
    
    [self.insuranceButtonConditionLable setTextAlignment : NSTextAlignmentCenter];
    
    self.insuranceButtonConditionLable.numberOfLines = 2;
    
    self.insuranceButtonConditionLable.font = [UIFont systemFontOfSize:14];
    
    [self.paymentFieldsView addSubview:self.insuranceButtonConditionLable];
    
    [self.insuranceButtonConditionLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.paymentFieldsView.mas_left).with.offset(5);
        
        make.right.mas_equalTo(self.paymentFieldsView.mas_right).with.offset(-5);
        
        make.height.mas_equalTo(15);
        
        make.top.equalTo(self.insuranceButton.mas_bottom).with.offset(3);
        
    }];
    
    // setUp promotionalCodeTextField
    
    self.promotionalCodeTextField = [ControlsFactory getTextField];
    
    self.promotionalCodeTextField.backgroundColor = [UIColor whiteColor];
    
    self.promotionalCodeTextField.placeholder = @" Enter Promotional Code";
    
    self.promotionalCodeTextField.textAlignment = NSTextAlignmentCenter;
    
    self.promotionalCodeTextField.delegate = self;
    
    [self.paymentFieldsView addSubview:self.promotionalCodeTextField];
    
    [self.promotionalCodeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.paymentFieldsView.mas_left).with.offset(5);
        
        make.right.mas_equalTo(self.paymentFieldsView.mas_right).with.offset(-5);
        
        make.height.mas_equalTo(38);
        
        make.top.equalTo(self.insuranceButtonConditionLable.mas_bottom).with.offset(25);
        
    }];
    
    // setUp payNowButton
    
    self.payNowButton = [ControlsFactory getButton];
    
    self.payNowButton.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];

    [self.payNowButton setTitle:@"Pay Now" forState:UIControlStateNormal];
    
    [self.payNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.payNowButton addTarget:self action:@selector(payNowBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.payNowButton.userInteractionEnabled = YES;
    
    [self.paymentFieldsView addSubview:self.payNowButton];
    
    [self.payNowButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.paymentFieldsView.mas_left).with.offset(5);
        
        make.right.mas_equalTo(self.paymentFieldsView.mas_right).with.offset(-5);
        
        make.height.mas_equalTo(45);
        
        make.top.equalTo(self.promotionalCodeTextField.mas_bottom).with.offset(15);
        
    }];
    
    // setUp currencyInfoLable
    
    self.currencyInfoLable = [ControlsFactory getLabel];
    
    self.currencyInfoLable.backgroundColor = [UIColor clearColor];
    
    self.currencyInfoLable.text = @"$8.95 per month Annual Subscription";
    
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedCountry"] isEqualToString:@"Canada"])
//    {
//        self.currencyInfoLable.text = @"CAD 8.95 per month Annual Subscription";
//    }
//    else
//    {
//        self.currencyInfoLable.text = @"USD 8.95 per month Annual Subscription";
//    }
    
    [self.currencyInfoLable setTextColor:[UIColor whiteColor]];
    
    [self.currencyInfoLable setTextAlignment : NSTextAlignmentCenter];
    
    self.currencyInfoLable.numberOfLines = 2;
    
    self.currencyInfoLable.font = [UIFont systemFontOfSize:14];
    
    [self.paymentFieldsView addSubview:self.currencyInfoLable];
    
    [self.currencyInfoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.paymentFieldsView.mas_left).with.offset(58);
        
        make.right.mas_equalTo(self.paymentFieldsView.mas_right).with.offset(-58);
        
        make.height.mas_equalTo(35);

        make.top.equalTo(self.payNowButton.mas_bottom).with.offset(3);
        
    }];

}

-(void) previousButton1Clicked:(UIButton *) btn
{
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void) tapGesture:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
 
    [self.promotionalCodeTextField resignFirstResponder];
}

-(void) payNowBtnClicked:(UIButton *) btn
{
    [self.view endEditing:YES];
    
    [self.promotionalCodeTextField resignFirstResponder];
    
    if( self.promotionalCodeTextField.text != nil && self.promotionalCodeTextField.text.length > 0)
    {
        [PromotionalCodeUtills setPromotionalCode:self.promotionalCodeTextField.text];
        
        [self checkForPromotionalCodeAndGetAmountToPay];
    }
    else {

        [[NSUserDefaults standardUserDefaults] setObject:@"107.4" forKey:AmountToBePaidByUser];
        
        [PromotionalCodeUtills setPromotionalCode:@""];
        
        PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
        
        [self.navigationController pushViewController:paymentTermsController animated:YES];
    }
}

-(void) checkForPromotionalCodeAndGetAmountToPay {

    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [sendPaymentRecivedNetworkObject promotionalCodeChecking:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode]];
}

-(void) paymentRecived {

    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [sendPaymentRecivedNetworkObject paymentRecieved:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode] :@"0"];
    
    [PromotionalCodeUtills setPromotionalCode:@""];
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Congratulations"
                                                         message:@"You got full discount! Your package has been activated"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"successfully recived"])
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self setDashBoardAsFirstController];
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
    else
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        paymentSuccessfullyRecieved.message = @"Promotional Code Not Accepted";
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
}

// response for *(promotionalCodeCheck)*

-(void)paymentResponse:(id) data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * promotionalCodeMessage =   [UIAlertController
                                                         alertControllerWithTitle:@"Message"
                                                         message:@"Sorry, promotional is not authenticated, Press 'OK' to continue, otherwise Press 'Cancel'"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if([[[NSUserDefaults standardUserDefaults] objectForKey:AmountToBePaidByUser]  isEqual:@"0"])
                             {
                                 [self paymentRecived];
                             }
                             else {
                                 
                                 PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
                             
                                 [self.navigationController pushViewController:paymentTermsController animated:YES];
                             }
                         }];
    
    UIAlertAction * cancel = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                              }];
    
    [promotionalCodeMessage addAction:ok];
    
    [promotionalCodeMessage addAction:cancel];
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"no coupon found with this code"] || [[data objectForKey:@"status"] isEqualToString:@"failure"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"107.4" forKey:AmountToBePaidByUser];
    }
    
    else if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
    
        [promotionalCodeMessage setMessage:@"Yahoo! promotional code is accepted, Press 'OK' to continue, otherwise Press 'Cancel'"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[data objectForKey:@"amount"] forKey:AmountToBePaidByUser];
    }
    
    else {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    }
    
    [self presentViewController:promotionalCodeMessage animated:YES completion:nil];
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Error"
                                                         message:@"Fails With Network Error, Please! Try Again"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //[self setDashBoardAsFirstController];
                         }];
    
    [paymentSuccessfullyRecieved addAction:ok];
    
    [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
}

-(void) setDashBoardAsFirstController
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLoggin"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
    
    DashboardController *dashboard=[[DashboardController alloc] init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [navController setNavigationBarHidden:YES animated:YES];
    
    [UIView transitionWithView:appDelegate.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        appDelegate.window.rootViewController = navController;
                    }
                    completion:nil];
    
    [appDelegate.window makeKeyAndVisible];
    
}

-(void) insuranceButtonClicked:(UIButton *) btn
{
    [self.view endEditing:YES];
    
    [self.promotionalCodeTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
