//
//  MapViewLocatorObject.m
//  Vet247Doctor
//
//  Created by APPLE on 8/20/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "MapViewLocatorObject.h"

@implementation MapViewLocatorObject

-(id)initWithTitle:(NSString *) newTitle Location:(CLLocationCoordinate2D)location Index:(int) index
{    
    self = [super init];
    
    if(self)
    {
        _title = newTitle;
        
        NSLog(@"%@", newTitle);
        
        _coordinate = location;
        
        self.indexNumber = [NSString stringWithFormat:@"%i",index];
        
        _button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        [_button setImage:[UIImage new] forState:UIControlStateNormal];
        
        _button.tag = index;
        
    }
    
    return self;
    
}

-(MKAnnotationView *) annotaionView
{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"MapViewLocatorObject"];
    
    annotationView.enabled = YES;
    
    annotationView.canShowCallout = NO;
    
    annotationView.image = [UIImage imageNamed:@"services_pin"];
    
    self.button.backgroundColor = [UIColor colorWithRed:0.5451 green:0.7176 blue:0.4980 alpha:1.0];
    
    annotationView.rightCalloutAccessoryView = self.button;
    
    return  annotationView;
    
}


@end

