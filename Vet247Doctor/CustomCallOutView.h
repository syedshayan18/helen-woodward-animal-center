//
//  CustomCallOutView.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 9/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomCallOutView : UIView

@property (strong,nonatomic) UIButton *customAnotationButton;

@property (strong,nonatomic) UILabel *callOutLabel;

//@property (strong,nonatomic) UILabel *distanceLabel;

-(void) setUpCustomView;

@end
