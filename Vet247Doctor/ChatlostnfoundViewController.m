//
//  ChatlostnfoundViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/1/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "ChatlostnfoundViewController.h"
#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessagesInputToolbar.h>
#import <JSQMessagesViewController/JSQMessagesAvatarImageFactory.h>
#import <JSQMessagesViewController/JSQMessagesBubbleImage.h>
#import <JSQMessagesViewController/JSQMessagesBubbleImageFactory.h>
#import <JSQMessagesViewController/JSQMessagesMediaViewBubbleImageMasker.h>
#import <JSQMessagesViewController/JSQAudioMediaItem.h>
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "Chatgettermodel.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "SDWebImageManager.h"
#import <JSQMessagesViewController/JSQPhotoMediaItem.h>
#import "Vet247Doctor-Swift.h"
#import "Pushermanager.h"



@interface ChatlostnfoundViewController ()

@end

@implementation ChatlostnfoundViewController
@synthesize reciever_id,report_id,SenderID,ReID,avatarimagered,avatarimagegreen,SenderNamePassed;
- (void)viewDidLoad {
    [super viewDidLoad];

    if (SenderNamePassed==nil){
        SenderNamePassed=@"Sender";
    }
  _messagedict = [[NSDictionary alloc]init];
    [self listeninntomsg];
    NSLog(@"%@",SenderNamePassed);
    
    // in case of you are approching the poster of lost (getting msg api call)
    if (reciever_id.length<1){
        reciever_id =[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    }
      NSLog(@"ReciverID for getting MSg%@",reciever_id);
    

    
  //senderid for sending the msg always will be you
     _UserID = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    if (![reciever_id isEqualToString:_UserID]){
        ReID = reciever_id;
    }
    else {
        
        ReID = SenderID;
    }
    NSLog(@"RecieverID for sending Msg%@",ReID);
       NSLog(@"senderid:%@,recieverid=%@,reportid=%@",SenderID,reciever_id,report_id);
    
   
    
    
   
  
    NSLog(@"sender for getting MSg%@",SenderID);
    NSLog(@"report ID%@",report_id);
    
    
   
    
    
    [self getmessage];
    
   
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//    dispatch_async(queue, ^{
//        
//        
//        // Perform async operation
//        // Call your method/function here
//        // Example:
//        // NSString *result = [anObject calculateSomething];
////        dispatch_sync(dispatch_get_main_queue(), ^{
////            // Update UI
////            // Example:
////            // self.myLabel.text = result;
////        [self.collectionView reloadData];
////        });
//    });
    
    
    
    
    
  
    self.senderId =_UserID;
NSString *username=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
    self.sendername = username;
    self.senderDisplayName = username;
    self.title=@"Chat";

  
}

    

-(void)getmessage {
    
     SDWebImageManager *manager = [SDWebImageManager sharedManager];
    NSString *geturl = [NSString stringWithFormat:@"%@chat/view_messages.php?sender_id=%@&receiver_id=%@&report_id=%@",BaseURL,SenderID,reciever_id,report_id];
    NSString *url = [geturl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",url);
 
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
[NetworkingClass getchatmessages:url completion:^(id finished, NSError *error) {
 
 
    if (error==nil){
        NSLog(@"%@",finished);
        NSLog(@"%i",[finished count ]);
   
        self.messages = [[NSMutableArray alloc] init];
        self.Chatarray = [[NSMutableArray alloc] init];
        if ([finished count]>0){
         
            for (int i =0;i<[finished count]; i++){
                
                
                Chatgettermodel *chatmodel = [[Chatgettermodel alloc]initWithDictionary:finished[i]];
                [_Chatarray addObject:chatmodel];
                
            }
            
            dispatch_group_t group = dispatch_group_create();

            NSLog(@"%@",_Chatarray);
            for (int i=0; i<_Chatarray.count; i ++){
                dispatch_group_enter(group);
                
                if ([[_Chatarray[i]msgtype] isEqualToString:@"0"]){
                    JSQMessage *object = [[JSQMessage alloc]initWithSenderId:[_Chatarray[i]msgsender] senderDisplayName:[_Chatarray[i] sendername] date:[[NSDate alloc]init]  text:[_Chatarray[i] chatmessages]];
                    [_messages addObject:object];
//                    [self finishReceivingMessage];
                     dispatch_group_leave(group);
                }
                
                
                else {
                    
//                    dispatch_group_enter(group);
                    
                    
                
                    
                    NSString *imagemsg= [_Chatarray[i] chatmessages];
                    
                    NSURL *imageurl = [NSURL URLWithString:imagemsg];
                    [manager downloadImageWithURL:imageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
                     
                     {
                     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                         
                         
                         dispatch_group_leave(group);
                         if(image){
                            
                             _downloadedimage = image;
                             JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:_downloadedimage];
                             
                             
                             JSQMessage *photoMessage = [JSQMessage messageWithSenderId:[_Chatarray[i]msgsender]
                                                                            displayName:[_Chatarray[i] sendername]
                                                                                  media:photoItem];
                             [self.messages addObject:photoMessage];
                            
//                             dispatch_async(dispatch_get_main_queue(), ^{
//                                
//                                 [self finishReceivingMessage];
//                                
//                             });
                         }
                         else {
                             
                             JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"chat"]];
                             JSQMessage *photoMessage = [JSQMessage messageWithSenderId:[_Chatarray[i]msgsender]
                                                                            displayName:[_Chatarray[i] sendername]
                                                                                  media:photoItem];
                             [self.messages addObject:photoMessage];
                         }
                     }];
                }
                
                if ([[_Chatarray[i]msgsender] isEqualToString:self.senderId]){
                     _myimage = [_Chatarray[i]userimage];
                }
                
                else{
                   _senderimage = [_Chatarray[i]userimage];
                }
                
                
            }
            
            
            dispatch_group_notify(group,dispatch_get_main_queue(), ^{
                [self finishReceivingMessage];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            
            
            
            
            
            
            
            
            
            
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [self finishReceivingMessage];
//                
//            });
            
        
            NSString *url = [NSString stringWithFormat:@"%@%@",userprofileURL,_myimage];
            NSURL *imageurl = [NSURL URLWithString:url ];
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            
            
            
            
            
            [manager downloadImageWithURL:imageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
             
             {
                 
                 
             } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                 
                 if(image){
                     _avatarimage = image;
                 }
             }];

            
            
            NSString *senderimage = [NSString stringWithFormat:@"%@%@",userprofileURL,_senderimage];
            NSURL *urlsender = [NSURL URLWithString:senderimage];
        
            
            [manager downloadImageWithURL:urlsender options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
             
             {
                 
                 
             } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                 
                 if(image){
                     _avatarimage2 = image;
                 }
             }];
       
          
           
            
 
          

            
            
            
           
            
            JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
            
            self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor lightGrayColor]];
            self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:60.0/255.0 green:123.0/255.0 blue:58.0/255.0 alpha:1.0]];

        }
    

            else {
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Chat" message:@"No Chat found!" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            }

            
      
    }
    else {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"Server Error or Please Check your Internet Connectivity" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            NSLog(@"ok");
            
            
            
            
            
        }];
        
        
        [alert addAction:okAction];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
    
}];
}


- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_messages objectAtIndex:indexPath.row];
    
    
}


- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
 //JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([[[_messages objectAtIndex:indexPath.item]senderId] isEqualToString:self.senderId]){
        return _outgoingBubbleImageData;
    }
    else {
        return self.incomingBubbleImageData;
    }

    }


- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    

    return cell;
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messages count];
}


- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{

    
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    

    
    if ([message.senderId isEqualToString:self.senderId]) {
        
        
        
  
        if (avatarimagered !=nil){
            return [JSQMessagesAvatarImageFactory avatarImageWithImage:avatarimagered diameter:100];
        }
        else {
            return [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"chat"] diameter:100];

        }
        
        
    }
    else {
        
        if (avatarimagegreen !=nil){
              return  [JSQMessagesAvatarImageFactory avatarImageWithImage:avatarimagegreen diameter:100];
        }
       
        else {
             return [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"chat"] diameter:100];
        }
 
    }
    
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}
- (void) selectFromLibrary {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
   
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *imagetobereduced = info[UIImagePickerControllerEditedImage];
    
    _chosenimage  =[self imageWithImage:imagetobereduced scaledToSize:CGSizeMake(200, 200)];
    //_chosenimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self saveImageToServer];
   
    
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:_chosenimage];
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.senderId
                                                   displayName:self.senderDisplayName
                                                         media:photoItem];
    [self.messages addObject:photoMessage];
    
    
    [self finishSendingMessage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


-(void)saveImageToServer
{
    
     //COnvert Image to NSData
    NSData *dataImage = UIImageJPEGRepresentation(_chosenimage, 1.0f);
    
   //  set your URL Where to Upload Image
   NSString *posturl =[NSString stringWithFormat:@"%@chat/upload.php",BaseURL];
    
    // set your Image Name
    
    NSString *filename =[NSString stringWithFormat:@"chat-msg%f",[[NSDate date] timeIntervalSince1970]];
    

    
    // Create 'POST' MutableRequest with Data and Other Image Attachment.
    NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:posturl]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
[postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"chatmsg.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithString:@"Content-Type: image/jpg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:dataImage]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
  //   Get Response of Your Request
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error == nil){
            NSLog(@"%@",data);
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
            NSLog(@"%@",json);
            BOOL active = [[json objectForKey:@"success"] boolValue];
            if (active){
                
                NSString *imagepath=[json objectForKey:@"path"];
      
                NSString *posturl =[NSString stringWithFormat:@"%@chat/send_msg.php",BaseURL];
                NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
                body[@"sender_id"] = self.senderId;
                body[@"receiver_id"] = ReID;
                body[@"message"] = imagepath;
                body[@"report_id"] = report_id;
                body[@"type"]= @"1";
                
                
                
                
                NSLog(@"%@",body);
                
                
                [NetworkingClass sendchatmessages:posturl param:body completion:^(id finished, NSError *error) {
                    NSLog(@"success:%@",finished);
                    
                    
                }];

                
            }
            else {
                
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"unable to send image" message:@"Please retry to send image or check your internet" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
             
                
            }

        }
        else {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"unable to send image" message:@"Please retry to send image or check your internet" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
               
            }];
            
            
            [alert addAction:okAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            
        
        }
        
    }];
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSString *responseString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    
//    NSLog(@"Response  %@",responseString);
   
    
}

- (void)addPhotoMediaMessage
{
    [self selectFromLibrary];
    
    
    
}




- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}


#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Media messages", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send photo", nil), nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}
-(void) didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date{

    JSQMessage *object = [[JSQMessage alloc]initWithSenderId:senderId senderDisplayName:senderDisplayName date:[[NSDate alloc]init]  text:text];
    [_messages addObject:object];
    [self finishSendingMessage];
    
    
    NSString *posturl =[NSString stringWithFormat:@"%@chat/send_msg.php",BaseURL];
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    body[@"sender_id"] = senderId;
    body[@"receiver_id"] = ReID;
    body[@"message"] = text;
    body[@"report_id"] = report_id;
    body[@"type"]= @"0";
    
    
    

    NSLog(@"%@",body);
   
    
    [NetworkingClass sendchatmessages:posturl param:body completion:^(id finished, NSError *error) {
        NSLog(@"success:%@",finished);
        
        
    }];
  
    

}




- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self addPhotoMediaMessage];
            break;

    }
    
    
    
    
}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







-(void) listeninntomsg {
    
    [[Pushermanager instance] subscribeManager:@"message_recieved" completion:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        _messagedict = [dict objectForKey:@"message"];
        _listeningmsg = [_messagedict objectForKey:@"message"];
        _listentingmsgtype = [_messagedict objectForKey:@"type"];
        _listeningmsgsenderID = [_messagedict objectForKey:@"sender_id"];
        
        NSLog(@"%@",_listeningmsg);
        
        
        if ([_listentingmsgtype isEqualToString:@"0"]){
            JSQMessage *object = [[JSQMessage alloc]initWithSenderId:_listeningmsgsenderID senderDisplayName:self.SenderNamePassed date:[[NSDate alloc]init]  text:_listeningmsg];
            [_messages addObject:object];
             [self finishReceivingMessage];
        }
        
        else {
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            NSURL *imageurl = [NSURL URLWithString:_listeningmsg];
            [manager downloadImageWithURL:imageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
             
             {
              
             } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                 
                 if(image){
                     _downloadedimage = image;
                     JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:_downloadedimage];
                     JSQMessage *photoMessage = [JSQMessage messageWithSenderId:_listeningmsgsenderID
                                                                    displayName:SenderNamePassed
                                                                          media:photoItem];
                     [self.messages addObject:photoMessage];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [self finishReceivingMessage];
                         
                     });
                 }
                 else {
                     
                     JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"chat"]];
                     JSQMessage *photoMessage = [JSQMessage messageWithSenderId:_listeningmsgsenderID
                                                                    displayName:SenderNamePassed
                                                                          media:photoItem];
                     [self.messages addObject:photoMessage];
                
                 }
             
             }];
            
        }}];
        
    }
     
        

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
