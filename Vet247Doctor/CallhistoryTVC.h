//
//  CallhistoryTVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallhistoryTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_calltime;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UIImageView *img_pet;

@end
