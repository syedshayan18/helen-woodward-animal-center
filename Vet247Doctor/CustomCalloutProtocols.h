#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class ShapeMarkerData;
@class ControlController;

@protocol CustomAnnotationProtocol <NSObject>

//- (MKAnnotationView*)annotationViewInMap:(MKMapView*) mapView : (ControlController *)controller;

@end

@protocol CustomAnnotationViewProtocol <NSObject>

- (void)didSelectAnnotationViewInMap:(MKMapView*) mapView;

- (void)didDeselectAnnotationViewInMap:(MKMapView*) mapView;

@end

@protocol AnnotationBtnPressed <NSObject>

@optional

-(void) startSchouwPressed:(ShapeMarkerData *)data;

@end
