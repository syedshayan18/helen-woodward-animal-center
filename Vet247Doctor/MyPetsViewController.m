//
//  MyPetsViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "MyPetsViewController.h"
#import "SWRevealViewController.h"
#import "AHEmptySegue.h"
#import "AHContainerViewController.h"
#import "AppDelegate.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "FirstViewController.h"
#import "PetProfileKeys.h"
#import "PetProfileDataManager.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "PetdetailsViewController.h"
#import "DashboardViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AddpetsViewController.h"


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPADDEVICE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)



@interface MyPetsViewController ()

@property BOOL checkbool;

@property BOOL removecheck;

@property BOOL fieldunable;

@end

@implementation MyPetsViewController


@synthesize check;
@synthesize petIDfirfirst;
AHContainerViewController *containerview;
UIImage *chosenImage;




- (void)viewDidLoad {
    
    NSLog(@"%@",_pet.profileimage);

    if (_pet.profileimage==nil){
        _checkbool =NO;
    }
    else {
        _checkbool=YES;
    }
    
    if (check==YES){
        
        [_btn_edit setAlpha:0.0f];
    }
    
    
    [self.btn_next2 setAlpha:0.0f];
    [self.btn_previous setAlpha:0.0f];
    [self.btn_next setAlpha:1.0f];
  
  
   
    
    
    
//    if ([self check]){
//        
//        self.navigationItem.rightBarButtonItem = nil;
//        
//        
//        
//    }
    
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture1:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    
    NSString *petid =_pet.PetID;
    
    FirstViewController *firstVC = (FirstViewController *) containerview.currentViewController;
    
    if (petid !=nil){
        firstVC.firstpet = self.pet;
        firstVC.txt_age.enabled =NO;
        firstVC.txt_species.enabled=NO;
        firstVC.txt_breed.enabled=NO;
        firstVC.txt_color.enabled=NO;
        firstVC.txt_weight.enabled=NO;
        firstVC.txt_animal.enabled = NO;
        firstVC.btn_male.userInteractionEnabled=NO;
        firstVC.btn_female.userInteractionEnabled=NO;
        firstVC.btn_spayrd.userInteractionEnabled=NO;
        firstVC.btn_neutered.userInteractionEnabled=NO;
        
         firstVC.lbl_male.userInteractionEnabled=NO;
         firstVC.lbl_female.userInteractionEnabled=NO;
         firstVC.lbl_spayed.userInteractionEnabled=NO;
         firstVC.lbl_neutered.userInteractionEnabled=NO;
    }
    
    
    
    
    
    
    
    
    firstVC.txt_animal.text=self.pet.animalname;
    firstVC.txt_animal.text=self.pet.animalname;
    
    
    
    
    
    
    
    
    
    if ([self.pet.Species isEqualToString:@"0"]){
        firstVC.txt_species.text=@"Cat";
    }
    if ([self.pet.Species isEqualToString:@"1"])
    {
        firstVC.txt_species.text=@"Dog";
    }
    firstVC.txt_breed.text=self.pet.breed;
    
    firstVC.txt_age.text=self.pet.stringage;
    firstVC.txt_color.text=self.pet.color;
    firstVC.txt_weight.text=self.pet.Weight;
    if ([self.pet.Sex isEqualToString:@"Male"]){
        [firstVC.lbl_neutered setHidden:NO];
        [firstVC.lbl_spayed setHidden:NO];
        [firstVC.btn_neutered setAlpha:1.0f];
        [firstVC.btn_spayrd setAlpha:1.0f];
        
        [firstVC.btn_male setSelected:YES];
        [firstVC.btn_female setSelected:NO];
        
        
    }
    if ([self.pet.Sex isEqualToString:@"Female"]){
        
        [firstVC.btn_female setSelected:YES];
        [firstVC.btn_male setSelected:NO];
        
        [firstVC.lbl_neutered setHidden:YES];
        [firstVC.lbl_spayed setHidden:YES];
        [firstVC.btn_neutered setAlpha:0.0f];
        [firstVC.btn_spayrd setAlpha:0.0f];
        
    }
    if ([self.pet.spayed isEqualToString:@"Spayed"]){
        [firstVC.btn_spayrd setSelected:YES];
        [firstVC.btn_neutered setSelected:NO];
        
    }
    if ([self.pet.spayed isEqualToString:@"Neutered"]){
        [firstVC.btn_neutered setSelected:YES];
        [firstVC.btn_spayrd setSelected:NO];
    }

    
    
    
    
    
    
    if(self.check){
        self.navigationItem.rightBarButtonItem = nil;
    }
 [_userSelectedImage sd_setImageWithURL:[NSURL URLWithString:_pet.profileimage] placeholderImage:[UIImage imageNamed:@"fl_image"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
     if (error==nil) {
         [self.userSelectedImage setContentMode:UIViewContentModeScaleAspectFill];
     }
 }];
   
    

    
   
//    FirstViewController *first = (FirstViewController*)containerview.currentViewController;
//       first.firstpet = self.petarray;
    [super viewDidLoad];
   
    self.check = NO;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
    {
        
        [self populateDataToView];
        
        self.setPictureStatus = @"Update";
        
    }
    else
    {
        self.setPictureStatus = @"No";
    }

  
    _btn_next.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_next.clipsToBounds = YES;
    _btn_next2.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_next2.clipsToBounds = YES;
    _btn_previous.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_previous.clipsToBounds = YES;
   
    // Do any additional setup after loading the view.
}

- (void) tapGesture1:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    
}



-(IBAction)textfieldunable:(UIButton *)sender{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"fieldsunable" object:self];

    
    [_btn_edit setSelected:YES];
    _fieldunable = YES;
   
   
//         SecondViewController *secondVC = (SecondViewController *) containerview.currentViewController;
//        secondVC.txt_history.enabled=YES;
//        secondVC.txt_Medical.enabled=YES;
//        secondVC.txt_medicalallergies.enabled=YES;
//        secondVC.txt_diet.enabled=YES;
//        secondVC.btn_vacineYes.userInteractionEnabled=YES;
//        secondVC.btn_vacineNo.userInteractionEnabled=YES;
//        
//        secondVC.lbl_vaccineYes.userInteractionEnabled=YES;
//        secondVC.lbl_VacineNo.userInteractionEnabled=YES;
    
// FirstViewController *firstVC = (FirstViewController *) containerview.currentViewController;
//   
//                  firstVC.txt_age.enabled =YES;
//        firstVC.txt_species.enabled=YES;
//        firstVC.txt_breed.enabled=YES;
//        firstVC.txt_color.enabled=YES;
//        firstVC.txt_weight.enabled=YES;
//        firstVC.txt_animal.enabled = YES;
//        firstVC.btn_male.userInteractionEnabled=YES;
//        firstVC.btn_female.userInteractionEnabled=YES;
//        firstVC.btn_spayrd.userInteractionEnabled=YES;
//        firstVC.btn_neutered.userInteractionEnabled=YES;
//        
//        firstVC.lbl_male.userInteractionEnabled=YES;
//        firstVC.lbl_female.userInteractionEnabled=YES;
//        firstVC.lbl_spayed.userInteractionEnabled=YES;
//        firstVC.lbl_neutered.userInteractionEnabled=YES;

    
    
//           ThirdViewController *thirdVC = (ThirdViewController *) containerview.currentViewController;
//        thirdVC.btn_insurranceyes.userInteractionEnabled=YES;
//        thirdVC.btn_insuuranceno.userInteractionEnabled=YES;
//        thirdVC.btn_trackerYes.userInteractionEnabled=YES;
//        thirdVC.btn_trackerNo.userInteractionEnabled=YES;
//        thirdVC.btn_vertYes.userInteractionEnabled=YES;
//        thirdVC.vertNo.userInteractionEnabled=YES;
//        thirdVC.txt_insurance.enabled=YES;
//        thirdVC.txt_policyno.enabled=YES;
//        thirdVC.txt_tracker.enabled=YES;
//        thirdVC.txt_pin.enabled=YES;
//        thirdVC.txt_name.enabled=YES;
//        thirdVC.txt_phoneno.enabled=YES;
//        
//        thirdVC.lbl_insuranceyes.userInteractionEnabled=YES;
//        thirdVC.lbl_insuranceno.userInteractionEnabled=YES;
//        thirdVC.lbl_trackerYes.userInteractionEnabled=YES;
//        thirdVC.lbl_trackerNo.userInteractionEnabled=YES;
//        thirdVC.lbl_vertYes.userInteractionEnabled=YES;
//        thirdVC.lbl_VertNo.userInteractionEnabled=YES;
    
   
    
    }
    
    
    
    
    
    
   
    




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






-(IBAction)nextbtn:(UIButton *)sender{
    
    

    if ([containerview.currentViewController isKindOfClass:FirstViewController.self]){
    
     
        FirstViewController *first = (FirstViewController*)containerview.currentViewController;
       
        
        
        
        
        if ([first checkGeneralInfoTextField])
        {
             [containerview segueIdentifierReceivedFromParent:@"second"];
            
            
            
                SecondViewController *secondVC = (SecondViewController *) containerview.currentViewController;
                secondVC.secondpet = self.pet;
            NSString *a= self.pet.allegeries;
            NSLog(@"%@",a);
            NSString *petid = secondVC.secondpet.PetID;
            if (petid !=nil)
            {
            secondVC.txt_history.enabled=NO;
            secondVC.txt_Medical.enabled=NO;
            secondVC.txt_medicalallergies.enabled=NO;
            secondVC.txt_diet.enabled=NO;
            secondVC.btn_vacineYes.userInteractionEnabled=NO;
            secondVC.btn_vacineNo.userInteractionEnabled=NO;
                
                secondVC.lbl_vaccineYes.userInteractionEnabled=NO;
                secondVC.lbl_VacineNo.userInteractionEnabled=NO;
            
            }
                secondVC.txt_history.text=self.pet.history;
                secondVC.txt_medicalallergies.text=self.pet.allegeries;
                secondVC.txt_Medical.text=self.pet.medical;
                secondVC.txt_diet.text=self.pet.diet;
                
                if ([self.pet.vaccinated isEqualToString:@"Yes"]){
                    
                    [secondVC.btn_vacineYes setSelected:YES];
                    [secondVC.btn_vacineNo setSelected:NO];
                    
                }
                if ([self.pet.vaccinated isEqualToString:@"No"]){
                    [secondVC.btn_vacineNo setSelected:YES];
                    [secondVC.btn_vacineYes setSelected:NO];
                    
                }
            

       
            

            
            
            [self.btn_next setAlpha:0.0f];
            [self.btn_next2 setAlpha:1.0f];
            [self.btn_previous setAlpha:1.0f];
        
       
        }
   
     
        
        
       
        
    }
    
 
            
    

    
    
    
    
}




-(IBAction)nextbtn2:(UIButton *)sender{
    
    
    
    
    
    if ([sender.currentTitle isEqualToString:@"Done"]){
        
        ThirdViewController *third = (ThirdViewController *)containerview.currentViewController;
        if ([third checkInsuranceInfoTextField]) {
            
            PetProfileDataManager * takePetProfileData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
            _petID = third.thirdpet.PetID;
            
            if ( _petID !=nil){
            
                takePetProfileData.getPetProfileInfo[@"Id"] = third.thirdpet.PetID;
            
            }
            else{
               
                takePetProfileData.getPetProfileInfo[@"Id"] = @"-1";
                
            
            }
            
            NSLog(@"done");

            NSMutableDictionary * sendProfileDict;
            NSLog(@"%@",_setPictureStatus);
            NSData *petProfileImage = UIImageJPEGRepresentation(chosenImage,0.4);
            
            sendProfileDict = [takePetProfileData getPetProfileInfo];
            
            
           
            
            //NSLog(@"\n Set Image : %@ \n", [sendProfileDict objectForKey:PetPictureYesOrNoKey]);
            
            NetworkingClass * sendPetProfileDataNetworkObject = [[NetworkingClass alloc] init];
            
            sendPetProfileDataNetworkObject.delegate = self;
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            
           // [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
          
            
            
            if (_checkbool==YES){
//                [sendProfileDict setObject:@"Update" forKey:PetPictureYesOrNoKey];
//                              [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
                 [sendProfileDict setObject:@"Yes" forKey:PetPictureYesOrNoKey];
                [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];

            }
            
            else if (_removecheck==YES||_checkbool==NO){
                [sendProfileDict setObject:@"No" forKey:PetPictureYesOrNoKey];
                [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
                
            }

            else  {
                [sendProfileDict setObject:@"Update" forKey:PetPictureYesOrNoKey];
                [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
            }
            
            
            
//            if ([self.userSelectedImage.image isEqual:[UIImage imageNamed:@"fl_image"]]){
//                [sendProfileDict setObject:@"Yes" forKey:PetPictureYesOrNoKey];
//                [sendPetProfileDataNetworkObject savePetProfileInfo : NULL : sendProfileDict];
//            }
//            else{
//                [sendProfileDict setObject:@"Update" forKey:PetPictureYesOrNoKey];
//                [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
//            
//            }
//           
            
            
            
          
            
            

        }
          

        
           
    }
    
   
    
     if ([containerview.currentViewController isKindOfClass:SecondViewController.self]){
             SecondViewController *second = (SecondViewController*)containerview.currentViewController;
         if ([second checkHistoryInfoTextField]){
             
             
              [containerview segueIdentifierReceivedFromParent:@"third"];
             
             ThirdViewController *thirdVC = (ThirdViewController *) containerview.currentViewController;
             thirdVC.thirdpet =self.pet;
             _petID = _pet.PetID;
             if (_petID!=nil)
             {
             thirdVC.btn_insurranceyes.userInteractionEnabled=NO;
             thirdVC.btn_insuuranceno.userInteractionEnabled=NO;
             thirdVC.btn_trackerYes.userInteractionEnabled=NO;
             thirdVC.btn_trackerNo.userInteractionEnabled=NO;
             thirdVC.btn_vertYes.userInteractionEnabled=NO;
             thirdVC.vertNo.userInteractionEnabled=NO;
             thirdVC.txt_insurance.enabled=NO;
             thirdVC.txt_policyno.enabled=NO;
             thirdVC.txt_tracker.enabled=NO;
             thirdVC.txt_pin.enabled=NO;
             thirdVC.txt_name.enabled=NO;
             thirdVC.txt_phoneno.enabled=NO;
                 
                 thirdVC.lbl_insuranceyes.userInteractionEnabled=NO;
                 thirdVC.lbl_insuranceno.userInteractionEnabled=NO;
                 thirdVC.lbl_trackerYes.userInteractionEnabled=NO;
                 thirdVC.lbl_trackerNo.userInteractionEnabled=NO;
                 thirdVC.lbl_vertYes.userInteractionEnabled=NO;
                 thirdVC.lbl_VertNo.userInteractionEnabled=NO;
                 
                 
                 if ([self.pet.insurance isEqualToString:@"Yes"]){
                     
                     
                     thirdVC.insuranceviewcontraint.constant =80;
                     [thirdVC.txt_insurance setHidden:NO];
                     
                     [thirdVC.txt_policyno setHidden:NO];
                     
                     
                     [thirdVC.btn_insurranceyes setSelected:YES];
                     [thirdVC.btn_insuuranceno setSelected:NO];
                     
                     NSString *a = self.pet.insurancecomp;
                     NSLog(@"%@",a);
                     thirdVC.txt_insurance.text =self.pet.insurancecomp;
                     thirdVC.txt_policyno.text = self.pet.policynumber;
                     
                 }
                 if ([self.pet.insurance isEqualToString:@"No"]){
                     
                     thirdVC.insuranceviewcontraint.constant =0;
                     [thirdVC.txt_insurance setHidden:YES];
                     [thirdVC.txt_policyno setHidden:YES];
                     [thirdVC.btn_insuuranceno setSelected:YES];
                     [thirdVC.btn_insurranceyes  setSelected:NO];
                     
                     
                     
                 }
                 
                 if ([self.pet.trackernum isEqualToString:@"Yes"]){
                     
                     thirdVC.trackerviewheight.constant =80;
                     [thirdVC.txt_tracker setHidden:NO];
                     
                     [thirdVC.txt_pin setHidden:NO];
                     
                     
                     
                     
                     [thirdVC.btn_trackerYes setSelected:YES];
                     [thirdVC.btn_trackerNo setSelected:NO];
                     thirdVC.txt_tracker.text =self.pet.trackercomp;
                     thirdVC.txt_pin.text =self.pet.pinnumber;
                     
                 }
                 if ([self.pet.trackernum isEqualToString:@"No"]){
                     
                     
                     thirdVC.trackerviewheight.constant =0;
                     [thirdVC.txt_pin setHidden:YES];
                     [thirdVC.txt_tracker setHidden:YES];
                     
                     [thirdVC.btn_trackerNo setSelected:YES];
                     [thirdVC.btn_trackerYes setSelected:NO];
                     
                 }
                 
                 if ([self.pet.vert isEqualToString:@"Yes"]){
                     
                     thirdVC.vertnviewheight.constant =80;
                     [thirdVC.txt_name setHidden:NO];
                     
                     [thirdVC.txt_phoneno setHidden:NO];
                     
                     
                     [thirdVC.btn_vertYes setSelected:YES];
                     [thirdVC.vertNo setSelected:NO];
                     thirdVC.txt_name.text=self.pet.name;
                     thirdVC.txt_phoneno.text=self.pet.phonoenum;
                 }
                 if ([self.pet.vert isEqualToString:@"No"]){
                     
                     thirdVC.vertnviewheight.constant =0;
                     [thirdVC.txt_name setHidden:YES];
                     
                     [thirdVC.txt_phoneno setHidden:YES];
                     [thirdVC.vertNo setSelected:YES];
                     [thirdVC.btn_vertYes setSelected:NO];
                     
                     
                 }
                 

             }
             

             [self.btn_next2 setAlpha:1.0f];
             [self.btn_previous setAlpha:1.0f];
             [_btn_next2 setTitle:@"Done" forState:UIControlStateNormal];
             
             
            
         }
        
         
     
        
    }
    
}

-(void)response:(id)data {
    
    UIAlertController * petAddedMessageAlert =   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:[data objectForKey:@"msg"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ook = [UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              // NSLog(@"PetProfile Insert Work is done");
                              
//                              if([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
//                              {
//                                  [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"UpdatePET"];
//                              }
//                              
//                              [PetProfileDataManager removePetProfileInfo];
//                              
//                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Reload_PETDATA"];
                              
                              [petAddedMessageAlert dismissViewControllerAnimated:YES completion:nil];
                              
//                              UIViewController *parentController = [self getContainerController];
//                              
//                              [parentController.navigationController popViewControllerAnimated:YES];
                              
                          }];
    
    [petAddedMessageAlert addAction:ook];
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"])
        {
            
            [petAddedMessageAlert setTitle:@"Failure"];
            
            [petAddedMessageAlert setMessage:@"Invalid Credentials"];
            
        }
        else if  ([[data objectForKey:@"status"] isEqualToString:@"success"])
        {
            
            [petAddedMessageAlert setTitle:@"Success"];
            
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
            {
                [petAddedMessageAlert setMessage:[data objectForKey:@"msg"]];
            }
            else
            {
                [petAddedMessageAlert setMessage:[data objectForKey:@"msg"]];
            }
            
        }
        
    }
    
//    UIViewController *parentController = [self getContainerController];
//    
//    [parentController presentViewController:petAddedMessageAlert animated:YES completion:nil];
    
    
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
  
   /* if (_petID!=nil)
    {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Pet Profile Updated"
                                 message:@"Pet Profile has been updated"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
        [self.navigationController popViewControllerAnimated:YES];
        
        
        //  [self.navigationController pushViewController:controller animated:YES];
        
      
        
        
        
        NSLog(@"ok");
        
        
        
        
        
    }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:okAction];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
    else {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Pet Profile Created"
                                     message:@"Pet Profile has been Created"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            for (int i = 0 ; i < self.navigationController.viewControllers.count ; i++ ){
                
               
                if ([self.navigationController.viewControllers[i] isKindOfClass:DashboardViewController.class ]){
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i]  animated:YES];
                }
            
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
            
            
            
            //  [self.navigationController pushViewController:controller animated:YES];
            
            
            
            
            
            NSLog(@"ok");
            
            
            
            
            
        }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        [alert addAction:okAction];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } */
}

-(void)viewDidAppear:(BOOL)animated {
    
            
 

}
-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if ([[NSPetData objectForKey:@"image"] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/pet/"])
    {
        
        self.setPictureStatus = @"No";
        
        // nothing else in this function "populateDataToView" will be executed after return
        return;
        
    }
    
    //self.uploadPetProfilePictureButton.hidden = YES;
    
    self.userSelectedImage.hidden = NO;
    
    [self.userSelectedImage sd_setImageWithURL:[NSURL URLWithString:[NSPetData objectForKey:@"image"]]];
    
  //  self.changePictureButton.hidden = NO;
    
   // self.removePictureButton.hidden = NO;
    
}

- (IBAction)uploadpicture:(UIBarButtonItem *)sender  {
    
  //  UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIAlertController * uploadOptionsAlert =   [UIAlertController
                                                alertControllerWithTitle:@"Choose option"
                                                message:@""
                                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:@"Take Picture (camera)"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                 picker.delegate = self;
                                 picker.allowsEditing = YES;
                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                 
                                [self presentViewController:picker animated:YES completion:NULL];
                                 
                                 [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"Select Picture (library)"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                  picker.delegate = self;
                                  picker.allowsEditing = YES;
                                  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  
                                 [self presentViewController:picker animated:YES completion:NULL];
                                  
                                  [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    
    UIAlertAction* removepic = [UIAlertAction
                              actionWithTitle:@"Remove Picture"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                                  self.setPictureStatus = @"No";
                                  self.userSelectedImage.image = [UIImage imageNamed:@"fl_image"];
                                  [self.userSelectedImage setContentMode:UIViewContentModeScaleAspectFit];
                                  _removecheck=YES;
                                  _checkbool=NO;
                                  NSLog(@"remove picture");
                                  
                              }];

    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [uploadOptionsAlert addAction:removepic];
    
    [uploadOptionsAlert addAction:camera];
    
    [uploadOptionsAlert addAction:library];
    
    [uploadOptionsAlert addAction:cancel];
    
   [self presentViewController:uploadOptionsAlert animated:YES completion:nil];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"container"]){
        
        containerview = (AHContainerViewController *) segue.destinationViewController;
       
        
     //   if ([containerview.currentViewController isKindOfClass:FirstViewController.self]){
            
            
       //     FirstViewController *firstVC = (FirstViewController *) containerview.currentViewController;
        //    firstVC.firstpet = self.pet;
            
       // }
    
                       
        
    }
    

}



-(void) viewWillAppear:(BOOL)animated{
 
    for (int i=0; i <self.navigationController.viewControllers.count; i++){
        if ([self.navigationController.viewControllers[i] isKindOfClass:AddpetsViewController.class]){
            
            [_btn_edit setAlpha:0.0f];
            
            
                   }
    }

    
    
    
    
    
    if (IS_IPHONE_5 ){
        _contianerheight.constant =220;
        _scrollviewcontent.constant=500;
    }
}
-(IBAction)previousbtn:(UIButton *)sender {
    

    
   
    //NSString *restorationId = self.restorationIdentifier;
    if ([containerview.currentViewController isKindOfClass:SecondViewController.self])
    {
        
        
       [containerview segueIdentifierReceivedFromParent:@"first"];
        
      
        [self.btn_next setAlpha:1.0f];
        [self.btn_previous setAlpha:0.0f];
        [self.btn_next2 setAlpha:0.0f];
        
        
        
    }
    
    
    if ([containerview.currentViewController isKindOfClass:ThirdViewController.self])
    {
          [containerview segueIdentifierReceivedFromParent:@"second"];
      
        [_btn_next2 setTitle:@"Next" forState:UIControlStateNormal];
       
            
        
        
        
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
     chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.setPictureStatus = @"Yes";
    
    //self.uploadPetProfilePictureButton.hidden = YES;
    
    self.userSelectedImage.hidden = NO;
   
    
    _checkbool =YES;
    _removecheck=NO;
    
    
   
        self.userSelectedImage.image = chosenImage;
   [self.userSelectedImage setContentMode:UIViewContentModeScaleAspectFill];
    //firstVC.btn_neutered setAlpha:1.0f];
  
    
    
 //   self.changePictureButton.hidden = NO;
    
  //  self.removePictureButton.hidden = NO;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
      //[_btn_next setAlpha:0.0f];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void) removePictureButtonClicked:(UIButton *) btn2
{
    
    self.setPictureStatus = @"No";
    
    self.userSelectedImage.image = nil;
    
 //   self.uploadPetProfilePictureButton.hidden = NO;
    
    self.userSelectedImage.hidden = YES;
    
   // self.removePictureButton.hidden = YES;
    
   // self.changePictureButton.hidden = YES;
    
}


@end
