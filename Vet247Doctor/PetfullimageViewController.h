//
//  PetfullimageViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/31/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetfullimageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
@property (weak, nonatomic) IBOutlet UIImageView *img_fullpet;
@property (strong,nonatomic) UIImage *petimage;

@end
