//
//  profileViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/13/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JVFloatLabeledTextField.h"
#import "NetworkingClass.h"
#import "TPKeyboardAvoidingScrollView.h"
@interface profileViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIActionSheetDelegate>
    
{
    
    UIButton *pickerButton;
    UIPickerView *myPickerView;
    NSMutableArray *array_from;
    UILabel *fromButton;
    UIButton *doneButton ;
    UIButton *backButton ;
    UIPickerView *dayPicker;
    UIPickerView *province;
}
@property(nonatomic,retain) IBOutlet UIButton *pickerButton;
@property(nonatomic,retain) IBOutlet UILabel *fromButton;

-(IBAction)PickerView:(id)sender;



@property (weak, nonatomic) IBOutlet UIStackView *stack_female;

@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (assign) BOOL imageExist;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_Country;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_Province;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_firstname;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_lastname;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_phonenum;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_streetadd;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_postcode;
@property (weak, nonatomic) IBOutlet UIButton *btn_donee;
@property (strong,nonatomic) NSString *gender,*setImage;
@property (strong,nonatomic) NSMutableDictionary *dataDict;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_dob;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;
@property (weak,nonatomic) NSString *imageurl;
@property (weak, nonatomic) IBOutlet UIStackView *stack_male;


@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_city;
@property (strong,nonatomic) NetworkingClass *getProfile;
@property (strong,nonatomic) NSString *callFromObject;
@property (weak, nonatomic) IBOutlet UILabel *lbl_male;
@property (weak, nonatomic) IBOutlet UILabel *lbl_female;
@property (strong,nonatomic) UIImage *chosenimage;
@property (strong,nonatomic) UIImage *profilebg;
@property (weak, nonatomic) IBOutlet UIButton *btn_province;

@property (strong,nonatomic) UIPickerView *provincePickerView,*countryPickerView;
@property (strong,nonatomic) NSMutableArray *provinceNames, *countryNames, *statesNames, *showProvinceOrStatesData;
@property (weak, nonatomic) IBOutlet UIButton *btn_male;
@property (weak, nonatomic) IBOutlet UIButton *btn_female;
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
@property (weak, nonatomic) IBOutlet UIButton *btn_country;
@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong,nonatomic) UITapGestureRecognizer *gestureRecognizer;



@end
