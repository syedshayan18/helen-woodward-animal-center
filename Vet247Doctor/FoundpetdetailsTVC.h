//
//  FoundpetdetailsTVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
@interface FoundpetdetailsTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_details;

@end
