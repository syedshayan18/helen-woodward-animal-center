//
//  PetProfilePictureSectionView.h
//  Vet247Doctor
//
//  Created by APPLE on 8/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetProfilePictureSectionView : UIView <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIImageView *petProfilePictureBGImage;

@property (strong, nonatomic) UIButton * uploadPetProfilePictureButton;

@property (strong, nonatomic) UIButton * savePetProfileButton;

@property (weak, nonatomic) UIViewController * presentViewController;

@property (strong, nonatomic) UIImageView * userSelectedImage;

@property (strong, nonatomic) UIButton * changePictureButton;

@property (strong, nonatomic) UIButton * removePictureButton;

-(void) addComponents;

@property (strong, nonatomic) UIImageView * choosenImage;

@property (strong, nonatomic) NSString * setPictureStatus;

@end
