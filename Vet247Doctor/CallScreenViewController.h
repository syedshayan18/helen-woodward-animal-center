//
//  CallScreenViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_endcall;
@property (weak, nonatomic) IBOutlet UIButton *btn_decline;
@property (weak, nonatomic) IBOutlet UIImageView *img_callavatar;
@property (weak, nonatomic) IBOutlet UILabel *lbl_calling;
@property (strong,nonatomic) NSMutableArray *petid;
@property (weak, nonatomic) IBOutlet UILabel *lbl_connecting;
@end
