//
//  ViewController.m
//  Vet247Doctor
//
//  Created by Malik on 7/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "LoginController.h"

#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "DashboardController.h"

#import "SignUpController.h"

#import "NetworkingClass.h"

#import "ProfileController.h"

#import "MBProgressHUD.h"

#import "signViewController.h"

@interface LoginController ()

@property  CGPoint centerPoint;

@property(nonatomic,retain) UILabel *userLbl;

@property(nonatomic,retain) UILabel *loginLbl;

@property(nonatomic,retain) UIImageView *backGroundImage;

@property(nonatomic,retain) UIImageView *logo;

@property(nonatomic,retain) UIImageView *fieldsBg;

@property(nonatomic,retain) UIScrollView * scrollView;

@property(nonatomic,retain) UIView *viewOfField1;

@property(nonatomic,retain) UIView *viewOfField2;

@property(nonatomic,retain) UIButton *loginBtn;

@property(nonatomic,retain) UIButton *registerBtn;

@property(nonatomic,retain) UIButton *forgetBtn;

@property(nonatomic,retain) UIImageView *userNameImage;

@property(nonatomic,retain) UIImageView *passwrdImage;

@property(nonatomic,retain) UITextField *paswrdTxtFld;

@property(nonatomic,retain) UITextField *userNameTxtFld;

@property (strong,nonatomic) UITapGestureRecognizer *logInGestureRecognizer;


@end
@implementation LoginController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.centerPoint=self.view.center;

    self.logInGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLoginGesture:)];
    
    self.logInGestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.logInGestureRecognizer];

    // Do any additional setup after loading the view, typically from a nib.
    
    [self addingComponents];
}

-(void)viewWillAppear:(BOOL)animated {
    
    
    [super viewWillAppear:animated];
    
    self.userNameTxtFld.text=@"";
    
    self.paswrdTxtFld.text=@"";
    
    [self.view endEditing:YES];
    
    
}
-(void)addingComponents
{
    //setting bg image of app
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
    //creating scroll view for Iphone 4
    UIEdgeInsets scrollViewPad = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.scrollView=[ControlsFactory getScrollView];

        
    self.scrollView.backgroundColor=[UIColor clearColor];
 
    [self.view addSubview:self.scrollView];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(scrollViewPad.top); //with is an optional semantic filler
        
        make.left.equalTo(self.view.mas_left).with.offset(scrollViewPad.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-scrollViewPad.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-scrollViewPad.right);
    }];
    
    
    //adding logo
    UIEdgeInsets padding2 = UIEdgeInsetsMake(30, 0, 0, 0);
    
    self.logo=[ControlsFactory getImageView];
    
    [  self.logo setImage:[UIImage imageNamed:@"logo.png"]];//setting bg image of app

     [self.scrollView addSubview:  self.logo];

    [  self.logo mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.top.equalTo(self.scrollView.mas_top).with.offset(padding2.top);

        make.centerX.mas_equalTo( self.view.mas_centerX);
        
        if (isiPhone6 || isiPhone6Plus) {
            
            make.height.mas_equalTo(197);
            
            make.width.mas_equalTo(176);
        }
        else
        {
            make.height.mas_equalTo(107);
            
            make.width.mas_equalTo(98);
        }

        
    }];
    
    //creating Bg Layer For Textfields
    
    UIEdgeInsets padding3 = UIEdgeInsetsMake(20, 10, 0, 10);
    
    self.fieldsBg=[ControlsFactory getImageView];
    
    [self.fieldsBg setImage:[UIImage imageNamed:@"fieldsBg.png"]];//setting bg image of app
    
    [self.scrollView addSubview:self.fieldsBg];

    [self.fieldsBg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.logo.mas_bottom).with.offset(padding3.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding3.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding3.right);
        
        make.height.mas_equalTo(250);

    }];

    //creating a uilabel
    UIEdgeInsets paddingForLabel = UIEdgeInsetsMake(10, 0, 0, 0);
    
    self.loginLbl = [ControlsFactory getLabel];
    
    [self.loginLbl setText:@"Login"];//setting login label
    
    [self.loginLbl setTextAlignment:NSTextAlignmentCenter];
    
    [self.loginLbl setTextColor:[UIColor whiteColor]];
    
    [self.scrollView addSubview:self.loginLbl];

    [self.loginLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
    make.top.equalTo(self.fieldsBg.mas_top).with.offset(paddingForLabel.top); //with is an optional semantic filler
        
    make.centerX.mas_equalTo( self.view.mas_centerX);
        
        // make.center.equalTo(self.view);
        
    make.width.mas_equalTo(200);
        
    make.height.mas_equalTo(30);
        
    }];

    
    //adding view now for field1 component
   
    UIEdgeInsets viewPadding = UIEdgeInsetsMake(10, 10, 0, 10);
    
    self.viewOfField1=[ControlsFactory getView];
    
    self.viewOfField1.backgroundColor=[UIColor whiteColor];
 
    [self.scrollView addSubview:self.viewOfField1];
    
    [self.viewOfField1 mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(self.fieldsBg.mas_left).with.offset(viewPadding.left);
        
        make.right.equalTo(self.fieldsBg.mas_right).with.offset(-viewPadding.right);
        
        make.top.equalTo(self.loginLbl.mas_bottom).with.offset(viewPadding.top);
        
        make.height.mas_equalTo(45);
        
        
    }];
    
    UIEdgeInsets viewPadding2 = UIEdgeInsetsMake(10, 10, 0, 10);
    
    self.viewOfField2=[ControlsFactory getView];
    
     self.viewOfField2.backgroundColor=[UIColor whiteColor];

    [self.scrollView addSubview: self.viewOfField2];

    
    [ self.viewOfField2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.fieldsBg.mas_left).with.offset(viewPadding2.left);
        
        make.right.equalTo(self.fieldsBg.mas_right).with.offset(-viewPadding2.right);
        
        make.top.equalTo(self.viewOfField1.mas_bottom).with.offset(viewPadding2.top);
        
        make.height.mas_equalTo(45);
        
    }];
    //creating login Button
    UIEdgeInsets loginBtnPadding = UIEdgeInsetsMake(15, 10, 0, 10);
    
    self.loginBtn=[ControlsFactory getButton];
    
    [self.loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    
    self.loginBtn.backgroundColor=[UIColor colorWithRed:68.0/255. green:144.0/255 blue:78.0/255 alpha:1.0];

    [self.loginBtn addTarget:self action:@selector(loginBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.loginBtn];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.fieldsBg.mas_left).with.offset(loginBtnPadding.left);
        
        make.right.equalTo(self.fieldsBg.mas_right).with.offset(-loginBtnPadding.right);
        
        make.top.equalTo( self.viewOfField2.mas_bottom).with.offset(loginBtnPadding.top);
        
        make.height.mas_equalTo(45);
        
        
    }];
    
    //creating Forget Paswrd Btn
    UIEdgeInsets forgetBtnPad = UIEdgeInsetsMake(15, 0, 0, 0);
    
    self.forgetBtn=[ControlsFactory getButton];
    
    [ self.forgetBtn setImage:[UIImage imageNamed:@"forgetPswrd.png"] forState:UIControlStateNormal];

        [self.scrollView addSubview: self.forgetBtn];

    [ self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        

        make.top.equalTo(self.loginBtn.mas_bottom).with.offset(forgetBtnPad.top);
        
        make.height.mas_equalTo(14);
        
        make.width.mas_equalTo(121);
        
        make.centerX.mas_equalTo( self.view.mas_centerX);
        
    }];
    
    
    //creating a uilabel for New User
    UIEdgeInsets newUserPad = UIEdgeInsetsMake(20, 0, 0, 0);
    
    self.userLbl=[ControlsFactory getLabel];
    
    [self.userLbl setText:@"New User? "];//setting login label
    
    [self.userLbl setTextAlignment:NSTextAlignmentCenter];
    
    [self.userLbl setTextColor:[UIColor whiteColor]];
    
    [self.scrollView addSubview:self.userLbl];

    [self.userLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.fieldsBg.mas_bottom).with.offset(newUserPad.top); //with is an optional semantic filler
        
        make.centerX.mas_equalTo( self.view.mas_centerX);
      
        make.width.mas_equalTo(200);
        
        make.height.mas_equalTo(30);
        
    }];
    
    //creating Register Button
    UIEdgeInsets registerBtnPad = UIEdgeInsetsMake(15, 10, 5, 10);
    
    self.registerBtn=[ControlsFactory getButton];
    
    [ self.registerBtn setTitle:@"Register" forState:UIControlStateNormal];
    
     self.registerBtn.backgroundColor=[UIColor blackColor];
    
    [self.registerBtn addTarget:self action:@selector(registerBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview: self.registerBtn];
    
    [ self.registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.fieldsBg.mas_left).with.offset(registerBtnPad.left);
        
        make.right.equalTo(self.fieldsBg.mas_right).with.offset(-registerBtnPad.right);
        
        make.top.equalTo(self.userLbl.mas_bottom).with.offset(registerBtnPad.top);
        
        make.bottom.equalTo(self.scrollView.mas_bottom).with.offset(-registerBtnPad.bottom);
        
        make.height.mas_equalTo(45);
        
        
    }];
    
    //image for username field
    UIEdgeInsets userNamePad = UIEdgeInsetsMake(0,15, 0, 0);
    
    self.userNameImage=[ControlsFactory getImageView];
    
    [self.userNameImage setImage:[UIImage imageNamed:@"emailImage.png"]];//setting bg image of app

       [self.scrollView addSubview:self.userNameImage];

    [self.userNameImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.viewOfField1.mas_left).with.offset(userNamePad.left);
        
        make.centerY.mas_equalTo( self.viewOfField1.mas_centerY);
        
        make.height.mas_equalTo(17);
        
        make.width.mas_equalTo(22);
    }];

    //image for Password field
    UIEdgeInsets paswrdPad = UIEdgeInsetsMake(0,15, 0, 0);
    
    self.passwrdImage=[ControlsFactory getImageView];
    
    [self.passwrdImage setImage:[UIImage imageNamed:@"paswrdImage.png"]];//setting bg image of app
    
    
    [self.scrollView addSubview:self.passwrdImage];

    [self.passwrdImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.viewOfField1.mas_left).with.offset(paswrdPad.left);
        
        make.centerY.mas_equalTo(  self.viewOfField2.mas_centerY);
        
        make.height.mas_equalTo(26);
        
        make.width.mas_equalTo(20);
    }];
    
    //Text field for user name
    UIEdgeInsets userNametextFldPad = UIEdgeInsetsMake(0,40, 0, 0);
    
    self.userNameTxtFld=[ControlsFactory getTextField];
    
    self.userNameTxtFld.placeholder=@"Email Address";
    
    [self.userNameTxtFld setValue:[UIColor colorWithRed:68.0/255. green:144.0/255 blue:78.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];

    
    //self.userNameTxtFld.textColor=;
    
    self.userNameTxtFld.delegate=self;
    
    //setting bg image of app
    
     [self.scrollView addSubview:self.userNameTxtFld];

    [self.userNameTxtFld mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.userNameImage.mas_left).with.offset(userNametextFldPad.left); //with is an optional semantic filler
        
        // make.center.equalTo(self.view);
        
        make.centerY.mas_equalTo( self.viewOfField1.mas_centerY);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(230);
    }];
    
    //text field for password
    UIEdgeInsets paswrdtextFldPad = UIEdgeInsetsMake(0,40, 0, 0);
    
    self.paswrdTxtFld=[ControlsFactory getTextField];
    
    [self.paswrdTxtFld setSecureTextEntry:YES];
    
    self.paswrdTxtFld.placeholder=@"Password";
    
     [self.paswrdTxtFld setValue:[UIColor colorWithRed:68.0/255. green:144.0/255 blue:78.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    // self.paswrdTxtFld.textColor=[UIColor colorWithRed:68.0/255. green:144.0/255 blue:78.0/255 alpha:1.0];
    
    self.paswrdTxtFld.delegate=self;
 
    [self.scrollView addSubview: self.paswrdTxtFld];

    
    [ self.paswrdTxtFld mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.passwrdImage.mas_left).with.offset(paswrdtextFldPad.left); //with is an optional semantic filler
        
        make.centerY.mas_equalTo( self.viewOfField2.mas_centerY);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(230);
    }];

}

- (void) loginBtnClicked: (UIButton *) btn{

    [self.view endEditing:YES];
    
    if ( self.userNameTxtFld.text && self.userNameTxtFld.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Username Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        [errorAlert show];
        
    }
    
    else if (self.paswrdTxtFld.text && self.paswrdTxtFld.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        
    }
    else
    {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
        
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if ([emailTest evaluateWithObject:self.userNameTxtFld.text] == NO) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Valid Email Address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        
        else {
            
            NetworkingClass *network = [[NetworkingClass alloc] init];
            
            network.delegate = self;
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            
            [network signIn:self.userNameTxtFld.text Password:self.paswrdTxtFld.text];
            
        }
       
    }
    
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([data isKindOfClass:[NSArray class]])
    {
        
       // NSLog(@"OK");
        
    }
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if([[data objectForKey:@"status"] isEqualToString:@"failure"] && [[data objectForKey:@"msg"] isEqualToString:@"block user"])
        {
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"We're sorry to inform you that,\nThis user is been blocked by the Admin for some reasone." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
            return;
            
        }
        
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"]) {
            
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid Credentials" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
    
        }
        
    }

    else {
        
        [[NSUserDefaults standardUserDefaults ] setObject:[[data objectAtIndex:0] objectForKey:@"id"] forKey:@"userId"];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            ProfileController *profile=[[ProfileController alloc] init];
            
             [self.navigationController pushViewController:profile animated:YES];
            
        
        }
        else
        {
            
            DashboardController *dashboard=[[DashboardController alloc] init];
        
            [self.navigationController pushViewController:dashboard animated:YES];
        }
    }

}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    
}

- (void) registerBtnClicked: (UIButton *) btn{
    
//    [self.view endEditing:YES];
    
    
    
//    signViewController *signUp=[[signViewController alloc] init];
//    
//    [self.navigationController pushViewController:signUp animated:YES];
//
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    self.view.center=CGPointMake(self.centerPoint.x, self.centerPoint.y);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (isiPhone4) {
        
        self.view.center=CGPointMake(self.centerPoint.x, 100);
    
    }
    
    else if (isiPhone5) {
        
        self.view.center=CGPointMake(self.centerPoint.x, 220);
        
    }
}

- (void) tapLoginGesture:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
