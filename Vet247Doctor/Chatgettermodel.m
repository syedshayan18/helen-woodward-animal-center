//
//  Chatgettermodel.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/6/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "Chatgettermodel.h"

@implementation Chatgettermodel
-(id)initWithDictionary:(NSDictionary *)dictionary{
    
    self = [super init];
    
    if (self) {
        
        
        self.chatmessages = dictionary[@"message"];
        self.userimage = dictionary [@"imagePath"];
        self.isread = dictionary[@"is_read"];
        self.msgid = dictionary[@"id"];
        self.sendername = dictionary[@"firstname"];
        self.msgsender = dictionary[@"sender_id"];
        self.msgtype = dictionary[@"type"];
              
    }
    _chatdict = [[NSDictionary alloc]init];
    
    
    

    return self;
}

-(id)initWithDict:(NSDictionary *)dictionary{
    
    if (self) {
        
         self = [super init];
        
        
        _lastmsgDict = [[NSDictionary alloc]init];
        _ReceiverDict =[[NSDictionary alloc]init];
        _SenderDict = [[NSDictionary alloc]init];
        
        
        if (self) {
            
            _lastmsgDict = dictionary[@"last_message"];
            _ReceiverDict = dictionary[@"receiver"];
            _SenderDict = dictionary[@"sender"];
            
            self.lastmsg = _lastmsgDict[@"message"];
            self.datecreated = _lastmsgDict[@"created_at"];
            self.isreadlastmsg = _lastmsgDict[@"is_read"];
            self.Report_ID = _lastmsgDict[@"report_id"];
            self.Sender_ID = _lastmsgDict[@"sender_id"];
            self.Receiver_ID = _lastmsgDict[@"receiver_id"];
            self.userimage = _ReceiverDict [@"imagePath"];
            
            self.senderimagepath = _SenderDict[@"imagePath"];
            self.Senderfirstname = _SenderDict[@"firstname"];
            self.Senderlastname = _SenderDict[@"lastname"];
            self.msgtype = _lastmsgDict[@"type"];
            
            
            
            
        }

        
    }
    
return self;
}


@end





