//
//  MyPetsViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyTrackerModel.h"


@protocol SecondViewControllerDelegate;






@interface MyPetsViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollviewcontent;

@property (weak, nonatomic) IBOutlet UIView *containerview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contianerheight;

@property (weak, nonatomic) IBOutlet UIButton *btn_edit;



@property BOOL check;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_menu;
@property (weak, nonatomic) IBOutlet UIButton *btn_previous;
@property (weak, nonatomic) IBOutlet UIButton *btn_next2;
@property (weak, nonatomic) IBOutlet UIImageView *userSelectedImage;
@property (strong, nonatomic) NSString * setPictureStatus;
@property PropertyTrackerModel *pet;
@property (strong,nonatomic) NSString *petID;

@property (strong,nonatomic) UITapGestureRecognizer *gestureRecognizer;
@property (strong,nonatomic) NSString *petIDfirfirst;
@property (weak, nonatomic) IBOutlet UIButton *btntest;



@end
