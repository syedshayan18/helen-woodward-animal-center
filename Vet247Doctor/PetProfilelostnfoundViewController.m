//
//  PetProfilelostnfoundViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.

#import "PetProfilelostnfoundViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "PetLocationViewController.h"
#import "PetfullimageViewController.h"
#import "ChatlostnfoundViewController.h"
#import "ChatinboxViewController.h"
#import "SDWebImageManager.h"
#import "NetworkingClass.h"
@interface PetProfilelostnfoundViewController ()

@end

@implementation PetProfilelostnfoundViewController
@synthesize petprofiledict,petmodel,userprofileimagetobepassed,senderprofileimagetobepassed;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self getreporterprofile];
    
    petprofiledict = [[NSMutableArray alloc]init];
    NSString *userid= [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSString *imagepath = [[NSUserDefaults standardUserDefaults]objectForKey:@"userimage"];
    NSURL *imageurl = [NSURL URLWithString:imagepath];
    NSLog(@"%@",imageurl);
                       
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    
    
    [manager downloadImageWithURL:imageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
     
     {
         
         
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
         
         if(image){
             userprofileimagetobepassed = image;
         }
     }];

    
  
    
    
    if ([petmodel.userID isEqualToString:userid]){
        [_btn_chat setAlpha:0.0];
        
       
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    
    UITapGestureRecognizer *taponimage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(taponimage:)];
    [self.img_petprofile addGestureRecognizer:taponimage];
    
  NSString *animaltag=petmodel.animaltag;
    if ([animaltag isEqualToString:@"LOST"]){
        
        _img_location.image = [UIImage imageNamed:@"lost_button"];
        _textview_description.text =petmodel.petdescription;
        _lbl_location.text = petmodel.locationname;
        _lbl_age.text = petmodel.petage;
        
        if ([petmodel.petlostSpecies isEqualToString:@"0"] ){
            
            _lbl_species.text = @"Cat";
        } else {
            
            _lbl_species.text= @"Dog";
        }
        
        
        _lbl_breed.text=petmodel.petlostbreed;
        _lbl_weight.text=petmodel.petlostWeight;
        _lbl_sex.text=petmodel.petlostSex;
        NSURL  *url = [[NSURL alloc]initWithString:petmodel.petimagelost];
       [ _img_petprofile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"profile_bg"]];
        
        if ([_textview_description.text isEqualToString:@""]){
            _textview_description.text= @"N/A";
        }
        if ([_lbl_species.text isEqualToString:@""]){
            _lbl_species.text= @"N/A";
        }
        if ([_lbl_breed.text isEqualToString:@""]){
            _lbl_breed.text= @"N/A";
        }
        if ([_lbl_age.text isEqualToString:@""]){
            _lbl_age.text= @"N/A";
        }
        if ([_lbl_weight.text isEqualToString:@""]){
            _lbl_weight.text= @"N/A";
        }
        if ([_lbl_sex.text isEqualToString:@""]){
            _lbl_sex.text= @"N/A";
        }
        

        
    }
    else {
        _textview_description.text =petmodel.petdescription;
        _lbl_location.text = petmodel.locationname;
        _lbl_species.text=petmodel.petfoundSpecies;
        _lbl_breed.text=petmodel.petfoundbreed;
        _lbl_weight.text=petmodel.petfoundweight;
        _lbl_sex.text=petmodel.petfoundsex;
        NSURL  *url = [[NSURL alloc]initWithString:petmodel.petimagefound];
        [ _img_petprofile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"profile_bg"]];
        
        _lbl_petaction.text = petmodel.petaction;
        
        if ([_textview_description.text isEqualToString:@""]){
            _textview_description.text= @"N/A";
        }
        if ([_lbl_species.text isEqualToString:@""]){
             _lbl_species.text= @"N/A";
        }
        if ([_lbl_breed.text isEqualToString:@""]){
            _lbl_breed.text= @"N/A";
        }
        if ([_lbl_age.text isEqualToString:@""]){
            _lbl_age.text= @"N/A";
        }
        if ([_lbl_weight.text isEqualToString:@""]){
            _lbl_weight.text= @"N/A";
        }
        if ([_lbl_sex.text isEqualToString:@""]){
            _lbl_sex.text= @"N/A";
        }
        
        
        
    }
    
    UITapGestureRecognizer *location = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(location:)];
    [self.img_location addGestureRecognizer:location];
    _btn_chat.layer.cornerRadius=15;
    _btn_chat.clipsToBounds=YES;
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void) getreporterprofile {
    
    NSString *getprofile = [NSString stringWithFormat:@"%@%@?id=%@",BaseURL,@"get_profile.php",petmodel.userID];
    [NetworkingClass getprofileinfo:getprofile completion:^(id finished, NSError *error) {
        
        if (error==nil){
      
            NSDictionary *dict = [[NSDictionary alloc]init];
            
            dict = [finished objectForKey:@"data"];
            _passingSenderName = [dict objectForKey:@"firstName"];
            NSString *profileimagepath = [dict objectForKey:@"picturePath"];
            NSURL *profileimageurl = [NSURL URLWithString:profileimagepath];
            
            
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            
            
            
            [manager downloadImageWithURL:profileimageurl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
             
             {
                 
                 
             } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                 
                 if(image){
                     senderprofileimagetobepassed = image;
                 }
             }];

            
            
            
            
        }
        
    }];
    
    
}




-(void)taponimage: (UIGestureRecognizer *)sender {
    
    [self performSegueWithIdentifier:@"petimage" sender:nil];
    
}

-(IBAction)btn_chat:(UIButton *)sender{
    
    [self performSegueWithIdentifier:@"chatscreen" sender:nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)location:(UIGestureRecognizer *)sender{
    [self performSegueWithIdentifier:@"petlocation" sender:nil];
}

-(IBAction)btn_barbutton:( UIBarButtonItem*)sender {
    
    [self performSegueWithIdentifier:@"inboxscreen" sender:nil];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"petlocation"]){
        PetLocationViewController *petlocation = (PetLocationViewController *)[segue destinationViewController];
        petlocation.lng = petmodel.longitude;
        petlocation.lat = petmodel.latitude;
        petlocation.pettag = petmodel.animaltag;
    }
  
    if ([segue.identifier isEqual:@"petimage"])
    {
        
        PetfullimageViewController *petimage = (PetfullimageViewController *)[segue destinationViewController];
        
        petimage.petimage = _img_petprofile.image;
    
    }
    if ([segue.identifier isEqual:@"chatscreen"]){
        
        ChatlostnfoundViewController *chatinitiate = (ChatlostnfoundViewController *)[segue destinationViewController];
       // chatinitiate.reciever_id =
        chatinitiate.ReID = petmodel.userID;
        chatinitiate.SenderID = petmodel.userID;
        chatinitiate.report_id = petmodel.reportID;
        chatinitiate.avatarimagered = userprofileimagetobepassed;
        chatinitiate.avatarimagegreen = senderprofileimagetobepassed;
        chatinitiate.SenderNamePassed = _passingSenderName;
        
        
    }
    if ([segue.identifier isEqual:@"inboxscreen"]){
        
        ChatinboxViewController *chatinbox = (ChatinboxViewController *)[segue destinationViewController];
        chatinbox.report_ID = petmodel.reportID;
        chatinbox.receiver_ID = petmodel.userID;
        
    
        
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
