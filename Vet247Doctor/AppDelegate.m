//
//  AppDelegate.m
//  Vet247Doctor
//
//  Created by Malik on 7/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "AppDelegate.h"

#import "MessageController.h"

#import "signViewController.h"

#import "loginscreenViewController.h"

#import "DashboardViewController.h"

#import "profileViewController.h"

#import "SocketManager.h"

#import "PromotionalCodeUtills.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <OneSignal/OneSignal.h>
#import "Pushermanager.h"

@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate

static SocketManager *_manager;

NSString *pushToken;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
 
    [Fabric with:@[[Crashlytics class]]];
    
    
   id  notificationReceiverBlock = ^(OSNotification *notification) {
     
        NSLog(@"Received Notification - %@ - %@", notification.payload.notificationID, notification.payload.title);
    };
    
    
    
   id notficationOpenedBlock = ^(OSNotificationOpenedResult *result) {
        
        // This block gets called when the user opens or taps an action on a notification
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"New Message";
         _fullMessage = [payload.body copy];
        
        if (payload.additionalData) {
            if (payload.title)
                messageTitle = payload.title;
            
            NSDictionary* additionalData = payload.additionalData;
            
          
            
            
            if (additionalData[@"actionSelected"])
                _fullMessage = [_fullMessage stringByAppendingString:[NSString stringWithFormat:@"\nPressed ButtonId:%@", additionalData[@"actionSelected"]]];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"chatscreen" object:self userInfo:additionalData];
            
            
            
            
        }
        
//        UIAlertView* alertView = [[UIAlertView alloc]
//                                  initWithTitle:messageTitle
//                                  message:_fullMessage
//                                  delegate:self
//                                  cancelButtonTitle:@"Close"
//                                  otherButtonTitles:nil, nil];
       
    };
    
    id onesignalInitSettings =@{kOSSettingsKeyAutoPrompt:@true,kOSSettingsKeyInAppLaunchURL:@true ,kOSSettingsKeyInAppAlerts:@false};
    
    //one signal push notification
    [OneSignal initWithLaunchOptions:launchOptions appId:@"33445f38-0809-457d-b407-908f034f170f" handleNotificationReceived:notificationReceiverBlock handleNotificationAction:notficationOpenedBlock settings:onesignalInitSettings];
    
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    OSPermissionSubscriptionState *status = [OneSignal getPermissionSubscriptionState];
    BOOL hasPromoted=status.permissionStatus.hasPrompted;
    NSLog(@"%@",hasPromoted ? @"YES" : @"NO");
    BOOL userStatus = status.permissionStatus.status;
    NSLog(@"%@",userStatus ? @"YES" : @"NO");
    BOOL  isSubscribed = status.subscriptionStatus.userSubscriptionSetting;
    NSLog(@"%@",isSubscribed ?  @"YES" : @"NO");
    NSString * userID = status.subscriptionStatus.userId;
    
    if (userID){
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"pushtoken"];


        }
    
    

    NSString *a =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSLog(@"%@",a);
   
    self.isSelectedAnyPaymentPackage = NO;
    
    self.selectedPaymentPackageID = -1;
    //AIzaSyCgdljgsWF6H4ErAjz6AnCxwlQhMQGSbnc
    [GMSServices provideAPIKey:@"AIzaSyC3R6Dy1Gv17c80JGX8BerMSiM41MUAC2k"];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    internetReach = [Reachability reachabilityForInternetConnection];
    
    [internetReach startNotifier];
    
    [self updateInterfaceWithReachability: internetReach];
    
    wifiReach = [Reachability reachabilityForLocalWiFi];
    
    [wifiReach startNotifier];
    
    [self updateInterfaceWithReachability: wifiReach];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]!=NULL) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        DashboardViewController *myVC = (DashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
//    }
    
   //   if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]) {
            
        //  profileViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profile"]; //or the homeController
        //  UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
       //    self.window.rootViewController = navController;
            
         
            

          
            
     //   }
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"NO"]) {
 //                      DashboardViewController *dashboardcontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"dashboard"]; //or the homeController
  //         UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:dashboardcontroller];
  //  self.window.rootViewController = navController;
   //     }
   //     else {
            
            
    
     //      loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"]; //or the homeController
     //        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:logincontroller];
     //     self.window.rootViewController = navController;
            
          
 //       }
  //  }
 //   else {
  
  //     loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"]; //or the homeController
   //    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:logincontroller];        self.window.rootViewController = navController;
        
  //  }
 
    // Override point for customization after application launch.
 
    
    
    
    // Override point for customization after application launch.
    return YES;
    
}






- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSLog(@"device token is: %@",deviceToken);
    
    pushToken = [[[[deviceToken description]
                   stringByReplacingOccurrencesOfString: @"<" withString: @""]
                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                 stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    [[NSUserDefaults standardUserDefaults] setObject:pushToken forKey:@"deviceToken"];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error is %@",error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    

    
    NSMutableDictionary *dataDict = [[userInfo objectForKey:@"aps"] objectForKey:@"data"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"messageFromDoctor" object:nil userInfo:dataDict];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"messageFromDoctorOnMessagesScreen" object:nil userInfo:dataDict];
}

-(BOOL)availableInternet
{
    [self updateInterfaceWithReachability: wifiReach];
    
    [self updateInterfaceWithReachability: internetReach];
    
    if (iswifiReach == NO && isinternetReach == NO)
    {
        
            UIAlertController * checkinternetAvailabilityErrorAlert =   [UIAlertController
                                                                    alertControllerWithTitle:@"Internet Disconnected"
                                                                    message:@"Try to Connect to a Network"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ook = [UIAlertAction
                                  actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [checkinternetAvailabilityErrorAlert dismissViewControllerAnimated:YES completion:nil];
        
                                  }];
        //    UIAlertAction* caancel = [UIAlertAction
        //                              actionWithTitle:@"Cancel"
        //                              style:UIAlertActionStyleDefault
        //                              handler:^(UIAlertAction * action)
        //                              {
        //                                  [checkPictureSelectedErrorAlert dismissViewControllerAnimated:YES completion:nil];
        //
        //                              }];
        //
            [checkinternetAvailabilityErrorAlert addAction:ook];
        //    [checkPictureSelectedErrorAlert addAction:caancel];
        
        [self.window.rootViewController presentViewController:checkinternetAvailabilityErrorAlert animated:TRUE completion:nil];
        //[errorShowViewController presentViewController:checkinternetAvailabilityErrorAlert animated:YES completion:nil];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Disconnect" message:@"Try to Connect to a Network" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        
//        [alert show];
//        
        return NO;
    }
    else
    {
        return YES;
    }
    //    SCNetworkReachabilityFlags flags;
    //    SCNetworkReachabilityRef address;
    //    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    //    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    //    CFRelease(address);
    //
    //    bool canReach = success
    //    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    //    && (flags & kSCNetworkReachabilityFlagsReachable);
    //
    //    return canReach;
}

-(BOOL)availableInternetWithoutMessage
{
    [self updateInterfaceWithReachability: wifiReach];
    
    [self updateInterfaceWithReachability: internetReach];
    
    if (iswifiReach == NO && isinternetReach == NO)
    {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    [self updateInterfaceWithReachability: curReach];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    if(curReach == internetReach)
    {
        //[self configureTextField: internetConnectionStatusField imageView: internetConnectionIcon reachability: curReach];
        switch (netStatus)
        {
            case NotReachable:
            {
                isinternetReach= NO;
                
                break;
            }
                
            case ReachableViaWWAN:
            {
                isinternetReach= YES;
                
                break;
            }
            case ReachableViaWiFi:
            {
                isinternetReach= YES;
                
                break;
            }
        }
    }
    if(curReach == wifiReach)
    {
        
        switch (netStatus)
        {
            case NotReachable:
            {
                iswifiReach= NO;
                
                break;
            }
                
            case ReachableViaWWAN:
            {
                iswifiReach= YES;
                
                break;
            }
            case ReachableViaWiFi:
            {
                iswifiReach= YES;
                break;
            }
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
   
    
    _manager=[SocketManager sharedInstance];

    if (![_manager getCallStatus]) {
        NSLog(@"nothing to be done");
    }
    else{
        
        NSLog(@"call Active");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:AppWentToBackGround object:nil];
        
    }

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[Pushermanager instance] connect];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[Pushermanager instance] disconnect];
}

@end
