//
//  SMMessageViewTableCell.m
//  JabberClient
//
//  Created by cesarerocchi on 9/8/11.
//  Copyright 2011 studiomagnolia.com. All rights reserved.
//

#import "ChatMessageCell.h"


@implementation ChatMessageCell

@synthesize senderAndTimeLabel, messageContentView, bgImageView,avatarImage,messageImage;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

		senderAndTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 300, 20)];
        
		senderAndTimeLabel.textAlignment = NSTextAlignmentCenter;
		
        senderAndTimeLabel.font = [UIFont systemFontOfSize:11.0];
		
        senderAndTimeLabel.textColor = [UIColor lightGrayColor];
		
        [self.contentView addSubview:senderAndTimeLabel];
		
        bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
		
        [self.contentView addSubview:bgImageView];
		
		messageContentView = [[UITextView alloc] init];
		
        messageContentView.backgroundColor = [UIColor clearColor];
		
        messageContentView.editable = NO;
		
        messageContentView.scrollEnabled = NO;
		
        [messageContentView sizeToFit];
		
        [self.contentView addSubview:messageContentView];
        
        avatarImage=[[UIImageView alloc] initWithFrame:CGRectZero];
        
        [self.contentView addSubview:avatarImage];
        
        messageImage=[[UIImageView alloc] initWithFrame:CGRectZero];
        
        [self.contentView addSubview:messageImage];
    
    }
	
    return self;
	
}








@end
