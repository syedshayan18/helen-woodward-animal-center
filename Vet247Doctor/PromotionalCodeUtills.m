//
//  PromotionalCodeUtills.m
//  Vet247Doctor
//
//  Created by APPLE on 9/29/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PromotionalCodeUtills.h"



@implementation PromotionalCodeUtills

//@synthesize promotionalCode;

static PromotionalCodeUtills * sharedInstance = nil;

//-(void) generatePromtionalCodeSharedInstance
//{
//    static dispatch_once_t onceToken;
//
//    dispatch_once(&onceToken, ^{
//
//        sharedInstance = [[PromotionalCodeUtills alloc] init];
//    });
//}

+(void)setPromotionalCode:(NSString *) promoCode
{
    if(sharedInstance == nil)
    {
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            
            sharedInstance = [[PromotionalCodeUtills alloc] init];
            
            sharedInstance.promotionalCode = promoCode;
        });
    }
    
    else
        
        sharedInstance.promotionalCode = promoCode;

}

+(NSString *) getPromotionalCode
{
    if(sharedInstance == nil)
    {
        return @"";
    }
    
    else
    {
        if(sharedInstance.promotionalCode == nil)
        {
            return @"";
        }
        else
        {
            return sharedInstance.promotionalCode;
        }
    }
}

@end
