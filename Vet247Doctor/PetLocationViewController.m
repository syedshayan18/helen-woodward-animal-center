//
//  PetLocationViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PetLocationViewController.h"
@import GoogleMaps;
@interface PetLocationViewController ()

@end

@implementation PetLocationViewController
@synthesize lng,lat,pettag;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%@%@",lng,lat);
   
    double latdouble = [lat doubleValue];
    double londouble = [lng doubleValue];
    _Mapview.camera = [GMSCameraPosition cameraWithLatitude:latdouble longitude:londouble zoom:14.0];
    GMSMarker *marker = [[GMSMarker alloc]init];
    marker.position =CLLocationCoordinate2DMake(latdouble, londouble);
    marker.appearAnimation =kGMSMarkerAnimationPop;
    if ([pettag isEqualToString:@"LOST"]){
         marker.icon = [UIImage imageNamed:@"lost_pin"];
    }
    else {
        marker.icon = [UIImage imageNamed:@"found_pin"];
    }
    marker.map = _Mapview;
   
   }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
