//
//  Lostnffoundmodel.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/26/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Lostnffoundmodel : NSObject

-(id)initWithDictionary:(NSDictionary *)dictionary;


@property NSString *petname;
@property NSString *userID;
@property NSString *petlostbreed;
@property NSString *petfoundbreed;
@property NSString *petlostSex;
@property NSString *petfoundsex;
@property NSString *petlostcolor;
@property NSString *petfoundcolor;
@property NSString *petimagefound;
@property NSString *petimagelost;
@property NSString *petlostSpecies;
@property NSString *petfoundSpecies;
@property NSString *PetID;
@property NSString *stringage;
@property NSInteger IDint;
@property NSString *petage;
@property NSInteger speciesint;
@property NSString * reportID;
@property NSString *recieverID;


@property NSString *petlostWeight;
@property NSString *petfoundweight;
@property NSString *spayed;
@property NSString *expirydate;
@property NSString *locationname;
@property NSString *latitude;
@property NSString *longitude;
@property NSString *animaltag;
@property NSString *petaction;
@property NSString *petdescription;
@property (strong,nonatomic )NSDictionary *datadict;

@end
