//
//  DashboardViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/13/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "DashboardViewController.h"
#import "SWRevealViewController.h"
#import "loginscreenViewController.h"
#import "PetdetailsViewController.h"
#import "MyPetsViewController.h"
#import "AppDelegate.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "PackagesViewController.h"
#import "PetReferralsController.h"
#import "AFNetworking.h"
#import "UIImage+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "ChatlostnfoundViewController.h"



@interface DashboardViewController()
@property BOOL noObjectsLeft;
@property (strong, nonatomic) NSString * calledByKeyValue;
@property AppDelegate *appDel;
@end

@implementation DashboardViewController
@synthesize locationManager;

- (void)viewDidLoad {
   
[super viewDidLoad];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoggin"] isEqualToString:@"YES"]){
        
         [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"firstLoggin" ];
        [self performSegueWithIdentifier:@"packages" sender:nil];
    }
    //userlatlong
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy =
        kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
   

    [self.locationManager requestWhenInUseAuthorization];
    
    
    
    [self getprofilerequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(chatscreen:)
                                                 name:@"chatscreen"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(norecordalert:)
                                                 name:@"norecord"
                                               object:nil];
    
    
     [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"firstLoggin" ];
    NSString *ab=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSLog(@"%@",ab);
    _ui_profile.hidden =true;
    
    delegate = self;
 
}



- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *curPos = locationManager.location;
    if (curPos){
        [locationManager stopUpdatingLocation];
        
    }
    
    _latitude = [[NSNumber numberWithDouble:locationManager.location.coordinate.latitude] stringValue];
    
    _longitude = [[NSNumber numberWithDouble:locationManager.location.coordinate.longitude] stringValue];
    
    
    
   
    //from Google Map SDK
    
    
    
}


- (void)chatscreen:(NSNotification *)note {
    _chatIDs = [[NSDictionary alloc]init];
    
    _chatIDs=note.userInfo;
    NSLog(@"%@",_chatIDs);
       [self performSegueWithIdentifier:@"chatscreen" sender:nil];
}
    

    
    

-(void)getusercoordinates {
    
    NSString *getlocationurl = [NSString stringWithFormat:@"%@update_location.php",BaseURL];
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    body[@"user_id"] = _userid;
    body [@"latitude"] = _latitude;
    body [@"longitude"] = _longitude;
    
    [NetworkingClass Userlatlongpostrequest:getlocationurl params:body completion:^(id finished, NSError *error) {
        
        
        if (error==nil){
            NSLog(@"%@",finished);
        }
        
    }];
    
    
}

-(void)getprofilerequest {
    _userid=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSString *getprofile = [NSString stringWithFormat:@"%@%@?id=%@",BaseURL,@"get_profile.php",_userid];
    
[NetworkingClass getprofileinfo:getprofile completion:^(id finished, NSError *error) {
    
    if (error ==nil){
        NSLog(@"%@",finished);
        
        NSDictionary *dict = [[NSDictionary alloc]init];
        
        dict = [finished objectForKey:@"data"];
        
        NSString *profileimagepath = [dict objectForKey:@"picturePath"];
        NSString *firstname = [dict objectForKey:@"firstName"];
        [[NSUserDefaults standardUserDefaults]setObject:profileimagepath forKey:@"userimage"];
        
         [[NSUserDefaults standardUserDefaults]setObject:firstname forKey:@"username"];

    }
    
    
}];
   
}


-(IBAction)btn_petservices:(UIButton *)sender{
    
    PetReferralsController *petref = [[PetReferralsController alloc]init];
    [self.navigationController pushViewController:petref animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [locationManager stopUpdatingLocation];

  [[NSNotificationCenter defaultCenter] removeObserver:@"norecord"];
     [[NSNotificationCenter defaultCenter] removeObserver:@"chatscreen"];
}
- (void)norecordalert:(NSNotification *)note {
    [self performSegueWithIdentifier:@"addpets" sender:nil];
}

-(void) paymentAuthentication {
    //by me after atnetworking applied
        if(![self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
    
        }
    
         NSString *userid=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        NSString *urlString = [NSString stringWithFormat:@"%@%@?%@=%@",BaseURL,@"payment_authentication.php",@"userId",userid];
    
    
        _iddict = [[NSMutableDictionary alloc]init];
    
        [_iddict setObject:userid forKey:@"userId"];
    
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer  = [AFHTTPResponseSerializer serializer];
    
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task,id responseobject){
    
             [MBProgressHUD hideHUDForView:self.view animated:YES];
              NSError* error;
            NSMutableDictionary *Jsonresponse = [NSJSONSerialization JSONObjectWithData:responseobject options:kNilOptions error:&error];
            NSLog(@"%@",Jsonresponse);
    
    
    
            if ([[Jsonresponse objectForKey:@"msg"] isEqualToString:@"your payment authentication not valid"] || [[Jsonresponse objectForKey:@"status"] isEqualToString:@"failure"]) {
                NSLog(@"%@",_calledByKeyValue);
                if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
                    return;
                }
                [self performSegueWithIdentifier:@"packages" sender:nil];
    
    
            }
    
            else if ([[Jsonresponse objectForKey:@"status"] isEqualToString:@"success"])
            {
                if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
    
                    [self performSegueWithIdentifier:@"callvet" sender:nil];
                    return;
                }
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
                [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_Cleared forKey:AnnualSubscriptionPaid_KEY];
    
                if ([self.calledByKeyValue isEqualToString:@"Message"]) {
    
                    //            MessageController *messageController=[[MessageController alloc] init];
                    //
                    //            [self.navigationController pushViewController:messageController animated:YES];
                }
    
                if ([self.calledByKeyValue isEqualToString:@"Call"]) {
    
    
                    [self performSegueWithIdentifier:@"callvet" sender:nil];
    
                    //[self callPortionCodeBlockWhenCallButtonClicked];
                }
            }
    
    
    
    
    
    
    
    
    
        } failure:^(NSURLSessionDataTask *operation, NSError * error) {
    
    
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Error"
                                         message:@"Error in getting response from Server or Check your Internet Connection"
                                         preferredStyle:UIAlertControllerStyleAlert];
    
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                _btn_callvet.userInteractionEnabled=YES;
                
                
                
                
                
            }];
            
            [alert addAction:okAction];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }];
    
    
    
    
    
    
   /* commented to resolved ipv6 issue
    
    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if(![self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
      
    }
    
    
    
    [sendPaymentRecivedNetworkObject paymentAuthentication:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
    */
    
    //commented by default
    // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}



-(IBAction)callButtonClicked:(UIButton*)button {
    
    
     _btn_callvet.userInteractionEnabled=NO;
    
    NSString *annualPaymentPackageChecking = [[NSUserDefaults standardUserDefaults] objectForKey:AnnualSubscriptionPaid_KEY];
    
    NSLog(@"\n\nChecking payment : %@\n\n",annualPaymentPackageChecking);
    
    if([annualPaymentPackageChecking  isEqualToString:AnnualSubscriptionPaid_NOT_Cleared] ||  annualPaymentPackageChecking == nil) {
        
        self.calledByKeyValue = @"Call";
        
        [self paymentAuthentication];
        
    }
    else if ([annualPaymentPackageChecking isEqualToString:AnnualSubscriptionPaid_Cleared])
    {
        self.calledByKeyValue = @"SetExtraCheck";
        [self paymentAuthentication];
        //[self callPortionCodeBlockWhenCallButtonClicked];
    }
}

/* commented by me to afnetwoking applied
-(void)paymentResponse:(id) data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"your payment authentication not valid"] || [[data objectForKey:@"status"] isEqualToString:@"failure"]) {
        NSLog(@"%@",_calledByKeyValue);
        if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
            return;
        }
        [self performSegueWithIdentifier:@"packages" sender:nil];
       
//        PackagesViewController * packages =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"packages"];
//        [self.navigationController presentViewController:packages animated:YES completion:nil];
        
        
//        PackagesViewController *packageController = [[PackagesViewController alloc] init];
//        
//        [self.navigationController pushViewController:packageController animated:YES];
        
        //        UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
        //                                                             alertControllerWithTitle:@"Message"
        //                                                             message:@"Sorry, your Package is not authenticated, Press 'OK' to activate, otherwise Press 'Cancel'"
        //                                                             preferredStyle:UIAlertControllerStyleAlert];
        //        UIAlertAction* ok = [UIAlertAction
        //                             actionWithTitle:@"OK"
        //                             style:UIAlertActionStyleDefault
        //                             handler:^(UIAlertAction * action)
        //                             {
        //                                 PackagesVC *packageController = [[PackagesVC alloc] init];
        //
        //                                 [self.navigationController pushViewController:packageController animated:YES];
        //                             }];
        //
        //        UIAlertAction * cancel = [UIAlertAction
        //                                  actionWithTitle:@"Cancel"
        //                                  style:UIAlertActionStyleDefault
        //                                  handler:^(UIAlertAction * action)
        //                                  {
        //
        //                                  }];
        //
        //        [paymentSuccessfullyRecieved addAction:ok];
        //
        //        [paymentSuccessfullyRecieved addAction:cancel];
        //
        //        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
    
    else if ([[data objectForKey:@"status"] isEqualToString:@"success"])
    {
        if ([self.calledByKeyValue isEqualToString:@"SetExtraCheck"]) {
            
            [self performSegueWithIdentifier:@"callvet" sender:nil]; 
            return;
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
        
        [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_Cleared forKey:AnnualSubscriptionPaid_KEY];
        
        if ([self.calledByKeyValue isEqualToString:@"Message"]) {
            
            //            MessageController *messageController=[[MessageController alloc] init];
            //
            //            [self.navigationController pushViewController:messageController animated:YES];
        }
        
        if ([self.calledByKeyValue isEqualToString:@"Call"]) {
            
            
            [self performSegueWithIdentifier:@"callvet" sender:nil];
            
            //[self callPortionCodeBlockWhenCallButtonClicked];
        }
    }
}

-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Check your Internet"
                                 message:@"Please Check your Internet Connection"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
          _btn_callvet.userInteractionEnabled=YES;
        
        
        
    }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    



  
}
*/







-(void)viewWillAppear:(BOOL)animated{
  
    
     [[self navigationController]setNavigationBarHidden:NO animated:YES];
     _btn_callvet.userInteractionEnabled=YES;
    
    [self getusercoordinates];
     [self.locationManager startUpdatingLocation];
     _ui_profile.hidden =true;
 //  AppDelegate * ppDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    if (![ppDel availableInternet]) {
//        
//        NSLog(@"Internetnotvialbe");
//    }
//    else{
// [self fetchPets];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btN_profile:(UIBarButtonItem *)sender{
    if (_ui_profile.hidden==true){
        _ui_profile.hidden=false;
    }
    else {
        _ui_profile.hidden =true;
    }
    
    

    
}

//- (void) fetchPets
//{
//    NetworkingClass *network=[[NetworkingClass alloc] init];
//    
//    network.delegate=self;
//    
//    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    [network getPets:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
//    
//}

//-(void)response:(id)data {
//    
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//    if([data isKindOfClass:[NSArray class]])
//    {
//        _petdata = [[NSMutableArray alloc]init];
//      
//    self.petdata=data;
//        NSLog(@"%@",_petdata);
//        
//       
//        
//               NSLog(@"%@",_petdata);
//    }
//    else if ([data objectForKey:@"msg"])
//    {
//        
//        
//       UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//       
//        [errorAlert show];
//        
//    }
//    
//}



-(IBAction)logoutButtonClicked: (UIButton *) sender {
    
    NSString *posturl = [NSString stringWithFormat:@"%@logout.php",BaseURL];
   
    NSMutableDictionary *body = [[NSMutableDictionary alloc]init];
    body[@"user_id"] = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
  
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
[NetworkingClass logout:posturl param:body completion:^(id finished, NSError *error) {
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    if (error==nil){
        NSLog(@"%@",finished);
//        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"No Chat" message:@"No Chat found!" preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//            
//            
//            NSLog(@"ok");
//            
//            
//            
//            
//            
//        }];
//        
//        
//        [alert addAction:okAction];
//        
//        
//        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
    
    }
}];
    
    NSLog(@"LOGOUT");
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"firstLoggin" ];
    [self performSegueWithIdentifier:@"unwindToContainerVC" sender:self];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"device_token"];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picturePath"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
    
    

//    for (int i=0;i<self.navigationController.viewControllers.count; i++){
//        UIViewController * vc = self.navigationController.viewControllers[i];
//        if ([vc isKindOfClass:loginscreenViewController.class]){
//            [self.navigationController popToViewController:vc animated:true];
//        
//        }
//    
//    }
    
    //loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"];
  
    
    
    
    
  // loginscreenViewController *entryController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginscreen"];
   //   [self.navigationController pushViewController:entryController animated:YES];
//
    
}


-(IBAction)mypet:(UIButton *)sender{
    
   
    _btn_mypets.userInteractionEnabled=NO;
    
//    if ((_petdata.count <1) || (self.noObjectsLeft == true)){
//        [self performSegueWithIdentifier:@"addpets" sender:nil];
//    }
    
//    else {
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![_appDel availableInternet]){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Check your Internet"
                                     message:@"Please Check your Internet Connection"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            _btn_mypets.userInteractionEnabled=YES;
            
            
            
        }];
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];

        
    //}
    }
    else {
        [self performSegueWithIdentifier:@"detailspet" sender:nil];

    }
}


-(void)petdetailsViewControllerDidFinishDelegate:(int)count {
    self.noObjectsLeft = true;
    
 
}

- (IBAction)profilebtn:(UIButton *)sender{
     [self performSegueWithIdentifier:@"profile" sender:nil];
  
    
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"chatscreen"]){
        ChatlostnfoundViewController *chatscreen = (ChatlostnfoundViewController *)[segue destinationViewController];
        chatscreen.reciever_id =[_chatIDs objectForKey:@"receiver_id"];
        chatscreen.SenderID = [_chatIDs objectForKey:@"sender_id"];
        chatscreen.report_id = [_chatIDs objectForKey:@"report_id"];
        
    }
}


@end
