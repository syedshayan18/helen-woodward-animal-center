
//  Created by Shayan Ali on 8/15/16.
//  Copyright © 2016 shayanali. All rights reserved.


#import <Foundation/Foundation.h>

@interface CheckInternet : NSObject
+ (BOOL) isInternetConnectionAvailable;


@end
