 //
//  SocketManager.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 9/4/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "SocketManager.h"



#import "SRWebSocket.h"

#import "LoginController.h"

//#import "CallManager.h"

static BOOL shouldCall;

static BOOL isCallActivated;

static BOOL reconnect;

static BOOL hasSocketResponded;

static BOOL hasSocketRegistered;

@implementation SocketManager{
    
   // NSTimer *timer;

}
@synthesize petidcall;
static     NSTimer *timer;

static SRWebSocket *_webSocket;


+(instancetype)sharedInstance
{
    static dispatch_once_t pred;
    static id sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

-(void) connect{
    
    
    NSLog(@"socket status %ld",(long)_webSocket.readyState);
    
    NSLog(@"is call activated: %d",isCallActivated);
    
    
    if ( _webSocket.readyState==SR_OPEN ){
        
                if (!isCallActivated) {
                    
                   // if (shouldCall) {
                        
                        
                        NSLog(@"call is not active but socket is connected go for registeration only");
                        
                        NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
                        
                        [dataDict setObject:@"clientRegistration" forKey:@"message"];
                    
                       // [dataDict setObject: forKey:@"pet_id"];
                    
                        [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
                    //[dataDict setObject: forKey:@"pet_id"];
                        
                        NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
                        
                        //shouldCall=FALSE;

                        
                        [_webSocket send:jsonData];
                    //}

                    
                        
        }

        
    }
    else{

    _webSocket.delegate=nil;
    
   [_webSocket close];
    
    _webSocket=[[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"ws://162.144.204.207:9300"]]];
    
    _webSocket.delegate=self;
    
    [_webSocket open];
    }
    
}

-(void) disconnect{
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    [dataDict setObject:@"endCall" forKey:@"message"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"session_id"] forKey:@"session_id"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [self sendDataToSocketToEndCall:jsonData];
    
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    
    NSLog(@"%@",message);
    
    NSError* error;
    
    id json = [NSJSONSerialization JSONObjectWithData:[message dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
  //  NSLog(@"%@",json);
    
    NSDictionary *metaData = (NSDictionary*) json;
    
    NSLog(@"%@",metaData );
    
    if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"registered"]) {
        
        //registered=TRUE;

        NSLog(@"registered Successfully and Value of hasSocketRegistered is : %hhd",hasSocketRegistered);
        
        hasSocketRegistered=TRUE;
        
       
        
        if (!isCallActivated) {
            
            
            NSLog(@"Call button has been pressed and Data sent for ");
                
                NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
                
                [dataDict setObject:@"call" forKey:@"message"];
                
                [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"from_id"];
            
            NSLog(@"%@",petidcall);
           [dataDict setObject:petidcall forKey:@"pet_id"];
            //[dataDict setObject:@"468" forKey:@"pet_id"];
                NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
                
                if (_webSocket.readyState==SR_OPEN) {
                    
                    isCallActivated=TRUE;

                    NSLog(@"shouldCall should be false: %d",shouldCall);
                    
                    shouldCall=FALSE;
                    
                    [webSocket send:jsonData];
                    
                    //If this condition does not ful fill try again. This happens some times whensocket is opened and user is registered but when the call was supposed to send then socket got disconected so we will open the socket and regiser again and then we will try again
                
                }

           
            
            
            
        }

       
    }
    
//    else if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"busy"])
//    {
//        [self.delegate responseFromSocket:metaData];
//    }
    
    else if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"call"]){
        
        //Now Call is going to Other Party (Docotor) We are supposed to show the ringer Screen
        
        [[NSUserDefaults standardUserDefaults] setObject:[metaData objectForKey:@"call_record_id"] forKey:@"call_record_id"];
        
        hasSocketResponded=TRUE;
        
        [self.delegate responseFromSocket:metaData];
        
    }
    else if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"call_activated"]){
        //if (!isCallActive) {
        
        hasSocketResponded=TRUE;
        
            
        
        [[NSUserDefaults standardUserDefaults] setObject:[metaData objectForKey:@"get_session_id"] forKey:@"get_session_id"];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:[metaData objectForKey:@"get_token"] forKey:@"get_token"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[metaData objectForKey:@"session_id"] forKey:@"session_id"];
        
       // isCallActive=TRUE;
        
        [self.delegate responseFromSocket:metaData];

        
    }
    
    else if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"call_end"]){
      //  isCallActive=FALSE;
        
        NSLog(@"when doctor ends a call");
        
        hasSocketResponded=TRUE;
        
       

        [[NSNotificationCenter defaultCenter] postNotificationName:LeaveCallScreen object:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"callEndedByDoctor" object:nil];
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Call disconnected" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alertView show];
        
    }
    
    else if ([[metaData objectForKey:@"msg_type"] isEqualToString:@"busy"]){
        //  isCallActive=FALSE;
        hasSocketResponded=TRUE;
        
        [self.delegate responseFromSocket:metaData];
        
    }



    
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
     NSLog(@"Websocket Connected");
    
   
    
  NSLog(@"isCallactivated value: %d",isCallActivated);
    if (!isCallActivated) {
        
        if (shouldCall) {
            
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
            
            [dataDict setObject:@"clientRegistration" forKey:@"message"];
            
            [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
            
            NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
            
            
            shouldCall=FALSE;

            [self sendDataToSocket:jsonData];
            //[webSocket send:jsonData];
            
        }
        
        
        
        
    }

    
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    
     NSLog(@":( Websocket Failed With Error %@", error);
//    
//    hasSocketResponded=TRUE;
//    
//    if (!isCallActivated) {
//        
//        
//        if (reconnect) {
//            [self checkSocket:@"stop"];
//            
//            NSLog(@"socket has been stoped");
//            
//            [self.delegate showSocketErrorPopUp];
//
//        }
//        
//        
//    }
//    
    //[self connect];
}
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    
    NSLog(@"Websocket closed with the reason:%@",reason);

}
- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload{
    
    NSLog(@"Websocket received pong");

}

-(void)initiateOpenTokSession {
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    [dataDict setObject:@"generateOpenTokSession" forKey:@"message"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] forKey:@"userId"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"session_id"]  forKey:@"session_id"];
    
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [self sendDataToSocket:jsonData];
    
}

-(void)checkSocket:(NSString*)status {
    
    
    if ([status isEqualToString:@"start"]) {
        
        reconnect=TRUE;
    
    
        [self checkSocketConnectivity];
    
    }
        else  if ([status isEqualToString:@"stop"]) {
    
        reconnect=FALSE;
    
    }
        
       
}
-(void)checkSocketConnectivity {
    
    if (reconnect ) {
    

        [self performSelector:@selector(checkSocketConnectivity) withObject:nil afterDelay:5.0f];
        
        if ( _webSocket.readyState==SR_OPEN ) {
            
            NSLog(@"Socket State: %ld",(long)_webSocket.readyState);
        }
        else
        {
            NSLog(@"Connecting Socket Again");
            
            [self connect];
        }
    }
    else {
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(checkSocketConnectivity)
                                                   object:nil];
    }

    

}

-(void)callDisconnectedByPatientBeforeAnswer  {
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    [dataDict setObject:@"isCallEnded" forKey:@"message"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"]  forKey:@"session_id"];
    
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [self sendDataToSocket:jsonData];
    
    [self closeSocket];
    
    [self checkSocket:@"stop"];
    
    [self checkSocketConnectivity];
}
-(void)socketShouldNotReconnect
{
    /* 
     *This function gets called when Call has been disconnected
     *socketShouldConnect=FALSE means socket autoconection should be susspended as call has been completed
     * registered=FALSE this is because when User will initiate call we will register once again.
     its current state is FALSE which means Register and then set its state to TRUE
     */
    
//    
//    socketShouldReConnect=FALSE;
//    
//    isCallActivated=FALSE;
}

-(void)closeSocket {

    if(_webSocket.readyState==SR_OPEN){
        
        [_webSocket close];
    }
}

-(void)callActivated
{
    isCallActivated=YES;
}

-(void)callDeactivated
{
    isCallActivated=NO;
    
    NSLog(@"call deactivated:");
    
}

-(void)shouldRegisterCall{
    
    shouldCall=TRUE;
    
    hasSocketResponded=FALSE;
    
    hasSocketRegistered=FALSE;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(socketWasNotOpened) object:self];
    
    [ self performSelector:@selector(socketWasNotOpened) withObject:self afterDelay:30.0];
    
    NSLog(@"user can register for CAll");
}

-(void)socketWasNotOpened{
    
    
    NSLog(@"hasSocketResponded Boolean: %hhd",hasSocketResponded);
    
    if (!hasSocketResponded) {
        
        NSLog(@"socketWasNotOpened");
        
    
        
        if (hasSocketRegistered) {
            
            NSLog(@"SOcket has Registered but we did not received docs count etc");
            
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(socketWasNotOpened) object:self];
            
            [ self performSelector:@selector(socketWasNotOpened) withObject:self afterDelay:15.0];
            
            hasSocketRegistered=FALSE; // just to make sure the next time if hasSocketResponded does not gets TRUE this function should throw an error

        }
        else
        {
            hasSocketResponded=TRUE;
            
            [self.delegate failedToConnectToSocket];

            
        }
        
        
    }
    
}

#pragma mark send Data to Socket

-(void)sendDataToSocket:(NSData*)data{
    
    
        BOOL loop=TRUE;
        
        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        while (loop) {
            
            if (_webSocket.readyState==SR_OPEN) {
                
                NSLog(@" Data has been sent");
                
                NSLog(@"Data in String Format %@ ",myString);
                
                [_webSocket send:data];
                
                loop=FALSE;
                
                NSLog(@"break loop");
                break;
            }
        }
        

    
}

-(void)pingForDoctorEveryFortySeconds {
    
    NSLog(@"pinged for Doc after 40 seconds");
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    [dataDict setObject:@"isDoctorResponded" forKey:@"message"];
    
    [dataDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"call_record_id"]  forKey:@"call_record_id"];
    
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [self sendDataToSocket:jsonData];
    
}

-(BOOL)getCallStatus{
    
    return isCallActivated;
}

#pragma mark send Data to EndCall

-(void)sendDataToSocketToEndCall:(NSData*)data{
    
    BOOL loop=TRUE;
    
    NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    while (loop) {
        
        if (_webSocket.readyState==SR_OPEN) {
            
            NSLog(@" Data has been sent");
            
            NSLog(@"Data in String Format %@ ",myString);
            [_webSocket send:data];
            
            loop=FALSE;
            
            NSLog(@"break loop");
            break;
        }
    }
    
    [self closeSocket];
    
    [self checkSocket:@"stop"];
    
    [self checkSocketConnectivity];

    
    
    
}


@end
