//
//  PetProfileHistorySectionView.h
//  Vet247Doctor
//
//  Created by APPLE on 7/30/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetProfileHistorySectionView : UIView <UITextViewDelegate>


@property UIImageView * historyFieldBGImage;

@property UITextView * historyTextView;

@property UIImageView * allergiFieldBGImage;

@property UITextView * allergiTextView;

@property UIImageView * medicationFieldBGImage;

@property UITextView * medicationTextView;

@property UIImageView * dietFieldBGImage;

@property UITextView * dietTextView;

@property (strong, nonatomic) UIImageView *vaccinatedFieldBGImage;

@property (strong, nonatomic) UIButton *vaccinatedYesButton, *vaccinatedYesButtonBG, *vaccinatedNoButton, *vaccinatedNoButtonBG;

@property (strong, nonatomic) UILabel *vaccinatedYesLable, *vaccinatedNoLable;

@property (strong, nonatomic) UIImage *selectedImage;

@property (strong, nonatomic) UIImage *unSelectedImage;

-(void) addComponents;

-(void) textViewDidChange:(UITextView *)textView;

-(BOOL) textViewShouldBeginEditing:(UITextView *)textView;

-(BOOL) checkHistoryInfoTextFields;

-(void) changeTextViewStatus;

@end
