//
//  NavigationTableViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/14/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "NavigationTableViewController.h"
#import "NavigationTableViewCell.h"
#import "MyPetsViewController.h"
#import "SWRevealViewController.h"
@interface NavigationTableViewController ()

@end

@implementation NavigationTableViewController{
    NSArray *Menu;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Menu = @[@"Home",@"My Profile",@"My Pets",@"Call a Vet",@"Pet Services",@"Lost Or Found Pet"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [Menu count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NavigationTableViewCell *cell =(NavigationTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.lbl_menulist.text = [Menu objectAtIndex:indexPath.row];
    cell.img_menulist.image = [UIImage imageNamed:[Menu objectAtIndex:indexPath.row]];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

//    
//    if (indexPath.row==0) {
//        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//        SWRevealViewController *Reviel = [self.storyboard instantiateViewControllerWithIdentifier:@"mypets"];
//        [navController setViewControllers: @[Reviel] animated: NO ];
//        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        
//    }
//    
//    else if (indexPath.row==1){
//        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//        
//        SWRevealViewController *Reviel = [self.storyboard instantiateViewControllerWithIdentifier:@"VRlocalVideos"];
//        
//        [navController setViewControllers: @[Reviel] animated: NO ];
//        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        
//    }
//    
//    else if (indexPath.row==2){
//        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//        
//        SWRevealViewController *Reviel = [self.storyboard instantiateViewControllerWithIdentifier:@"VR Videos "];
//        
//        [navController setViewControllers: @[Reviel] animated: NO ];
//        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        
//    }
//    
//    
//    else if (indexPath.row==3){
//        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//        
//        SWRevealViewController *Reviel = [self.storyboard instantiateViewControllerWithIdentifier:@"Bookmark Videos"];
//        
//        [navController setViewControllers: @[Reviel] animated: NO ];
//        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        
//    }
//    
//    
//    else if (indexPath.row==4){
//        
//        
//        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//        
//        SWRevealViewController *Reviel = [self.storyboard instantiateViewControllerWithIdentifier:@"My VR 360 Videos"];
//        
//        [navController setViewControllers: @[Reviel] animated: NO ];
//        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        
//    }
//    
//    
//    
//    else if (indexPath.row==5)
//    {
//        
//        
//       
//        
//    }
    


}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
