//
//  signViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/9/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "signViewController.h"

#import "Masonry.h"

#import "Define.h"

#import "loginscreenViewController.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import <QuartzCore/QuartzCore.h>
@interface signViewController ()

@property BOOL radioButton;
@end

@implementation signViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 [self.txt_phone setKeyboardType:UIKeyboardTypePhonePad];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.btn_login setAlpha:0.0f];
    self.txt_firstname.delegate=self;
   
    self.txt_lastname.delegate=self;
  
    self.txt_email.delegate=self;
  
    self.txt_phone.delegate=self;
    
    self.txt_password.delegate=self;
    
    self.txt_confirmpass.delegate=self;
    
    self.radioButton = false;
    
    NSAttributedString *firstname = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_firstname.attributedPlaceholder = firstname;
    NSAttributedString *lastname = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_lastname.attributedPlaceholder = lastname;
    
    NSAttributedString *email = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_email.attributedPlaceholder = email;
    NSAttributedString *confirmpass = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_confirmpass.attributedPlaceholder = confirmpass;

    NSAttributedString *phone = [[NSAttributedString alloc] initWithString:@"Phone Number" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_phone.attributedPlaceholder = phone;
    NSAttributedString *password = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_password.attributedPlaceholder = password;
    
    
    _btn_donee.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_donee.clipsToBounds = YES;
    
    _btn_login.layer.cornerRadius = 15; // this value vary as per your desire
    _btn_login.clipsToBounds = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)btn_register:(UIButton *)sender{
    
    [self.view endEditing:YES];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
    
    if ([self checkTextFields]) {
        
        NetworkingClass *network = [[NetworkingClass alloc] init];
        
        network.delegate = self;
        
        [dataDict setObject:self.txt_firstname.text forKey:@"fName"];
        
        [dataDict setObject:self.txt_lastname.text forKey:@"lName"];
        
        [dataDict setObject:self.txt_email.text forKey:@"email"];
        
        [dataDict setObject:self.txt_phone.text forKey:@"phonenumber"];
        
        [dataDict setObject:self.txt_password.text forKey:@"password"];
        
        [dataDict setObject:self.txt_confirmpass.text forKey:@"confirmpassword"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [network signUp:dataDict];
        
        
        
    }

    
}




/*
-(void)termsAndConditionsButtonClicked:(UIButton *)btn
{
    if (!self.termsAndConditionsButtonClicked)
    {
        self.tickImageView.hidden = NO;
        
        self.termsAndConditionsButtonClicked=YES;
    }
    else if (self.termsAndConditionsButtonClicked)
    {
        self.tickImageView.hidden = YES;
        
        self.termsAndConditionsButtonClicked=NO;
    }
    
}

*/




- (BOOL) checkTextFields {
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ( self.txt_firstname.text && self.txt_firstname.text.length<1) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"First Name Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    else if ( self.txt_lastname.text && self.txt_lastname.text.length<1) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    
    else if (self.txt_email.text && self.txt_email.text.length<1) {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Email Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
        
    }
    else if ([emailTest evaluateWithObject:self.txt_email.text] == NO) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Valid Email Address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    
//    else if (self.txt_phone.text && self.txt_phone.text.length<1)
//    {
//        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Phonenumber Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [errorAlert show];
//        
//    }
    
    else if (self.txt_password.text && self.txt_password.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    else if (self.txt_confirmpass.text && self.txt_confirmpass.text.length<1)
    {
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
        
    }
    else if ([self.txt_password.text isEqualToString:self.txt_confirmpass.text] )
    {
        
        
        
        if (self.radioButton) {
        
            return YES;
            
                    }
                    else
                    {
                        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Agree to terms and conditions" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
                        [errorAlert show];
            
                        return NO;
                    }
        
        }
        
        else {
            
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Password missmatch" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [errorAlert show];
            
        }
        
        return NO;
}


        



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:self.txt_firstname]) {
        
        [self.txt_lastname becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_lastname])
    {
        [self.txt_email becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_email])
    {
        
        [self.txt_phone becomeFirstResponder];
        
    }
    else if ([textField isEqual:self.txt_phone])
    {
        
        [self.txt_password becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_password])
    {
        
        [self.txt_confirmpass becomeFirstResponder];
        
    }
    else if (textField== self.txt_confirmpass) {
        
        [textField resignFirstResponder];
        
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txt_phone)
    {
        NSString *newString = [self.txt_phone.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        NSError *error = nil;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    
    return YES;
}






-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"success"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"firstLoggin"];
            
            //  [[NSUserDefaults standardUserDefaults] setObject:AnnualSubscriptionPaid_NOT_Cleared forKey:AnnualSubscriptionPaid_KEY];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:AnnualSubscriptionPaid_KEY];
            
            [self.navigationController popViewControllerAnimated:YES];
            
           
            
            [self performSegueWithIdentifier:@"unwindToContainerVC" sender:self];
            
//            loginscreenViewController *logincontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginscreen"]; //or the homeController
//            AppDelegate *app = (AppDelegate *)([UIApplication sharedApplication].delegate);
//            [app.window setRootViewController:logincontroller];
        }
        else
        {
            UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [errorAlert show];
            
        }
        
    }
    else {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Email Already Exists" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [errorAlert show];
    }
    
}



-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
}




- (IBAction)btn_radio:( UIButton *)sender {
    
    self.radioButton = !self.radioButton;
    [sender setSelected:self.radioButton];
    
    
}
@end
