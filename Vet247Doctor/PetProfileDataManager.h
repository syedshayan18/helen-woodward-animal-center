//
//  PetProfileDataManager.h
//  Vet247Doctor
//
//  Created by APPLE on 8/8/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PetProfileDataManager : NSObject

@property (nonatomic,retain) NSMutableDictionary * dataContainer;

+(PetProfileDataManager*) getPetProfileDataManagerSharedInstance;

-(void) insertPetProfileInfo:(NSString*)value :(NSString*)NsKey;

-(NSMutableDictionary *) getPetProfileInfo;

-(void) setPetData:(NSMutableDictionary*) petData;

+(void) removePetProfileInfo;

+(void) removePetProfileObjectForKey:(NSString *)Key;

-(void)inserttacked:(int )value :(NSString*)Nskey;

@end
