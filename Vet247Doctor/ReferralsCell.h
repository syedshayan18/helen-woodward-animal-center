//
//  ReferralsCell.h
//  Vet247Doctor
//
//  Created by APPLE on 8/18/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferralsCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLbl;

@property (strong,nonatomic) UILabel *descriptionText;

@property (strong, nonatomic) UIImageView *imageIcon;

@property (strong,nonatomic) UILabel *numLbl;

@end
