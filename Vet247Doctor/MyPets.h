//
//  MyPets.h
//  Vet247Doctor
//
//  Created by Muhammad Raza on 30/03/2017.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPets : UIView
@property (weak, nonatomic) IBOutlet UIImageView *img_petprofile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_animalname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sex;
@property (weak, nonatomic) IBOutlet UILabel *lbl_breed;
@property (weak, nonatomic) IBOutlet UILabel *lbl_age;
@property (weak, nonatomic) IBOutlet UIButton *btn_editview;
@property (weak, nonatomic) IBOutlet UIButton *btn_delete;
@property (weak, nonatomic) IBOutlet UIButton *btn_callpet;
@property (weak, nonatomic) IBOutlet UIButton *btn_message;
@property (weak, nonatomic) IBOutlet UIButton *btn_report;

@end
