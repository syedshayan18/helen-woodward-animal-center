
#import "ExampleCalloutAnnotation.h"
#import "ExampleCalloutView.h"

#define CustomCallOutHorizontalAccomodation (isiPhoneDevice) ? 90.0 : 180
#define CustomCallOutVerticalAccomodation   (isiPhoneDevice) ? 90.0 : 140

@implementation ExampleCalloutAnnotation
@synthesize mapView;

- (id) initWithLat:(CGFloat)latitute lon:(CGFloat)longitude;
{
    _coordinate = CLLocationCoordinate2DMake(latitute, longitude);
    return self;
}

- (MKAnnotationView*)annotationViewInMap:(MKMapView *)aMapView :(ControlController *)controller{
    
//    if(!calloutView){
//        calloutView = [(ExampleCalloutView*)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"ExampleCalloutView"] retain];
//        
//        if(!calloutView)
//            calloutView = [[ExampleCalloutView alloc] initWithAnnotation:self Map:self.mapView];
//
//    } else {
//        calloutView.annotation = self;
//    }
    
    calloutView = [[ExampleCalloutView alloc] initWithAnnotation:self Map:self.mapView];
    
    self.mapView = aMapView;

    
//  Saving the POI data to callout view for Annotation voew population ...
    calloutView.calloutDataInfo = self.calloutDataInfo;
    
//  For sending ANnotation View click to Map controller ...
    calloutView.delegate = controller;
        
    [calloutView populateCalloutView:self.calloutDataInfo];
    
//  Move Map view to Adjust the Annotation View within Visible Rect ...
    [self adjustAnnotationViewWithinVisibleRect];
    
    return calloutView;
}

-(void)adjustAnnotationViewWithinVisibleRect
{

}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    _coordinate = newCoordinate;

    NSLog(@"\nOld: %f %f New: %f %f", _coordinate.latitude, _coordinate.longitude, newCoordinate.latitude, newCoordinate.longitude);
    
    if(calloutView) {
        [calloutView setAnnotation:self];
    }
}

- (CLLocationCoordinate2D)coordinate
{
    return _coordinate;
}

- (void)dealloc
{
    [calloutView release];
    [super dealloc];
}

@end