//
//  FoundpetdetailsViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "FoundpetdetailsViewController.h"
#import "FoundpetdetailsTVC.h"
#import <UITextView_Placeholder/UITextView+Placeholder.h>
#import <CoreLocation/CoreLocation.h>
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "Vet247Doctor-Swift.h"

@import GoogleMaps;




@interface FoundpetdetailsViewController ()<PlaceSearchTextFieldDelegate,UITextFieldDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate>

@property AppDelegate *appDel;

@end

@implementation FoundpetdetailsViewController
CLLocationDegrees lat;
CLLocationDegrees longtitude;
int radius;
 
@synthesize petimage, marker;
@synthesize locationManager;
- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    NSLog(@"%@",petimage);
    
    _appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([_appDel availableInternet]){
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    UITapGestureRecognizer *taploose = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(taploose:)];
    [self.lbl_loose addGestureRecognizer:taploose];
    UITapGestureRecognizer *taptaken = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(taptaken:)];
    [self.lbl_taken addGestureRecognizer:taptaken];
    UITapGestureRecognizer *tapshelter = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapshelter:)];
    [self.lbl_shelter addGestureRecognizer:tapshelter];
    
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.txt_sex.inputAccessoryView = keyboardToolbar;
    self.txt_breed.inputAccessoryView=keyboardToolbar;
    self.txt_weight.inputAccessoryView=keyboardToolbar;
    self.txt_color.inputAccessoryView=keyboardToolbar;
    self.PlacesTextField.inputAccessoryView=keyboardToolbar;
    self.textview_details.inputAccessoryView=keyboardToolbar;
    self.txt_species.inputAccessoryView=keyboardToolbar;
    
    self.txt_weight.keyboardType = UIKeyboardTypeNumberPad;

  
   
    self.txt_sex.delegate=self;
    self.txt_breed.delegate=self;
    self.txt_color.delegate=self;
    self.txt_weight.delegate=self;
    
  _reducedimage  =[self imageWithImage:petimage scaledToSize:CGSizeMake(250, 250)];
    
   

    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy =
        kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    [self.locationManager startUpdatingLocation];

    _firstLocationUpdate = false;

    
    
        //unhidding navigation bar
    
       [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    //initializing custom marker for location
    

    
    
    
    
    //setting default check for pet
    [_btn_petloose setSelected:YES];
    
    
 
    //species picker view on textfield
    UIPickerView *speciespicker = [[UIPickerView alloc]init];
    speciespicker.delegate =self;
    speciespicker.dataSource=self;
    [self.txt_species setInputView:speciespicker];
    
    //storing data in dictionary to post on server
    _founddict = [[NSMutableDictionary alloc]init];
    
   
    //placeholder for textview
    _textview_details.placeholder = @"Provide Extra Details if any";
    _textview_details.placeholderColor = [UIColor lightGrayColor];
    self.PlacesTextField.delegate=self;
    
    
 
    //tap gesture to end editing
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapview:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
 

//    NSAttributedString *species = [[NSAttributedString alloc] initWithString:@"SPECIES" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
//    self.txt_species.attributedPlaceholder = species;
//    NSAttributedString *breed = [[NSAttributedString alloc] initWithString:@"BREED IF KNOWN" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
//    self.txt_breed.attributedPlaceholder = breed;
//    NSAttributedString *color = [[NSAttributedString alloc] initWithString:@"COLOR" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
//    self.txt_color.attributedPlaceholder = color;
//    NSAttributedString *weight = [[NSAttributedString alloc] initWithString:@"WEIGHT IF KNOWN" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
//    self.txt_weight.attributedPlaceholder = weight;
//    NSAttributedString *sex = [[NSAttributedString alloc] initWithString:@"SEX IF KNOWN" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
//    self.txt_sex.attributedPlaceholder = sex;
    
    _btn_sendreport.layer.cornerRadius=15;
    _btn_sendreport.clipsToBounds=YES;
    

    //_mapView.settings.compassButton = YES;
    //_mapView.settings.myLocationButton = YES;
    
    
    _PlacesTextField.placeSearchDelegate = self;
    _PlacesTextField.strApiKey = @"AIzaSyCgdljgsWF6H4ErAjz6AnCxwlQhMQGSbnc";
    _PlacesTextField.superViewOfList = self.view; // View, on which Autocompletion list should be appeared.
    _PlacesTextField.autoCompleteShouldHideOnSelection = true;
    _PlacesTextField.maximumNumberOfAutoCompleteRows = 5;
    _PlacesTextField.autoCompleteShouldHideOnSelection = true;
    _PlacesTextField.autoCompleteTableAppearsAsKeyboardAccessory = true;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 1)];
    _PlacesTextField.leftView = paddingView;
    _PlacesTextField.leftViewMode = UITextFieldViewModeAlways;

    // Listen to the myLocation property of GMSMapView.
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
   
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
}

-(void)taploose:(UITapGestureRecognizer *)sender {
    if (self.btn_petloose.isSelected==NO && self.btn_pettaken.isSelected==NO&&self.btn_petshelter.isSelected==NO)
    {
        
        [self.btn_petloose setSelected:YES];
    }
    
    else if (self.btn_petloose.isSelected==NO && (self.btn_pettaken.isSelected==YES||self.btn_petshelter.isSelected==YES))
    {
        
        [self.btn_petloose setSelected:YES];
        
        [self.btn_petshelter setSelected:NO];
        [self.btn_pettaken setSelected:NO];
        
        
        
    }
}
-(void)taptaken:(UITapGestureRecognizer *)sender {
    if (self.btn_pettaken.isSelected==NO && self.btn_petloose.isSelected==NO&&self.btn_petshelter.isSelected==NO)
    {
        
        [self.btn_pettaken setSelected:YES];
    }
    
    else if (self.btn_pettaken.isSelected==NO && (self.btn_petloose.isSelected==YES||self.btn_petshelter.isSelected==YES))
    {
        
        [self.btn_pettaken setSelected:YES];
        
        [self.btn_petloose setSelected:NO];
        [self.btn_petshelter setSelected:NO];
        
        
        
    }
    

}




-(void)tapshelter:(UITapGestureRecognizer *)sender {
    if (self.btn_petshelter.isSelected==NO && self.btn_petloose.isSelected==NO&&self.btn_pettaken.isSelected==NO)
    {
        
        [self.btn_petshelter setSelected:YES];
    }
    
    else if (self.btn_petshelter.isSelected==NO && (self.btn_petloose.isSelected==YES||self.btn_pettaken.isSelected==YES))
    {
        
        [self.btn_petshelter setSelected:YES];
        
        [self.btn_petloose setSelected:NO];
        [self.btn_pettaken setSelected:NO];
        
        
        
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    _globaltextfield = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    _globaltextfield =nil;
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}




-(void)requestWhenInUseAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            NSLog(@"ok");
            
            
            
            
            
        }];
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            // [[UIApplication sharedApplication] openURL:
            //[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
            // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
            
            
        }];
        
        
        [alert addAction:okAction];
        [alert addAction:settings];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
        
        
        
        
        
    }
    
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestWhenInUseAuthorization];
    }
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}


- (void)dealloc {
    [_mapView removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
        
       marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"pet_pin"];
        marker.map = _mapView;
        CLLocation *geolocation=[[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        //from Google Map SDK
        GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
        GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
        {
            GMSAddress *address=response.firstResult;
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (address){
                _PlacesTextField.placeholder = [NSString stringWithFormat:@"%@, %@, %@(%i KM)",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country,radius];
                _locationname = [NSString stringWithFormat:@"%@, %@, %@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
            }
            
        };
        [geocode reverseGeocodeCoordinate:geolocation.coordinate completionHandler:handler];

        
   
    }
    // Do any additional setup after loading the view.
}


    

    




- (void) tapview:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    
}
-(void)yourTextViewDoneButtonPressed
{
    [self.txt_sex resignFirstResponder];
    [self.textview_details resignFirstResponder];
    [self.txt_species resignFirstResponder];
    [self.txt_color resignFirstResponder];
    [self.txt_breed resignFirstResponder];
    [self.txt_weight resignFirstResponder];
   
    
     
}

-(void) placestext:(UITapGestureRecognizer *)recognizer{
    
    
    NSLog(@"places tap");
    
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (!_firstLocationUpdate){
        
        _firstLocationUpdate = YES;
        
        CLLocation *curPos = locationManager.location;
        //        NSString *latitude = [[NSNumber numberWithDouble:curPos.coordinate.latitude] stringValue];
        //
        //        NSString *longitude = [[NSNumber numberWithDouble:curPos.coordinate.longitude] stringValue];
        
        _mapView.camera = [GMSCameraPosition cameraWithTarget:locationManager.location.coordinate zoom:14];
        marker = [[GMSMarker alloc] init];
        marker.position = locationManager.location.coordinate;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"pet_pin"];
        marker.map = _mapView;

        
        //from Google Map SDK
        CLLocation *geolocation=[[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        //from Google Map SDK
        
        GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
               GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
        {
            GMSAddress *address=response.firstResult;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (address)
            {
                
               

                _PlacesTextField.placeholder = [NSString stringWithFormat:@"%@, %@, %@(%i KM)",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country,radius];
                _locationname = [NSString stringWithFormat:@"%@, %@, %@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
            }
        };
        [geocode reverseGeocodeCoordinate:geolocation.coordinate completionHandler:handler];

        
       
      

    }
}
- (void) locationManager:(CLLocationManager *)manager
        didFailWithError:(NSError *)error
{
    NSLog(@"%@", @"Core location can't get a fix.");
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
  
    radius =[self getRadius];
    if (_locationname.length==0){
        _PlacesTextField.text=@"";
    }
    else if (_PlacesTextField.text.length==0){
        _PlacesTextField.text=@"";
    }
    else {
         _PlacesTextField.text = [NSString stringWithFormat:@"%@ (%i  KM)",_locationname,radius];
    }
   

  
    NSLog(@"%i",radius);
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (row)
    {
        case 0:
            self.txt_species.text = @"Cat";
            return @"Cat";
        case 1:
            self.txt_species.text = @"Dog";
            return @"Dog";
            
            
        default: return nil;
    }
}

- (CLLocationCoordinate2D)getCenterCoordinate
{
    CGPoint centerPoint = self.mapView.center;
     CLLocationCoordinate2D centerCoord = [self.mapView.projection coordinateForPoint:(centerPoint)];
    return centerCoord;
}
- (CLLocationCoordinate2D)getTopCenterCoordinate
{
    // to get coordinate from CGPoint of your map
    CGPoint topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width/2.0, 0) toView:self.mapView];
       CLLocationCoordinate2D point = [self.mapView.projection coordinateForPoint:(topCenterCoor)];
    return point;
}
- (CLLocationDistance)getRadius
{
    CLLocationCoordinate2D centerCoor = [self getCenterCoordinate];
    // init center location from center coordinate
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    
    CLLocationCoordinate2D topCenterCoor = [self getTopCenterCoordinate];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    CLLocationDistance radiusKM=radius/1000;
    int radiusint = (int) radiusKM;
    if (radiusint ==0)
    {
        radiusint=1;
    }
    return radiusint;
}

//-(void)viewWillLayoutSubviews{
//    
//    [super viewWillLayoutSubviews];
//    
//    self.mapView.padding=UIEdgeInsetsMake(self.topLayoutGuide.length +5, 0, self.bottomLayoutGuide.length+260, 0);
//}


- (void)viewWillDisappear:(BOOL)animated {
    
    
    [super viewWillDisappear:animated];

    [self deregisterFromKeyboardNotifications];
    

    [locationManager stopUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)btn_report:(UIButton *)sender{
  
    
    
    
    if (_locationname.length <1) {
        _locationname=@"N/A";
    }
    
    
        
    [_founddict setValue:_locationname forKey:@"location"];

        _userid= [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        [_founddict setValue:_txt_species.text forKey:@"species"];
        [_founddict setValue:_txt_breed.text forKey:@"breed"];
        [_founddict setValue:_txt_color.text forKey:@"color"];
        [_founddict setValue:_txt_weight.text forKey:@"weight"];
        [_founddict setValue:_txt_sex.text forKey:@"sex"];
        [_founddict setValue:_textview_details.text forKey:@"detail"];
        
        if (self.btn_petloose.isSelected==YES){
            
            [_founddict setValue:@"Pet still on the loose" forKey:@"action"];
            
        }
        else if (self.btn_pettaken.isSelected==YES){
            
            [_founddict setValue:@"Pet has been taken" forKey:@"action"];
        }
        else if (self.btn_petshelter.isSelected==YES){
            
            [_founddict setValue:@"Taken pet to a shelter or clinic" forKey:@"action"];
        }
        
        [_founddict setValue:[NSNumber numberWithDouble:radius]  forKey:@"radius"];
        [_founddict setValue:_userid forKey:@"user_id"];
        
        double latitude=marker.position.latitude;
        double longitude = marker.position.longitude;
    
        [_founddict setValue:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
        [_founddict setValue:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
        
        
        
        
        NSLog(@"%@",_founddict);
        
        NSString *posturl = [NSString stringWithFormat:@"%@%@",BaseURL,@"found_pet.php"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        ImageDataUpload *uploadTask = [[ImageDataUpload alloc]init ];
        [uploadTask imageUploadRequestWithParams:_founddict url:posturl image:_reducedimage completion:^(NSDictionary *data, NSError * error) {
            
            if (error==nil){
                NSLog(@"%@",data);
                
                
            }
            else {
                
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Error"
                                             message:@"Server Error or Please Check your Internet Connectivity"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    
                    
                    
                    
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Found Report Send"
                                             message:@"Found Report has been Sent"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [self performSegueWithIdentifier:@"unwindtolostdashboard" sender:nil];
                    
                    
                    NSLog(@"ok");
                    
                    
                    
                    
                    
                }];
                
                
                [alert addAction:okAction];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            });
            
            
            
        }];

    


}


-(void) placeSearchResponseForSelectedPlace:(GMSPlace *)responseDict{
    
       _locationname =_PlacesTextField.text;
    
    
  
   [ _mapView clear];
    CLLocationCoordinate2D coordinates= responseDict.coordinate;
   
    //GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude longitude:coordinates.longitude zoom:12.0];
  

    _mapView.camera = [GMSCameraPosition cameraWithTarget:coordinates zoom:12];
   marker = [[GMSMarker alloc] init];
    marker.position = coordinates;
    marker.appearAnimation = kGMSMarkerAnimationPop;
   marker.icon = [UIImage imageNamed:@"pet_pin"];
    marker.map = _mapView;
    
    

}

-(IBAction)btn_petloose:(UIButton *)sender{
    
    
    if (self.btn_petloose.isSelected==NO && self.btn_pettaken.isSelected==NO&&self.btn_petshelter.isSelected==NO)
    {
        
        [self.btn_petloose setSelected:YES];
    }
    
    else if (self.btn_petloose.isSelected==NO && (self.btn_pettaken.isSelected==YES||self.btn_petshelter.isSelected==YES))
    {
        
        [self.btn_petloose setSelected:YES];
        
        [self.btn_petshelter setSelected:NO];
        [self.btn_pettaken setSelected:NO];
        
     
        
    }

}

-(IBAction)btn_pettaken:(UIButton *)sender{
    
    if (self.btn_pettaken.isSelected==NO && self.btn_petloose.isSelected==NO&&self.btn_petshelter.isSelected==NO)
    {
        
        [self.btn_pettaken setSelected:YES];
    }
    
    else if (self.btn_pettaken.isSelected==NO && (self.btn_petloose.isSelected==YES||self.btn_petshelter.isSelected==YES))
    {
        
        [self.btn_pettaken setSelected:YES];
        
        [self.btn_petloose setSelected:NO];
        [self.btn_petshelter setSelected:NO];
        
        
        
    }

    
}
-(IBAction)btn_petshelter:(UIButton *)sender{
    if (self.btn_petshelter.isSelected==NO && self.btn_petloose.isSelected==NO&&self.btn_pettaken.isSelected==NO)
    {
        
        [self.btn_petshelter setSelected:YES];
    }
    
    else if (self.btn_petshelter.isSelected==NO && (self.btn_petloose.isSelected==YES||self.btn_pettaken.isSelected==YES))
    {
        
        [self.btn_petshelter setSelected:YES];
        
        [self.btn_petloose setSelected:NO];
        [self.btn_pettaken setSelected:NO];
        
        
        
    }

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    if (_globaltextfield == _PlacesTextField){
        
        NSLog(@"do nothing");
    }
    else {
        
        // Assign new frame to your view
        [UIView animateWithDuration:0.25 animations:^
         {
             
             CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
             
             
             
             CGRect frame = self.view.frame;
             frame.origin.y = -keyboardSize.height+64;
             self.view.frame=frame;
             
             
             
         }completion:^(BOOL finished)
         {
             
         }];

    }
    
    

    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
 
        
        if ([textField isEqual:self.txt_species]) {
            
            [self.txt_breed becomeFirstResponder];
            
        }
        
        else if ([textField isEqual:self.txt_breed])
        {
            [self.txt_color becomeFirstResponder];
            
        }
        
        else if ([textField isEqual:self.txt_color])
        {
            
            [self.txt_weight becomeFirstResponder];
            
        }
        else if ([textField isEqual:self.txt_weight])
        {
            
            [self.txt_sex becomeFirstResponder];
            
        }
        
        else if ([textField isEqual:self.txt_sex])
        {
            
            [self.textview_details becomeFirstResponder];
            
        }
        else if ( self.textview_details) {
            
            [textField resignFirstResponder];
            
        }
        
   

    
    
    
    return YES;
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    
    if (_globaltextfield ==_PlacesTextField){
        NSLog(@"do nothing") ;
    }
    else {
        [UIView animateWithDuration:0.25 animations:^
         {
             
             CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
             
             CGRect f = self.view.frame;
             f.origin.y = 0.0f+64;
             self.view.frame = f;
             
         }completion:^(BOOL finished)
         {
             
         }];
        

    }
    
}



@end
