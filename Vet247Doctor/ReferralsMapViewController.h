//
//  ReferralsMapViewController.h
//  Vet247Doctor
//
//  Created by APPLE on 8/20/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

#import <MapKit/MapKit.h>

#import "SMCalloutView.h"

#import "MVPlaceSearchTextField.h"

//,MLPAutoCompleteTextFieldDataSource,MLPAutoCompleteTextFieldDelegate

@interface ReferralsMapViewController : UIViewController <UITextFieldDelegate, CLLocationManagerDelegate, SMCalloutViewDelegate,PlaceSearchTextFieldDelegate>

@property(nonatomic,strong) CLLocationManager *locationManager;
@property (strong,nonatomic) NSString *headertitle;
@end
