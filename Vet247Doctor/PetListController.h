//
//  PetListController.h
//  Vet247Doctor
//
//  Created by APPLE on 8/10/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetListController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@end
