//
//  TermsnconditionViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/22/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "TermsnconditionViewController.h"
#import "PetdetailsViewController.h"
@interface TermsnconditionViewController ()

@end

@implementation TermsnconditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btn_accept.layer.cornerRadius = 15;
    _btn_accept.clipsToBounds = YES;
    _btn_decline.layer.cornerRadius =15;
    _btn_decline.clipsToBounds = YES;
    
    
    // Do any additional setup after loading the view.
}
-(IBAction)btn_accept:(UIButton *)sender{
    
    [self performSegueWithIdentifier:@"callpetaccept" sender:nil];
}
-(IBAction)btn_decline:(UIButton *)sender{
    
    [  self.navigationController popViewControllerAnimated:YES ];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PetdetailsViewController *petdetails = (PetdetailsViewController *) [segue destinationViewController];
    petdetails.hideButton =YES;
}


@end
