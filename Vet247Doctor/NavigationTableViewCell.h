//
//  NavigationTableViewCell.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_menulist;
@property (weak, nonatomic) IBOutlet UILabel *lbl_menulist;


@end
