//
//  AddpetsViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/20/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddpetsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_addpet;

@end
