//
//  CallvetViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/13/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "CallvetViewController.h"
#import "MessageController.h"



@interface CallvetViewController ()

@end

@implementation CallvetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}
-(IBAction)btn_msgavet:(UIButton *)sender{
    
    MessageController *messagecontroller = [[MessageController alloc]init];
    [self.navigationController pushViewController:messagecontroller animated:YES];
   
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
