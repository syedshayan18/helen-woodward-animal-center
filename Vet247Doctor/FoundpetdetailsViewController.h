//
//  FoundpetdetailsViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "MLPAutoCompleteTextField.h"
#import "MVPlaceSearchTextField.h"
#import "Vet247Doctor-Swift.h"
@import GoogleMaps;

@interface FoundpetdetailsViewController : UIViewController <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_species;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_breed;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_color;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_weight;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_sex;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomviewconstraints;

@property (weak, nonatomic) IBOutlet UITextView *textview_details;
@property (weak, nonatomic) IBOutlet UIButton *btn_petloose;
@property (weak, nonatomic) IBOutlet UIButton *btn_pettaken;
@property (weak, nonatomic) IBOutlet UIButton *btn_petshelter;
@property (weak, nonatomic) IBOutlet UIButton *btn_sendreport;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong,nonatomic) UITapGestureRecognizer *gestureRecognizer;
@property BOOL firstLocationUpdate;
@property (strong,nonatomic) NSMutableDictionary *founddict;
@property (strong,nonatomic) UIImage *petimage;
@property (strong,nonatomic) UIImage *reducedimage;
@property (strong,nonatomic) NSString *place;
@property (strong,nonatomic) NSString *location;
@property (strong,nonatomic) NSString *locationfromcoding;
@property (strong,nonatomic) NSString *userid;

@property (strong,nonatomic) NSString *locationname;
@property (strong, nonatomic) GMSMarker *marker;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *PlacesTextField;
@property (weak,nonatomic) UITextField *globaltextfield;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *lbl_loose;
@property (weak, nonatomic) IBOutlet UILabel *lbl_taken;
@property (weak, nonatomic) IBOutlet UILabel *lbl_shelter;

@property double doublelat,doublelong;
@end
