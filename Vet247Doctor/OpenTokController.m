//
//  ViewController.m
//  Hello-World
//
//  Copyright (c) 2013 TokBox, Inc. All rights reserved.
//

#import "OpenTokController.h"
#import <OpenTok/OpenTok.h>
#import "Masonry.h"

#import "Define.h"

#import "ControlsFactory.h"

#import "SocketManager.h"

static SocketManager *_manager;

@interface OpenTokController ()
<OTSessionDelegate, OTSubscriberKitDelegate, OTPublisherDelegate>


@property (strong,nonatomic) UIView *topBarView;

@property (strong,nonatomic) UIView *bottomBarView;

@property (strong,nonatomic) UIButton *cameraToggleButton,*endCallButton,*audioPubUnpubButton,*audioSubUnsubButton;


@property (strong,nonatomic) UILabel *broadCasterLabel,*connecting;

@property (strong,nonatomic) UITapGestureRecognizer *gesture;

@property (strong,nonatomic) UIButton *speakerToggleButton;

@property (strong,nonatomic) UIView *connectionView;

@property (strong,nonatomic) UIImageView *callViewBG,*avatarImage;

@property (strong,nonatomic) NSString* shouldShowOpenTokScreen;

@end

@implementation OpenTokController {
    OTSession* _session;
    OTPublisher* _publisher;
    OTSubscriber* _subscriber;
}

#define BAR_HEIGHT 50.0f

#define SPEAKERBUTTON_WIDTH 90.0f

// *** Fill the following variables using your own Project info  ***
// ***          https://dashboard.tokbox.com/projects            **



//static NSString *const kApiKey = @"45328922";
static NSString *const kApiKey = @"45349142";



static bool subscribeToSelf = NO;

static bool isFullScreen=NO;

-(void) addComponents {
    
    [self setUpTopBarView];
    
    [self setUpBottomBarView];
    
    [self setUpConnectionView];
    
    [self.view addSubview:self.connectionView];

    
    self.gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeScreenSize)];
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addComponents];
    
    self.shouldShowOpenTokScreen=@"NO";
    
    [self performSelector:@selector(removeConnectionScreen) withObject:self afterDelay:40.0f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callEndedByDoctor:) name:@"callEndedByDoctor" object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(appWentToBG) name:AppWentToBackGround object:nil];


    
    // Step 1: As the view comes into the foreground, initialize a new instance
    // of OTSession and begin the connection process.
    _session = [[OTSession alloc] initWithApiKey:kApiKey
                                       sessionId:[[NSUserDefaults standardUserDefaults] objectForKey:@"get_session_id"]
                                        delegate:self];
    [self doConnect];
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (UIUserInterfaceIdiomPhone == [[UIDevice currentDevice]
                                      userInterfaceIdiom])
    {
        return NO;
    } else {
        return YES;
    }
}
#pragma mark - OpenTok methods

/**
 * Asynchronously begins the session connect process. Some time later, we will
 * expect a delegate method to call us back with the results of this action.
 */
- (void)doConnect
{
    OTError *error = nil;
    
    [_session connectWithToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"get_token"] error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
}

/**
 * Sets up an instance of OTPublisher to use with this session. OTPubilsher
 * binds to the device camera and microphone, and will provide A/V streams
 * to the OpenTok session.
 */
- (void)doPublish
{
    _publisher =
    [[OTPublisher alloc] initWithDelegate:self
                                     name:[[UIDevice currentDevice] name]];
    
    OTError *error = nil;
    [_session publish:_publisher error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    
   // [self.view addSubview:_publisher.view];
   // [_publisher.view setFrame:CGRectMake(0, 0, widgetWidth, widgetHeight)];
    
     [_publisher.view setFrame:CGRectMake(0, self.view.frame.size.height- self.view.frame.size.width/2.5-BAR_HEIGHT, self.view.frame.size.width/2.5, self.view.frame.size.width/2.5)];
    
    [_publisher.view addGestureRecognizer:self.gesture];
}

/**
 * Cleans up the publisher and its view. At this point, the publisher should not
 * be attached to the session any more.
 */
- (void)cleanupPublisher {
    [_publisher.view removeFromSuperview];
    _publisher = nil;
    // this is a good place to notify the end-user that publishing has stopped.
}

/**
 * Instantiates a subscriber for the given stream and asynchronously begins the
 * process to begin receiving A/V content for this stream. Unlike doPublish,
 * this method does not add the subscriber to the view hierarchy. Instead, we
 * add the subscriber only after it has connected and begins receiving data.
 */
- (void)doSubscribe:(OTStream*)stream
{
    _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
    
    OTError *error = nil;
    [_session subscribe:_subscriber error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
}

/**
 * Cleans the subscriber from the view hierarchy, if any.
 * NB: You do *not* have to call unsubscribe in your controller in response to
 * a streamDestroyed event. Any subscribers (or the publisher) for a stream will
 * be automatically removed from the session during cleanup of the stream.
 */
- (void)cleanupSubscriber
{
    [_subscriber.view removeFromSuperview];
    _subscriber = nil;
}

# pragma mark - OTSession delegate callbacks

- (void)sessionDidConnect:(OTSession*)session
{
    NSLog(@"sessionDidConnect (%@)", session.sessionId);
    
    // Step 2: We have successfully connected, now instantiate a publisher and
    // begin pushing A/V streams into OpenTok.
    [self doPublish];
}

- (void)sessionDidDisconnect:(OTSession*)session
{
    NSString* alertMessage =
    [NSString stringWithFormat:@"Session disconnected: (%@)",
     session.sessionId];
    NSLog(@"sessionDidDisconnect (%@)", alertMessage);
}


- (void)session:(OTSession*)mySession
  streamCreated:(OTStream *)stream
{
    NSLog(@"session streamCreated (%@)", stream.streamId);
    
    // Step 3a: (if NO == subscribeToSelf): Begin subscribing to a stream we
    // have seen on the OpenTok session.
    if (nil == _subscriber && !subscribeToSelf)
    {
        [self doSubscribe:stream];
    }
}

- (void)session:(OTSession*)session
streamDestroyed:(OTStream *)stream
{
    NSLog(@"session streamDestroyed (%@)", stream.streamId);
    
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupSubscriber];
    }
    
    [self disconnectCall:nil];
    UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:@"Call Disconnected" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [errorAlert show];

}

- (void)  session:(OTSession *)session
connectionCreated:(OTConnection *)connection
{
    NSLog(@"session connectionCreated (%@)", connection.connectionId);
}

- (void)    session:(OTSession *)session
connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"session connectionDestroyed (%@)", connection.connectionId);
    if ([_subscriber.stream.connection.connectionId
         isEqualToString:connection.connectionId])
    {
        [self cleanupSubscriber];
    }
}

- (void) session:(OTSession*)session
didFailWithError:(OTError*)error
{
    NSLog(@"didFailWithError: (%@)", error);
    
     [_session disconnect:nil];
}

# pragma mark - OTSubscriber delegate callbacks

- (void)subscriberDidConnectToStream:(OTSubscriberKit*)subscriber
{
    NSLog(@"subscriberDidConnectToStream (%@)",
          subscriber.stream.connection.connectionId);
    
    assert(_subscriber == subscriber);
    //[_subscriber.view setFrame:CGRectMake(0, widgetHeight, widgetWidth,widgetHeight)];
    
    self.shouldShowOpenTokScreen=@"YES";
    
    [self.view bringSubviewToFront:self.connectionView];
    
    [self.connectionView removeFromSuperview];
    
    [_subscriber.view setFrame:CGRectMake(0, BAR_HEIGHT, self.view.frame.size.width,
                                          self.view.frame.size.height-2*BAR_HEIGHT)];
    [self.view addSubview:_subscriber.view];
    
    [self.view addSubview:_publisher.view];
    
    [_subscriber.view addGestureRecognizer:self.gesture];
}

- (void)subscriber:(OTSubscriberKit*)subscriber
  didFailWithError:(OTError*)error
{
    NSLog(@"subscriber %@ didFailWithError %@",
          subscriber.stream.streamId,
          error);
    
     [_session disconnect:nil];
}

# pragma mark - OTPublisher delegate callbacks

- (void)publisher:(OTPublisherKit *)publisher
    streamCreated:(OTStream *)stream
{
    // Step 3b: (if YES == subscribeToSelf): Our own publisher is now visible to
    // all participants in the OpenTok session. We will attempt to subscribe to
    // our own stream. Expect to see a slight delay in the subscriber video and
    // an echo of the audio coming from the device microphone.
    if (nil == _subscriber && subscribeToSelf)
    {
        [self doSubscribe:stream];
    }
}

- (void)publisher:(OTPublisherKit*)publisher
  streamDestroyed:(OTStream *)stream
{
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupSubscriber];
    }
    
    [self cleanupPublisher];
}

- (void)publisher:(OTPublisherKit*)publisher
 didFailWithError:(OTError*) error
{
    NSLog(@"publisher didFailWithError %@", error);
    [self cleanupPublisher];
}

- (void)showAlert:(NSString *)string
{
    // show alertview on main UI
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTError"
                                                        message:string
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil] ;
        [alert show];
    });
}


#pragma mark Top Bar View

-(void)setUpTopBarView

{
    
    NSTimer *timer=[[NSTimer alloc] init];
    
    [timer timeInterval];
    
    
    self.topBarView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, BAR_HEIGHT)];
    
    self.topBarView.backgroundColor=[UIColor blackColor];
    
    [self.view addSubview:self.topBarView];
    
    self.broadCasterLabel=[ControlsFactory getLabel];
    
    self.broadCasterLabel.text=@"Broadcaster";
    
    self.broadCasterLabel.textAlignment=NSTextAlignmentLeft;
    
    self.broadCasterLabel.textColor=[UIColor whiteColor];
    
    self.broadCasterLabel.frame=CGRectMake(0, 0, self.view.frame.size.width-SPEAKERBUTTON_WIDTH, 40);
    
    [self.topBarView addSubview:self.broadCasterLabel];
    
    self.audioSubUnsubButton=[ControlsFactory getButton];
    
    self.audioSubUnsubButton.backgroundColor=[UIColor clearColor];
    
  //  [self.audioSubUnsubButton setImage:[UIImage imageNamed:@"Subscriber-Speaker-35.png"] forState:UIControlStateNormal];
    
    [self.audioSubUnsubButton addTarget:self action:@selector(toggleAudioSubscribe:) forControlEvents:UIControlEventTouchUpInside];
    
    self.audioSubUnsubButton.frame=CGRectMake(self.broadCasterLabel.frame.size.width, 0, 90, 50);
    
    [self.topBarView addSubview:self.audioSubUnsubButton];
    
    
    
    
}

- (void)toggleAudioSubscribe:(UIButton*)sender
{
//    if (_subscriber.subscribeToAudio == YES) {
//        _subscriber.subscribeToAudio = NO;
//        self.audioSubUnsubButton.selected = YES;
//        [self.audioSubUnsubButton setImage:[UIImage imageNamed:@"Subscriber-Speaker-35.png"] forState:UIControlStateNormal];
//        
//    } else {
//        _subscriber.subscribeToAudio = YES;
//        self.audioSubUnsubButton.selected = NO;
//        [self.audioSubUnsubButton setImage:[UIImage imageNamed:@"Subscriber-Speaker-Mute-35.png"] forState:UIControlStateNormal];
//        
//    }
}


#pragma mark Bottom Bar View

-(void)setUpBottomBarView
{
    self.bottomBarView=[[UIView alloc] initWithFrame:CGRectMake(0,  self.view.frame.size.height-BAR_HEIGHT, self.view.frame.size.width, BAR_HEIGHT)];
    
    self.bottomBarView.backgroundColor=[UIColor blackColor];
    
    [self.view addSubview:self.bottomBarView];
    
    self.cameraToggleButton=[ControlsFactory getButton];
    
    self.cameraToggleButton.backgroundColor=[UIColor clearColor];
    
    [self.cameraToggleButton setImage:[UIImage imageNamed:@"camera_switch-33.png"] forState:UIControlStateNormal];
    
    [self.cameraToggleButton addTarget:self action:@selector(toggleCameraPosition:) forControlEvents:UIControlEventTouchUpInside];
    
    self.cameraToggleButton.frame=CGRectMake(0, 0, 90, 50);
    
    [self.bottomBarView addSubview:self.cameraToggleButton];
    
    self.endCallButton=[ControlsFactory getButton];
    
    [self.endCallButton setTitle:@"End Call" forState:UIControlStateNormal];
    
    [self.endCallButton setTintColor:[UIColor whiteColor]];
    
    [self.endCallButton addTarget:self action:@selector(disconnectCall:) forControlEvents:UIControlEventTouchUpInside];
    
    self.endCallButton.frame=CGRectMake(90, 0, self.view.frame.size.width-180, 50);
    
    [self.bottomBarView addSubview:self.endCallButton];
    
    self.audioPubUnpubButton =[ControlsFactory getButton];
    
    self.audioPubUnpubButton.backgroundColor=[UIColor clearColor];
    
    [self.audioPubUnpubButton setImage:[UIImage imageNamed:@"mic-24.png"] forState:UIControlStateNormal];
    
    [self.audioPubUnpubButton addTarget:self action:@selector(toggleAudioPublish:) forControlEvents:UIControlEventTouchUpInside];
    
    self.audioPubUnpubButton.frame=CGRectMake(self.cameraToggleButton.frame.size.width+self.endCallButton.frame.size.width, 0, 90, 50);
    
    [self.bottomBarView addSubview:self.audioPubUnpubButton];
    
    
    
}

# pragma mark Bottom Bar Buttons Implimentation
- (void)toggleCameraPosition:(UIButton*)sender
{
    if (_publisher.cameraPosition == AVCaptureDevicePositionBack) {
        _publisher.cameraPosition = AVCaptureDevicePositionFront;
        self.cameraToggleButton.selected = NO;
        self.cameraToggleButton.highlighted = NO;
        [self.cameraToggleButton setImage:[UIImage imageNamed:@"camera_switch-33.png"] forState:UIControlStateNormal];
    } else if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
        _publisher.cameraPosition = AVCaptureDevicePositionBack;
        self.cameraToggleButton.selected = YES;
        self.cameraToggleButton.highlighted = YES;
        [self.cameraToggleButton setImage:[UIImage imageNamed:@"camera-switch_black-33.png"] forState:UIControlStateNormal];
        
    }
}

-(void)disconnectCall:(UIButton*)button {
    
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"callclicked" object:self];
    
    _manager=[SocketManager sharedInstance];
    
    if ([_manager getCallStatus]) {
        
    
    
    NSLog(@"%@", self.navigationController.viewControllers);
    
    [_session disconnect:nil];

    [self.navigationController popViewControllerAnimated:YES];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^ {
        NSLog(@" Background work here");
        [self socketManagerCallDisconnected];
        dispatch_async(dispatch_get_main_queue(), ^ {
            NSLog(@"Back on main thread");
           
            
        });
    });
        
    }

    
    
}

- (void)toggleAudioPublish:(UIButton*)sender
{
    if (_publisher.publishAudio == YES) {
        _publisher.publishAudio = NO;
        self.audioPubUnpubButton.selected = YES;
        [self.audioPubUnpubButton setImage:[UIImage imageNamed:@"mic_muted-24.png"] forState:UIControlStateNormal];
        
    } else if (_publisher.publishAudio == NO){
        _publisher.publishAudio = YES;
        self.audioPubUnpubButton.selected = NO;
        [self.audioPubUnpubButton setImage:[UIImage imageNamed:@"mic-24.png"] forState:UIControlStateNormal];
        
    }
}

#pragma mark ChanegScreen Function

-(void) changeScreenSize {
    if (!isFullScreen) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.topBarView.frame =  CGRectMake(0, -BAR_HEIGHT, self.view.frame.size.width, BAR_HEIGHT);
            
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.bottomBarView.frame =  CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, BAR_HEIGHT);
            
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [ _subscriber.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [_publisher.view setFrame:CGRectMake(0, self.view.frame.size.height- self.view.frame.size.width/2.5, self.view.frame.size.width/2.5, self.view.frame.size.width/2.5)];
            
        }];
        
        
        
        
        isFullScreen=YES;
    }
    else{
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.topBarView.frame =CGRectMake(0, 0, self.view.frame.size.width, BAR_HEIGHT);
            
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.bottomBarView.frame =  CGRectMake(0,  self.view.frame.size.height-BAR_HEIGHT, self.view.frame.size.width, BAR_HEIGHT);
            
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [_subscriber.view setFrame:CGRectMake(0, BAR_HEIGHT, self.view.frame.size.width,
                                                  self.view.frame.size.height-2*BAR_HEIGHT)];

            
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [_publisher.view setFrame:CGRectMake(0, self.view.frame.size.height- self.view.frame.size.width/2.5-BAR_HEIGHT, self.view.frame.size.width/2.5, self.view.frame.size.width/2.5)];
            
        }];
        
        
        isFullScreen=NO;
        
        
        
        
    }
    
}

#pragma mark CallEndedByDoctor

-(void)callEndedByDoctor:(NSNotification*)note {
    
    
    
    NSLog(@"%@", self.navigationController.viewControllers);
    
    [_session disconnect:nil];
    
    NSLog(@"call deactivated by doctor");
    
    [self.navigationController popViewControllerAnimated:YES];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^ {
        NSLog(@" Background work here");
        [self socketManagerCallDisconnected];
        dispatch_async(dispatch_get_main_queue(), ^ {
            NSLog(@"Back on main thread");
        });
    });

    
    
}

-(void)socketManagerCallDisconnected {
    
    _manager=[SocketManager sharedInstance];

    
    NSLog(@"socketManager CallDicsonnected");
    
    [_manager disconnect];
    
    [_manager callDeactivated];
    
    
    
}


-(void)viewDidDisappear:(BOOL)animated{
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(removeConnectionScreen) object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"callEndedByDoctor" object:nil];
    
}


#pragma mark Connecting

-(void) setUpConnectionView{
    
    self.connectionView=[[UIView alloc] init];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview: self.connectionView];
    
    [ self.connectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
    self.callViewBG=[ControlsFactory getImageView];
    
    [ self.callViewBG setImage:[UIImage imageNamed:@"login_bg"]];
    
    [self.connectionView addSubview: self.callViewBG];
    
    [ self.callViewBG mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.connectionView.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.connectionView.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.connectionView.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.connectionView.mas_right).with.offset(-padding.right);
    }];
    
    padding = UIEdgeInsetsMake(70, 0, 0, 0);
    
    self.avatarImage=[ControlsFactory getImageView];
    
    [ self.avatarImage setImage:[UIImage imageNamed:@"incomingAvatar.jpg"]];
    
    self.avatarImage.layer.cornerRadius =75;
    _avatarImage.clipsToBounds =YES;
    
    
    [self.connectionView addSubview: self.avatarImage];
    
    [ self.avatarImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.connectionView.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.connectionView.mas_centerX);
        
        make.height.mas_equalTo(150);
        
        make.width.mas_equalTo(150);
    }];
    
    padding = UIEdgeInsetsMake(100, 0, 100, 0);
    
    self.connecting=[ControlsFactory getLabel];
    
    self.connecting.text=@"Setting up...";
    
    self.connecting.textAlignment=NSTextAlignmentCenter;
    
    self.connecting.textColor=[UIColor whiteColor];
    
    [self.connectionView addSubview: self.connecting];
    
    [ self.connecting mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.connectionView.mas_bottom).with.offset(-padding.bottom);
        
        make.centerX.mas_equalTo(self.connectionView.mas_centerX);
        
        make.height.mas_equalTo(80);
        
        make.width.mas_equalTo(200);
    }];
    
}


-(void)removeConnectionScreen{
    
    if ([self.shouldShowOpenTokScreen isEqualToString:@"NO"]) {
        NSLog(@"after 60 seconds end call as opentok was not generated");
        
         [self disconnectCall:nil];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Due to some technical reasons call cannot be initiated. Pleas try again after some time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        alert.tag=1;
        
        [alert show];

       
        
    }
}
-(void)appWentToBG{
    
    NSLog(@"Notification Fired to Put App in BG");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AppWentToBackGround object:nil];
    
    [self disconnectCall:nil];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        
        if (buttonIndex == [alertView cancelButtonIndex]){
            
             [self disconnectCall:nil];
        }
        
    }
    
    
}



@end
