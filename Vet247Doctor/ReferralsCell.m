//
//  ReferralsCell.m
//  Vet247Doctor
//
//  Created by APPLE on 8/18/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "Masonry.h"

#import "ReferralsCell.h"

@implementation ReferralsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.imageIcon=[[UIImageView alloc] init];
        
        self.imageIcon.backgroundColor=[UIColor clearColor];
        
        UIEdgeInsets paddingForImageView = UIEdgeInsetsMake(10, 15, 10, 0);
        
        [self.contentView addSubview:self.imageIcon];
        
        [self.imageIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.contentView.mas_left).with.offset(paddingForImageView.left);
            
            make.top.equalTo(self.contentView.mas_top).with.offset(paddingForImageView.top);
            
            make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-paddingForImageView.bottom);
            
            make.height.mas_equalTo(70);
            
            make.width.mas_equalTo(70);
        }];
        
        
        self.titleLbl =[[UILabel alloc] init];
        
        [self.titleLbl setNumberOfLines:1];
        
        [self.titleLbl setTextAlignment:NSTextAlignmentLeft];
        
        [self.titleLbl setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
        
        self.titleLbl.font=[UIFont systemFontOfSize:17.0];
        
        [self.titleLbl setBackgroundColor:[UIColor clearColor]];
        
        UIEdgeInsets paddingForTitleLbl = UIEdgeInsetsMake(25, 15, 0, 0);
        
        [self.contentView addSubview:self.titleLbl];
        
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.imageIcon.mas_right).with.offset(paddingForTitleLbl.left);
            
            make.top.equalTo(self.contentView.mas_top).with.offset(paddingForTitleLbl.top);
            
            make.height.mas_equalTo(25.0);
        }];
        
        
        self.descriptionText =[[UILabel alloc] init];
        
        [self.descriptionText setNumberOfLines:2];
        
        [self.descriptionText setTextColor:[UIColor colorWithRed:0.4196 green:0.4196 blue:0.4196 alpha:1.0]];
        
        self.descriptionText.backgroundColor = [UIColor clearColor];
        
        self.descriptionText.textAlignment = NSTextAlignmentLeft;
        
        self.descriptionText.font = [UIFont systemFontOfSize:13.0];
        
        self.descriptionText.lineBreakMode = NSLineBreakByTruncatingTail;
        
        [self.contentView addSubview:self.descriptionText];
        
        UIEdgeInsets paddingForMessageText = UIEdgeInsetsMake(5, 15, 8, 15);
        
        [self.descriptionText mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.imageIcon.mas_right).with.offset(paddingForMessageText.left);
            
            make.top.equalTo(self.contentView.mas_top).with.offset(45);
            
            make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-paddingForMessageText.bottom);
            
            make.right.equalTo(self.contentView.mas_right).with.offset(-paddingForMessageText.right);
            
            make.height.mas_equalTo(17);
        }];
        
    }
    return self;
}

@end
