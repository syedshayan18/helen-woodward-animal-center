//
//  ChatinboxViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 6/5/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatinboxViewController : UIViewController
@property (strong,nonatomic) NSMutableArray *chatinboxarray;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (strong,nonatomic) NSString *report_ID,*receiver_ID,*nameconcat,*passingSenderName;
@property (strong,nonatomic) UIImage *myavatarimagetobepassed,*senderavatarimagetobepassed;


@property NSInteger selectedindex;
@end
