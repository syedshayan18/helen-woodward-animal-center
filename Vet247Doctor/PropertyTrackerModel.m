//
//  PropertyTrackerModel.m
//  TMACC
//
//  Created by Muhammad Raza on 15/08/2016.
//  Copyright © 2016 Mortgage Alliance Company of Canada Inc. All rights reserved.
//

#import "PropertyTrackerModel.h"
#import "PetProfileController.h"
#import "PetdetailsViewController.h"
@implementation PropertyTrackerModel

-(id)initWithDictionary:(NSDictionary *)petdata{
    self = [super init];
    
    if (self) {
        self.animalname =petdata[@"animalName"];
        self.breed =petdata[@"breed" ];
        self.Sex =petdata[@"sex" ];
        self.stringage=petdata[@"age"];
        _age = [_stringage doubleValue];
        self.PetID = petdata[@"id"];
        self.ID =petdata[@"id"];
        self.Species = petdata[@"selectSpecies"];
        NSInteger speciesint = [_Species integerValue];
        _speciesint =speciesint;
        self.color = petdata[@"color"];
        _diet = petdata[@"diet"];
        _history = petdata[@"history"];
        _insurance = petdata[@"petInsurance"];
        _insurancecomp = petdata[@"insuranceCompany"];
        _medical = petdata[@"medication"];
        _trackernum = petdata[@"petTracker"];
        _trackercomp = petdata[@"trackerCompany"];
        _spayed = petdata[@"type"];
        _Weight =petdata[@"weight"];
        _allegeries = petdata[@"allergies"];
        _vaccinated = petdata[@"vaccinated"];
        _policynumber = petdata[@"policyNumber"];
        _pinnumber=petdata[@"pin_number"];
        _vert = petdata[@"veterinarian"];
        _name=petdata[@"veterinarian_name"];
        _phonoenum=petdata[@"veterinarian_phone"];
        _userID = petdata [@"userId"];
        
        

        _profileimage =petdata[@"image"];
        
    }
    
    return self;
}




@end
