//
//  PetfullimageViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/31/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PetfullimageViewController.h"

@interface PetfullimageViewController ()

@end

@implementation PetfullimageViewController
@synthesize  petimage;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%@",_img_fullpet.image);
    
    _img_fullpet.image = petimage;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    
    [[self navigationController] setNavigationBarHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:NO];
}

-(IBAction)btn_back:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
