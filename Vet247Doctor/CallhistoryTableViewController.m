//
//  CallhistoryTableViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "CallhistoryTableViewController.h"
#import "PetdetailsViewController.h"
#import "AFNetworking.h"
#import "UIImage+AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "CallhistoryTVC.h"
#import "MBProgressHUD.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NetworkingClass.h"


@interface CallhistoryTableViewController ()

@end

@implementation CallhistoryTableViewController
@synthesize useridhistory;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self webservice];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callclicked:)
                                                 name:@"callclicked"
                                               object:nil];
   
    
   // [self webservice];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:@"callclicked"];
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    
}
- (void)callclicked:(NSNotification *)note {
    [self webservice];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webservice {
    
    NSString *userid=[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    
    NSString *gethistoryurl = [NSString stringWithFormat:@"%@get_call_history.php?%@%@",BaseURL,@"uid=",userid];
    
    
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
   
    manager.responseSerializer  = [AFHTTPResponseSerializer serializer];
    
    [manager GET:gethistoryurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"success!%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        
        
       [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
      
        _callhistroy =[[NSMutableDictionary alloc]init];
        NSError* error;
        
        _callhistroy = [NSJSONSerialization JSONObjectWithData:responseObject
                                                       options:kNilOptions
                                                         error:&error];
        
        NSLog(@"%@",_callhistroy);
   

        _doctorinfo =[[NSDictionary alloc]init];
        _userdict = [[NSDictionary alloc]init];
        _petinfo = [[NSDictionary alloc]init];
         _datearray=[[NSMutableArray alloc]init];
         _imagearray=[[NSMutableArray alloc]init];
        _durationarray=[[NSMutableArray alloc]init];
        _currentdate=[[NSMutableArray alloc]init];
        _historyid=[[NSMutableArray alloc]init];
        for (NSDictionary *subarray in _callhistroy) {
            
           
            
        
            
            if (![[subarray objectForKey:@"duration"] isEqualToString:@"00:00:00"])
            {
                
                
        
                
                
                _doctorinfo = [subarray objectForKey:@"doctor"];
                
                _userdict = [subarray objectForKey:@"user"];
                
                
            
                
                _petinfo = [subarray objectForKey:@"pet"];
                
                if ([_petinfo isKindOfClass:[NSDictionary class]]){
                    
                    if ([_petinfo objectForKey:@"imagePath"]!=nil){
                        NSString *imagepath= [_petinfo objectForKey:@"imagePath"];
                        
                        [_imagearray addObject:imagepath];
                  
                    }
                    
                    else {
                        
                        [_imagearray addObject:@"noimage"];
                        
                        
                        
                    }
                    
                    
                    NSString *histid = [subarray objectForKey:@"id"];
                    [_historyid addObject:histid];
                    
                    
                    NSString *currentdate = [subarray objectForKey:@"current_date" ];
                    
                    
                    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
                    
                    [formatter1 setDateFormat:@"yyyy-MM-dd"];
                    
                    NSDate *date1 = [formatter1 dateFromString:currentdate];
                    
                    
                    
                    NSString *formatteddate1 = [formatter1 stringFromDate:date1];
                    
                    
                    
                    [_currentdate addObject:formatteddate1];
                    
                    
                    NSString *duration = [subarray objectForKey:@"duration"];
                    [_durationarray addObject:duration];
                    
                    

                    
                    
                    
                }
               
                
                
                

                }
            
        

            
            
            
               // tel == (NSString *)[NSNull null]
                //if (![[subarray objectForKey:@"current_date"] isEqual:[NSNull null]]){
                    
                    
            
                //}
               // else {
                    
                    
                    
                    //[_currentdate addObject:@"NA"];
                    
              //  }
                
                
//                NSDictionary *petdetails =[subarray objectForKey:@"pet"];
//                
//                if ([petdetails isKindOfClass:[NSDictionary class]]){
//                   
//   
//            }
            
        
           
            
            
        }
        NSLog(@"%i",_durationarray.count);
        NSLog(@"%i",_historyid.count);
        NSLog(@"%i",_imagearray.count);
      
          [self.tableView reloadData];
       
        if (_historyid.count <1){
            
            if (![self.navigationController.topViewController isKindOfClass:PetdetailsViewController.self]){
               
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"No Call History"
                                             message:@"Please click on Plus button to Call"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                }];
                
                [alert addAction:okAction];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }

            
        }

        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        if (![self.navigationController.topViewController isKindOfClass:PetdetailsViewController.self]){
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Error"
                                         message:@"Error in getting response from Server or Check your Internet Connection"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                
                
                
                
            }];
            
            [alert addAction:okAction];
            
            [self presentViewController:alert animated:YES completion:nil];
    
        }
        
        
        

        NSLog(@"Error: %@", error);
    }];
    
    
    
  
    
    
    
              }

    


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return _historyid.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CallhistoryTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    
        cell.lbl_date.text = [_currentdate objectAtIndex:indexPath.row];
        cell.lbl_calltime.text=[_durationarray objectAtIndex:indexPath.row];
        NSURL *imgurl = [[NSURL alloc]initWithString:[_imagearray objectAtIndex:indexPath.row]];
        [cell.img_pet sd_setImageWithURL:imgurl placeholderImage:[UIImage imageNamed:@"pet_list_image"]];
        cell.img_pet.layer.cornerRadius =25;
        cell.img_pet.clipsToBounds=YES;
        

    
    
    
    
    
    
    
    
    
    return cell;
    
    
   
   
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        
        NSString *deletehistory = [NSString stringWithFormat:@"%@%@",BaseURL,@"delete_call_history.php"];
        NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
        
        NSString *selectedhistory = [_historyid objectAtIndex:indexPath.row];
        
        NSInteger a = indexPath.row;
        NSString *key = [NSString stringWithFormat:@"%@%i%@",@"history[",a,@"]"];
       [body setObject:selectedhistory forKey:key];
        NSLog(@"%@",body);    //[body setObject:userid forKey:@"uid"];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NetworkingClass historyrequest:deletehistory params:body completion:^(id finished, NSError *error) {
            
            NSDictionary* response = [NSJSONSerialization JSONObjectWithData:finished
                                                                 options:kNilOptions
                                                                   error:&error];
            
          
            NSLog(@"%@",response);
            NSNumber *success =[response objectForKey:@"success"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            BOOL sucbool= [success boolValue];
            if (sucbool==NO){
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Failed to Delete History "
                                             message:@"Server Error or Check your Internet Connectivity"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                }];
                
                [alert addAction:okAction];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            else {
                
                  [self.historyid removeObjectAtIndex:indexPath.row];
                
            }
          
            [self.tableView reloadData];
   
        }];

        
        
        
        
        
        NSLog(@"%i",_historyid.count);// tell table to refresh now
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(IBAction)callpet:(UIBarButtonItem *)sender{
    
    [self performSegueWithIdentifier:@"callpet" sender:nil];
}

#pragma mark - Navigation

 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    
}


@end
