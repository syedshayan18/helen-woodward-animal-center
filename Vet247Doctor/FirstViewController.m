//
//  FirstViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/15/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "FirstViewController.h"
#import "AHContainerViewController.h"
#import "PetProfileKeys.h"
#import "PetProfileDataManager.h"
#import "PropertyTrackerModel.h"
#import "MyPetsViewController.h"
@interface FirstViewController ()
@property BOOL radioButton;
@property BOOL radioButton1;


@end

@implementation FirstViewController

    int width;
    int height;
    


- (void)viewDidLoad {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fieldsunable:)
                                                 name:@"fieldsunable"
                                               object:nil];

    
  

    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesturemale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(male:)];
    
    [self.lbl_male addGestureRecognizer:tapGesturemale];
    
    UITapGestureRecognizer *tapGesturefemale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(female:)];
    
    [self.lbl_female addGestureRecognizer:tapGesturefemale];
    
    UITapGestureRecognizer *tapGesturespayed = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(spayed:)];
    
    [self.lbl_spayed addGestureRecognizer:tapGesturespayed];
    
    UITapGestureRecognizer *tapGestureneutered = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(neutered:)];
    
    [self.lbl_neutered addGestureRecognizer:tapGestureneutered];
    
    
     UITapGestureRecognizer *taptackedlbl = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tacked:)];
    
    [self.lbl_tacked addGestureRecognizer:taptackedlbl];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    

    
    [self.view addGestureRecognizer:tap];
       _speciesFieldValues = @[@"Dog", @"Cat"];
    self.txt_age.keyboardType = UIKeyboardTypeNumberPad;
    self.txt_weight.keyboardType = UIKeyboardTypePhonePad;
    NSAttributedString *animal = [[NSAttributedString alloc] initWithString:@"ANIMAL NAME" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_animal.attributedPlaceholder = animal;
    NSAttributedString *species = [[NSAttributedString alloc] initWithString:@"SPECIES" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_species.attributedPlaceholder = species;
    NSAttributedString *breed = [[NSAttributedString alloc] initWithString:@"BREED" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_breed.attributedPlaceholder = breed;
    NSAttributedString *age = [[NSAttributedString alloc] initWithString:@"AGE" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_age.attributedPlaceholder = age;
    NSAttributedString *color = [[NSAttributedString alloc] initWithString:@"COLOR" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_color.attributedPlaceholder = color;
    NSAttributedString *weight = [[NSAttributedString alloc] initWithString:@"WEIGHT LBS" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:7.0/255.0f green:22.0/255.0f blue:137.0/255.0f alpha:1.0] }];
    self.txt_weight.attributedPlaceholder = weight;
    // Do any additional setup after loading the view.
 //   if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
   // {
        
        [self populateDataToView];
        
 //   }
    
    UIPickerView *speciespicker = [[UIPickerView alloc] init];
    speciespicker.delegate = self;
    speciespicker.dataSource = self;
     [self.txt_species setInputView:speciespicker];
    
    
    
    
}

- (void)fieldsunable:(NSNotification *)note {
 
    
    _txt_age.enabled =YES;
    _txt_species.enabled=YES;
    _txt_breed.enabled=YES;
    _txt_color.enabled=YES;
    _txt_weight.enabled=YES;
    _txt_animal.enabled = YES;
    _btn_male.userInteractionEnabled=YES;
    _btn_female.userInteractionEnabled=YES;
    _btn_spayrd.userInteractionEnabled=YES;
    _btn_neutered.userInteractionEnabled=YES;
    
    _lbl_male.userInteractionEnabled=YES;
    _lbl_female.userInteractionEnabled=YES;
    _lbl_spayed.userInteractionEnabled=YES;
    _lbl_neutered.userInteractionEnabled=YES;

}

-(void)viewDidAppear:(BOOL)animated {
    AHContainerViewController *container = (AHContainerViewController*) self.parentViewController;
    MyPetsViewController *mypet = (MyPetsViewController*) container.parentViewController;

    if (mypet.btn_edit==YES){
    _txt_age.enabled =YES;
    _txt_species.enabled=YES;
    _txt_breed.enabled=YES;
    _txt_color.enabled=YES;
    _txt_weight.enabled=YES;
    _txt_animal.enabled = YES;
    _btn_male.userInteractionEnabled=YES;
    _btn_female.userInteractionEnabled=YES;
    _btn_spayrd.userInteractionEnabled=YES;
    _btn_neutered.userInteractionEnabled=YES;
    
    _lbl_male.userInteractionEnabled=YES;
    _lbl_female.userInteractionEnabled=YES;
    _lbl_spayed.userInteractionEnabled=YES;
    _lbl_neutered.userInteractionEnabled=YES;
    
    }
    
   
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    self.txt_animal.text = [NSPetData objectForKey:AnimalNameKey];
    
    //NSLog(@"\n%d\n",(int)[NSPetData objectForKey:@"selectSpecies"]);
    
    
    self.txt_breed.text = [NSPetData objectForKey:AnimalBreadKey];
    
    BOOL tacked = [[NSPetData objectForKey:@"tacked"] boolValue];
    if (tacked){
        [self.btn_tacked setSelected:YES];
    }
    
    if ([[NSPetData objectForKey:AnimalSexKey] isEqualToString:@"Male"]) {
        
        [self.btn_male setSelected:YES];
    }
    
    else if ([[NSPetData objectForKey:AnimalSexKey] isEqualToString:@"Female"])
    {
        [self.btn_female setSelected:YES];
    }
    
    if ([[NSPetData objectForKey:AnimalNeuteredOrSpayedKey] isEqualToString:@"Spayed"]) {
        
        [self.btn_spayrd setSelected:YES];
    }
    
    else if ([[NSPetData objectForKey:AnimalNeuteredOrSpayedKey] isEqualToString:@"Neutered"])
    {
        [self.btn_neutered setSelected:YES];
    }
    
    self.txt_age.text = [NSPetData objectForKey:AnimalAgeKey];
    
    self.txt_color.text = [NSPetData objectForKey:AnimalColorKey];
    
    self.txt_weight.text = [NSPetData objectForKey:AnimalWeightKey];
    
    
}





- (BOOL) checkGeneralInfoTextField

{
    
    //NSString *phoneRegEx = @"[0-9]{1,11}";
    
    //NSPredicate *ageTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
   // UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    
    UIAlertController * checkGeneralInfoFieldsErrorAlert =   [UIAlertController
                                                              alertControllerWithTitle:@"Error"
                                                              message:@""
                                                              preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [checkGeneralInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [checkGeneralInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [checkGeneralInfoFieldsErrorAlert addAction:ok];
    [checkGeneralInfoFieldsErrorAlert addAction:cancel];
    
    NSLog(@"_txt_animal.text = %@", self.txt_animal.text);
    if (self.txt_animal.text && _txt_animal.text.length < 1) {
        
//        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Last Name Field Empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [errorAlert show];
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Name"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.txt_species.text && self.txt_species.text.length<1) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please select Pet's specie"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.txt_breed.text && self.txt_breed.text.length<1) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Breed"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.btn_female.isSelected==NO  && self.btn_male.isSelected==NO) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please select sex"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.btn_spayrd.isSelected==NO && self.btn_neutered.isSelected==NO&& self.btn_male.isSelected==YES) {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please answer neutered or spayed"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    else if (self.txt_age.text && self.txt_age.text.length < 1)
    {
        
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Age"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    //    else if ([ageTest evaluateWithObject:self.ageTextField.text] == NO) {
    //
    //        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter valid age"];
    //
    //        [errorShowViewController presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
    //
    //        return  NO;
    //    }
    //
    else if (self.txt_color.text && self.txt_color.text.length<1)
    {
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's Color"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
    else if (self.txt_weight.text && self.txt_weight.text.length < 1)
    {
        [checkGeneralInfoFieldsErrorAlert setMessage:@"Please enter Pet's weight"];
        
        [self presentViewController:checkGeneralInfoFieldsErrorAlert animated:YES completion:nil];
        
        return  NO;
    }
    
        // adding data to NSMutable dictionary for ultimately sending over server
    
    [PetProfileDataManager removePetProfileObjectForKey:@"selectSpecies"];
    
    PetProfileDataManager * generalInfoDictionary = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    [generalInfoDictionary insertPetProfileInfo:self.txt_animal.text :AnimalNameKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.txt_species.text :AnimalSpeciesKey];
    
    [generalInfoDictionary insertPetProfileInfo: self.txt_breed.text :AnimalBreadKey];
    
    NSLog(@"%@",_firstpet.PetID);
    NSString *a = _firstpet.PetID;
    
    if (a !=nil) {
        
        [generalInfoDictionary insertPetProfileInfo:self.firstpet.PetID:PetIdKey];
        
    }
    
    else
    {
        [generalInfoDictionary insertPetProfileInfo:@"-1" :PetIdKey];
    }
    
    if(self.btn_male.isSelected==YES)
    {
        [generalInfoDictionary insertPetProfileInfo:@"Male" :AnimalSexKey];
    }
    else
    {
        [generalInfoDictionary insertPetProfileInfo:@"Female" :AnimalSexKey];
    }
    
    if(self.btn_neutered.isSelected==YES)
    {
        [generalInfoDictionary insertPetProfileInfo:@"Neutered" :AnimalNeuteredOrSpayedKey];
    }
    if (self.btn_spayrd.isSelected==YES)
    {
        [generalInfoDictionary insertPetProfileInfo:@"Spayed" :AnimalNeuteredOrSpayedKey];
    }
    
    if (self.btn_tacked.isSelected==YES) {
        
        int tacked = 1;
        [generalInfoDictionary inserttacked:tacked :@"tacked"];
        
        
    }
    else {
        int tacked = 0;
        [generalInfoDictionary inserttacked:tacked :@"tacked"];
    }
    
    
    [generalInfoDictionary insertPetProfileInfo:self.txt_age.text :AnimalAgeKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.txt_color.text :AnimalColorKey];
    
    [generalInfoDictionary insertPetProfileInfo:self.txt_weight.text :AnimalWeightKey];
    
    return  YES;
    
    
}//end Check Fields value





- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (row)
    {
        case 0:
            self.txt_species.text = @"Cat";
            return @"Cat";
        case 1:
            self.txt_species.text = @"Dog";
            return @"dog";
      
            
        default: return nil;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:self.txt_animal]) {
        
        [self.txt_species becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_species])
    {
        [self.txt_breed becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_breed])
    {
        
        [self.txt_age becomeFirstResponder];
        
    }
    else if ([textField isEqual:self.txt_age])
    {
        
        [self.txt_color becomeFirstResponder];
        
    }
    
    else if ([textField isEqual:self.txt_color])
    {
        
        [self.txt_weight becomeFirstResponder];
        
    }
    else if (textField== self.txt_weight) {
        
        [textField resignFirstResponder];
        
    }
    
    return YES;
}


-(void)male:(UIGestureRecognizer *)sender {
    if (self.btn_male.isSelected==NO && self.btn_female.isSelected ==NO)
    {
        
        
        [self.btn_male setSelected:YES];
        
    }
    
    else if (self.btn_male.isSelected==NO &&self.btn_female.isSelected ==YES )
    {
        
//        [_lbl_neutered setHidden:NO];
//        [_lbl_spayed setHidden:NO];
//        [self.btn_neutered setAlpha:1.0f];
//        [self.btn_spayrd setAlpha:1.0f];
//        
        
        [self.btn_male setSelected:YES];
        
        [self.btn_female setSelected:NO];
    }

    
}

-(void)female:(UIGestureRecognizer *)sender{
    if (self.btn_female.isSelected==NO && self.btn_male.isSelected ==NO)
    {
        
        [self.btn_female setSelected:YES];
//        
//        [_lbl_neutered setHidden:YES];
//        [_lbl_spayed setHidden:YES];
//        [self.btn_neutered setAlpha:0.0f];
//        [self.btn_spayrd setAlpha:0.0f];
        
    }
    
    else if (self.btn_female.isSelected==NO &&self.btn_male.isSelected ==YES )
    {
        [self.btn_female setSelected:YES];
        [self.btn_male setSelected:NO];
        
//        [_lbl_neutered setHidden:YES];
//        [_lbl_spayed setHidden:YES];
//        [self.btn_neutered setAlpha:0.0f];
//        [self.btn_spayrd setAlpha:0.0f];
    }
    

}

-(void)spayed:(UIGestureRecognizer *)sender{
    if (self.btn_spayrd.isSelected==NO && self.btn_neutered.isSelected ==NO)
    {
        [self.btn_spayrd setSelected:YES];
    }
    
    else if (self.btn_spayrd.isSelected==NO &&self.btn_neutered.isSelected ==YES )
    {
        [self.btn_spayrd setSelected:YES];
        
        [self.btn_neutered setSelected:NO];
    }
    
    
    

}

-(void)neutered:(UIGestureRecognizer *)sender {
    if (self.btn_neutered.isSelected==NO && self.btn_spayrd.isSelected ==NO)
    {
        [self.btn_neutered setSelected:YES];
    }
    
    else if (self.btn_neutered.isSelected==NO &&self.btn_spayrd.isSelected ==YES  )
    {
        [self.btn_neutered setSelected:YES];
        
        [self.btn_spayrd setSelected:NO];
    }
    

}

-(void)tacked:(UIGestureRecognizer *)sender {
   
    
    
    self.radioButton = !self.radioButton;
    [_btn_tacked setSelected:self.radioButton];
}

-(IBAction)btn_tacked:(UIButton *)sender{
    
    self.radioButton = !self.radioButton;
    [_btn_tacked setSelected:self.radioButton];
    
    
}

- (IBAction)btn_male:( UIButton *)sender {
    
    if (self.btn_male.isSelected==NO && self.btn_female.isSelected ==NO)
    {
        
       
        [self.btn_male setSelected:YES];
      
    }
    
    else if (self.btn_male.isSelected==NO &&self.btn_female.isSelected ==YES )
    {
        
//        [_lbl_neutered setHidden:NO];
//        [_lbl_spayed setHidden:NO];
//        [self.btn_neutered setAlpha:1.0f];
//        [self.btn_spayrd setAlpha:1.0f];
        [self.btn_male setSelected:YES];
        
        [self.btn_female setSelected:NO];
    }

    
    
}

- (IBAction)btn_female:( UIButton *)sender {
    
    if (self.btn_female.isSelected==NO && self.btn_male.isSelected ==NO)
    {
        
        [self.btn_female setSelected:YES];
        
//         [_lbl_neutered setHidden:YES];
//         [_lbl_spayed setHidden:YES];
//        [self.btn_neutered setAlpha:0.0f];
//        [self.btn_spayrd setAlpha:0.0f];
        
    }
    
    else if (self.btn_female.isSelected==NO &&self.btn_male.isSelected ==YES )
    {
        [self.btn_female setSelected:YES];
        
        [self.btn_male setSelected:NO];
       // [_lbl_neutered setHidden:YES];
       // [_lbl_spayed setHidden:YES];
     //   [self.btn_neutered setAlpha:0.0f];
       // [self.btn_spayrd setAlpha:0.0f];
    }

    
    
}

- (IBAction)btn_neutered:( UIButton *)sender {
    
    
    
    if (self.btn_neutered.isSelected==NO && self.btn_spayrd.isSelected ==NO)
    {
        [self.btn_neutered setSelected:YES];
    }
    
    else if (self.btn_neutered.isSelected==NO &&self.btn_spayrd.isSelected ==YES )
    {
        [self.btn_neutered setSelected:YES];
        
        [self.btn_spayrd setSelected:NO];
    }
    
    
}

- (IBAction)btn_spayred:( UIButton *)sender {
    
    if (self.btn_spayrd.isSelected==NO && self.btn_neutered.isSelected ==NO)
    {
        [self.btn_spayrd setSelected:YES];
    }
    
    else if (self.btn_spayrd.isSelected==NO &&self.btn_neutered.isSelected ==YES )
    {
        [self.btn_spayrd setSelected:YES];
        
        [self.btn_neutered setSelected:NO];
    }
    
  
    }


    

    
    



-(void)dismissKeyboard
{
    [_txt_age resignFirstResponder];
    [_txt_animal resignFirstResponder];
    [_txt_breed resignFirstResponder];
    [_txt_color resignFirstResponder];
    [_txt_species resignFirstResponder];
    [_txt_weight resignFirstResponder];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
