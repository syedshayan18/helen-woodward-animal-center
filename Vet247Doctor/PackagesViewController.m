//
//  PackagesViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 3/14/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PackagesViewController.h"
#import "AppDelegate.h"
#import "PromotionalCodeUtills.h"
#import "ZZMainViewController.h"
#import "PaymentController.h"
@interface PackagesViewController ()

@end

@implementation PackagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    
    _btn_coupon.layer.cornerRadius = 15;
    _btn_coupon.clipsToBounds=YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(package32:)];
    
    [self.view_3months addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(package3:)];
    
    [self.view_1day addGestureRecognizer:tapGesture1];
    
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(package12:)];
    
    [self.view_1month addGestureRecognizer:tapGesture2];
    
    
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(package60:)];
    
    [self.view_6months addGestureRecognizer:tapGesture3];
    
    UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(package100:)];
    
    [self.view_1year addGestureRecognizer:tapGesture4];
    
    
    UITapGestureRecognizer *tapGesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(packagefree:)];
    
    [self.view_free addGestureRecognizer:tapGesture5];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Btn_3months:(UIButton *)sender {
    NSLog(@"button 3");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"32.85" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 3;
    
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    //  [self presentViewController:paypalController animated:YES completion:nil];
    
    [self.navigationController pushViewController:paypalController animated:YES];
    // PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    // [self.navigationController pushViewController:paymentTermsController animated:YES];

}

-(void)viewDidAppear:(BOOL)animated{
    
      NSLog(@"image frame:%@", NSStringFromCGRect(_img_line.frame));
}

-(void)package32:(UIGestureRecognizer *)sender
{
    NSLog(@"button 3");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"32.85" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 3;
    
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
  //  [self presentViewController:paypalController animated:YES completion:nil];

   [self.navigationController pushViewController:paypalController animated:YES];
   // PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
   // [self.navigationController pushViewController:paymentTermsController animated:YES];
}

-(IBAction)Btn_1day:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"3.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 1;
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    // [self presentViewController:paypalController animated:YES completion:nil];
    
    [self.navigationController pushViewController:paypalController animated:YES];
 
    
}


-(void)package3:(UIGestureRecognizer *)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"3.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 1;
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
     // [self presentViewController:paypalController animated:YES completion:nil];
    
    [self.navigationController pushViewController:paypalController animated:YES];
}


-(IBAction)Btn_1month:(UIButton *)sender {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"12.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 2;
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];

}


-(void)package12:(UIGestureRecognizer *)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"12.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 2;
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];
    
}


-(IBAction)Btn_6months:(UIButton *)sender {
    NSLog(@"button 4");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"60" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 4;
    
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];
}

-(void)package60:(UIGestureRecognizer *)sender
{
    
    NSLog(@"button 4");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"60" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 4;
    
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];
      }
-(IBAction)btn_1year:(UIButton *)sender{
    NSLog(@"button 5");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"107" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 5;
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];

}

-(void)package100:(UIGestureRecognizer *)sender
{
    NSLog(@"button 5");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"107" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 5;

    
    ZZMainViewController *paypalController = [[ZZMainViewController alloc] init];
    [self.navigationController pushViewController:paypalController animated:YES];
}

-(IBAction)btn_free:(UIButton *)sender {
     [self performSegueWithIdentifier:@"unwindToContainerVC" sender:nil];
}

-(void)packagefree:(UIGestureRecognizer *)sender
{
    [self performSegueWithIdentifier:@"unwindToContainerVC" sender:nil];
}



-(IBAction)couponbtn:(UIButton *)sender{
    
    [self performSegueWithIdentifier:@"paymentscreen" sender:nil];
    
    //PaymentController *paymentController = [[PaymentController alloc] init];
   // [self presentViewController:paymentController animated:YES completion:nil];
    //[self.navigationController pushViewController:paymentController animated:YES];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
