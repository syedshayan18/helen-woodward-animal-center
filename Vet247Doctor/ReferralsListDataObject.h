//
//  ReferralsListDataObject.h
//  Vet247Doctor
//
//  Created by APPLE on 9/17/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#define Referrals_Id @"id"

#define Referrals_Name @"name"

#define Referrals_Description @"description"

#define Referrals_Image @"image"

#import <Foundation/Foundation.h>

@interface ReferralsListDataObject : NSObject

@property (strong, nonatomic) NSString * referralsId;

@property (strong, nonatomic) NSString * referralsName;

@property (strong, nonatomic) NSString * referralsDecription;

@property (strong, nonatomic) NSString * referralsImage;

@end
