//
//  PetProfileInsuranceSectionView.m
//  Vet247Doctor
//
//  Created by APPLE on 7/31/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfileInsuranceSectionView.h"

#import "ControlsFactory.h"

#import "Define.h"

#import "Masonry.h"

#import "PetProfileKeys.h"

#import "PetProfileDataManager.h"

@implementation PetProfileInsuranceSectionView


- (id) initWithFrame:(CGRect)frame1 {
    
    self = [super initWithFrame:frame1];
    if (self) {
        
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void) addComponents {
    
    self.backgroundColor=[UIColor clearColor];
    
    self.selectedImage = [[UIImage alloc] init];
    
    self.selectedImage = [UIImage imageNamed:@"selectedButton.png"];
    
    self.unSelectedImage = [[UIImage alloc] init];
    
    self.unSelectedImage = [UIImage imageNamed:@"unSelectedButton.png"];
    
    [self setUpPetInsuranceField];
    
    [self setUpPetTrackerField];
    
    [self setUpVeterinarianField];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Reload_PETDATA"] isEqualToString:@"Yes"])
    {
        
        [self populateDataToView];
        
    }
    
}

-(void) setUpPetInsuranceField
{
    self.petInsuranceFieldBGImage = [ControlsFactory getImageView];
    
    self.petInsuranceFieldBGImage.image = [UIImage imageNamed:@"petInsuranceBGImage.png"];
    
    self.petInsuranceFieldBGImage.backgroundColor = [UIColor clearColor];
    
    self.petInsuranceFieldBGImage.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.petInsuranceFieldBGImage];
    
    [self.petInsuranceFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.height.mas_equalTo(180);
        
    }];
    
    // setting up Insurance YES/NO seletion field lables and button
    
    
    // setting YES button
    
    self.petInsuranceYesButton = [ControlsFactory getButton];
    
    self.petInsuranceYesButton.backgroundColor = [UIColor clearColor];
    
    [self.petInsuranceYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petInsuranceYesButton addTarget:self action:@selector(petInsuranceYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25, 40, 5, 7.5);
    
    [self addSubview:self.petInsuranceYesButton];
    
    [self.petInsuranceYesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petInsuranceFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.petInsuranceFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.petInsuranceFieldBGImage.mas_centerX).with.offset(-60);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting YES Lable
    
    self.petInsuranceYesLable = [ControlsFactory getLabel];
    
    self.petInsuranceYesLable.backgroundColor = [UIColor clearColor];
    
    [self.petInsuranceYesLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petInsuranceYesLable.text = @"Yes";
    
    self.petInsuranceYesLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 30, 5, 7.5);
    
    [self addSubview:self.petInsuranceYesLable];
    
    [self.petInsuranceYesLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petInsuranceFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petInsuranceFieldBGImage.mas_centerX).with.offset(-28);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(35);
        
    }];
    
    // setting NO Button
    
    self.petInsuranceNoButton = [ControlsFactory getButton];
    
    self.petInsuranceNoButton.backgroundColor = [UIColor clearColor];
    
    [self.petInsuranceNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petInsuranceNoButton addTarget:self action:@selector(petInsuranceNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25,15,0,0);
    
    [self addSubview:self.petInsuranceNoButton];
    
    [self.petInsuranceNoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petInsuranceFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petInsuranceYesLable.mas_right).with.offset(30);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting NO button's Lable
    
    self.petInsuranceNoLable = [ControlsFactory getLabel];
    
    self.petInsuranceNoLable.backgroundColor = [UIColor clearColor];
    
    [self.petInsuranceNoLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petInsuranceNoLable.text = @"No";
    
    self.petInsuranceNoLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 40, 5, 7.5);
    
    [self addSubview:self.petInsuranceNoLable];
    
    [self.petInsuranceNoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petInsuranceFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petInsuranceFieldBGImage.mas_centerX).with.offset(72);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
    
    
    // Pet Insurance Compnay Name text field view
    
    self.petInsuranceCompanyTextField = [ControlsFactory getTextField];
    
    self.petInsuranceCompanyTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petInsuranceCompanyTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petInsuranceCompanyTextField.placeholder = @"Insurance Company";
    
    self.petInsuranceCompanyTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petInsuranceCompanyTextField.delegate = self;
    
    [self.petInsuranceCompanyTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petInsuranceCompanyTextField.enabled = YES;
    
    padding = UIEdgeInsetsMake(12, 7.5, 000 , 7.5);
    
    [self addSubview:self.petInsuranceCompanyTextField];
    
    [self.petInsuranceCompanyTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petInsuranceYesButton.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petInsuranceFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petInsuranceFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
    
    // Pet Insurance Policy Number text field view
    
    self.petInsurancePolicyNumberTextField = [ControlsFactory getTextField];
    
    self.petInsurancePolicyNumberTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petInsurancePolicyNumberTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petInsurancePolicyNumberTextField.placeholder = @"Policy Number";
    
    self.petInsurancePolicyNumberTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petInsurancePolicyNumberTextField.delegate = self;
    
    [self.petInsurancePolicyNumberTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petInsuranceCompanyTextField.enabled = YES;
    
    padding = UIEdgeInsetsMake(10, 7.5, 000 , 7.5);
    
    [self addSubview:self.petInsurancePolicyNumberTextField];
    
    [self.petInsurancePolicyNumberTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petInsuranceCompanyTextField.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petInsuranceFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petInsuranceFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
}

-(void) setUpPetTrackerField
{
    
    self.petTrackerFieldBGImage = [ControlsFactory getImageView];
    
    self.petTrackerFieldBGImage.image = [UIImage imageNamed:@"petTrackerBGImage.png"];
    
    self.petTrackerFieldBGImage.backgroundColor = [UIColor clearColor];
    
    self.petTrackerFieldBGImage.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.petTrackerFieldBGImage];
    
    [self.petTrackerFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petInsuranceFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.height.mas_equalTo(180);
        
    }];
    
    // setting up Pet Tracker YES/NO seletion field lables and button
    
    
    // setting YES button
    
    self.petTrackerYesButton = [ControlsFactory getButton];
    
    self.petTrackerYesButton.backgroundColor = [UIColor clearColor];
    
    [self.petTrackerYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petTrackerYesButton addTarget:self action:@selector(petTrackerYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25, 40, 5, 7.5);
    
    [self addSubview:self.petTrackerYesButton];
    
    [self.petTrackerYesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petTrackerFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.petTrackerFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.petTrackerFieldBGImage.mas_centerX).with.offset(-60);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting YES Lable
    
    self.petTrackerYesLable = [ControlsFactory getLabel];
    
    self.petTrackerYesLable.backgroundColor = [UIColor clearColor];
    
    [self.petTrackerYesLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petTrackerYesLable.text = @"Yes";
    
    self.petTrackerYesLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 30, 5, 7.5);
    
    [self addSubview:self.petTrackerYesLable];
    
    [self.petTrackerYesLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petTrackerFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petTrackerFieldBGImage.mas_centerX).with.offset(-28);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(35);
        
    }];
    
    // setting NO Button
    
    self.petTrackerNoButton = [ControlsFactory getButton];
    
    self.petTrackerNoButton.backgroundColor = [UIColor clearColor];
    
    [self.petTrackerNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petTrackerNoButton addTarget:self action:@selector(petTrackerNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25,15,0,0);
    
    [self addSubview:self.petTrackerNoButton];
    
    [self.petTrackerNoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petTrackerFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petTrackerYesLable.mas_right).with.offset(30);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting NO button's Lable
    
    self.petTrackerNoLable = [ControlsFactory getLabel];
    
    self.petTrackerNoLable.backgroundColor = [UIColor clearColor];
    
    [self.petTrackerNoLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petTrackerNoLable.text = @"No";
    
    self.petTrackerNoLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 40, 5, 7.5);
    
    [self addSubview:self.petTrackerNoLable];
    
    [self.petTrackerNoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petTrackerFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petTrackerFieldBGImage.mas_centerX).with.offset(72);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
    
    
    // Pet Tracker Compnay Name text field view
    
    self.petTrackerCompanyTextField = [ControlsFactory getTextField];
    
    self.petTrackerCompanyTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petTrackerCompanyTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petTrackerCompanyTextField.placeholder = @"Tracker Company";
    
    self.petTrackerCompanyTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petTrackerCompanyTextField.delegate = self;
    
    [self.petTrackerCompanyTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petTrackerCompanyTextField.enabled = YES;
    
    padding = UIEdgeInsetsMake(10, 7.5, 000 , 7.5);
    
    [self addSubview:self.petTrackerCompanyTextField];
    
    [self.petTrackerCompanyTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petTrackerYesButton.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petTrackerFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petTrackerFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
    
    // Pet Tracker Pin Number text field view
    
    self.petTrackerPinNumberTextField = [ControlsFactory getTextField];
    
    self.petTrackerPinNumberTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petTrackerPinNumberTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petTrackerPinNumberTextField.placeholder = @"Pin Number";
    
    self.petTrackerPinNumberTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petTrackerPinNumberTextField.delegate = self;
    
    [self.petTrackerPinNumberTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petTrackerPinNumberTextField.enabled = YES;
    
    padding = UIEdgeInsetsMake(10, 7.5, 000 , 7.5);
    
    [self addSubview:self.petTrackerPinNumberTextField];
    
    [self.petTrackerPinNumberTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petTrackerCompanyTextField.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petTrackerFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petTrackerFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
}

-(void) setUpVeterinarianField
{
    
    self.petVeterinarionFieldBGImage = [ControlsFactory getImageView];
    
    self.petVeterinarionFieldBGImage.image = [UIImage imageNamed:@"petVeterinarianBGImage.png"];
    
    self.petVeterinarionFieldBGImage.backgroundColor = [UIColor clearColor];
    
    self.petVeterinarionFieldBGImage.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.petVeterinarionFieldBGImage];
    
    [self.petVeterinarionFieldBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petTrackerFieldBGImage.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.height.mas_equalTo(180);
        
        make.bottom.equalTo(self.mas_bottom).with.offset(-5);
        
    }];
    
    // setting up Pet Veterinarian YES/NO seletion field lables and button
    
    
    // setting YES button
    
    self.petVeterinarianYesButton = [ControlsFactory getButton];
    
    self.petVeterinarianYesButton.backgroundColor = [UIColor clearColor];
    
    [self.petVeterinarianYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petVeterinarianYesButton addTarget:self action:@selector(petVeterinarianYesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25, 40, 5, 7.5);
    
    [self addSubview:self.petVeterinarianYesButton];
    
    [self.petVeterinarianYesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petVeterinarionFieldBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.petVeterinarionFieldBGImage.mas_left).with.offset(padding.left);
        
        make.centerX.mas_equalTo(self.petVeterinarionFieldBGImage.mas_centerX).with.offset(-60);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting YES Lable
    
    self.petVeterinarianYesLable = [ControlsFactory getLabel];
    
    self.petVeterinarianYesLable.backgroundColor = [UIColor clearColor];
    
    [self.petVeterinarianYesLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petVeterinarianYesLable.text = @"Yes";
    
    self.petVeterinarianYesLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 30, 5, 7.5);
    
    [self addSubview:self.petVeterinarianYesLable];
    
    [self.petVeterinarianYesLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petVeterinarionFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petVeterinarionFieldBGImage.mas_centerX).with.offset(-28);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(35);
        
    }];
    
    // setting NO Button
    
    self.petVeterinarianNoButton = [ControlsFactory getButton];
    
    self.petVeterinarianNoButton.backgroundColor = [UIColor clearColor];
    
    [self.petVeterinarianNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    
    [self.petVeterinarianNoButton addTarget:self action:@selector(petVeterinarianNoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(25,15,0,0);
    
    [self addSubview:self.petVeterinarianNoButton];
    
    [self.petVeterinarianNoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petVeterinarionFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petVeterinarianYesLable.mas_right).with.offset(30);
        
        make.width.mas_equalTo(19);
        
        make.height.mas_equalTo(19);
        
    }];
    
    // setting NO button's Lable
    
    self.petVeterinarianNoLable = [ControlsFactory getLabel];
    
    self.petVeterinarianNoLable.backgroundColor = [UIColor clearColor];
    
    [self.petVeterinarianNoLable setFont:[UIFont boldSystemFontOfSize:19]];
    
    self.petVeterinarianNoLable.text = @"No";
    
    self.petVeterinarianNoLable.textColor = [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    padding = UIEdgeInsetsMake(17, 40, 5, 7.5);
    
    [self addSubview:self.petVeterinarianNoLable];
    
    [self.petVeterinarianNoLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petVeterinarionFieldBGImage.mas_top).with.offset(padding.top);
        
        make.centerX.mas_equalTo(self.petVeterinarionFieldBGImage.mas_centerX).with.offset(72);
        
        make.height.mas_equalTo(35);
        
        make.width.mas_equalTo(75);
        
    }];
    
    
    
    // Pet Veterinarian Name text field view
    
    self.petVeterinarianNameTextField = [ControlsFactory getTextField];
    
    self.petVeterinarianNameTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petVeterinarianNameTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petVeterinarianNameTextField.placeholder = @"Name";
    
    self.petVeterinarianNameTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petVeterinarianNameTextField.delegate = self;
    
    [self.petVeterinarianNameTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petVeterinarianNameTextField.enabled = YES;
    
    padding = UIEdgeInsetsMake(10, 7.5, 000 , 7.5);
    
    [self addSubview:self.petVeterinarianNameTextField];
    
    [self.petVeterinarianNameTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petVeterinarianYesButton.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petVeterinarionFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petVeterinarionFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
    
    // Pet Veterinarian phone Number text field view
    
    self.petVeterinarianPhoneNumberTextField = [ControlsFactory getTextField];
    
    self.petVeterinarianPhoneNumberTextField.backgroundColor= [UIColor colorWithRed:1.0000 green:0.9529 blue:0.8980 alpha:1.0];
    
    [self.petVeterinarianPhoneNumberTextField setFont:[UIFont systemFontOfSize:18]];
    
    self.petVeterinarianPhoneNumberTextField.placeholder = @"Phone Number";
    
    self.petVeterinarianPhoneNumberTextField.textAlignment = NSTextAlignmentCenter;
    
    self.petVeterinarianPhoneNumberTextField.delegate = self;
    
    [self.petVeterinarianPhoneNumberTextField setValue:[UIColor colorWithRed:0.4392 green:0.4510 blue:0.4588 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.petVeterinarianPhoneNumberTextField.enabled = YES;
    
    [self.petVeterinarianPhoneNumberTextField setKeyboardType:UIKeyboardTypeNumberPad];
    
    padding = UIEdgeInsetsMake(10, 7.5, 000 , 7.5);
    
    [self addSubview:self.petVeterinarianPhoneNumberTextField];
    
    [self.petVeterinarianPhoneNumberTextField mas_makeConstraints:^(MASConstraintMaker * make){
        
        make.top.equalTo(self.petVeterinarianNameTextField.mas_bottom).with.offset(padding.top);
        
        make.left.equalTo(self.petVeterinarionFieldBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petVeterinarionFieldBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(50);
        
    }];
    
}

//
-(BOOL) checkInsuranceInfoTextFields
{
    
    // Making view controller to handle error messages
        UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    
        UIAlertController * insuranceInfoFieldsErrorAlert =   [UIAlertController
                                                                  alertControllerWithTitle:@"Error"
                                                                  message:@""
                                                                  preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [insuranceInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
    
                             }];
//        UIAlertAction* cancel = [UIAlertAction
//                                 actionWithTitle:@"Cancel"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction * action)
//                                 {
//                                     [insuranceInfoFieldsErrorAlert dismissViewControllerAnimated:YES completion:nil];
//    
//                                 }];
//    
        [insuranceInfoFieldsErrorAlert addAction:ok];
        //[insuranceInfoFieldsErrorAlert addAction:cancel];
    
    
        if ([self.petInsuranceYesButton.currentImage isEqual:self.unSelectedImage] && [self.petInsuranceNoButton.currentImage isEqual:self.unSelectedImage]) {
    
            [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Insurance YES or NO"];
    
            [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
            return  NO;
        }
    
        else if([self.petInsuranceYesButton.currentImage isEqual:self.selectedImage])
        {
    
            if (self.petInsuranceCompanyTextField.text && self.petInsuranceCompanyTextField.text.length<1)
            {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Insurance Company's Name"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
    
            else if (self.petInsurancePolicyNumberTextField.text && self.petInsurancePolicyNumberTextField.text.length<1) {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Insurance Ploicy Number"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
        }
    
        if ([self.petTrackerYesButton.currentImage isEqual:self.unSelectedImage] && [self.petTrackerNoButton.currentImage isEqual:self.unSelectedImage]) {
    
            [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Tracker YES or NO"];
    
            [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
            return  NO;
        }
    
        else if([self.petTrackerYesButton.currentImage isEqual:self.selectedImage])
        {
    
            if (self.petTrackerCompanyTextField.text && self.petTrackerCompanyTextField.text.length<1)
            {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter your Pet's Tracker, Company Name"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
    
            else if (self.petTrackerPinNumberTextField.text && self.petTrackerPinNumberTextField.text.length<1) {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter tracking Pin Number"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
        }
    
        if ([self.petVeterinarianYesButton.currentImage isEqual:self.unSelectedImage] && [self.petVeterinarianNoButton.currentImage isEqual:self.unSelectedImage]) {
    
            [insuranceInfoFieldsErrorAlert setMessage:@"Kindly select Pet's Veterinarian YES or NO"];
    
            [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
            return  NO;
        }
    
        else if([self.petVeterinarianYesButton.currentImage isEqual:self.selectedImage])
        {
    
            if (self.petVeterinarianNameTextField.text && self.petVeterinarianNameTextField.text.length<1)
            {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter your Pet's Veterinarian Name"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
    
            else if (self.petVeterinarianPhoneNumberTextField.text && self.petVeterinarianPhoneNumberTextField.text.length<1)
            {
    
                [insuranceInfoFieldsErrorAlert setMessage:@"Please enter Pet's Veterinarian Phone Number"];
    
                [errorShowViewController presentViewController:insuranceInfoFieldsErrorAlert animated:YES completion:nil];
    
                return  NO;
            }
        }
    
    PetProfileDataManager * addInsuranceInfo = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    if([self.petInsuranceYesButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petInsuranceCompanyTextField.text : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petInsurancePolicyNumberTextField.text : InsurancePolicyNumberKey];
        
    }
    else if([self.petInsuranceNoButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsurancePolicyNumberKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"" :PetInsuranceYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsuranceCompanyKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : InsurancePolicyNumberKey];
        
    }
    
    if([self.petTrackerYesButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petTrackerCompanyTextField.text :TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petTrackerPinNumberTextField.text :TrackingPinNumberKey];
        
    }
    else if([self.petTrackerNoButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackingPinNumberKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"" : PetTrackerYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackerCompanyNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" : TrackingPinNumberKey];
        
    }
    
    if([self.petVeterinarianYesButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"Yes" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petVeterinarianNameTextField.text :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: self.petVeterinarianPhoneNumberTextField.text :VeterinarianPhoneKey];
        
    }
    else if([self.petVeterinarianNoButton.currentImage isEqual:self.selectedImage])
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"" :VeterinarianPhoneKey];
        
    }
    else
    {
        [addInsuranceInfo insertPetProfileInfo: @"No" : PetVeterinarianYesOrNoKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"No" :VeterinarianNameKey];
        
        [addInsuranceInfo insertPetProfileInfo: @"No" :VeterinarianPhoneKey];
    }
    
    return  YES;
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if ([[NSPetData objectForKey:PetInsuranceYesOrNoKey] isEqualToString:@"Yes"]) {
        
        [self.petInsuranceYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        self.petInsuranceCompanyTextField.text = [NSPetData objectForKey:InsuranceCompanyKey];
        
        self.petInsurancePolicyNumberTextField.text = [NSPetData objectForKey:InsurancePolicyNumberKey];
        
    }
    
    else if([[NSPetData objectForKey:PetInsuranceYesOrNoKey] isEqualToString:@"No"])
    {
        [self.petInsuranceNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    if ([[NSPetData objectForKey:PetTrackerYesOrNoKey] isEqualToString:@"Yes"])
    {
        
        [self.petTrackerYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        self.petTrackerCompanyTextField.text = [NSPetData objectForKey:TrackerCompanyNameKey];
        
        if([NSPetData objectForKey:@"pin_number"] != nil)
        {
            self.petTrackerPinNumberTextField.text = [NSPetData objectForKey:@"pin_number"];
        }
        else
        {
            self.petTrackerPinNumberTextField.text = [NSPetData objectForKey:TrackingPinNumberKey];
        }
    }
    
    else if([[NSPetData objectForKey:PetTrackerYesOrNoKey] isEqualToString:@"No"])
    {
        [self.petTrackerNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    if ([[NSPetData objectForKey:PetVeterinarianYesOrNoKey] isEqualToString:@"Yes"]) {
        
        [self.petVeterinarianYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        self.petVeterinarianNameTextField.text = [NSPetData objectForKey:VeterinarianNameKey];
        
        self.petVeterinarianPhoneNumberTextField.text = [NSPetData objectForKey:VeterinarianPhoneKey];
        
    }
    
    else if([[NSPetData objectForKey:PetVeterinarianYesOrNoKey] isEqualToString:@"No"])
    {
        [self.petVeterinarianNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
//    if([textField isEqual:self.petTrackerCompanyTextField] || [textField isEqual:self.petTrackerPinNumberTextField])
//    {
//        //UIViewController * setCenterYController = [self getContainerController];
//        
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center = CGPointMake(self.center.x, 40);
//        }
//        //        else
//        //        {
//        //            self.center = CGPointMake(self.center.x, 100);
//        //        }
//    }
//    else if([textField isEqual:self.petVeterinarianNameTextField] || [textField isEqual:self.petVeterinarianPhoneNumberTextField])
//    {
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center = CGPointMake(self.center.x, 60);
//        }
//        else
//        {
//            self.center = CGPointMake(self.center.x, 100);
//        }
//    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
//    if([textField isEqual:self.petTrackerCompanyTextField] || [textField isEqual:self.petTrackerPinNumberTextField])
//    {
//        UIViewController * setCenterYController = [self getContainerController];
//        
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -70);
//        }
//        //        else
//        //        {
//        //            self.center = CGPointMake(self.center.x, 100);
//        //        }
//    }
//    else if([textField isEqual:self.petVeterinarianNameTextField] || [textField isEqual:self.petVeterinarianPhoneNumberTextField])
//    {
//        UIViewController * setCenterYController = [self getContainerController];
//        
//        if(self.superview.frame.size.width == 310)
//        {
//            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -70);
//        }
//        else
//        {
//            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -95);
//        }
//    }
    
/*    //    //    if(self.superview.frame.size.width == 310)
    //    //    {
    //    if(!([textField isEqual:self.animalNameTextField] || [textField isEqual:self.breadTextField]))
    //    {
    //        UIViewController * setCenterYController = [self getContainerController];
    //        if(self.superview.frame.size.width == 310)
    //        {
    //            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -70);
    //        }
    //        else
    //        {
    //            self.center=CGPointMake(self.center.x, setCenterYController.view.center.y -95);
    //        }
    //    }
    //    //    }*/
    
}

- (UIViewController *)getContainerController
{
    /// Finds the view's view controller.
    
    // Take the view controller class object here and avoid sending the same message iteratively unnecessarily.
    Class vcc = [UIViewController class];
    
    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass: vcc])
            return (UIViewController *)responder;
    
    // If the view controller isn't found, return nil.
    return nil;
}

-(void) petInsuranceYesButtonClicked:(UIButton *) insuranceYesButton{
    
    if ([self.petInsuranceYesButton.currentImage isEqual:self.unSelectedImage] && [self.petInsuranceNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petInsuranceYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petInsuranceYesButton.currentImage isEqual:self.unSelectedImage] && [self.petInsuranceNoButton.currentImage isEqual:self.selectedImage])
    {
        [self.petInsuranceYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petInsuranceNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) petInsuranceNoButtonClicked:(UIButton *) insuranceNoButton{
    
    if ([self.petInsuranceYesButton.currentImage isEqual:self.unSelectedImage] && [self.petInsuranceNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petInsuranceNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petInsuranceNoButton.currentImage isEqual:self.unSelectedImage] && [self.petInsuranceYesButton.currentImage isEqual:self.selectedImage])
    {
        [self.petInsuranceNoButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petInsuranceYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) petTrackerYesButtonClicked:(UIButton *) trackerYesButton{
    
    if ([self.petTrackerYesButton.currentImage isEqual:self.unSelectedImage] && [self.petTrackerNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petTrackerYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petTrackerYesButton.currentImage isEqual:self.unSelectedImage] && [self.petTrackerNoButton.currentImage isEqual:self.selectedImage])
    {
        [self.petTrackerYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petTrackerNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
}

-(void) petTrackerNoButtonClicked:(UIButton *) trackerNoButton{
    
    if ([self.petTrackerYesButton.currentImage isEqual:self.unSelectedImage] && [self.petTrackerNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petTrackerNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petTrackerNoButton.currentImage isEqual:self.unSelectedImage] && [self.petTrackerYesButton.currentImage isEqual:self.selectedImage])
    {
        [self.petTrackerNoButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petTrackerYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) petVeterinarianYesButtonClicked:(UIButton *) veterinarianYesButton{
    
    if ([self.petVeterinarianYesButton.currentImage isEqual:self.unSelectedImage] && [self.petVeterinarianNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petVeterinarianYesButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petVeterinarianYesButton.currentImage isEqual:self.unSelectedImage] && [self.petVeterinarianNoButton.currentImage isEqual:self.selectedImage])
    {
        [self.petVeterinarianYesButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petVeterinarianNoButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

-(void) petVeterinarianNoButtonClicked:(UIButton *) veterinarianNoButton{
    
    if ([self.petVeterinarianYesButton.currentImage isEqual:self.unSelectedImage] && [self.petVeterinarianNoButton.currentImage isEqual:self.unSelectedImage])
    {
        [self.petVeterinarianNoButton setImage:self.selectedImage forState:UIControlStateNormal];
    }
    
    else if ([self.petVeterinarianNoButton.currentImage isEqual:self.unSelectedImage] && [self.petVeterinarianYesButton.currentImage isEqual:self.selectedImage])
    {
        [self.petVeterinarianNoButton setImage:self.selectedImage forState:UIControlStateNormal];
        
        [self.petVeterinarianYesButton setImage:self.unSelectedImage forState:UIControlStateNormal];
    }
    
}

@end
