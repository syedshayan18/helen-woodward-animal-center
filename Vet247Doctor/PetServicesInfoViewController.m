//
//  PetServicesInfoViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/3/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PetServicesInfoViewController.h"
#import "PetServicesinfoTVC.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ControlsFactory.h"
#import "Masonry.h"
@interface PetServicesInfoViewController ()
@end
NSArray *infotitle;

@implementation PetServicesInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    infotitle = @[@"Phone",@"Fax",@"Email",@"Address"];
    _refpetdetails =[[NSMutableArray alloc]init];
    [_refpetdetails insertObject:_loadReferralDetails.companyPhone atIndex:0];
    [_refpetdetails insertObject:_loadReferralDetails.companyFax atIndex:1];
    [_refpetdetails insertObject:_loadReferralDetails.companyEmail atIndex:2];
    [_refpetdetails insertObject:_loadReferralDetails.companyAddress atIndex:3];
    [_refpetdetails insertObject:_loadReferralDetails.aboutCompany atIndex:4];
    
    _tableview.estimatedRowHeight = 85;
    _tableview.rowHeight =UITableViewAutomaticDimension;
    if([self.loadReferralDetails.profilePicture isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/referral/"])
    {
        // http://yourvetsnow.com/petappportal/app/webroot/img/referral/
        
      // [ _img_Servicesimg setImage:[UIImage imageNamed:@""]];
        
    }
    //@"petFood.png",@"petVet.png",@"petClinic.png"
    else
    {
    
        
        [_img_Servicesimg sd_setImageWithURL:[NSURL URLWithString:self.loadReferralDetails.profilePicture] placeholderImage:[UIImage imageNamed:@"pet_store_bg"]];
        
       
    }
    

    
    // Do any additional setup after loading the view.
}


-(IBAction)btn_bar:(UIBarButtonItem *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) getReferralsData:(ReferralsInfo *) info
{
    
    self.loadReferralDetails = info;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"";
    if (indexPath.row<4){
        
        CellIdentifier = @"CELL";
    }
    else {
        CellIdentifier = @"CELL1";
    }
    
     PetServicesinfoTVC *cell =(PetServicesinfoTVC *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.row<4){
        
        
        cell.lbl_Infotitle.text= [infotitle objectAtIndex:indexPath.row];
        cell.lbl_details.text= [_refpetdetails objectAtIndex:indexPath.row];
        if ([cell.lbl_details.text isEqualToString:@""]){
            
            
            
            cell.lbl_details.text=@"N/A";
            
            
        }
    }
        else {
            
            cell.txt_about.tag=1;
            cell.txt_about.text=[_refpetdetails objectAtIndex:4];
            
            if ([cell.txt_about.text isEqualToString:@""]){
                
                
                cell.txt_about.text=@"N/A";
                
                
            }

        
        
        }
    
//    if (indexPath.row<4)
//    {
//        PetServicesinfoTVC *cell =(PetServicesinfoTVC *) [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//        
//        cell.lbl_Infotitle.text= [infotitle objectAtIndex:indexPath.row];
//        cell.lbl_details.text= [_refpetdetails objectAtIndex:indexPath.row];
//        if ([cell.lbl_details.text isEqualToString:@""]){
//            
//            cell.lbl_details.text=@"N/A";
//            
//            
//        }
//        
//        return cell;
//    }
//    
//        if (indexPath.row==4){
//            
//            PetServicesinfoTVC *cell2 = (PetServicesinfoTVC *)[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
//            cell2.txt_about.text=[_refpetdetails objectAtIndex:4];
//            
//            if ([cell2.txt_about.text isEqualToString:@""]){
//                cell2.txt_about.text=@"N/A";
//            }
//            //cell2.txt_about.sizeToFit;
//            return cell2;
//            
//            
//        }

    return cell;
    
  
}



  
    
    
    



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
