//
//  AreaLostnFoundViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/11/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"

@interface AreaLostnFoundViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (weak, nonatomic) IBOutlet UITextField *txt_radius;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *placestextfield;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (strong,nonatomic) NSString *locationname;
@property (strong,nonatomic) NSString *latitude;
@property (strong,nonatomic) NSString *longitude;
@property (strong,nonatomic) NSDictionary *lostfoundict;
@property (strong,nonatomic) NSString *posturl;
@property (strong,nonatomic) NSMutableArray *petData;
@property (strong,nonatomic) NSDictionary *petJson;
@property (strong,nonatomic) UIPickerView *radiuspicker;
@property (strong,nonatomic) NSMutableArray *radiusArray;
@property NSUInteger index;



@end
