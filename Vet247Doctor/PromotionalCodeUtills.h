//
//  PromotionalCodeUtills.h
//  Vet247Doctor
//
//  Created by APPLE on 9/29/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#define PromotionalCode_Key @"pCode"

#define AmountToBePaidByUser @"PayThisMuchAmount001101"


#import <Foundation/Foundation.h>

@interface PromotionalCodeUtills : NSObject

@property (strong, nonatomic) NSString * promotionalCode;

+(void)setPromotionalCode:(NSString *) promoCode;

+(NSString *) getPromotionalCode;

@end
