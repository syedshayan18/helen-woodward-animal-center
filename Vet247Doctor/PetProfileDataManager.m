//
//  PetProfileDataManager.m
//  Vet247Doctor
//
//  Created by APPLE on 8/8/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfileDataManager.h"

@implementation PetProfileDataManager

static PetProfileDataManager * sharedInstance = nil;

+(PetProfileDataManager*) getPetProfileDataManagerSharedInstance
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[PetProfileDataManager alloc] init];
        sharedInstance.dataContainer = [[NSMutableDictionary alloc] init];
    });
    
    return sharedInstance;
}

-(void)insertPetProfileInfo:(NSString*)value :(NSString*)Nskey
{

    [self.dataContainer setObject:value forKey:Nskey];

}

-(void)inserttacked:(int )value :(NSString*)Nskey
{
    [self.dataContainer setObject:[NSNumber numberWithInt:value] forKey:Nskey];
    
    
}

-(NSMutableDictionary *)getPetProfileInfo
{
    if(sharedInstance != nil)
    {
        return sharedInstance.dataContainer;
    }
    else
    {
        return nil;
    }
}

-(void) setPetData:(NSMutableDictionary*) petData
{
    petData = [petData mutableCopy];
    
    if(sharedInstance != nil)
    {
        sharedInstance.dataContainer = petData;
    }
}

+(void) removePetProfileInfo
{

    [sharedInstance.dataContainer removeAllObjects];

    //NSLog(@"\n Data is removed from ");
}

+(void) removePetProfileObjectForKey:(NSString *)Key
{
    if(sharedInstance != nil)
    {
        [sharedInstance.dataContainer removeObjectForKey:Key];
    }
}


@end
