//
//  PaymentScreenViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/14/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "PaymentScreenViewController.h"
#import "PromotionalCodeUtills.h"
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "DashboardViewController.h"
@interface PaymentScreenViewController ()

@end

@implementation PaymentScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btn_coupon.layer.cornerRadius = 15;
    _btn_coupon.clipsToBounds=YES;
//    NSAttributedString *code = [[NSAttributedString alloc] initWithString:@"Enter Promotional Code" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0/255.0f green:147.0/255.0f blue:51.0/255.0f alpha:1.0] }];
   // self.txt_couponenter.attributedPlaceholder = code;
    self.gestureRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    
    self.gestureRecognizer.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) tap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
     [self.txt_couponenter resignFirstResponder];    
}


-(IBAction)paynowbtn:(UIButton *)sender{
    [self.txt_couponenter resignFirstResponder];
    
    if( self.txt_couponenter.text != nil && self.txt_couponenter.text.length > 0)
    {
        [PromotionalCodeUtills setPromotionalCode:self.txt_couponenter.text];
        
        [self checkForPromotionalCodeAndGetAmountToPay];
    }
    else {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"107.4" forKey:AmountToBePaidByUser];
        
        [PromotionalCodeUtills setPromotionalCode:@""];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Coupon field Empty" message:@"Please Enter the Coupon Field" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"Cancel");
        }];
        
        
        
        [alertController addAction:ok];
        [self presentViewController:alertController animated: YES completion: nil];
     

        
        
//        PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
//        
//        [self.navigationController pushViewController:paymentTermsController animated:YES];
    }

}

-(void) checkForPromotionalCodeAndGetAmountToPay {
    
    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [sendPaymentRecivedNetworkObject promotionalCodeChecking:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode]];
}


-(void) paymentRecived {
    
    NetworkingClass * sendPaymentRecivedNetworkObject = [[NetworkingClass alloc] init];
    
    sendPaymentRecivedNetworkObject.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [sendPaymentRecivedNetworkObject paymentRecieved:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]: [PromotionalCodeUtills getPromotionalCode] :@"0"];
    
    [PromotionalCodeUtills setPromotionalCode:@""];
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Congratulations"
                                                         message:@"You got full discount! Your package has been activated"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"successfully recived"])
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self setDashBoardAsFirstController];
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
    else
    {
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                             }];
        
        [paymentSuccessfullyRecieved addAction:ok];
        
        paymentSuccessfullyRecieved.message = @"Promotional Code Not Accepted";
        
        [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
    }
}

// response for *(promotionalCodeCheck)*

-(void)paymentResponse:(id) data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * promotionalCodeMessage =   [UIAlertController
                                                    alertControllerWithTitle:@"Message"
                                                    message:@"Sorry, promotional is not authenticated, Press 'OK' to continue, otherwise Press 'Cancel'"
                                                    preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if([[[NSUserDefaults standardUserDefaults] objectForKey:AmountToBePaidByUser]  isEqual:@"0"])
                             {
                                 [self paymentRecived];
                             }
                             else {
                                 
//                                 PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
//                                 
//                                 [self.navigationController pushViewController:paymentTermsController animated:YES];
                             }
                         }];
    
    UIAlertAction * cancel = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                              }];
    
    [promotionalCodeMessage addAction:ok];
    
    [promotionalCodeMessage addAction:cancel];
    
    if ([[data objectForKey:@"msg"] isEqualToString:@"no coupon found with this code"] || [[data objectForKey:@"status"] isEqualToString:@"failure"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"107.4" forKey:AmountToBePaidByUser];
    }
    
    else if ([[data objectForKey:@"status"] isEqualToString:@"success"]) {
        
        [promotionalCodeMessage setMessage:@"Yahoo! promotional code is accepted, Press 'OK' to continue, otherwise Press 'Cancel'"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[data objectForKey:@"amount"] forKey:AmountToBePaidByUser];
    }
    
    else {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    }
    
    [self presentViewController:promotionalCodeMessage animated:YES completion:nil];
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController * paymentSuccessfullyRecieved =   [UIAlertController
                                                         alertControllerWithTitle:@"Error"
                                                         message:@"Fails With Network Error, Please! Try Again"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //[self setDashBoardAsFirstController];
                         }];
    
    [paymentSuccessfullyRecieved addAction:ok];
    
    [self presentViewController:paymentSuccessfullyRecieved animated:YES completion:nil];
}

-(void) setDashBoardAsFirstController
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLoggin"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
    
   [self performSegueWithIdentifier:@"unwindToContainerVC" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
