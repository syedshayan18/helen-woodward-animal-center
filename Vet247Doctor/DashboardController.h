//
//  DashboardController.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/7/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SocketManager.h"

#import "AppDelegate.h"

@interface DashboardController : UIViewController<SoccketManagerDelegate>

@property (strong, nonatomic) AppDelegate *appDel;

@end
