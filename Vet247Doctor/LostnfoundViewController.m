//
//  LostnfoundViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "LostnfoundViewController.h"
#import "PetdetailsViewController.h"
#import "FoundpetdetailsViewController.h"

@interface LostnfoundViewController ()

@end

@implementation LostnfoundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _img_foundapet.userInteractionEnabled = YES;
    _img_lostapet.userInteractionEnabled = YES;

    _img_lostnfoundarea.userInteractionEnabled = YES;

    _img_mylostandfoundpet.userInteractionEnabled = YES;


    UITapGestureRecognizer *foundapet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(foundapet:)];
    [self.img_foundapet addGestureRecognizer:foundapet];
    
   
    UITapGestureRecognizer *lostapet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lostapet:)];
    [self.img_lostapet addGestureRecognizer:lostapet];
    
    UITapGestureRecognizer *lostnfoundarea=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lostnfoundarea:)];
    [self.img_lostnfoundarea addGestureRecognizer:lostnfoundarea];
    
    UITapGestureRecognizer *mylostnfoundpet =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(mylostnfoundpet:)];
    [self.img_mylostandfoundpet addGestureRecognizer:mylostnfoundpet];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[self navigationController]setNavigationBarHidden:NO animated:YES ];
}

-(void)lostapet:(UIGestureRecognizer *)sender {
    
    [self performSegueWithIdentifier:@"lostnfound" sender:nil];
    NSLog(@"lostapet");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)foundapet:(UIGestureRecognizer *)sender {
    NSLog(@"foundapet tapped");
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Found Pet Picture" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self performSegueWithIdentifier:@"opencamera" sender:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
   }

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    _image = info[UIImagePickerControllerEditedImage];
    _selectedimage =
    [UIImage imageWithCGImage:[_image CGImage]
                        scale:[_image scale]
                  orientation: UIImageOrientationUp];
    
  
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [self performSegueWithIdentifier:@"founddetails" sender:nil];
   // here saving image to some imageview
    
    
}

- (IBAction)unwindtolostdashboard:(UIStoryboardSegue *)segue {
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
}

- (IBAction)unwindsegue:(UIStoryboardSegue *)segue {
    
    
}


-(void)lostnfoundarea:(UIGestureRecognizer *)sender {
    [self performSegueWithIdentifier:@"petlostnfoundarea" sender:nil];
    NSLog(@"ostnfoundarea");
}
-(void)mylostnfoundpet:(UIGestureRecognizer *)sender {
    [self performSegueWithIdentifier:@"mylostnfound" sender:nil];
}



#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"lostnfound"]){
        
        PetdetailsViewController *petdetails = (PetdetailsViewController *)[segue destinationViewController];
        petdetails.hideButtonforlostfound=YES;
    }
    if ([segue.identifier isEqual:@"founddetails"]){
        FoundpetdetailsViewController *founddetails = (FoundpetdetailsViewController *)[segue destinationViewController];
        
        founddetails.petimage = _selectedimage;
    }
}


@end
