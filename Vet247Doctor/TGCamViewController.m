//
//  TGCamViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "TGCamViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ImagepreviewViewController.h"

@interface TGCamViewController ()


@end

@implementation TGCamViewController
AVCaptureSession *session;
AVCaptureStillImageOutput *stillImageOutput;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    session = [[AVCaptureSession alloc] init];
    [session setSessionPreset:AVCaptureSessionPresetPhoto];
    
    AVCaptureDevice *inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:&error];
    
    if ([session canAddInput:deviceInput]) {
        [session addInput:deviceInput];
    }
    
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    CALayer *rootLayer = [[self cameraview] layer];
    [rootLayer setMasksToBounds:YES];
    CGRect frame = self.cameraview .frame;
    [previewLayer setFrame:frame];
    [rootLayer insertSublayer:previewLayer atIndex:0];
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    [session addOutput:stillImageOutput];
    
    [session startRunning];
    
//    _captureSession = [[AVCaptureSession alloc] init];
//    
//    //-- Creata a video device and input from that Device.  Add the input to the capture session.
//    AVCaptureDevice * videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    if(videoDevice == nil)
//        assert(0);
//    
//    //-- Add the device to the session.
//    NSError *error;
//    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice
//                                                                        error:&error];
//    if(error)
//        assert(0);
//    
//    [_captureSession addInput:input];
//    
//    //-- Configure the preview layer
//    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
//    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
//    
//    [_previewLayer setFrame:CGRectMake(0, 0,
//                                       self.cameraview.frame.size.width,
//                                       self.cameraview.frame.size.height)];
//    
//    //-- Add the layer to the view that should display the camera input
//    [self.cameraview.layer addSublayer:_previewLayer];
//    
//    //-- Start the camera
//    [_captureSession startRunning];
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[self navigationController]setNavigationBarHidden:YES  animated:YES];
}
- (IBAction)takePhoto:(id)sender {
    
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                  completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                                                      if (imageDataSampleBuffer != NULL) {
                                                          _imagedata = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                                                          
                                                          _image = [UIImage imageWithData:_imagedata];
                                                          
                                                          [self performSegueWithIdentifier:@"camerapreview" sender:nil];
                                                      }
                                                  }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(IBAction)btn_back:(UIButton *)sender{
  
    [self.navigationController popViewControllerAnimated:YES];

    
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
     ImagepreviewViewController *imagepreview = (ImagepreviewViewController *) [segue destinationViewController];
    imagepreview.dvImage=_image;
    
    
}

@end
