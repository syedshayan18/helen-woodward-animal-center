//
//  TGCamViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGCamViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *cameraview;
@property (strong,nonatomic) UIImage *image;
@property (strong,nonatomic) NSData *imagedata;
@end
