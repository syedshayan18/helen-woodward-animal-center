//
//  ImagepreviewViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/18/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagepreviewViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_Yes;
@property (weak, nonatomic) IBOutlet UIButton *btn_No;
@property (weak, nonatomic) IBOutlet UIImageView *img_imagepreview;
@property(nonatomic,strong)UIImage *dvImage;

@end
