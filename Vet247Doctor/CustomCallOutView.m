//
//  CustomCallOutView.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 9/2/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "CustomCallOutView.h"

#import "Masonry.h"

#import "ControlsFactory.h"

@implementation CustomCallOutView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithFrame:(CGRect)frame1
{
    self = [super initWithFrame:frame1];
    if (self) {
        
        self.userInteractionEnabled = YES;
        
    }
    
    return self;
}

-(void) setUpCustomView {


    self.customAnotationButton = [ControlsFactory getButton];

    self.frame = CGRectMake(0, 0, 250, 40);

    //self.backgroundColor = [UIColor colorWithRed:142/255.0 green:183/255.0 blue:130/255.0 alpha:1.0];

    self.backgroundColor = [UIColor clearColor];
    
    self.customAnotationButton.frame = CGRectMake(0, 0, 250, 40);

    [self.customAnotationButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.customAnotationButton.backgroundColor = [UIColor clearColor];
    
    self.callOutLabel = [ControlsFactory getLabel];
    
    [self.callOutLabel setNumberOfLines:0];
    
    [self.callOutLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.callOutLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:13]];
    
    [self.callOutLabel adjustsFontSizeToFitWidth];
    
    [self.callOutLabel setTextColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    
    //[self.callOutLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
    
    //self.callOutLabel.font=[UIFont systemFontOfSize:18.0];
    
    [self.callOutLabel setBackgroundColor:[UIColor clearColor]];
    
    self.callOutLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    //UIEdgeInsets paddingForLabel = UIEdgeInsetsMake(5, 5, 0, 40);
    
    UIEdgeInsets paddingForLabel = UIEdgeInsetsMake(5, 5, 0, 5);
    
    [self addSubview:self.callOutLabel];
    
    [self.callOutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.mas_left).with.offset(paddingForLabel.left);
        
        make.right.equalTo(self.mas_right).with.offset(-paddingForLabel.right);
        
        // make.top.equalTo(self.view.mas_top);
        
        make.top.equalTo(self.mas_top).with.offset(paddingForLabel.top);
        
        make.height.mas_equalTo(30);
        
    }];
    
    // distance Lable Setting
    
//    self.distanceLabel = [ControlsFactory getLabel];
//    
//    [self.distanceLabel setBackgroundColor:[UIColor blueColor]];
//    
//    //self.distanceLabel.lineBreakMode = NSLineBreakByTruncatingTail;
//    
//    paddingForLabel = UIEdgeInsetsMake(0, 0, 0, 12);
//    
//    self.distanceLabel.text = @"Dis";
//    
//    self.distanceLabel.textAlignment = NSTextAlignmentCenter;
//    
//    [[self.distanceLabel layer] setCornerRadius:22.5f];
//    
//    [[self.distanceLabel layer] setMasksToBounds:YES];
//    
//    [self addSubview:self.distanceLabel];
//    
//    [self.distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.right.equalTo(self.mas_right).with.offset(paddingForLabel.right);
//        
//        make.centerY.equalTo(self.mas_centerY).with.offset(-5);
//        
//        make.width.mas_equalTo(45);
//        
//        make.height.mas_equalTo(45);
//        
//    }];
    
    [self addSubview:self.customAnotationButton];

}

-(void)buttonClicked: (UIButton*)button
{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"fromCallOutView" object:nil];

}

@end
