//
//  ReferralsDetailController.h
//  Vet247Doctor
//
//  Created by APPLE on 8/18/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "ReferralsInfo.h"

#import <UIKit/UIKit.h>

@interface ReferralsDetailController : UIViewController <UIScrollViewDelegate>

-(void) getReferralsData:(ReferralsInfo *) info;


@end
