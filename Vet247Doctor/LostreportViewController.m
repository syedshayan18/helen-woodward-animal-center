//
//  LostreportViewController.m
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/16/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import "LostreportViewController.h"
#import <UITextView_Placeholder/UITextView+Placeholder.h>

#import <CoreLocation/CoreLocation.h>
#import "NetworkingClass.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
@import GoogleMaps;

@interface LostreportViewController ()<PlaceSearchTextFieldDelegate,UITextFieldDelegate,GMSMapViewDelegate>
@property AppDelegate *appDel;

@end

@implementation LostreportViewController

@synthesize passedpetid;
@synthesize marker,locationManager;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",passedpetid);

    
    _appDel =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([_appDel availableInternet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
  
    [self requestWhenInUseAuthorization];
    _lostdict = [[NSMutableDictionary alloc]init];
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.txtview_details.inputAccessoryView = keyboardToolbar;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    

    self.mapView.delegate = self;
   
    
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy =
        kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    [self.locationManager startUpdatingLocation];
    
  self.PlacesTextField.delegate=self;
    
   
    _txtview_details.placeholder = @"Provide Extra Details if any";
    _txtview_details.placeholderColor = [UIColor lightGrayColor]; // optional
   
    
    _firstLocationUpdate =false;
    self.txtview_details.delegate=self;
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//    
    _btn_sendlostreport.layer.cornerRadius=15;
    _btn_sendlostreport.clipsToBounds=YES;
    //_mapView.settings.compassButton = YES;
    //_mapView.settings.myLocationButton = YES;
    
    
    _PlacesTextField.placeSearchDelegate = self;
    _PlacesTextField.strApiKey = @"AIzaSyCgdljgsWF6H4ErAjz6AnCxwlQhMQGSbnc";
    _PlacesTextField.superViewOfList = self.view; // View, on which Autocompletion list should be appeared.
    _PlacesTextField.autoCompleteShouldHideOnSelection = true;
    _PlacesTextField.maximumNumberOfAutoCompleteRows = 5;
    _PlacesTextField.autoCompleteShouldHideOnSelection = true;
    _PlacesTextField.autoCompleteTableAppearsAsKeyboardAccessory = true;

    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 1)];
    _PlacesTextField.leftView = paddingView;
    _PlacesTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    // Listen to the myLocation property of GMSMapView.
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    

    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
}


-(void)requestWhenInUseAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            NSLog(@"ok");
            
            
            
            
            
        }];
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
           // [[UIApplication sharedApplication] openURL:
             //[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
           // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
            
            
        }];
        
        
        [alert addAction:okAction];
        [alert addAction:settings];
        
        
        [self presentViewController:alert animated:YES completion:nil];

        
        
        
        
        
        
        
        
    }
    
            // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestWhenInUseAuthorization];
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)dealloc {
    [_mapView removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}

-(void)yourTextViewDoneButtonPressed
{
    [self.txtview_details resignFirstResponder];
    
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
        marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"pet_pin"];
        marker.map = _mapView;
        
        
        
        CLLocation *geolocation=[[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        //from Google Map SDK
        GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
        GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
        {
            GMSAddress *address=response.firstResult;
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (address){
                _PlacesTextField.placeholder = [NSString stringWithFormat:@"%@, %@, %@(%i KM)",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country,_radius];
                _locationname = [NSString stringWithFormat:@"%@, %@, %@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
            }
            
        };
        [geocode reverseGeocodeCoordinate:geolocation.coordinate completionHandler:handler];
        
        
        
        
        
    

    
           }

    
    // Do any additional setup after loading the view.
}

- (void) locationManager:(CLLocationManager *)manager
        didFailWithError:(NSError *)error
{
    NSLog(@"%@", @"Core location can't get a fix.");
}


- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (!_firstLocationUpdate){
        
        _firstLocationUpdate = YES;
        
        CLLocation *curPos = locationManager.location;
        //        NSString *latitude = [[NSNumber numberWithDouble:curPos.coordinate.latitude] stringValue];
        //
        //        NSString *longitude = [[NSNumber numberWithDouble:curPos.coordinate.longitude] stringValue];
        
        _mapView.camera = [GMSCameraPosition cameraWithTarget:locationManager.location.coordinate zoom:14];
        marker = [[GMSMarker alloc] init];
        marker.position = locationManager.location.coordinate;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"pet_pin"];
        marker.map = _mapView;
        
        
        //from Google Map SDK
        CLLocation *geolocation=[[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        //from Google Map SDK
        GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
        GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
        {
            GMSAddress *address=response.firstResult;
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (address){
                _PlacesTextField.placeholder = [NSString stringWithFormat:@"%@, %@, %@(%i KM)",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country,_radius];
                _locationname = [NSString stringWithFormat:@"%@, %@, %@",[address.lines objectAtIndex:0],[address.lines objectAtIndex:1],address.country];
            }
            
        };
        [geocode reverseGeocodeCoordinate:geolocation.coordinate completionHandler:handler];
        
        
        
        
        
    }
}




- (CLLocationCoordinate2D)getCenterCoordinate
{
    CGPoint centerPoint = self.mapView.center;
    CLLocationCoordinate2D centerCoord = [self.mapView.projection coordinateForPoint:(centerPoint)];
    return centerCoord;
}
- (CLLocationCoordinate2D)getTopCenterCoordinate
{
    // to get coordinate from CGPoint of your map
    CGPoint topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width/2.0, 0) toView:self.mapView];
    CLLocationCoordinate2D point = [self.mapView.projection coordinateForPoint:(topCenterCoor)];
    return point;
}
- (CLLocationDistance)getRadius
{
    CLLocationCoordinate2D centerCoor = [self getCenterCoordinate];
    // init center location from center coordinate
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    
    CLLocationCoordinate2D topCenterCoor = [self getTopCenterCoordinate];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    CLLocationDistance radiusKM=radius/1000;
    int radiusint = (int) radiusKM;
    if (radiusint==0){
        radiusint=1;
    }
    return radiusint;
   
}


-(IBAction)btn_report:(UIButton *)sender{
    NSLog(@"%i",_radius);
    if (_locationname.length <1) {
        _locationname=@"N/A";
    }
    
  
    
    [_lostdict setValue:_locationname forKey:@"location"];

        _userid= [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        [_lostdict setValue:_userid forKey:@"user_id"];
 [_lostdict setValue:passedpetid forKey:@"pet_id"];
        [_lostdict setValue:_txtview_details.text forKey:@"detail"];
        
    
   
    
        [_lostdict setValue:[NSNumber numberWithDouble:_radius]  forKey:@"radius"];
        
        double latitude=marker.position.latitude;
        double longitude = marker.position.longitude;
        
        [_lostdict setValue:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
        [_lostdict setValue:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
        
        NSLog(@"%@",_lostdict);
        NSString *posturl = [NSString stringWithFormat:@"%@%@",BaseURL,@"lost_pet.php"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NetworkingClass *network  =[[NetworkingClass alloc]init];
       
       [ network reportlostpet:posturl param:_lostdict completion:^(id finished, NSError *error) {
        
           [MBProgressHUD hideHUDForView:self.view animated:YES];
           
           if (error==nil){
               NSLog(@"%@",finished);
               
               
               
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:@"Lost Report Send"
                                            message:@"Lost Report has been Sent"
                                            preferredStyle:UIAlertControllerStyleAlert];
               
               UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                   
                   [self performSegueWithIdentifier:@"unwindtolostdashboard" sender:nil];
                   
                   
                   NSLog(@"ok");
                   
                   
                   
                   
                   
               }];
               
               
               [alert addAction:okAction];
               
               
               [self presentViewController:alert animated:YES completion:nil];
               
           }
           else {
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:@"Error"
                                            message:@"Server Error or Please Check your Internet Connectivity"
                                            preferredStyle:UIAlertControllerStyleAlert];
               
               UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                   
                   
                   
                   
                   
                   
                   
                   
                   NSLog(@"ok");
                   
                   
                   
                   
                   
               }];
               
               
               [alert addAction:okAction];
               
               
               [self presentViewController:alert animated:YES completion:nil];
               
               
           }
           
           
           
           
           
       }];
        
 
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    
    
    _radius =[self getRadius];
    if (_locationname.length==0){
        _PlacesTextField.text=@"";
    }
    else if (_PlacesTextField.text.length==0){
        _PlacesTextField.text=@"";
    }
    else {
        _PlacesTextField.text = [NSString stringWithFormat:@"%@ (%i  KM)",_locationname,_radius];
    }
    
    
    
    NSLog(@"%i",_radius);
    

    
}


-(void) placeSearchResponseForSelectedPlace:(GMSPlace *)responseDict{
    
_locationname =_PlacesTextField.text;
    
    //_place = _PlacesTextField.text;
    [ _mapView clear];
    CLLocationCoordinate2D coordinates= responseDict.coordinate;
    
    //GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude longitude:coordinates.longitude zoom:12.0];
    
    
    _mapView.camera = [GMSCameraPosition cameraWithTarget:coordinates zoom:12];
    marker = [[GMSMarker alloc] init];
    marker.position = coordinates;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"pet_pin"];
    marker.map = _mapView;
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
   
}

- (void)viewWillDisappear:(BOOL)animated {
    
   
    [locationManager stopUpdatingLocation];
    
}


- (void)keyboardWasShown:(NSNotification *)notification {
    
    if (_globaltextfield == _PlacesTextField){
        
        NSLog(@"do nothing");
    }
    else {
        
        // Assign new frame to your view
        [UIView animateWithDuration:0.25 animations:^
         {
             
             CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
             
             
             
             CGRect frame = self.view.frame;
             frame.origin.y = -keyboardSize.height+64;
             self.view.frame=frame;
             
             
             
         }completion:^(BOOL finished)
         {
             
         }];
        
    }
    
    
    
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    _globaltextfield = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    _globaltextfield=nil;
}
- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    
    if (_globaltextfield ==_PlacesTextField){
        NSLog(@"do nothing") ;
    }
    else {
        [UIView animateWithDuration:0.25 animations:^
         {
             
             CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
             
             CGRect f = self.view.frame;
             f.origin.y = 0.0f+64;
             self.view.frame = f;
             
         }completion:^(BOOL finished)
         {
             
         }];
        
        
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
