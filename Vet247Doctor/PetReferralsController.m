//
//  PetRefferalsController.m
//  Vet247Doctor
//
//  Created by APPLE on 8/18/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "Masonry.h"

#import "MBProgressHUD.h"

#import "ControlsFactory.h"

#import "ReferralsCell.h"

#import "PetReferralsController.h"

#import "NetworkingClass.h"

#import "ReferralsListDataObject.h"

#import "ReferralsMapViewController.h"

#import "MBProgressHUD.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import "SDWebImage/UIImageView+WebCache.h"

#define ReferralsCellIdentifier @"ReferralsCell"

static float cellHeight=91.5f;

@interface PetReferralsController ()

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) UILabel *messagesLbl;

@property (strong,nonatomic) UIButton *backBtn;

@property (strong, nonatomic) UIView * searchView;

@property (strong, nonatomic) UITextField * txtFld;

@property (strong, nonatomic) UIImageView * txtFldImgView;

@property (strong, nonatomic) UIImageView * searchIcon;

@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) NSMutableArray *referralsData;

@property (strong,nonatomic) NSMutableArray *referralsFilteredData;

@property (assign) BOOL searching;

@end

@implementation PetReferralsController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.referralsData = [[NSMutableArray alloc] init];
    
    self.referralsFilteredData =[[NSMutableArray alloc] init];
    //
    
    [self addComponents];
    
    [self getReferralsList];
    
    //    self.referralsImages =[[NSMutableArray alloc] initWithObjects:@"petFood.png",@"petVet.png",@"petClinic.png", nil];
    //
    // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    self.searching = FALSE;
    
    [self.tableView reloadData];
}

//- (void)updateSearchFieldUsingContentsOfTextField:(id)sender {
//    
//    NSString *serachFieldVal = [NSString stringWithFormat:@"Hello %@", ((UITextField *)sender).text];
//}

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:255.0/255 green:156.0/255 blue:0.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    self.backBtn=[ControlsFactory getButton];
    
    [self.backBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    [self.backBtn addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets paddingForbackBtn = UIEdgeInsetsMake(0, 10, 15, 0);
    
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForbackBtn.left);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(20);
        
        make.bottom.equalTo(self.headerView.mas_bottom).with.offset(-paddingForbackBtn.bottom);
    }];
    
    UIEdgeInsets paddingForMessagesLabel = UIEdgeInsetsMake(0, 0, 15, 0);
    
    self.messagesLbl =[ControlsFactory getLabel];
    
    self.messagesLbl.text=@"Services";
    
    [self.messagesLbl setTextAlignment:NSTextAlignmentCenter];
    
    self.messagesLbl.font= [UIFont systemFontOfSize:20.0];
    
    self.messagesLbl.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.messagesLbl];
    
    [self.messagesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForMessagesLabel.bottom);
        
        
    }];
    
    self.searchView=[ControlsFactory getView];
    
    self.searchView.backgroundColor=[UIColor whiteColor];
    
    UIEdgeInsets paddingForSearchVIew= UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.searchView];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.height.mas_equalTo(70);
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForSearchVIew.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForSearchVIew.right);
        
        make.top.equalTo(self.headerView.mas_bottom).with.offset(paddingForSearchVIew.top);
        
    }];
    
    self.txtFldImgView=[ControlsFactory getImageView];
    
    self.txtFldImgView.image=[UIImage imageNamed:@"searchRectangle.png"];
    
    UIEdgeInsets paddingForTxtFldImg = UIEdgeInsetsMake(15, 15, 15, 15);
    
    [self.searchView addSubview:self.txtFldImgView];
    
    [self.txtFldImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.searchView.mas_left).with.offset(paddingForTxtFldImg.left);
        
        make.right.equalTo(self.searchView.mas_right).with.offset(-paddingForTxtFldImg.right);
        
        // make.top.equalTo(self.searchView.mas_bottom);
        
        make.top.equalTo(self.searchView.mas_top).with.offset(paddingForTxtFldImg.top);
        
        make.bottom.equalTo(self.searchView.mas_bottom).with.offset(-paddingForTxtFldImg.bottom);
        
    }];
    
    self.searchIcon=[ControlsFactory getImageView];
    
    self.searchIcon.image=[UIImage imageNamed:@"searchIcon.png"];
    
    UIEdgeInsets paddingForSearchIcon= UIEdgeInsetsMake(10, 10, 10, 15);
    
    [self.txtFldImgView addSubview:self.searchIcon];
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForSearchIcon.right);
        
        make.centerY.mas_equalTo(self.txtFldImgView.mas_centerY);
        
        make.height.mas_equalTo(21);
        
        make.width.mas_equalTo(18);
        
    }];
    
    
    self.txtFld = [ControlsFactory getTextField];
    
    self.txtFld.backgroundColor=[UIColor clearColor];
    
    self.txtFld.placeholder=@"Search Services";
    
    [self.txtFld setValue:[UIColor colorWithRed:255.0/255 green:157.0/255 blue:59.0/255 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.txtFld.delegate = self;
    
    UIEdgeInsets paddingForTxtFld= UIEdgeInsetsMake(0, 25, 0, 30);
    
    [self.txtFldImgView addSubview:self.txtFld];
    
    self.txtFldImgView.userInteractionEnabled = YES;
    
    [self.txtFld mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.txtFldImgView.mas_left).with.offset(paddingForTxtFld.left);
        
        make.right.equalTo(self.txtFldImgView.mas_right).with.offset(-paddingForTxtFld.right);
        
        make.top.equalTo(self.txtFldImgView.mas_top).with.offset(paddingForTxtFld.top);
        
        make.bottom.equalTo(self.txtFldImgView.mas_bottom).with.offset(-paddingForTxtFld.bottom);
        
    }];
}

-(void) setUpTableView {
    
    self.tableView =[ControlsFactory getTableView];
    
    self.tableView.delegate=self;
    
    self.tableView.dataSource=self;
    
    self.tableView.backgroundColor=[UIColor clearColor];
    
    self.tableView.bounces = NO;
    
    [self.tableView registerClass:[ReferralsCell class]   forCellReuseIdentifier:ReferralsCellIdentifier];
    
    [self.view addSubview:self.tableView];
    
    UIEdgeInsets paddingforTableView =UIEdgeInsetsMake(0, 10, 0, 10);
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make){
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingforTableView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingforTableView.right);
        
        make.top.equalTo(self.searchView.mas_bottom).with.offset(paddingforTableView.top);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-paddingforTableView.bottom);
        
    }];
    
}

-(void) getReferralsList
{
    
    NetworkingClass *network=[[NetworkingClass alloc] init];
    
    network.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [network getReferrals];
    
}

-(void)response:(id)data {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSDictionary * saveReferralsData;
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        saveReferralsData = [data objectForKey:@"data"];
        
        int i = 0;
        
        for (id obj in saveReferralsData) {
            
            ReferralsListDataObject * refData = [[ReferralsListDataObject alloc] init];
            
            refData.referralsId = [obj objectForKey:Referrals_Id];
            
            refData.referralsName = [obj objectForKey:Referrals_Name];
            
            refData.referralsDecription = [obj objectForKey:Referrals_Description];
            
            refData.referralsImage = [obj objectForKey:Referrals_Image];
            
            [self.referralsData insertObject:refData atIndex:i];
            
            i++;
            
        }
    }
    
    else if ([data objectForKey:@"msg"])
    {
        
        UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:[data objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [errorAlert show];
        
    }
    
    [self.tableView reloadData];
    
}

-(void)error:(NSError*)error {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *errorAlert=[[UIAlertView alloc] initWithTitle:nil message:@"Error" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [errorAlert show];
    
}

-(void) addComponents
{
    [self setUpHeaderView];
    
    [self setUpTableView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    string = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (string.length == 0) {
        
        self.searching=false;
        
        [self.referralsFilteredData removeAllObjects];
        
        [self.tableView reloadData];
        
        return YES;
    }
    
    if (self.txtFld.text.length==0)
    {
        self.searching = TRUE;
        
        [self.referralsFilteredData removeAllObjects];
        
        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF.referralsName contains[c] %@",string];
        
        self.referralsFilteredData  =[NSMutableArray arrayWithArray:[self.referralsData filteredArrayUsingPredicate:predicate]];
    }
    else
    {
        self.searching=TRUE;
        
        [self.referralsFilteredData removeAllObjects];
        
        NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF.referralsName contains[c] %@",string];
        
        self.referralsFilteredData  =[NSMutableArray arrayWithArray:[self.referralsData filteredArrayUsingPredicate:predicate]];
        
    }
    
    [self.tableView reloadData];
    
    return YES;
}

-(void) backButtonClicked: (UIButton *) btn
{
    
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91.5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!self.searching)
    {
        if (self.referralsData.count == 0)
        {
            return 0;
        }
        else
        {
            return self.referralsData.count;
        }
    }
    else
    {
        if (self.referralsFilteredData.count == 0)
        {
            return 0;
        }
        else
        {
            return self.referralsFilteredData.count;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    //  Cleanning Old Data!
    UILabel *temp1_name_lbl = (UILabel*) [cell viewWithTag:1001];
    UIImageView *temp_imageView = (UIImageView*) [cell viewWithTag:2001];
    UILabel *temp2_name_lbl = (UILabel*) [cell viewWithTag:3001];
    
    
    [temp_imageView removeFromSuperview];
    [temp1_name_lbl removeFromSuperview];
    [temp2_name_lbl removeFromSuperview];
    
    if(!self.searching)
    {
        if (!self.referralsData.count)
        {
        }
        else
        {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 60, 60)];
            
            imgView.tag = 2001;
            
            imgView.backgroundColor = [UIColor clearColor];
            
            [imgView.layer setCornerRadius:30.0f];
            
            [imgView.layer setMasksToBounds:YES];
            
            ReferralsListDataObject * dataObj;
            
            dataObj = (ReferralsListDataObject *) self.referralsData[indexPath.row];
            
            if([dataObj.referralsImage isEqualToString:@"www.yourvetsnow.com/petappportal/app/webroot/img/category/"])
            {
                [imgView setImage:[UIImage imageNamed:@"petFood.png"]];
            }
            else
            {
                NSString * imageUrlString = [NSString stringWithFormat:@"http://%@",dataObj.referralsImage];
                
                [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            }
            
            [cell.contentView addSubview:imgView];
            
            UILabel *nameLable = [[UILabel alloc] initWithFrame:CGRectMake(90, 18, self.view.frame.size.width-100 , 40)];
            
            nameLable.tag = 1001;
            
            UILabel *descriptionLable = [[UILabel alloc] initWithFrame:CGRectMake(90, 38, self.view.frame.size.width-100 , 40)];
            
            descriptionLable.text = dataObj.referralsDecription;
            
            descriptionLable.tag = 3001;
            
            nameLable.text = dataObj.referralsName;
            
            descriptionLable.textColor = [UIColor grayColor];
            
            descriptionLable.font = [UIFont systemFontOfSize:12];
            
            descriptionLable.numberOfLines = 2;
            
            nameLable.textColor = [UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0];
            
            [cell.contentView addSubview:nameLable];
            
            [cell.contentView addSubview:descriptionLable];
        }
    }
    
    else
    {
        if (!self.referralsFilteredData.count)
        {
            
        }
        else
        {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 60, 60)];
            
            imgView.tag = 2001;
            
            imgView.backgroundColor = [UIColor clearColor];
            
            [imgView.layer setCornerRadius:30.0f];
            
            [imgView.layer setMasksToBounds:YES];
            
            ReferralsListDataObject * dataObj;
            
            dataObj = (ReferralsListDataObject *) self.referralsFilteredData[indexPath.row];
            
            if([dataObj.referralsImage isEqualToString:@"www.yourvetsnow.com/petappportal/app/webroot/img/category/"])
            {
                
                [imgView setImage:[UIImage imageNamed:@"petFood.png"]];
                
            }
            else
            {
                NSString * imageUrlString = [NSString stringWithFormat:@"http://%@",dataObj.referralsImage];
                
                [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            }
            
            [cell.contentView addSubview:imgView];
            
            UILabel *nameLable = [[UILabel alloc] initWithFrame:CGRectMake(90, 18, self.view.frame.size.width-100 , 40)];
            
            nameLable.tag = 1001;
            
            UILabel *descriptionLable = [[UILabel alloc] initWithFrame:CGRectMake(90, 38, self.view.frame.size.width-100 , 40)];
            
            descriptionLable.text = dataObj.referralsDecription;
            
            descriptionLable.tag = 3001;
            
            nameLable.text = dataObj.referralsName;
            
            descriptionLable.textColor = [UIColor grayColor];
            
            descriptionLable.font = [UIFont systemFontOfSize:12];
            
            descriptionLable.numberOfLines = 2;
            
            nameLable.textColor = [UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0];
            
            [cell.contentView addSubview:nameLable];
            
            [cell.contentView addSubview:descriptionLable];
        }
    }
    return cell;
}

//@"Pet Food",@"Pet Vet",@"Pet Clinic"

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    
    ReferralsListDataObject * dataObj;
    
    if(!self.searching)
    {
        dataObj = (ReferralsListDataObject *) self.referralsData[indexPath.row];
    }
    else
    {
        dataObj = (ReferralsListDataObject *) self.referralsFilteredData[indexPath.row];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject: dataObj.referralsId forKey:@"GETReferralsPointsData"];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ReferralsMapViewController * referralsMapView = [[ReferralsMapViewController alloc] init];
    
    [self.navigationController pushViewController:referralsMapView animated:YES];
    
    referralsMapView.headertitle =dataObj.referralsName;
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
//    [self.view endEditing:YES];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
