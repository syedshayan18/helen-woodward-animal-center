//
//  maptestViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/12/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface maptestViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mapview;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView_;

@end
