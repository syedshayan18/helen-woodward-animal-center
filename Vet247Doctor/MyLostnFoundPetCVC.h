//
//  MyLostnFoundPetCVC.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 5/17/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLostnFoundPetCVC : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_lostnfound;
@property (weak, nonatomic) IBOutlet UIButton *btn_deletepet;
@property (weak, nonatomic) IBOutlet UILabel *lbl_petname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_petstatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_age;
@property (weak, nonatomic) IBOutlet UILabel *lbl_loston;
@property (weak, nonatomic) IBOutlet UILabel *lbl_petbreed;
@property (weak, nonatomic) IBOutlet UILabel *lbl_petsex;


@end
