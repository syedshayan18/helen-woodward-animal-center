//
//  PetProfilePictureSectionView.m
//  Vet247Doctor
//
//  Created by APPLE on 8/1/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import "PetProfilePictureSectionView.h"

#import "PetProfileController.h"

#import "ControlsFactory.h"

#import "Define.h"

#import "Masonry.h"

#import "NetworkingClass.h"

#import "MBProgressHUD.h"

#import "PetProfileDataManager.h"

#import "PetProfileKeys.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import "SDWebImage/UIImageView+WebCache.h"


@implementation PetProfilePictureSectionView

- (id) initWithFrame:(CGRect)frame1 {
    
    self = [super initWithFrame:frame1];
    if (self) {
        
        self.userInteractionEnabled = YES;
        
    }
    
    return self;
}


- (void) addComponents {
    
    self.backgroundColor=[UIColor clearColor];
    
    [self setUpPetProfileFieldBG];
    
    [self setUpAfterPictureSelectedView];
    
    self.userSelectedImage.hidden = YES;
    self.changePictureButton.hidden = YES;
    self.removePictureButton.hidden = YES;
    
    self.userSelectedImage.image = nil;
    
    [self setUpUploadPictureButton];
    
    [self setUpSavePetProfileButton];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
    {
        
        [self populateDataToView];
        
        self.setPictureStatus = @"Update";
        
    }
    else
    {
        self.setPictureStatus = @"No";
    }
    
}

-(void) setUpPetProfileFieldBG{
    
    self.petProfilePictureBGImage = [ControlsFactory getImageView];
    
    self.petProfilePictureBGImage.image = [UIImage imageNamed:@"petProfilePictureBGImage.png"];
    
    self.petProfilePictureBGImage.backgroundColor = [UIColor clearColor];
    
    self.petProfilePictureBGImage.userInteractionEnabled=YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 7.5, 5, 7.5);
    
    [self addSubview:self.petProfilePictureBGImage];
    
    [self.petProfilePictureBGImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.mas_left).with.offset(padding.left);
        
        //make.width.mas_equalTo(self.superview.mas_width).with.offset(-15);
        
        make.right.equalTo(self.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(140);
        
    }];
    
}

-(void) setUpUploadPictureButton{
    
    // setting up upload pet Profile Picture Button
    
    self.uploadPetProfilePictureButton = [ControlsFactory getButton];
    
    self.uploadPetProfilePictureButton.backgroundColor = [UIColor clearColor];
    
    UIImage * btnImage = [[UIImage alloc] init];
    
    btnImage = [UIImage imageNamed:@"uploadPetPictureButtonImage.png"];
    
    [self.uploadPetProfilePictureButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    
    [self.uploadPetProfilePictureButton addTarget:self action:@selector(uploadPetProfilePictureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //
    UIEdgeInsets padding = UIEdgeInsetsMake(55, 80, 5, 80);
    
    [self addSubview:self.uploadPetProfilePictureButton];
    
    [self.uploadPetProfilePictureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petProfilePictureBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.petProfilePictureBGImage.mas_left).with.offset(padding.left);
        
        make.right.equalTo(self.petProfilePictureBGImage.mas_right).with.offset(-padding.right);
        
        make.height.mas_equalTo(48);
        
    }];
}

-(void) setUpSavePetProfileButton{
    
    self.savePetProfileButton = [ControlsFactory getButton];
    
    self.savePetProfileButton.backgroundColor = [UIColor clearColor];
    
    UIImage * btnImage = [[UIImage alloc] init];
    
    btnImage = [UIImage imageNamed:@"saveProfile.png"];
    
    [self.savePetProfileButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    
    [self.savePetProfileButton addTarget:self action:@selector(savePetProfileButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(15, 12, 5, 30);
    
    [self addSubview:self.savePetProfileButton];
    
    [self.savePetProfileButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petProfilePictureBGImage.mas_bottom).with.offset(padding.top);
        
        make.centerX.equalTo(self.petProfilePictureBGImage.mas_centerX).with.offset(0);
        
        make.width.equalTo(self.petProfilePictureBGImage.mas_width).with.offset(0);
        
        make.height.mas_equalTo(52);
        
    }];
}

-(void) setUpAfterPictureSelectedView{
    
    
    // user selected image section view
    self.userSelectedImage = [ControlsFactory getImageView];
    
    self.userSelectedImage.backgroundColor = [UIColor clearColor];
    
    self.userSelectedImage.userInteractionEnabled = YES;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(25, 10, 15, 15);
    
    [self addSubview:self.userSelectedImage];
    
    [self.userSelectedImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.petProfilePictureBGImage.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.petProfilePictureBGImage.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.petProfilePictureBGImage.mas_bottom).with.offset(-padding.bottom);
        
        make.width.mas_equalTo(self.petProfilePictureBGImage.mas_height).with.offset(-40);
    }];
    
    // setting remove button
    
    self.removePictureButton = [ControlsFactory getButton];
    
    self.removePictureButton.backgroundColor = [UIColor clearColor];
    
    UIImage * btnImage = [[UIImage alloc] init];
    
    btnImage = [UIImage imageNamed:@"removeButtonBG.png"];
    
    [self.removePictureButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    
    padding = UIEdgeInsetsMake(15, 12, 20, 15);
    
    [self addSubview:self.removePictureButton];
    
    [self.removePictureButton addTarget:self action:@selector(removePictureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.removePictureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.petProfilePictureBGImage.mas_right).with.offset(-padding.right);
        
        make.bottom.equalTo(self.petProfilePictureBGImage.mas_bottom).with.offset(-padding.bottom);
        
        //make.width.mas_equalTo(90);
        
        make.height.mas_equalTo(30);
        
    }];
    
    // setting change button
    
    self.changePictureButton = [ControlsFactory getButton];
    
    self.changePictureButton.backgroundColor = [UIColor clearColor];
    
    btnImage = [UIImage imageNamed:@"changeButtonBG.png"];
    
    [self.changePictureButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    
    [self.changePictureButton addTarget:self action:@selector(uploadPetProfilePictureButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    padding = UIEdgeInsetsMake(15, 12, 20, 15);
    
    [self addSubview:self.changePictureButton];
    
    [self.changePictureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.removePictureButton.mas_left).with.offset(-padding.left);
        
        make.bottom.equalTo(self.petProfilePictureBGImage.mas_bottom).with.offset(-padding.bottom);
        
        make.left.equalTo(self.userSelectedImage.mas_right).with.offset(10);
        
        make.width.mas_equalTo(self.removePictureButton.mas_width);
        
        make.height.mas_equalTo(30);
        
    }];
    
    
}

-(void) savePetProfileButtonClicked: (UIButton *) saveProfileButton
{
    
    //    UIViewController *errorShowViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    //
    //
    //    UIAlertController * checkPictureSelectedErrorAlert =   [UIAlertController
    //                                                            alertControllerWithTitle:@"Error"
    //                                                            message:@""
    //                                                            preferredStyle:UIAlertControllerStyleAlert];
    //    UIAlertAction* ook = [UIAlertAction
    //                          actionWithTitle:@"OK"
    //                          style:UIAlertActionStyleDefault
    //                          handler:^(UIAlertAction * action)
    //                          {
    //                              [checkPictureSelectedErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                          }];
    //    UIAlertAction* caancel = [UIAlertAction
    //                              actionWithTitle:@"Cancel"
    //                              style:UIAlertActionStyleDefault
    //                              handler:^(UIAlertAction * action)
    //                              {
    //                                  [checkPictureSelectedErrorAlert dismissViewControllerAnimated:YES completion:nil];
    //
    //                              }];
    //
    //    [checkPictureSelectedErrorAlert addAction:ook];
    //    [checkPictureSelectedErrorAlert addAction:caancel];
    //
    //    if (self.userSelectedImage.image == nil)
    //    {
    //
    //        [checkPictureSelectedErrorAlert setMessage:@"Please select a Picture for your Pet"];
    //
    //        [errorShowViewController presentViewController:checkPictureSelectedErrorAlert animated:YES completion:nil];
    //
    //        return;
    //    }
    //
    //
    PetProfileDataManager * takePetProfileData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * sendProfileDict;
    
    NSData *petProfileImage = UIImageJPEGRepresentation(self.userSelectedImage.image,0.4);
    
    sendProfileDict = [takePetProfileData getPetProfileInfo];
    
    [sendProfileDict setObject:self.setPictureStatus forKey:PetPictureYesOrNoKey];
    
    //NSLog(@"\n Set Image : %@ \n", [sendProfileDict objectForKey:PetPictureYesOrNoKey]);
    
    NetworkingClass * sendPetProfileDataNetworkObject = [[NetworkingClass alloc] init];
    
    sendPetProfileDataNetworkObject.delegate = self;

    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    [sendPetProfileDataNetworkObject savePetProfileInfo : petProfileImage : sendProfileDict];
    
}//end savePetProfile Button clicked

- (UIViewController *)getContainerController
{
    /// Finds the view's view controller.
    
    // Take the view controller class object here and avoid sending the same message iteratively unnecessarily.
    Class vcc = [UIViewController class];
    
    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass: vcc])
            return (UIViewController *)responder;
    
    // If the view controller isn't found, return nil.
    return nil;
}

// delegates for Picture profile section to network

-(void)response:(id)data {
    
    UIAlertController * petAddedMessageAlert =   [UIAlertController
                                                  alertControllerWithTitle:@"Message"
                                                  message:[data objectForKey:@"msg"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ook = [UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              // NSLog(@"PetProfile Insert Work is done");
                              
                              if([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
                              {
                                  [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"UpdatePET"];
                              }
                              
                              [PetProfileDataManager removePetProfileInfo];
                              
                              [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"Reload_PETDATA"];
                              
                              [petAddedMessageAlert dismissViewControllerAnimated:YES completion:nil];
                              
                              UIViewController *parentController = [self getContainerController];
                              
                              [parentController.navigationController popViewControllerAnimated:YES];
                              
                          }];
    
    [petAddedMessageAlert addAction:ook];
    
    if([data isKindOfClass:[NSDictionary class]])
    {
        if ([[data objectForKey:@"msg"] isEqualToString:@"failure"])
        {
            
            [petAddedMessageAlert setTitle:@"Failure"];
            
            [petAddedMessageAlert setMessage:@"Invalid Credentials"];
            
        }
        else if  ([[data objectForKey:@"status"] isEqualToString:@"success"])
        {
            
            [petAddedMessageAlert setTitle:@"Success"];
            
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"UpdatePET"] isEqualToString:@"Yes"])
            {
                [petAddedMessageAlert setMessage:[data objectForKey:@"msg"]];
            }
            else
            {
                [petAddedMessageAlert setMessage:[data objectForKey:@"msg"]];
            }
            
        }
        
    }
    
    UIViewController *parentController = [self getContainerController];
    
    [parentController presentViewController:petAddedMessageAlert animated:YES completion:nil];
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}

-(void)error:(NSError*)error
{
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}

-(void) populateDataToView
{
    PetProfileDataManager * getPetsData = [PetProfileDataManager getPetProfileDataManagerSharedInstance];
    
    NSMutableDictionary * NSPetData;
    
    NSPetData = [getPetsData getPetProfileInfo];
    
    if ([[NSPetData objectForKey:@"image"] isEqualToString:@"http://yourvetsnow.com/petappportal/app/webroot/img/pet/"])
    {
        
        self.setPictureStatus = @"No";
        
        // nothing else in this function "populateDataToView" will be executed after return
        return;
        
    }
    
    self.uploadPetProfilePictureButton.hidden = YES;
    
    self.userSelectedImage.hidden = NO;
    
    [self.userSelectedImage sd_setImageWithURL:[NSURL URLWithString:[NSPetData objectForKey:@"image"]]];
    
    self.changePictureButton.hidden = NO;
    
    self.removePictureButton.hidden = NO;
    
}


- (void) uploadPetProfilePictureButtonClicked:(UIButton *) button {
    
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIAlertController * uploadOptionsAlert =   [UIAlertController
                                                alertControllerWithTitle:@"Choose option"
                                                message:@""
                                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:@"Take Picture (camera)"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                 picker.delegate = self;
                                 picker.allowsEditing = YES;
                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                 
                                 [rootViewController presentViewController:picker animated:YES completion:NULL];
                                 
                                 [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    UIAlertAction* library = [UIAlertAction
                              actionWithTitle:@"Select Picture (library)"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                                  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                  picker.delegate = self;
                                  picker.allowsEditing = YES;
                                  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                  
                                  [rootViewController presentViewController:picker animated:YES completion:NULL];
                                  
                                  [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [uploadOptionsAlert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [uploadOptionsAlert addAction:camera];
    
    [uploadOptionsAlert addAction:library];
    
    [uploadOptionsAlert addAction:cancel];
    
    [rootViewController presentViewController:uploadOptionsAlert animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.setPictureStatus = @"Yes";
    
    self.uploadPetProfilePictureButton.hidden = YES;
    
    self.userSelectedImage.hidden = NO;
    
    self.userSelectedImage.image = chosenImage;
    
    self.changePictureButton.hidden = NO;
    
    self.removePictureButton.hidden = NO;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void) removePictureButtonClicked:(UIButton *) btn2
{
    
    self.setPictureStatus = @"No";
    
    self.userSelectedImage.image = nil;
    
    self.uploadPetProfilePictureButton.hidden = NO;
    
    self.userSelectedImage.hidden = YES;
    
    self.removePictureButton.hidden = YES;
    
    self.changePictureButton.hidden = YES;
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
