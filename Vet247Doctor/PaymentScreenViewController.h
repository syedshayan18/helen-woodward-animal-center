//
//  PaymentScreenViewController.h
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/14/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
@interface PaymentScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txt_couponenter;
@property (weak, nonatomic) IBOutlet UIButton *btn_coupon;
@property (strong, nonatomic) UITapGestureRecognizer * gestureRecognizer;
@end
