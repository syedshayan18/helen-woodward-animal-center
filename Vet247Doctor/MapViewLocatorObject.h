//
//  MapViewLocatorObject.h
//  Vet247Doctor
//
//  Created by APPLE on 8/20/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <MapKit/MapKit.h>

#import <Foundation/Foundation.h>

@interface MapViewLocatorObject : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@property (copy, nonatomic) NSString *title;

@property (retain) NSString *indexNumber;

@property NSString *distance;

@property (strong, nonatomic) UIButton* button;

//@property NSString* locationName;

-(id)initWithTitle:(NSString *) newTitle Location:(CLLocationCoordinate2D)location Index:(int) index;

-(MKAnnotationView *) annotaionView;

@end
