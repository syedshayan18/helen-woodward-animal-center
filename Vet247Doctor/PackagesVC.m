//
//  PaymentVC.m
//  Vet247Doctor
//
//  Created by MacBook  on 1/7/16.
//  Copyright © 2016 Inventions Studio. All rights reserved.

#import "PackagesVC.h"
#import "ControlsFactory.h"
#import "Masonry.h"
#import "DashboardController.h"
#import "PromotionalCodeUtills.h"
#import "PaymentTermsAndConditionController.h"
#import "PaymentController.h"

@interface PackagesVC()
{
    CGSize size;
}

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *headerViewlabel;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *backGroundImage;
@property (nonatomic, strong) UIView *boxView1;
@property (nonatomic, strong) UIView *blankView1;
@property (nonatomic, strong) UILabel *accessLabel1;
@property (nonatomic, strong) UILabel *unlimitedLabel1;
@property (nonatomic, strong) UIButton *blankbutton1;
@property (nonatomic, strong) UILabel *costLabel1;
@property (nonatomic, strong) UIView *blankView2;
@property (nonatomic, strong) UILabel *accessLabel2;
@property (nonatomic, strong) UILabel *unlimitedLabel2;
@property (nonatomic, strong) UIButton *blankbutton2;
@property (nonatomic, strong) UILabel *costLabel2;
@property (nonatomic, strong) UIView *blankView3;
@property (nonatomic, strong) UILabel *accessLabel3;
@property (nonatomic, strong) UILabel *unlimitedLabel3;
@property (nonatomic, strong) UIButton *blankbutton3;
@property (nonatomic, strong) UILabel *costLabel3;
@property (nonatomic, strong) UIView *blankView4;
@property (nonatomic, strong) UILabel *accessLabel4;
@property (nonatomic, strong) UILabel *unlimitedLabel4;
@property (nonatomic, strong) UIButton *blankbutton4;
@property (nonatomic, strong) UILabel *costLabel4;
@property (nonatomic, strong) UIView *blankView5;
@property (nonatomic, strong) UILabel *accessLabel5;
@property (nonatomic, strong) UILabel *unlimitedLabel5;
@property (nonatomic, strong) UIButton *blankbutton5;
@property (nonatomic, strong) UILabel *costLabel5;
@property (nonatomic, strong) UIView *blankView6;
@property (nonatomic, strong) UILabel *accessLabel6;
@property (nonatomic, strong) UILabel *unlimitedLabel6;
@property (nonatomic, strong) UIButton *blankbutton6;
@property (nonatomic, strong) UILabel *costLabel6;
@property (nonatomic, strong) UIView *boxView2;
@property (nonatomic, strong) UIView *boxView3;
@property (nonatomic, strong) UIView *boxView4;
@property (nonatomic, strong) UIView *boxView5;
@property (nonatomic, strong) UIView *boxView6;

@property (nonatomic, strong) UIButton *couponButton;

@end

@implementation PackagesVC

#pragma -mark LifeCycle

-(void)viewDidLoad
{
    
    
    [super viewDidLoad];
    [self mainViewFunc];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma -mark HeaderView

- (void) setUpHeaderView
{
    self.headerView=[ControlsFactory getView];
    
    self.headerView.backgroundColor=[UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    
    UIEdgeInsets paddingForHeaderView = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.left.equalTo(self.view.mas_left).with.offset(paddingForHeaderView.left);
        
        make.right.equalTo(self.view.mas_right).with.offset(-paddingForHeaderView.right);
        
        make.top.equalTo(self.view.mas_top).with.offset(paddingForHeaderView.top);
        
        make.height.mas_equalTo(75);
        
    }];
    
    UIEdgeInsets paddingForheaderViewlabel = UIEdgeInsetsMake(0, 0, 5, 0);
    
    self.headerViewlabel =[ControlsFactory getLabel];
    
    self.headerViewlabel.text=@"Payment";
    
    [self.headerViewlabel setTextAlignment:NSTextAlignmentCenter];
    
    self.headerViewlabel.font= [UIFont systemFontOfSize:20.0];
    
    self.headerViewlabel.textColor=[UIColor whiteColor];
    
    [self.headerView addSubview:self.headerViewlabel];
    
    [self.headerViewlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.headerView.mas_centerX);
        
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-paddingForheaderViewlabel.bottom);
        
        make.height.mas_equalTo(40);
        
        make.width.mas_equalTo(130);
        
    }];
    
}


-(void) setUpBackButton {
    
    UIEdgeInsets padding = UIEdgeInsetsMake(40, 10, 15, 0);
    
    self.backButton=[ControlsFactory getButton];
    
    self.backButton.backgroundColor=[UIColor clearColor];
    
    //self.backButton.contentEdgeInsets= UIEdgeInsetsMake(0, 0, -10, 0);
    
    [self.backButton setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.headerView.mas_left).with.offset(padding.left);
        
        //make.top.mas_equalTo(self.headerView.mas_top).with.offset(padding.top);
        make.bottom.mas_equalTo(self.headerView.mas_bottom).with.offset(-padding.bottom);
        
        make.height.mas_equalTo(20);
        
        make.width.mas_equalTo(30);
        
    }];
}

- (void) backButtonClicked: (UIButton *) button {
    
    DashboardController *dashboard=[[DashboardController alloc] init];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [navController setNavigationBarHidden:YES animated:YES];
    
    [UIView transitionWithView:appDelegate.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        appDelegate.window.rootViewController = navController;
                    }
                    completion:nil];
    
    [appDelegate.window makeKeyAndVisible];
}

#pragma mark-MainView

-(void) mainViewFunc
{
    [self setUpBGImage];
    [self setUpHeaderView];
    [self boxView1Func];
    [self boxView2Func];
    [self boxView3Func];
    [self boxView4Func];
    [self boxView5Func];
    [self boxView6Func];
    
    [self couponFunc];
    
    
    
}

- (void) setUpBGImage{
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.backGroundImage=[ControlsFactory getImageView];
    
    [ self.backGroundImage setImage:[UIImage imageNamed:@"app_bg.png"]];
    
    [self.view addSubview: self.backGroundImage];
    
    [ self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top).with.offset(padding.top);
        
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}

#pragma mark-button1

-(void)boxView1Func
{
     size = self.view.bounds.size;
    self.boxView1 = [ControlsFactory getView];
    self.boxView1.layer.borderWidth = 2.0;
    self.boxView1.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView1.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView1];
    [self.boxView1 mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.top.mas_equalTo(self.headerView.mas_bottom).with.offset(15);
        make.left.mas_equalTo(self.backGroundImage.mas_left).with.offset(15);
        make.height.mas_equalTo((size.height-135)/3.5);
        make.width.mas_equalTo((size.width-45)/2);
    }];
    [self blankView1Func];
}

-(void)blankView1Func
{
    size = self.view.bounds.size;
    self.blankView1 = [ControlsFactory getView];
    self.blankView1.backgroundColor = [UIColor clearColor];
    [self.boxView1 addSubview:self.blankView1];
    [self.blankView1 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView1.mas_centerY);
         make.left.mas_equalTo(self.boxView1.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView1.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel1Func];
    [self unlimitedLabel1Func];
    [self costLabel1Func];
    [self blankButton1Func];
}


-(void)accessLabel1Func
{
    self.accessLabel1 = [ControlsFactory getLabel];
    self.accessLabel1.text = @"For 1 day access";
     self.accessLabel1.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel1.textColor = [UIColor whiteColor];
    self.accessLabel1.textAlignment = NSTextAlignmentCenter;
    [self.blankView1 addSubview:self.accessLabel1];
    [self.accessLabel1 mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.top.mas_equalTo(self.blankView1.mas_top).with.offset(0);
        make.left.mas_equalTo(self.blankView1.mas_left).with.offset(0);
        make.right.mas_equalTo(self.blankView1.mas_right).with.offset(0);
        make.height.mas_equalTo(20);
    }];
}

-(void)unlimitedLabel1Func
{
    self.unlimitedLabel1 = [ControlsFactory getLabel];
    self.unlimitedLabel1.text = @"UNLIMITED";
    self.unlimitedLabel1.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel1.textColor = [UIColor blackColor];
    self.unlimitedLabel1.textAlignment = NSTextAlignmentCenter;
    [self.blankView1 addSubview:self.unlimitedLabel1];
    [self.unlimitedLabel1 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel1.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView1.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView1.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel1Func
{
    self.costLabel1 = [ControlsFactory getLabel];
    self.costLabel1.text = @"$3.99";
    self.costLabel1.font = [UIFont systemFontOfSize:14.0];
    self.costLabel1.textColor = [UIColor whiteColor];
    self.costLabel1.textAlignment = NSTextAlignmentCenter;
    [self.blankView1 addSubview:self.costLabel1];
    [self.costLabel1 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel1.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView1.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView1.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button1Action:(UIButton *)sender
{
    NSLog(@"button 1");

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"3.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 1;
    PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    [self.navigationController pushViewController:paymentTermsController animated:YES];
    
}

-(void)blankButton1Func
{
    self.blankbutton1 = [ControlsFactory getButton];
    self.blankbutton1.backgroundColor = [UIColor clearColor];
    [self.blankbutton1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView1 addSubview:self.blankbutton1];
    [self.blankbutton1 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView1.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView1.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView1.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView1.mas_bottom).with.offset(0);
     }];
}

#pragma mark-button2

-(void)boxView2Func
{
    size = self.view.bounds.size;
    self.boxView2 = [ControlsFactory getView];
    self.boxView2.layer.borderWidth = 2.0;
    self.boxView2.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView2.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView2];
    [self.boxView2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.headerView.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.boxView1.mas_right).with.offset(15);
         make.height.mas_equalTo((size.height-135)/3.5);
         make.width.mas_equalTo((size.width-45)/2);
     }];
    [self blankView2Func];
}

-(void)blankView2Func
{
    size = self.view.bounds.size;
    self.blankView2 = [ControlsFactory getView];
    self.blankView2.backgroundColor = [UIColor clearColor];
    [self.boxView2 addSubview:self.blankView2];
    [self.blankView2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView2.mas_centerY);
         make.left.mas_equalTo(self.boxView2.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView2.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel2Func];
    [self unlimitedLabel2Func];
    [self costLabel2Func];
    [self blankButton2Func];
}


-(void)accessLabel2Func
{
    self.accessLabel2 = [ControlsFactory getLabel];
    self.accessLabel2.text = @"For 1 Month access";
    self.accessLabel2.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel2.numberOfLines = 0;
    self.accessLabel2.textColor = [UIColor whiteColor];
    self.accessLabel2.textAlignment = NSTextAlignmentCenter;
    [self.blankView2 addSubview:self.accessLabel2];
    [self.accessLabel2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.blankView2.mas_top).with.offset(0);
         make.left.mas_equalTo(self.blankView2.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView2.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)unlimitedLabel2Func
{
    self.unlimitedLabel2 = [ControlsFactory getLabel];
    self.unlimitedLabel2.text = @"UNLIMITED";
     self.unlimitedLabel2.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel2.textColor = [UIColor blackColor];
    self.unlimitedLabel2.textAlignment = NSTextAlignmentCenter;
    [self.blankView2 addSubview:self.unlimitedLabel2];
    [self.unlimitedLabel2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel2.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView2.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView2.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel2Func
{
    self.costLabel2 = [ControlsFactory getLabel];
    self.costLabel2.text = @"$12.99";
     self.costLabel2.font = [UIFont systemFontOfSize:14.0];
    self.costLabel2.textColor = [UIColor whiteColor];
    self.costLabel2.textAlignment = NSTextAlignmentCenter;
    [self.blankView2 addSubview:self.costLabel2];
    [self.costLabel2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel2.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView2.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView2.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button2Action:(UIButton *)sender
{
    NSLog(@"button 2");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"12.99" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 2;
    PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    [self.navigationController pushViewController:paymentTermsController animated:YES];
}

-(void)blankButton2Func
{
    self.blankbutton2 = [ControlsFactory getButton];
    self.blankbutton2.backgroundColor = [UIColor clearColor];
    [self.blankbutton2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView2 addSubview:self.blankbutton2];
    [self.blankbutton2 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView2.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView2.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView2.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView2.mas_bottom).with.offset(0);
     }];
}


#pragma mark-button3

-(void)boxView3Func
{
    size = self.view.bounds.size;
    self.boxView3 = [ControlsFactory getView];
    self.boxView3.layer.borderWidth = 2.0;
    self.boxView3.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView3.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView3];
    [self.boxView3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView1.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.backGroundImage.mas_left).with.offset(15);
         make.height.mas_equalTo((size.height-135)/3.5);
         make.width.mas_equalTo((size.width-45)/2);
     }];
    [self blankView3Func];
}

-(void)blankView3Func
{
    size = self.view.bounds.size;
    self.blankView3 = [ControlsFactory getView];
    self.blankView3.backgroundColor = [UIColor clearColor];
    [self.boxView3 addSubview:self.blankView3];
    [self.blankView3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView3.mas_centerY);
         make.left.mas_equalTo(self.boxView3.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView3.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel3Func];
    [self unlimitedLabel3Func];
    [self costLabel3Func];
    [self blankButton3Func];
}


-(void)accessLabel3Func
{
    self.accessLabel3 = [ControlsFactory getLabel];
    self.accessLabel3.text = @"For 3 Month access";
     self.accessLabel3.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel3.textColor = [UIColor whiteColor];
    self.accessLabel3.textAlignment = NSTextAlignmentCenter;
    [self.blankView3 addSubview:self.accessLabel3];
    [self.accessLabel3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.blankView3.mas_top).with.offset(0);
         make.left.mas_equalTo(self.blankView3.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView3.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)unlimitedLabel3Func
{
    self.unlimitedLabel3 = [ControlsFactory getLabel];
    self.unlimitedLabel3.text = @"UNLIMITED";
     self.unlimitedLabel3.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel3.textColor = [UIColor blackColor];
    self.unlimitedLabel3.textAlignment = NSTextAlignmentCenter;
    [self.blankView3 addSubview:self.unlimitedLabel3];
    [self.unlimitedLabel3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel3.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView3.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView3.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel3Func
{
    self.costLabel3 = [ControlsFactory getLabel];
    self.costLabel3.text = @"$32.85";
     self.costLabel3.font = [UIFont systemFontOfSize:14.0];
    self.costLabel3.textColor = [UIColor whiteColor];
    self.costLabel3.textAlignment = NSTextAlignmentCenter;
    [self.blankView3 addSubview:self.costLabel3];
    [self.costLabel3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel3.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView3.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView3.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button3Action:(UIButton *)sender
{
    NSLog(@"button 3");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"32.85" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 3;
    PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    [self.navigationController pushViewController:paymentTermsController animated:YES];
}

-(void)blankButton3Func
{
    self.blankbutton3 = [ControlsFactory getButton];
    self.blankbutton3.backgroundColor = [UIColor clearColor];
    [self.blankbutton3 addTarget:self action:@selector(button3Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView3 addSubview:self.blankbutton3];
    [self.blankbutton3 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView3.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView3.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView3.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView3.mas_bottom).with.offset(0);
     }];
}


#pragma mark-button4

-(void)boxView4Func
{
    size = self.view.bounds.size;
    self.boxView4 = [ControlsFactory getView];
    self.boxView4.layer.borderWidth = 2.0;
    self.boxView4.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView4.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView4];
    [self.boxView4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView1.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.boxView3.mas_right).with.offset(15);
         make.height.mas_equalTo((size.height-135)/3.5);
         make.width.mas_equalTo((size.width-45)/2);
     }];
    [self blankView4Func];
}

-(void)blankView4Func
{
    size = self.view.bounds.size;
    self.blankView4 = [ControlsFactory getView];
    self.blankView4.backgroundColor = [UIColor clearColor];
    [self.boxView4 addSubview:self.blankView4];
    [self.blankView4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView4.mas_centerY);
         make.left.mas_equalTo(self.boxView4.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView4.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel4Func];
    [self unlimitedLabel4Func];
    [self costLabel4Func];
    [self blankButton4Func];
}


-(void)accessLabel4Func
{
    self.accessLabel4 = [ControlsFactory getLabel];
    self.accessLabel4.text = @"For 6 Month access";
     self.accessLabel4.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel4.textColor = [UIColor whiteColor];
    self.accessLabel4.textAlignment = NSTextAlignmentCenter;
    [self.blankView4 addSubview:self.accessLabel4];
    [self.accessLabel4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.blankView4.mas_top).with.offset(0);
         make.left.mas_equalTo(self.blankView4.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView4.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)unlimitedLabel4Func
{
    self.unlimitedLabel4 = [ControlsFactory getLabel];
    self.unlimitedLabel4.text = @"UNLIMITED";
     self.unlimitedLabel4.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel4.textColor = [UIColor blackColor];
    self.unlimitedLabel4.textAlignment = NSTextAlignmentCenter;
    [self.blankView4 addSubview:self.unlimitedLabel4];
    [self.unlimitedLabel4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel4.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView4.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView4.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel4Func
{
    self.costLabel4 = [ControlsFactory getLabel];
    self.costLabel4.text = @"$60";
     self.costLabel4.font = [UIFont systemFontOfSize:14.0];
    self.costLabel4.textColor = [UIColor whiteColor];
    self.costLabel4.textAlignment = NSTextAlignmentCenter;
    [self.blankView4 addSubview:self.costLabel4];
    [self.costLabel4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel4.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView4.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView4.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button4Action:(UIButton *)sender
{
    NSLog(@"button 4");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"60" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 4;
    PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    [self.navigationController pushViewController:paymentTermsController animated:YES];
}

-(void)blankButton4Func
{
    self.blankbutton4 = [ControlsFactory getButton];
    self.blankbutton4.backgroundColor = [UIColor clearColor];
    [self.blankbutton4 addTarget:self action:@selector(button4Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView4 addSubview:self.blankbutton4];
    [self.blankbutton4 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView4.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView4.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView4.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView4.mas_bottom).with.offset(0);
     }];
}


#pragma mark-button5

-(void)boxView5Func
{
    size = self.view.bounds.size;
    self.boxView5 = [ControlsFactory getView];
    self.boxView5.layer.borderWidth = 2.0;
    self.boxView5.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView5.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView5];
    [self.boxView5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView3.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.backGroundImage.mas_left).with.offset(15);
         make.height.mas_equalTo((size.height-135)/3.5);
         make.width.mas_equalTo((size.width-45)/2);
     }];
    [self blankView5Func];
}

-(void)blankView5Func
{
    size = self.view.bounds.size;
    self.blankView5 = [ControlsFactory getView];
    self.blankView5.backgroundColor = [UIColor clearColor];
    [self.boxView5 addSubview:self.blankView5];
    [self.blankView5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView5.mas_centerY);
         make.left.mas_equalTo(self.boxView5.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView5.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel5Func];
    [self unlimitedLabel5Func];
    [self costLabel5Func];
    [self blankButton5Func];
}


-(void)accessLabel5Func
{
    self.accessLabel5 = [ControlsFactory getLabel];
    self.accessLabel5.text = @"For 1 year access";
    self.accessLabel5.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel5.textColor = [UIColor whiteColor];
    self.accessLabel5.textAlignment = NSTextAlignmentCenter;
    [self.blankView5 addSubview:self.accessLabel5];
    [self.accessLabel5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.blankView5.mas_top).with.offset(0);
         make.left.mas_equalTo(self.blankView5.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView5.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)unlimitedLabel5Func
{
    self.unlimitedLabel5 = [ControlsFactory getLabel];
    self.unlimitedLabel5.text = @"UNLIMITED";
    self.unlimitedLabel5.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel5.textColor = [UIColor blackColor];
    self.unlimitedLabel5.textAlignment = NSTextAlignmentCenter;
    [self.blankView5 addSubview:self.unlimitedLabel5];
    [self.unlimitedLabel5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel5.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView5.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView5.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel5Func
{
    self.costLabel5 = [ControlsFactory getLabel];
    self.costLabel5.text = @"$107.40";
    self.costLabel5.font = [UIFont systemFontOfSize:14.0];
    self.costLabel5.textColor = [UIColor whiteColor];
    self.costLabel5.textAlignment = NSTextAlignmentCenter;
    [self.blankView5 addSubview:self.costLabel5];
    [self.costLabel5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel5.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView5.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView5.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button5Action:(UIButton *)sender
{
    NSLog(@"button 5");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AmountToBePaidByUser];
    [[NSUserDefaults standardUserDefaults] setObject:@"107" forKey:AmountToBePaidByUser];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.isSelectedAnyPaymentPackage = YES;
    app.selectedPaymentPackageID = 5;
    PaymentTermsAndConditionController *paymentTermsController = [[PaymentTermsAndConditionController alloc] init];
    [self.navigationController pushViewController:paymentTermsController animated:YES];
}

-(void)blankButton5Func
{
    self.blankbutton5 = [ControlsFactory getButton];
    self.blankbutton5.backgroundColor = [UIColor clearColor];
    [self.blankbutton5 addTarget:self action:@selector(button5Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView5 addSubview:self.blankbutton5];
    [self.blankbutton5 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView5.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView5.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView5.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView5.mas_bottom).with.offset(0);
     }];
}


#pragma mark-button6

-(void)boxView6Func
{
    size = self.view.bounds.size;
    self.boxView6 = [ControlsFactory getView];
    self.boxView6.layer.borderWidth = 2.0;
    self.boxView6.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.boxView6.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.boxView6];
    [self.boxView6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView3.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.boxView5.mas_right).with.offset(15);
         make.height.mas_equalTo((size.height-135)/3.5);
         make.width.mas_equalTo((size.width-45)/2);
     }];
    [self blankView6Func];
}

-(void)blankView6Func
{
    size = self.view.bounds.size;
    self.blankView6 = [ControlsFactory getView];
    self.blankView6.backgroundColor = [UIColor clearColor];
    [self.boxView6 addSubview:self.blankView6];
    [self.blankView6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.centerY.mas_equalTo(self.boxView6.mas_centerY);
         make.left.mas_equalTo(self.boxView6.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView6.mas_right).with.offset(0);
         make.height.mas_equalTo(65);
     }];
    [self accessLabel6Func];
    [self unlimitedLabel6Func];
    [self costLabel6Func];
    [self blankButton6Func];
}


-(void)accessLabel6Func
{
    self.accessLabel6 = [ControlsFactory getLabel];
    self.accessLabel6.text = @"Access our referrals";
    self.accessLabel6.font = [UIFont systemFontOfSize:14.0];
    self.accessLabel6.textColor = [UIColor whiteColor];
    self.accessLabel6.textAlignment = NSTextAlignmentCenter;
    [self.blankView6 addSubview:self.accessLabel6];
    [self.accessLabel6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.blankView6.mas_top).with.offset(0);
         make.left.mas_equalTo(self.blankView6.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView6.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)unlimitedLabel6Func
{
    self.unlimitedLabel6 = [ControlsFactory getLabel];
    self.unlimitedLabel6.text = @"UNLIMITED";
    self.unlimitedLabel6.font = [UIFont systemFontOfSize:14.0];
    self.unlimitedLabel6.textColor = [UIColor blackColor];
    self.unlimitedLabel6.textAlignment = NSTextAlignmentCenter;
    [self.blankView6 addSubview:self.unlimitedLabel6];
    [self.unlimitedLabel6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.accessLabel6.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView6.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView6.mas_right).with.offset(0);
         make.height.mas_equalTo(25);
     }];
}

-(void)costLabel6Func
{
    self.costLabel6 = [ControlsFactory getLabel];
    self.costLabel6.text = @"Free";
    self.costLabel6.font = [UIFont systemFontOfSize:14.0];
    self.costLabel6.textColor = [UIColor whiteColor];
    self.costLabel6.textAlignment = NSTextAlignmentCenter;
    [self.blankView6 addSubview:self.costLabel6];
    [self.costLabel6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.unlimitedLabel6.mas_bottom).with.offset(0);
         make.left.mas_equalTo(self.blankView6.mas_left).with.offset(0);
         make.right.mas_equalTo(self.blankView6.mas_right).with.offset(0);
         make.height.mas_equalTo(20);
     }];
}

-(void)button6Action:(UIButton *)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLoggin"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"firstLoggin"];
    
    DashboardController *dashboard=[[DashboardController alloc] init];
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:dashboard];
        
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
    [navController setNavigationBarHidden:YES animated:YES];
        
    [UIView transitionWithView:appDelegate.window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            appDelegate.window.rootViewController = navController;
                        }
                        completion:nil];
        
    [appDelegate.window makeKeyAndVisible];
}

-(void)blankButton6Func
{
    self.blankbutton6 = [ControlsFactory getButton];
    self.blankbutton6.backgroundColor = [UIColor clearColor];
    [self.blankbutton6 addTarget:self action:@selector(button6Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.boxView6 addSubview:self.blankbutton6];
    [self.blankbutton6 mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView6.mas_top).with.offset(0);
         make.left.mas_equalTo(self.boxView6.mas_left).with.offset(0);
         make.right.mas_equalTo(self.boxView6.mas_right).with.offset(0);
         make.bottom.mas_equalTo(self.boxView6.mas_bottom).with.offset(0);
     }];
}

#pragma mark-Coupon Section

-(void)couponFunc
{
    size = self.view.bounds.size;
    self.couponButton = [ControlsFactory getButton];
    self.couponButton.layer.borderWidth = 2.0;
    self.couponButton.layer.borderColor = [[UIColor colorWithRed:55/255.0 green:91/255.0 blue:90/255.0 alpha:1] CGColor];
    self.couponButton.backgroundColor = [UIColor colorWithRed:68.0/255 green:144.0/255 blue:78.0/255 alpha:1.0];
    [self.view addSubview:self.couponButton];
    
    [self.couponButton setTitle:@"Coupon Gift Card" forState:UIControlStateNormal];
    
    self.couponButton.titleLabel.tintColor = [UIColor whiteColor];
    
    [self.couponButton mas_makeConstraints:^(MASConstraintMaker *make)
     {
         make.top.mas_equalTo(self.boxView6.mas_bottom).with.offset(15);
         make.left.mas_equalTo(self.view.mas_left).with.offset(15);
         make.height.mas_equalTo((size.height-135)/8);
         make.right.mas_equalTo(self.view.mas_right).with.offset(-15);
     }];
    
    [self.couponButton addTarget:self action:@selector(moveToPaymentController) forControlEvents:UIControlEventTouchUpInside];
}

-(void)moveToPaymentController
{
    PaymentController *paymentController = [[PaymentController alloc] init];
    [self.navigationController pushViewController:paymentController animated:YES];
}


@end
