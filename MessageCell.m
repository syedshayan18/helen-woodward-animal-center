//
//  MessageCell.m
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/3/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//
#import "Masonry.h"
#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.imageIcon=[[UIImageView alloc] init];
        
        self.imageIcon.backgroundColor=[UIColor clearColor];
        
        UIEdgeInsets paddingForImageView = UIEdgeInsetsMake(15, 15, 15, 0);
        
        [self.contentView addSubview:self.imageIcon];
        
        [self.imageIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.contentView.mas_left).with.offset(paddingForImageView.left);
            
            make.top.equalTo(self.contentView.mas_top).with.offset(paddingForImageView.top);
            
            make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-paddingForImageView.bottom);
            
            make.height.mas_equalTo(41.5);
            
            make.width.mas_equalTo(41.5);
        }];

        
        self.nameLbl =[[UILabel alloc] init];

        [self.nameLbl setNumberOfLines:1];
        
        
        [self.nameLbl setTextAlignment:NSTextAlignmentLeft];
        
        [self.nameLbl setTextColor:[UIColor colorWithRed:255.0/255 green:144.0/255 blue:62.0/255 alpha:1.0]];
        
        self.nameLbl.font=[UIFont systemFontOfSize:18.0];
        
        [self.nameLbl setBackgroundColor:[UIColor clearColor]];
        
        UIEdgeInsets paddingForNameLbl = UIEdgeInsetsMake(15, 15, 0, 0);
        
        [self.contentView addSubview:self.nameLbl];
        
        [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(self.imageIcon.mas_right).with.offset(paddingForNameLbl.left);
            
            make.top.equalTo(self.contentView.mas_top).with.offset(paddingForNameLbl.top);
            
            make.height.mas_equalTo(25.0);
        }];
        
        
        self.messageTxt=[[UILabel alloc] init];
    
        [self.messageTxt setTextColor:[UIColor colorWithRed:109.0/255 green:109.0/255 blue:109.0/255 alpha:1.0]];
        
        self.messageTxt.backgroundColor=[UIColor clearColor];
        
        self.messageTxt.textAlignment=NSTextAlignmentLeft;
    
        self.messageTxt.font=[UIFont systemFontOfSize:13.0];
        
        self.messageTxt.lineBreakMode=NSLineBreakByTruncatingTail;
        
        [self.contentView addSubview:self.messageTxt];
        
        UIEdgeInsets paddingForMessageText = UIEdgeInsetsMake(5, 15, 8, 0);
        
        [self.contentView addSubview:self.nameLbl];
        
        [self.messageTxt mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.imageIcon.mas_right).with.offset(paddingForMessageText.left);
            
            make.top.equalTo(self.nameLbl.mas_bottom).with.offset(paddingForMessageText.top);
            
            make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-paddingForMessageText.bottom);
            
            make.right.equalTo(self.contentView.mas_right).with.offset(-paddingForMessageText.right);
            
            make.height.mas_equalTo(17);
        }];
        
    }
    return self;

}

@end
