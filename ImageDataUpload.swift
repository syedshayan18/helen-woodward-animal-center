//
//  ImageDataUpload.swift
//  Vet247Doctor
//
//  Created by Shayan Ali on 4/10/17.
//  Copyright © 2017 Inventions Studio. All rights reserved.
//

import UIKit

@objc class ImageDataUpload : NSObject{


    func myImageUploadRequest(params: NSDictionary , url: String, image: UIImage?, completion:@escaping (_ data: NSDictionary?, _ error: NSError?)-> Void)
    {
        
        
        let myUrl = NSURL(string: url);
        
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        //   activity.frame = CGRectMake(view.frame.width/2 - 60, view.frame.height/2, 60, 60)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if let img = image{
        let imageData = UIImageJPEGRepresentation(img, 1)! as NSData
        request.httpBody = createBodyWithParameters(parameters: params as? [String : String], filePathKey: "file", imageDataKey: imageData , boundary: boundary) as Data
            
        
        }
        else{
        request.httpBody = createBodyWithParameters(parameters: params as? [String : String], filePathKey: "file", imageDataKey: nil , boundary: boundary) as Data
        
        }
        
        
        //    myActivityIndicator.startAnimating();
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
               completion(nil,error as? NSError)
                return
            }
                
                
            else{
                do{
                let jsonObject = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                if let object = jsonObject as? NSDictionary{
                print(object)
                completion(object, nil)
                }
                // Print out reponse body
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("****** response data = \(responseString!)")
                    
            }
                catch let error as NSError{
                
                
                }
            }
            
        }
        
        task.resume()
      
    }
    
    
    
    func imageUploadRequest(params: NSDictionary? , url: String, image: UIImage?, completion:@escaping (_ data: NSDictionary?, _ error: NSError?)-> Void)
    {
        
        
        let myUrl = NSURL(string: url);
        
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        //   activity.frame = CGRectMake(view.frame.width/2 - 60, view.frame.height/2, 60, 60)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if let img = image{
            let imageData = UIImageJPEGRepresentation(img, 1)! as NSData
            request.httpBody = createBodyWithParams(parameters: params  , filePathKey: "image", imageDataKey: imageData , boundary: boundary) as Data
            
            
        }
        else{
            request.httpBody = createBodyWithParams(parameters: params  , filePathKey: "image", imageDataKey: nil , boundary: boundary) as Data
            
        }
        
        
        //    myActivityIndicator.startAnimating();
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                completion(nil,error as? NSError)
                return
            }
                
                
            else{
                do{
                    let jsonObject = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                    if let object = jsonObject as? NSDictionary{
                        print(object)
                        completion(object, nil)
                    }
                    // Print out reponse body
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("****** response data = \(responseString!)")
                    
                }
                catch let error as NSError{
                    
                    
                }
            }
            
        }
        
        task.resume()
        
    }
    
    
   
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData?, boundary: String) -> NSData {
        var body = NSMutableData()
        
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(string:"--\(boundary)\r\n")
                body.append(string:"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append(string:"\(value)\r\n")
            }
        }
        
        let filename = "user-profile-\(Date().timeIntervalSince1970).jpg"
        
        let mimetype = "image/jpg"
        
        body.append(string:"--\(boundary)\r\n")
        body.append(string:"Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.append(string:"Content-Type: \(mimetype)\r\n\r\n")
        if let data = imageDataKey as? Data{
        body.append(data)
        }
        body.append(string:"\r\n")
        
        
        
        body.append(string:"--\(boundary)--\r\n")
        
        return body
    }
    func createBodyWithParams(parameters: NSDictionary?, filePathKey: String?, imageDataKey: NSData?, boundary: String) -> NSData {
        var body = NSMutableData()
        
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(string:"--\(boundary)\r\n")
                body.append(string:"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append(string:"\(value)\r\n")
            }
        }
        
        let filename = "lost-pet-\(Date().timeIntervalSince1970).jpg"
        
        let mimetype = "image/jpg"
        
        body.append(string:"--\(boundary)\r\n")
        body.append(string:"Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.append(string:"Content-Type: \(mimetype)\r\n\r\n")
        if let data = imageDataKey as? Data{
            body.append(data)
        }
        body.append(string:"\r\n")
        
        
        
        body.append(string:"--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }



}
extension NSMutableData {
    func append(string: String) {
        let data = string.data(
            using: String.Encoding.utf8,
            allowLossyConversion: true)
        append(data!)
    }
    
    

    
}
