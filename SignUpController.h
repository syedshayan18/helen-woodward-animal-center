//
//  SignUpController.h
//  Vet247Doctor
//
//  Created by Golfam_EiConix on 7/4/15.
//  Copyright (c) 2015 Inventions Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>

@end
