//
//  CustomMapView.m
//  Wadden App
//
//  Created by Paul Bar on 4/12/13.
//  Copyright (c) 2013 Serge Gorbachev. All rights reserved.
//

#import "CustomMapView.h"

@interface MKMapView (UIGestureRecognizer)

// this tells the compiler that MKMapView actually implements this method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end

@implementation CustomMapView

// override UIGestureRecognizer's delegate method so we can prevent MKMapView's recognizer from firing
// when we interact with UIControl subclasses inside our callout view.
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    NSLog(@"touch view :%@", touch.view);
    
    if ([touch.view isKindOfClass:[UIButton class]])
    {
        UIButton *annotationButton = (UIButton*)touch.view;

        if(annotationButton.bounds.size.height < annotationButton.frame.size.height)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
//    else if ([touch.view isKindOfClass:[UITableViewCell class]])
//    {
//        return YES;
//    
////        if ([touch.view isKindOfClass:[UITextView class]])
////        {
////            UITextView *textView = (UITextView*)touch.view;
////            if(textView.bounds.size.height < textView.contentSize.height)
////            {
////                return NO;
////            }
////            else
////            {
////                return YES;
////            }
////        }
////    
////    
////    
//    
//    
//    }
    else if([touch.view isKindOfClass:[UIControl class]])
    {
        return NO;
    }
    else
    {
      
        return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
    }
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    NSLog(@"%lu",(unsigned long)[self.selectedAnnotations count]);
    
    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    if (calloutMaybe)
    {
        return calloutMaybe;

        
    }
    return [super hitTest:point withEvent:event];
}

@end

