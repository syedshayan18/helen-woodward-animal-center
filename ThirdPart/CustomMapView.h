//
//  CustomMapView.h
//  Wadden App
//
//  Created by Paul Bar on 4/12/13.
//  Copyright (c) 2013 Serge Gorbachev. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SMCalloutView.h"

@interface CustomMapView : MKMapView

@property (nonatomic, strong) SMCalloutView *calloutView;

@end
