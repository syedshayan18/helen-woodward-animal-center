//
//  MapAnnotationView.m
//  Wadden App
//
//  Created by Paul Bar on 4/2/13.
//  Copyright (c) 2013 Serge Gorbachev. All rights reserved.
//

#import "MapAnnotationView.h"

@interface MapAnnotationView()

@property (strong,nonatomic)  NSString *classIs;

-(UIImage*) imageForSelectedState:(BOOL)selected;
@end

@implementation MapAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
//    if ([annotation isKindOfClass:[YellowLocation class] ])
//    {
//        self.classIs = @"yellow";
//    }
//    else if([annotation isKindOfClass:[Location class] ])
//    {
//        self.classIs = @"location";
//  
//        //self.locationCode= ((Location*)annotation).locationCode;
//    }
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        [self setSelected:NO animated:NO];
        
        self.calloutOffset = CGPointMake(-1.0, 0.0);
    }
    
    return self;
}

// See this for more information: https://github.com/nfarina/calloutview/pull/9

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    //NSLog(@"calloutMaybe: %@", calloutMaybe);
    return calloutMaybe ?: [super hitTest:point withEvent:event];
}

-(void) setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.image = [self imageForSelectedState:selected];
}

#pragma mark -
#pragma mark Private
-(UIImage*) imageForSelectedState:(BOOL)selected
{
    NSString *imageName;
//    
    NSString *title=  [[self annotation] title];
    
    NSLog(@"%@",title);

//    if ( lat == 53.387168 || lat==53.473141 || lat==53.474674 || lat==53.479475||lat==53.499545||lat== 52.934388||lat==53.236718)
//    {
//        imageName= selected ? @"pin-yellow.png" : @"pin-yellow.png";
//        
//        NSLog(@"he is here but not showing image:");
//    }
//    else
//    {
//    if ([self.locationCode isEqualToString:@"LAUWOG"] ||[self.locationCode isEqualToString:@"VLIELHVN"]||[self.locationCode isEqualToString:@"TERSLNZE"]||[self.locationCode isEqualToString:@"NES"]||[self.locationCode isEqualToString:@"DENHDR"]||[self.locationCode isEqualToString:@"EEMHVN"]||[self.locationCode isEqualToString:@"WEIRMGDN"]||[self.locationCode isEqualToString:@"SCHIERMNOG"])
    
    
   if([self.classIs isEqualToString:@"yellow"])
    {
      imageName= selected ? @"pin-yellow.png" : @"pin-yellow.png";
    }
    else
    {
        imageName= selected ? @"pin-1.png" : @"pin-0.png";
        
    }
    
  
    return [UIImage imageNamed:imageName];
}

@end
