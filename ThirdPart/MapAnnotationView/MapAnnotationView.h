//
//  MapAnnotationView.h
//  Wadden App
//
//  Created by Paul Bar on 4/2/13.
//  Copyright (c) 2013 Serge Gorbachev. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SMCalloutView.h"
//#import "Location.h"

@interface MapAnnotationView : MKAnnotationView

@property (strong, nonatomic) SMCalloutView *calloutView;

@property (strong,nonatomic) NSString *locationCode;
@end
